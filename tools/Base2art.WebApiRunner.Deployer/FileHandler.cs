﻿namespace Base2art.WebApiRunner.Deployer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Xml;
    using Base2art.WebApiRunner.Deployer.Config;

    public class FileHandler
    {
        private readonly string configDirName;

        private readonly DirectoryInfo configDir;

        private readonly DirectoryInfo iisDir;

        private readonly DirectoryInfo consoleDir;
        
        public FileHandler(
            string configDirName,
            DirectoryInfo configDir,
            DirectoryInfo iisDir,
            DirectoryInfo consoleDir)
        {
            this.consoleDir = consoleDir;
            this.iisDir = iisDir;
            this.configDir = configDir;
            this.configDirName = configDirName;
        }

        public void HandleFile(KeyValuePair<string, FileInformation> file)
        {
            if (file.Value.Type == FileType.Config)
            {
                this.HandleConfig(file.Key, file.Value.Destination);
                return;
            }
            
            if (file.Value.Type == FileType.WebConfigInclude)
            {
                this.HandleWebConfigInclude(file.Key);
                return;
            }
            
            if (file.Value.Type == FileType.Asset)
            {
                this.HandleAsset(file.Key, file.Value.Destination);
                return;
            }
            
            if (file.Value.Type == FileType.Bin)
            {
                this.HandleBin(file.Key);
                return;
            }
            
            if (file.Value.Type == FileType.BinDir)
            {
                this.HandleBinDir(file.Key);
                return;
            }
            
            throw new InvalidOperationException("Not Supported Type");
        }
        
        private void HandleConfig(string name, string destination)
        {
            string newName = string.IsNullOrWhiteSpace(destination)
                ? "configuration.config"
                : destination;
            
            var source = Path.Combine(configDir.FullName, name);
            var destIIS = Path.Combine(iisDir.FullName, configDirName, newName);
            var destConsole = Path.Combine(consoleDir.FullName, configDirName, newName);
            
            Directory.CreateDirectory(Path.GetDirectoryName(destIIS));
            Directory.CreateDirectory(Path.GetDirectoryName(destConsole));
            
            File.Copy(source, destIIS, true);
            File.Copy(source, destConsole, true);
        }
        
        private void HandleWebConfigInclude(string name)
        {
            const string newName = "web.config";
            //            var source = Path.Combine(configDir., name);
            var destIIS = Path.Combine(iisDir.FullName, newName);
            var destForImport = Path.Combine(this.configDir.FullName, name);
            
//            var destConsole = Path.Combine(consoleDir.FullName, configDirName, newName);
            XmlDocument doc = new XmlDocument();
            doc.Load(destIIS);
            
            XmlDocument docForImport = new XmlDocument();
            var content = File.ReadAllText(destForImport);
            docForImport.LoadXml(content);
            
            var node = doc.ImportNode(docForImport.DocumentElement, true);
            
            doc.DocumentElement.InsertAfter(node, doc.DocumentElement.ChildNodes.OfType<XmlElement>().First());
            
            doc.Save(destIIS);
//            File.WriteAllText(destIIS, doc.OuterXml);
        }
        
        private void HandleAsset(string name, string destination)
        {
            var source = Path.Combine(configDir.FullName, name);
            if (File.Exists(source))
            {
                var realDest = Path.GetFileName(name);
                if (!string.IsNullOrWhiteSpace(destination))
                {
                    realDest = destination;
                }
                
                var destIIS = Path.Combine(iisDir.FullName, realDest);
                var destConsole = Path.Combine(consoleDir.FullName, realDest);
                
                Directory.CreateDirectory(Path.GetDirectoryName(destIIS));
                Directory.CreateDirectory(Path.GetDirectoryName(destConsole));
                
                File.Copy(source, destIIS, true);
                File.Copy(source, destConsole, true);
            }
            else if (Directory.Exists(source))
            {
                var realDest = Path.GetFileName(name);
                if (!string.IsNullOrWhiteSpace(destination))
                {
                    realDest = destination;
                }
                
                var destIIS = Path.Combine(iisDir.FullName, realDest);
                var destConsole = Path.Combine(consoleDir.FullName, realDest);
                
                Directory.CreateDirectory(destIIS);
                Directory.CreateDirectory(destConsole);
                
                FSUtil.DirectoryCopy(source, destIIS, true);
                FSUtil.DirectoryCopy(source, destConsole, true);
            }
            else
            {
                throw new InvalidOperationException("source does not exist... `" + source + "`");
            }
        }
        
        private void HandleBin(string name)
        {
            var source = Path.Combine(configDir.FullName, name);
            var destIIS = Path.Combine(iisDir.FullName, "bin", Path.GetFileName(name));
            var destConsole = Path.Combine(consoleDir.FullName, "bin", Path.GetFileName(name));
            
            Directory.CreateDirectory(Path.GetDirectoryName(destIIS));
            Directory.CreateDirectory(Path.GetDirectoryName(destConsole));
            
            File.Copy(source, destIIS, true);
            File.Copy(source, destConsole, true);
        }
        
        private void HandleBinDir(string name)
        {
            var source = new DirectoryInfo(Path.Combine(configDir.FullName, name));
            var files = source.GetFiles("*.dll")
                .Union(source.GetFiles("*.pdb"))
                .Union(source.GetFiles("*.config"))
                .Union(source.GetFiles("*.exe"));
            
            var destIIS = Path.Combine(iisDir.FullName, "bin");
            var destConsole = Path.Combine(consoleDir.FullName, "bin");
            
            Directory.CreateDirectory(destIIS);
            Directory.CreateDirectory(destConsole);
            
            foreach (var item in files)
            {
                item.CopyTo(Path.Combine(destIIS, item.Name), true);
                item.CopyTo(Path.Combine(destConsole, item.Name), true);
            }
        }
    }
}
