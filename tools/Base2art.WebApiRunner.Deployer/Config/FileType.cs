﻿namespace Base2art.WebApiRunner.Deployer.Config
{
    public enum FileType
    {
        Asset,
        Bin,
        BinDir,
        Config,
        WebConfigInclude
    }
}


