﻿namespace Base2art.WebApiRunner.Deployer.Config
{
    using System.Collections.Generic;

    public class FileInformation
    {
        public string[] Env { get; set; }

        public FileType Type { get; set; }

        public string Destination { get; set; }
    }
}


