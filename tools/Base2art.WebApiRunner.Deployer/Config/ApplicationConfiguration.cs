﻿namespace Base2art.WebApiRunner.Deployer.Config
{
    using System.Collections.Generic;
    
    public class ApplicationConfiguration
    {
        public string ConfigDir { get; set; }
        
        public Dictionary<string, FileInformation> Files { get; set; }
    }
}
