﻿
using System.IO;
using System.Text;
namespace Base2art.WebApiRunner.Deployer
{
    public static class Streamer
    {
        public static byte[] ReadFully(this Stream input)
        {
            byte[] buffer = new byte[16 * 1024];
            using (MemoryStream ms = new MemoryStream())
            {
                int read;
                while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                {
                    ms.Write(buffer, 0, read);
                }
                
                return ms.ToArray();
            }
        }
        
        public static string ReadString(this Stream input)
        {
            using (StreamReader sr = new StreamReader(input))
            {
                return sr.ReadToEnd();
            }
        }
        
        public static string ByteArrayToString(this byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba)
            {
                hex.AppendFormat("{0:x2}", b);
            }
            
            return hex.ToString();
        }
    }
}
