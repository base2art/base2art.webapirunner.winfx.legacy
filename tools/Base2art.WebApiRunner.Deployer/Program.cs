﻿
namespace Base2art.WebApiRunner.Deployer
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.IO.Compression;
    using System.Linq;
    using System.Reflection;
    using System.Xml;
    using AppDomainToolkit;
    using Base2art.WebApiRunner.Deployer.Config;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;
    using Base2art.WebApiRunner.Deployer.DomainLoader;
    
    public static class Program
    {
        public static int Main(string[] args)
        {
            var configPath = args[0];
            
            var configFile = new FileInfo(configPath);
            var configDir = configFile.Directory;
            
            var envsText = File.ReadAllText(configFile.FullName);
            
            var settings = new JsonSerializerSettings();
            settings.Formatting = Newtonsoft.Json.Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            var envs = Newtonsoft.Json.JsonConvert.DeserializeObject<ApplicationConfiguration>(envsText, settings);
            
            var diip = new FileInfo(configPath).Directory;
            
            HashSet<string> knownEnvironments = new HashSet<string>();
            
            foreach (var file in envs.Files)
            {
                foreach (var env in file.Value.Env)
                {
                    knownEnvironments.Add(env);
                }
            }
            
            foreach (var env in knownEnvironments)
            {
                var dii = new DirectoryInfo(Path.Combine(diip.FullName, ".deploy", env));
                dii.Create();
                var items = diip.GetFiles();
                
                var iisDir = new DirectoryInfo(Path.Combine(dii.FullName, "IIS"));
                var consoleDir = new DirectoryInfo(Path.Combine(dii.FullName, "Console"));
                
                iisDir.Create();
                consoleDir.Create();
                
                var asm = Assembly.GetExecutingAssembly();
                WriteRunnerAsms(iisDir, consoleDir, asm);
                
                var exe = Path.Combine("bin", "Base2art.WebApiRunner.Console.exe");
                var config = Path.Combine(envs.ConfigDir, "configuration.config");
                
                File.WriteAllText(
                    Path.Combine(consoleDir.FullName, "run.bat"),
                    exe + " server " + config);
                
                var handler = new FileHandler(envs.ConfigDir, configDir, iisDir, consoleDir);
                
                foreach (var file in envs.Files)
                {
                    if (file.Value.Env.Contains(env))
                    {
                        if (file.Value.Type != FileType.WebConfigInclude)
                        {
                            handler.HandleFile(file);
                        }
                    }
                }
                
                WriteRunnerAsms(iisDir, consoleDir, asm);
                
                WriteConfig(asm, iisDir, "web.config", "web.config", Path.Combine(envs.ConfigDir, "configuration.config"));
                WriteConfig(asm, consoleDir, "app.config", Path.Combine("bin", "Base2art.WebApiRunner.Console.exe.config"), null);
                
                foreach (var file in envs.Files)
                {
                    if (file.Value.Env.Contains(env))
                    {
                        if (file.Value.Type == FileType.WebConfigInclude)
                        {
                            handler.HandleFile(file);
                        }
                    }
                }
            }
            
            
            foreach (var env in knownEnvironments)
            {
                var dii = new DirectoryInfo(Path.Combine(diip.FullName, ".deploy", env));
                
                var iisDir = new DirectoryInfo(Path.Combine(dii.FullName, "IIS"));
                var consoleDir = new DirectoryInfo(Path.Combine(dii.FullName, "Console"));
                ZipFile.CreateFromDirectory(iisDir.FullName, Path.Combine(diip.FullName, ".deploy", env + "-iis.zip"), CompressionLevel.Optimal, false);
                ZipFile.CreateFromDirectory(consoleDir.FullName, Path.Combine(diip.FullName, ".deploy", env + "-console.zip"), CompressionLevel.Optimal, false);
            }
            
            return 0;
        }

        private static void WriteRunnerAsms(DirectoryInfo iisDir, DirectoryInfo consoleDir, Assembly asm)
        {
            var writer = new AssemblyStreamWriter(asm);
            var iisDirBin = new DirectoryInfo(Path.Combine(iisDir.FullName, "bin"));
            var consoleDirBin = new DirectoryInfo(Path.Combine(consoleDir.FullName, "bin"));
            iisDirBin.Create();
            consoleDirBin.Create();
            writer.WriteAll(iisDirBin, "IISBin");
            writer.WriteAll(consoleDirBin, "ConsoleBin");
        }

        public static void WriteConfig(
            Assembly asm,
            DirectoryInfo baseDir,
            string resxName,
            string fileName,
            string mainConfigName)
        {
            var binDir = new DirectoryInfo(Path.Combine(baseDir.FullName, "bin"));
            var xml = new XmlDocument();
            xml.Load(asm.GetManifestResourceStream("Base2art.WebApiRunner.Deployer.Resources." + resxName));
            
            var files = binDir.GetFiles("*.dll");
            
            List<AssemblyName> names = new List<AssemblyName>();
            using (var context = AppDomainContext.Create())
            {
                foreach (var file in files)
                {
                    var remoteGreeter = Remote<Greeter>.CreateProxy(context.Domain, file.FullName);
                    
                    var name = remoteGreeter.RemoteObject.GetValue().Item1;
                    var @byte = name.GetPublicKeyToken();
                    if (@byte != null && @byte.Length > 0)
                    {
                        names.Add(name);
                    }
                }
            }
            
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(xml.NameTable);
            nsmgr.AddNamespace("asm", "urn:schemas-microsoft-com:asm.v1");
            var assemblyBinding = xml.SelectSingleNode("/configuration/runtime/asm:assemblyBinding", nsmgr) as XmlElement;
            
            foreach (var name in names.GroupBy(x => x.Name))
            {
                var dependentAssemblyEl = assemblyBinding.OwnerDocument.CreateElement("dependentAssembly", "urn:schemas-microsoft-com:asm.v1");
                var assemblyIdentityEl = assemblyBinding.OwnerDocument.CreateElement("assemblyIdentity", "urn:schemas-microsoft-com:asm.v1");
                var bindingRedirectEl = assemblyBinding.OwnerDocument.CreateElement("bindingRedirect", "urn:schemas-microsoft-com:asm.v1");
                
                assemblyIdentityEl.SetAttribute("name", name.FirstOrDefault().Name);
                assemblyIdentityEl.SetAttribute("culture", "neutral");
                assemblyIdentityEl.SetAttribute("publicKeyToken", name.FirstOrDefault().GetPublicKeyToken().ByteArrayToString());
                
                bindingRedirectEl.SetAttribute("oldVersion", "0.0.0.0-" + name.FirstOrDefault().Version);
                bindingRedirectEl.SetAttribute("newVersion", name.FirstOrDefault().Version.ToString());
                
                dependentAssemblyEl.AppendChild(assemblyIdentityEl);
                dependentAssemblyEl.AppendChild(bindingRedirectEl);
                
                assemblyBinding.AppendChild(dependentAssemblyEl);
            }
            
            if (!string.IsNullOrWhiteSpace(mainConfigName))
            {
                var appSettings = xml.SelectSingleNode("/configuration/appSettings", nsmgr) as XmlElement;
                var addEl = appSettings.OwnerDocument.CreateElement("add");
                addEl.SetAttribute("key", "configFile");
                addEl.SetAttribute("value", mainConfigName);
                appSettings.AppendChild(addEl);
            }
            
            var output = Path.Combine(baseDir.FullName, fileName);
            XmlWriterSettings settings = new XmlWriterSettings { Indent = true };
            
            using (XmlWriter writer = XmlWriter.Create(output, settings))
            {
                xml.WriteTo(writer);
            }
        }
    }
}
