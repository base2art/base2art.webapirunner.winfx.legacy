﻿namespace Base2art.WebApiRunner.Deployer.DomainLoader
{
    using System;
    using System.IO;
    using System.Net;
    using System.Reflection;
    using System.Security.Policy;

    public interface IAssemblyLoader
    {
        Assembly GetAssembly(string assemblyPath);
    }
    
    public static class AppDomainFactory
    {
        public static IAssemblyLoader Create(string basePath)
        {
            AppDomainSetup domaininfo = new AppDomainSetup();
            domaininfo.ApplicationBase = AppDomain.CurrentDomain.SetupInformation.ApplicationBase;
            Evidence adevidence = AppDomain.CurrentDomain.Evidence;
            AppDomain domain = AppDomain.CreateDomain("MyDomain", adevidence, domaininfo);

            Type type = typeof(Proxy);
            var value = (Proxy)domain.CreateInstanceAndUnwrap(
                type.Assembly.FullName,
                type.FullName);

            value.BasePath = basePath;
            return value;
        }
        

        private class Proxy : MarshalByRefObject, IAssemblyLoader
        {
            public string BasePath { get; set; }

            public Proxy()
            {
                AppDomain.CurrentDomain.ReflectionOnlyAssemblyResolve += AppDomain_CurrentDomain_ReflectionOnlyAssemblyResolve;
                AppDomain.CurrentDomain.AssemblyResolve += AppDomain_CurrentDomain_AssemblyResolve;
            }
            
            public Assembly GetAssembly(string assemblyPath)
            {
                try
                {
                    return Assembly.LoadFile(assemblyPath);
//                    var bytes = File.ReadAllBytes(assemblyPath);
//                    return Assembly.ReflectionOnlyLoad(bytes);
                }
                catch (Exception)
                {
                    return null;
                    // throw new InvalidOperationException(ex);
                }
            }
            
            private Assembly AppDomain_CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
            {
                Console.WriteLine(args.Name);
                return null;
            }
            
            private Assembly AppDomain_CurrentDomain_ReflectionOnlyAssemblyResolve(object sender, ResolveEventArgs args)
            {
                Console.WriteLine(args.Name);
                return null;
            }
        }
    }
}
