﻿namespace Base2art.WebApiRunner.Deployer.DomainLoader
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Reflection;
    
    public class Greeter : MarshalByRefObject
    {
        private readonly string fullPath;

        public Greeter(string fullPath)
        {
            this.fullPath = fullPath;
        }

        public Tuple<AssemblyName, string> GetValue()
        {
            var bytes = File.ReadAllBytes(fullPath);
            var newAsm = AppDomain.CurrentDomain.Load(bytes);
            var version = FileVersionInfo.GetVersionInfo(this.fullPath);
            
            return Tuple.Create(newAsm.GetName(), version.FileVersion);
        }
    }
}
