﻿namespace Base2art.WebApiRunner.Deployer
{
    using System;
    using System.IO;
    using System.Reflection;
    
    public class AssemblyStreamWriter
    {
        private readonly Assembly asm;

        public AssemblyStreamWriter(Assembly asm)
        {
            this.asm = asm;
        }

        public void WriteAll(DirectoryInfo dir, string suffix)
        {
            var streams = asm.GetManifestResourceNames();
            foreach (var streamName in streams)
            {
                this.WriteStream("Base2art.WebApiRunner.Deployer.Resources.CommonBin.", dir, streamName);
                this.WriteStream("Base2art.WebApiRunner.Deployer.Resources." + suffix + ".", dir, streamName);
            }
        }

        private void WriteStream(string prefix, DirectoryInfo dir, string streamName)
        {
            if (streamName.StartsWith(prefix, StringComparison.InvariantCultureIgnoreCase))
            {
                var name = streamName.Substring(prefix.Length);
                var consoleFile = Path.Combine(dir.FullName, name);
                var stream = asm.GetManifestResourceStream(streamName).ReadFully();
                
                if (!File.Exists(consoleFile))
                {
                    File.WriteAllBytes(consoleFile, stream);
                    return;
                }
                
                if (name.ToUpperInvariant().Contains("Base2art.WebApiRunner".ToUpperInvariant()))
                {
                    File.WriteAllBytes(consoleFile, stream);
                }
            }
        }
    }
}


