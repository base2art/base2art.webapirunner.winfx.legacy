﻿
namespace Base2art.WebApiRunner.Api.Features
{
    using System;
    using System.Reflection;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.SystemServices;
    using NUnit.Framework;
    
    [TestFixture]
    public class CacheFeature
    {
        [Test]
        public void ShouldGetItem_SystemCache()
        {
            int i = 0;
            var cache = new InMemoryCacheProvider();
            Func< int> method = () => {i += 1; return i;};
            var rez = cache.GetSystemCache().TryGet(method);
            Assert.AreEqual(rez, 1);
            rez = cache.GetSystemCache().TryGet(method);
            Assert.AreEqual(rez, 1);
            cache.GetSystemCache().Clear();
            rez = cache.GetSystemCache().TryGet(method);
            Assert.AreEqual(rez, 2);
        }
        
        [Test]
        public async void ShouldGetItem_DynamicCache()
        {
            ValueContainer<int> i = new ValueContainer<int>();
            var hold = TimeSpan.FromMilliseconds(45);
            var cache = new InMemoryCacheProvider().CreateCache<string, ValueContainer<int>>(hold);
            Func<ValueContainer<int>> method = () => {i.Value += 1; return i;};
            var rez = cache.TryGet(method);
            Assert.AreEqual(rez.Value, 1);
            rez = cache.TryGet(method);
            Assert.AreEqual(rez.Value, 1);
            cache.Clear();
            rez = cache.TryGet(method);
            Assert.AreEqual(rez.Value, 2);
            rez = cache.TryGet(method);
            Assert.AreEqual(rez.Value, 2);
            
            await Task.Delay(hold);
            rez = cache.TryGet(method);
            Assert.AreEqual(rez.Value, 3);
        }
        
        [Test]
        public void ShouldGetItem_AsApi()
        {
            ValueContainer<int> i = new ValueContainer<int>();
            var hold = TimeSpan.FromMilliseconds(45);
            var cache = new InMemoryCacheProvider().GetSystemCache();
            Func< ValueContainer<int>> method = () => {i.Value += 1; return i;};
            var rez = cache.As<ValueContainer<int>>().TryGet(method);
            Assert.AreEqual(rez.Value, 1);
            rez = cache.As<ValueContainer<int>>().TryGet(method);
            Assert.AreEqual(rez.Value, 1);
            cache.Clear();
            rez = cache.As<ValueContainer<int>>().TryGet(method);
            Assert.AreEqual(rez.Value, 2);
            rez = cache.As<ValueContainer<int>>().TryGet(method);
            Assert.AreEqual(rez.Value, 2);
            
        }
        
        private class ValueContainer<T>
        {
            public T Value { get; set; }
        }
    }
}