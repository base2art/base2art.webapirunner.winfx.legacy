﻿
using System;

namespace Base2art.WebApiRunner.Contrib.Features
{
    public class SignInInfo
    {
        public string Name{ get; set; }
        public string Password{ get; set; }
        public string Token{ get; set; }
        public string RawToken{ get; set; }
        public SignInStatus Status{ get; set; }
    }
    
    public enum SignInStatus
    {
        Success,
        LockedOut,
        BadPassword,
        UnknownUser,
        InvalidRequest
    }
}
