﻿namespace Base2art.WebApiRunner.Contrib.Features
{
    using System;
    using System.IO;
    using System.Net;
    using Base2art.WebApiRunner.Security;
    using Base2art.WebApiRunner.SystemServices;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class AuthParserTest
    {
        private string system = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6InNzby1wcm9kIn0.eyJzdWIiOiJZb3VuUyIsIm5hbWUiOiJZb3VuZ2JsdXQsIFNjb3R0IiwidmVyc2lvbiI6IjEiLCJpc3MiOiJodHRwOi8vc3NvLmR3dGFwcHMuY29tLyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3QvIiwiZXhwIjoxNDkyMjg0ODY3LCJuYmYiOjE0OTIxOTg0NjcsImdyb3VwIjpbIklUV0VCREVWIC0gSVQgV2ViIERldmVsb3BtZW50IFRlYW0iLCJJVFdFQkRFViIsImlTdCBBTEwiLCJERVBULUlTVCJdfQ.IVxXI1k4JlJkWpApmr5_9ob1k6T8TymV9wSpZIwRDt7kssGaaw_-cWkpWcsqHr_wb0kaXhLnJI4isFeMP-4FIUj8Y19bB4csREtOnnUWDjfpnla9whQbk0qTVHUOQk_Bq8a29qSj_Y0jMWlySf6mI_hwKaUoH7M-5vHlMsPfLfEKdwaAGAtvLHDRJT0p9SZJbAFNJVYZo8J2AzSga5MAqEvddU_9UT4vyURsnoc1iCU-IK-StuWosNt8Na-wdsv24sUx_-hMx9MXxtH-xgeZ_6KxHM1Ymr1HaksSGpP8AllIQFhYPS2aGMsv3mlKc91_NpGCJOQboN6UZjkLdvyemg";
        //        private string current = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6InNzby1wcm9kIn0.eyJzdWIiOiJZb3VuUyIsIm5hbWUiOiJZb3VuZ2JsdXQsIFNjb3R0IiwidmVyc2lvbiI6IjEiLCJpc3MiOiJodHRwOi8vc3NvLmR3dGFwcHMuY29tLyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3QvIiwiZXhwIjoxNDkyMjg0ODY3LCJuYmYiOjE0OTIxOTg0NjcsImdyb3VwIjpbIklUV0VCREVWIC0gSVQgV2ViIERldmVsb3BtZW50IFRlYW0iLCJJVFdFQkRFViIsImlTdCBBTEwiLCJERVBULUlTVCJdfQ.IVxXI1k4JlJkWpApmr5_9ob1k6T8TymV9wSpZIwRDt7kssGaaw_-cWkpWcsqHr_wb0kaXhLnJI4isFeMP-4FIUj8Y19bB4csREtOnnUWDjfpnla9whQbk0qTVHUOQk_Bq8a29qSj_Y0jMWlySf6mI_hwKaUoH7M-5vHlMsPfLfEKdwaAGAtvLHDRJT0p9SZJbAFNJVYZo8J2AzSga5MAqEvddU_9UT4vyURsnoc1iCU-IK-StuWosNt8Na-wdsv24sUx_-hMx9MXxtH-xgeZ_6KxHM1Ymr1HaksSGpP8AllIQFhYPS2aGMsv3mlKc91_NpGCJOQboN6UZjkLdvyemg";
        private string expired = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6InNzby1wcm9kIn0.eyJzdWIiOiJZb3VuUyIsIm5hbWUiOiJZb3VuZ2JsdXQsIFNjb3R0IiwidmVyc2lvbiI6IjEiLCJpc3MiOiJodHRwOi8vc3NvLmR3dGFwcHMuY29tLyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3QvIiwiZXhwIjoxNDkyMTM2MjY0LCJuYmYiOjE0OTIwNDk4NjQsImdyb3VwIjpbIklUV0VCREVWIC0gSVQgV2ViIERldmVsb3BtZW50IFRlYW0iLCJJVFdFQkRFViIsImlTdCBBTEwiLCJERVBULUlTVCJdfQ.flPpm9D76kBuziJns3QxMT7i7CiT01K2Mk70ccHz96E0zFWCDwulBcjTHo4IEgWDjQJ5cSnymCY6QZIObuS-OhVngNNsgaI4DRte9VWHR7FGb5Yhsrt6MeTqHUmhbZz1yy1OzPPQs8OqrDoRyAerBiO3og-bintAV4Qap4wwP3MIVzZp4quwlmvgUkn0A0RhL4SVcoLJEQ151_zWZrXv___8OkzDdHVf7F_XYAhZKYUzbZeGeFGwsMEcKyczjUo4F7LBLd4HfNWxImcwCJizHxcXJUR2Fkwijr8otCoaR186IMrqvlqy1lySW4Gx-0OapJ5q5ooFk5P8ezmCHNjM9w";
        
        private string current
        {
            get
            {
                
                var obj = Newtonsoft.Json.JsonConvert.SerializeObject(new SignInInfo {
                                                                          Name = "test@base2art.com",
                                                                          Password = "d321e6fb-efe9-42b5-b425-6ca966ca37b7"
                                                                      });
                
                var bytes = System.Text.Encoding.Default.GetBytes(obj);
                var result = this.Send<SignInInfo>(bytes);
                
                return result.Token;
            }
        }
        
        [Test]
        public void ShouldParse()
        {
            var parser = new JwtClaimsPrincipalParser(
                "https://sso.base2art.com/api/keys/{keyId}",
                "{keyId}",
                false,
                "https://sso.base2art.com/",
                new InMemoryCacheProvider());
            
            var item = parser.CreateValidated(current);
            item.Should().NotBeNull();
        }
        
        [Test]
        public void ShouldParseExpired()
        {
            var parser = new JwtClaimsPrincipalParser(
                "https://sso.base2art.com/api/keys/{keyId}",
                "{keyId}",
                false,
                "https://sso.base2art.com/",
                new InMemoryCacheProvider());
            
            var item = parser.CreateNonValidated(expired);
            item.Should().NotBeNull();
        }
        
        [Test]
        public void ShouldParseExpired_ThrowsException()
        {
            var parser = new JwtClaimsPrincipalParser(
                "https://sso.base2art.com/api/keys/{keyId}",
                "{keyId}",
                false,
                "https://sso.base2art.com/",
                new InMemoryCacheProvider());
            
            Action a = () =>
            {
                var item = parser.CreateValidated(expired);
                item.Should().NotBeNull();
            };
            
            a.ShouldThrow<Exception>();
        }

        private T Send<T>(byte[] bytes)
        {
            var http = WebRequest.CreateHttp("https://sso.base2art.com/api/session?app=localhost");
            http.Method = "POST";
            http.ContentType = "application/json";
            
            if (bytes != null && bytes.Length > 0)
            {
                http.ContentLength = bytes.Length;
                
                using (var stream = http.GetRequestStream())
                {
                    stream.Write(bytes, 0, bytes.Length);
                }
            }
            
            
            using (var respone = http.GetResponse())
            {
                using (var stream = respone.GetResponseStream())
                {
                    using (StreamReader sr = new StreamReader(stream))
                    {
                        var result = sr.ReadToEnd();
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(result);
                    }
                }
            }
        }
    }
}
