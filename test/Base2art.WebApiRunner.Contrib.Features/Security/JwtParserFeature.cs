﻿namespace Base2art.WebApiRunner.Contrib.Features.Security
{
    using System;
    using System.Net;
    using System.Security.Cryptography;
    using Base2art.WebApiRunner.Security;
    using Base2art.WebApiRunner.SystemServices;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class JwtParserFeature
    {
        [Test]
        public void Parse()
        {
            IClaimsPrincipalParser parser = new JwtClaimsPrincipalParser(
                "http://fake.site/{keyId}",
                "{keyId}",
                true,
                "NULL",
                new InMemoryCacheProvider());
            
            var raw = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6InNzby1wcm9kIn0.eyJzdWIiOiJZb3VuUyIsIm5hbWUiOiJZb3VuZ2JsdXQsIFNjb3R0IiwidmVyc2lvbiI6IjEiLCJpc3MiOiJodHRwOi8vc3NvLmR3dGFwcHMuY29tLyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3QvIiwiZXhwIjoxNDkyMTM2MjY0LCJuYmYiOjE0OTIwNDk4NjQsImdyb3VwIjpbIklUV0VCREVWIC0gSVQgV2ViIERldmVsb3BtZW50IFRlYW0iLCJJVFdFQkRFViIsImlTdCBBTEwiLCJERVBULUlTVCJdfQ.flPpm9D76kBuziJns3QxMT7i7CiT01K2Mk70ccHz96E0zFWCDwulBcjTHo4IEgWDjQJ5cSnymCY6QZIObuS-OhVngNNsgaI4DRte9VWHR7FGb5Yhsrt6MeTqHUmhbZz1yy1OzPPQs8OqrDoRyAerBiO3og-bintAV4Qap4wwP3MIVzZp4quwlmvgUkn0A0RhL4SVcoLJEQ151_zWZrXv___8OkzDdHVf7F_XYAhZKYUzbZeGeFGwsMEcKyczjUo4F7LBLd4HfNWxImcwCJizHxcXJUR2Fkwijr8otCoaR186IMrqvlqy1lySW4Gx-0OapJ5q5ooFk5P8ezmCHNjM9w";
            var token = parser.CreateNonValidated(raw);
            token.Should().NotBeNull();
            
            token.Identity.Name.Should().BeEquivalentTo("youns");
            token.Groups().Length.Should().Be(4);
            token.Token().Should().Be(raw);
        }
        
        [Test]
        public void ParseValidated()
        {
            IClaimsPrincipalParser parser = new JwtClaimsPrincipalParser(
                "https://sso.base2art.com/api/keys/{keyId}",
                "{keyId}",
                false,
                "https://sso.base2art.com/",
                new InMemoryCacheProvider());

            try
            {
                var token = parser.CreateValidated("eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6InNzby1wcm9kIn0.eyJzdWIiOiJZb3VuUyIsIm5hbWUiOiJZb3VuZ2JsdXQsIFNjb3R0IiwidmVyc2lvbiI6IjEiLCJpc3MiOiJodHRwOi8vc3NvLmR3dGFwcHMuY29tLyIsImF1ZCI6Imh0dHA6Ly9sb2NhbGhvc3QvIiwiZXhwIjoxNDkyMTM2MjY0LCJuYmYiOjE0OTIwNDk4NjQsImdyb3VwIjpbIklUV0VCREVWIC0gSVQgV2ViIERldmVsb3BtZW50IFRlYW0iLCJJVFdFQkRFViIsImlTdCBBTEwiLCJERVBULUlTVCJdfQ.flPpm9D76kBuziJns3QxMT7i7CiT01K2Mk70ccHz96E0zFWCDwulBcjTHo4IEgWDjQJ5cSnymCY6QZIObuS-OhVngNNsgaI4DRte9VWHR7FGb5Yhsrt6MeTqHUmhbZz1yy1OzPPQs8OqrDoRyAerBiO3og-bintAV4Qap4wwP3MIVzZp4quwlmvgUkn0A0RhL4SVcoLJEQ151_zWZrXv___8OkzDdHVf7F_XYAhZKYUzbZeGeFGwsMEcKyczjUo4F7LBLd4HfNWxImcwCJizHxcXJUR2Fkwijr8otCoaR186IMrqvlqy1lySW4Gx-0OapJ5q5ooFk5P8ezmCHNjM9w");
                token.Should().NotBeNull();
            }
            catch (WebException)
            {
                return;
            }
            catch (CryptographicException)
            {
                return;
            }
            
            Assert.Fail();
        }
    }
}
