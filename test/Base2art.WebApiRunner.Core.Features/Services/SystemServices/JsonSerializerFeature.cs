﻿namespace Base2art.WebApiRunner.Services.SystemServices
{
    using System;
    using System.Collections.Generic;
    using Base2art.WebApiRunner.SystemServices;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixtureAttribute]
    public class JsonSerializerFeature
    {
        private IJsonSerializer target;
        
        [SetUp]
        public void BeforeEach()
        {
            this.target  = new JsonSerializerWrapper(Startup.SetupSerializer(new Newtonsoft.Json.JsonSerializerSettings()));
        }
        
        [Test]
        public void ShouldSerialize()
        {
            var dictionary = new Dictionary<string, int>();
            var outputNotFormatted = this.target.SerializeObject(dictionary).Should().Be("{}").And.Subject;
            var outputFormatted = this.target.SerializeObjectFormatted(dictionary).Should().Be("{}").And.Subject;
            
            var dict1 = this.target.DeserializeObject<Dictionary<string, int>>(outputNotFormatted);
            var dict2 = this.target.DeserializeObject<Dictionary<string, int>>(outputFormatted);
            
            dict1.Count.Should().Be(0);
            dict2.Count.Should().Be(0);
        }
        
        [Test]
        public void ShouldSerialize_withItems()
        {
            var dictionary = new Dictionary<string, int>();
            dictionary["abc"] = 32;
            var outputNotFormatted = this.target.SerializeObject(dictionary).Should().Be("{\"abc\":32}").And.Subject;
            var outputFormatted = this.target.SerializeObjectFormatted(dictionary).Should().Be("{\r\n  \"abc\": 32\r\n}").And.Subject;
            
            var dict1 = this.target.DeserializeObject<Dictionary<string, int>>(outputNotFormatted);
            var dict2 = this.target.DeserializeObject<Dictionary<string, int>>(outputFormatted);
            
            dict1.Count.Should().Be(1);
            dict2.Count.Should().Be(1);
        }
        
        [Test]
        public void ShouldSerialize_TypedObject()
        {
            var person = new Person { Name = "SjY" };
            
            this.target.SerializeObject(person).Should().Be("{\"name\":\"SjY\"}");
            this.target.SerializeObjectFormatted(person).Should().Be("{\r\n  \"name\": \"SjY\"\r\n}");
            
            this.target.SerializeObject<object>(person).Should().Be("{\"name\":\"SjY\"}");
            this.target.SerializeObjectFormatted<object>(person).Should().Be("{\r\n  \"name\": \"SjY\"\r\n}");
        }
        
        private class Person
        {
            public string Name { get; set; }
        }
        
        [Test]
        public async void ShouldSerializeAsync()
        {
            var dictionary = new Dictionary<string, int>();
            var outputNotFormatted = this.target.SerializeObject(dictionary).Should().Be("{}").And.Subject;
            var outputFormatted = this.target.SerializeObjectFormatted(dictionary).Should().Be("{}").And.Subject;
            
            var dict1 = await this.target.DeserializeObjectAsync<Dictionary<string, int>>(outputNotFormatted);
            var dict2 = await this.target.DeserializeObjectAsync<Dictionary<string, int>>(outputFormatted);
            
            dict1.Count.Should().Be(0);
            dict2.Count.Should().Be(0);
        }
        
        [Test]
        public async void ShouldSerialize_withItemsAsync()
        {
            var dictionary = new Dictionary<string, int>();
            dictionary["abc"] = 32;
            var outputNotFormatted = this.target.SerializeObject(dictionary).Should().Be("{\"abc\":32}").And.Subject;
            var outputFormatted = this.target.SerializeObjectFormatted(dictionary).Should().Be("{\r\n  \"abc\": 32\r\n}").And.Subject;
            
            var dict1 = await this.target.DeserializeObjectAsync<Dictionary<string, int>>(outputNotFormatted);
            var dict2 = await this.target.DeserializeObjectAsync<Dictionary<string, int>>(outputFormatted);
            
            dict1.Count.Should().Be(1);
            dict2.Count.Should().Be(1);
        }
    }
}
