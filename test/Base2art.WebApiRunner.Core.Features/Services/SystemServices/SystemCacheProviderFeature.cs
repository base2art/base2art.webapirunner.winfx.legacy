﻿
namespace Base2art.WebApiRunner.Features.Services.SystemServices
{
    using Base2art.WebApiRunner.Services.SystemServices;
    using Base2art.WebApiRunner.SystemServices;
    using NUnit.Framework;
    
    [TestFixtureAttribute]
    public class SystemCacheProviderFeature
    {
        [Test]
        public void ShouldSetGetClear()
        {
            ICache<string, object> cache = new SystemCacheProvider().GetSystemCache();
            Assert.IsNull(cache.GetItem("test"));
            cache.Upsert("test", 1);
            Assert.AreEqual(1, cache.GetItem("test"));
            cache.Clear();
            Assert.IsNull(cache.GetItem("test"));
            cache.Upsert("test", 1);
            Assert.AreEqual(1, cache.GetItem("test"));
        }
    }
}
