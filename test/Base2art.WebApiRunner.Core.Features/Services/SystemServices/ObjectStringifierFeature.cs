﻿namespace Base2art.WebApiRunner.Services.SystemServices
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using Base2art.WebApiRunner.SystemServices;
    using Base2art.WebApiRunner.SystemServices.DefaultServiceImplementations;
    using FluentAssertions;
    using NUnit.Framework;

    
    [TestFixtureAttribute]
    public class ObjectStringifierFeature
    {
        [Test]
        public void ShouldSerialize_Null()
        {
            var output = this.PrettyPrint<object>(null);
            output.Should().Be("null");
        }
        
        [Test]
        public void ShouldSerialize_String()
        {
            var output = this.PrettyPrint("sdfdf");
            output.Should().Be("sdfdf");
        }
        
        [Test]
        public void ShouldSerialize_Int()
        {
            var output = this.PrettyPrint(12345);
            output.Should().Be("12345");
        }
        
        [Test]
        public void ShouldSerialize_Double()
        {
            var output = this.PrettyPrint(34.6m);
            output.Should().Be("34.6");
        }
        
        [Test]
        public async void ShouldSerializeAsync_Double()
        {
            IObjectStringifier ifier = new ObjectStringifier();
            
            var output = await ifier.PrettyPrintAsync(34.6m);
            output.Should().Be("34.6");
        }
        
        
        [Test]
        public async void ShouldSerializeAsync_Exception()
        {
            IObjectStringifier ifier = new ObjectStringifier();
            
            var ticks = Environment.TickCount;
            
            int zero = Math.Abs(ticks) - Math.Abs(ticks);
            DivideByZeroException ex = null;
            Debug.Assert(zero == 0);
            try
            {
                var data = Environment.TickCount / zero;
                Console.WriteLine(data);
            }
            catch (DivideByZeroException dbze)
            {
                ex = dbze;
            }
            
            Assert.NotNull(ex);
            
            var output = await ifier.PrettyPrintAsync(ex);
            Console.WriteLine(output);
            output.Trim().Should().BeEquivalentTo(@"
            Message: Attempted to divide by zero.
Data:
InnerException: null
TargetSite: Void MoveNext()
StackTrace:    at Base2art.WebApiRunner.Services.SystemServices.ObjectStringifierFeature.<ShouldSerializeAsync_Exception>d__5.MoveNext() in c:\Users\{USER}\code\b2a\base2art.webapirunner\test\Base2art.WebApiRunner.Core.Features\Services\SystemServices\ObjectStringifierFeature.cs:line 65
HelpLink: null
Source: Base2art.WebApiRunner.Core.Features
HResult: -2147352558


".Trim().Replace("{USER}", Environment.UserName.ToLower()));
        }
        
        [Test]
        public void ShouldSerialize_NestedList()
        {
            {
                var data = new {
                    Items = new List<string>
                    {
                        "abc",
                        "def",
                    }
                };
                
                var output = this.PrettyPrint(data);
                output.Should().Be("Items:\r\n  - abc\r\n  - def");
            }
        }
        
        [Test]
        public void ShouldSerialize_List()
        {
            
            {
                var data = new List<string>
                {
                    "abc",
                    "def",
                };
                
                var output = this.PrettyPrint(data);
                output.Should().Be("- abc\r\n- def");
            }
        }
        
        [Test]
        public void ShouldSerialize()
        {
            var data = new InnerData
            { Item = new Dictionary<string, string>
                {
                    {
                        "abc",
                        "123"
                    },
                    {
                        "def",
                        "456"
                    }
                }
            };
            
            var content = this.PrettyPrint(data);
            Console.WriteLine(content);
            content.Should().Be(@"
Item:
  - Key: abc
    Value: 123
  - Key: def
    Value: 456
".Trim());
        }
        
        [Test]
        public void ShouldSerialize_NullDict()
        {
            var data = new InnerData
            {
            };
            var output1 = this.PrettyPrint(data);
            output1.Should().Be("Item: null");
        }
        
        [Test]
        public void ShouldSerializeAnonymous()
        {
            var data = new {Item = "abc"};
            var output = this.PrettyPrint(data);
            output.Should().Be("Item: abc");
        }

        [Test]
        public void ShouldSerialize_NestedDictionary()
        {
            var output = this.PrettyPrint(new {Values = new Dictionary<string, string>
                                              {
                                                  {
                                                      "abc",
                                                      "123"
                                                  },
                                                  {
                                                      "def",
                                                      "456"
                                                  }
                                              }});
            
            output.Should().Be(@"
Values:
  - Key: abc
    Value: 123
  - Key: def
    Value: 456
".Trim());
        }

        [Test]
        public void ShouldSerialize_Dictionary()
        {
            var output = this.PrettyPrint(new Dictionary<string, string>
                                          {
                                              {
                                                  "abc",
                                                  "123"
                                              },
                                              {
                                                  "def",
                                                  "456"
                                              }
                                          });
            
            output.Should().Be(@"
- Key: abc
  Value: 123
- Key: def
  Value: 456
".Trim());
        }

        private string PrettyPrint<T>(T data)
        {
            ObjectStringifier ifier = new ObjectStringifier();
            var output = ifier.PrettyPrint(data);
            Console.WriteLine(output);
            return output;
        }

        public class InnerData
        {
            public IDictionary<string, string> Item { get; set; }
        }
    }
}