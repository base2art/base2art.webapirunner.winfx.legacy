﻿
namespace Base2art.WebApiRunner.Features.Proxies
{
    using System;
    using Base2art.WebApiRunner.Proxies;
    using NUnit.Framework;
    
    [TestFixture]
    public class DuckTypeOutFeatures
    {
        [Test]
        public void ShouldCreate_OutVariant()
        {
            {
                var obj = new PersonCreator();
                var duck = obj.AsDuck<ICreator<Person>>();
                Assert.IsTrue(duck.IsDuck);
                Assert.IsNotNull(duck.Instance);
                Assert.IsNotNull(duck.Instance.Create());
            }
            {
                var obj = new EmployeeCreator();
                var duck = obj.AsDuck<ICreator<Person>>();
                Assert.IsTrue(duck.IsDuck);
                Assert.IsNotNull(duck.Instance);
                Assert.IsNotNull(duck.Instance.Create());
            }
            {
                var obj = new StringCreator();
                var duck = obj.AsDuck<ICreator<Person>>();
                Assert.IsFalse(duck.IsDuck);
                Assert.IsNull(duck.Instance);
            }
            {
                var obj = new ObjectCreator();
                var duck = obj.AsDuck<ICreator<Person>>();
                Assert.IsFalse(duck.IsDuck);
                Assert.IsNull(duck.Instance);
            }
        }
        
        //        [Test]
        //        public void ShouldCreate_GenericDuckCreator()
        //        {
        //            var duck = obj.GenericDuck<ICreator<object>>(typeof(ICreator2<, int>));
        //        }
        
        [Test]
        public void ShouldCreate_OutVariantGeneric()
        {
            {
                var obj = new PersonCreator();
                var duck = obj.AsGenericDuck<ICreator<object>>();
                
                Assert.IsTrue(duck.IsDuck);
                
                Assert.IsTrue(duck.SupportsType(typeof(Person)));
                Assert.IsTrue(duck.SupportsType(typeof(Employee)));
                Assert.IsFalse(duck.SupportsType(typeof(object)));
                Assert.IsFalse(duck.SupportsType(typeof(int)));
                Assert.IsNotNull(duck.Instance);
                Assert.IsNotNull(duck.Instance.Create());
            }
            {
                var obj = new EmployeeCreator();
                var duck = obj.AsDuck<ICreator<Person>>();
                Assert.IsTrue(duck.IsDuck);
                Assert.IsNotNull(duck.Instance);
                Assert.IsNotNull(duck.Instance.Create());
            }
            {
                var obj = new StringCreator();
                var duck = obj.AsDuck<ICreator<Person>>();
                Assert.IsFalse(duck.IsDuck);
                Assert.IsNull(duck.Instance);
            }
            {
                var obj = new ObjectCreator();
                var duck = obj.AsDuck<ICreator<Person>>();
                Assert.IsFalse(duck.IsDuck);
                Assert.IsNull(duck.Instance);
            }
        }
        
        private class PersonCreator
        {
            public Person Create()
            {
                return new Person();
            }
        }
        private class EmployeeCreator
        {
            public Employee Create()
            {
                return new Employee();
            }
        }
        private class ObjectCreator
        {
            public Object Create()
            {
                return new Employee();
            }
        }
        private class StringCreator
        {
            public String Create()
            {
                return "My String";
            }
        }
        
        private class Person
        {
        }
        private class Employee : Person
        {
        }
        
        public interface ICreator<T>
            where T : class
        {
            T Create();
        }
        
        public interface IConsumer<T>
        {
            void Create(T input);
        }
        
        public interface ICreator2<T1, T2>
        {
            T2 Create(T1 input);
        }
    }
}
