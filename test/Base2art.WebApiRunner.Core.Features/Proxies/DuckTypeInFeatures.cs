﻿
namespace Base2art.WebApiRunner.Features.Proxies
{
    using System;
    using Base2art.WebApiRunner.Proxies;
    using NUnit.Framework;

    [TestFixture]
    public class DuckTypeInFeatures
    {
        [Test]
        public void ShouldCreate_InVariant()
        {
            {
                var obj = new PersonConsumer();
                var duck = obj.AsDuck<IConsumer<Person>>();
                Assert.IsTrue(duck.IsDuck);
                Assert.IsNotNull(duck.Instance);
            }
            {
                var obj = new EmployeeConsumer();
                var duck = obj.AsDuck<IConsumer<Person>>();
                Assert.IsTrue(duck.IsDuck);
                Assert.IsNotNull(duck.Instance);
            }
            {
                var obj = new StringConsumer();
                var duck = obj.AsDuck<IConsumer<Person>>();
                Assert.IsFalse(duck.IsDuck);
                Assert.IsNull(duck.Instance);
            }
            {
                var obj = new ObjectConsumer();
                var duck = obj.AsDuck<IConsumer<Person>>();
                Assert.IsFalse(duck.IsDuck);
                Assert.IsNull(duck.Instance);
            }
        }
        
        [Test]
        public void ShouldCreate_InVariantGeneric()
        {
            {
                var obj = new StringConsumer();
                var duck = obj.AsGenericDuck<IConsumer<object>>();
                Assert.IsTrue(duck.IsDuck);
                Assert.IsTrue(duck.SupportsType(typeof(string)));
                Assert.IsFalse(duck.SupportsType(typeof(object)));
                Assert.IsFalse(duck.SupportsType(typeof(Person)));
                Assert.IsFalse(duck.SupportsType(typeof(Employee)));
                Assert.NotNull(duck.Instance);
            }
            {
                var obj = new PersonConsumer();
                var duck = obj.AsGenericDuck<IConsumer<Person>>();
                Assert.IsTrue(duck.IsDuck);
                Assert.IsFalse(duck.SupportsType(typeof(string)));
                Assert.IsFalse(duck.SupportsType(typeof(object)));
                Assert.IsTrue(duck.SupportsType(typeof(Person)));
                Assert.IsTrue(duck.SupportsType(typeof(Employee)));
                Assert.IsNotNull(duck.Instance);
            }
            {
                var obj = new EmployeeConsumer();
                var duck = obj.AsGenericDuck<IConsumer<Person>>();
                Assert.IsTrue(duck.IsDuck);
                Assert.IsFalse(duck.SupportsType(typeof(string)));
                Assert.IsFalse(duck.SupportsType(typeof(object)));
                Assert.IsFalse(duck.SupportsType(typeof(Person)));
                Assert.IsTrue(duck.SupportsType(typeof(Employee)));
                Assert.IsNotNull(duck.Instance);
            }
            {
                var obj = new ObjectConsumer();
                var duck = obj.AsGenericDuck<IConsumer<Person>>();
                Assert.IsTrue(duck.IsDuck);
                Assert.IsTrue(duck.SupportsType(typeof(string)));
                Assert.IsTrue(duck.SupportsType(typeof(object)));
                Assert.IsTrue(duck.SupportsType(typeof(Person)));
                Assert.IsTrue(duck.SupportsType(typeof(Employee)));
                Assert.IsNotNull(duck.Instance);
            }
        }

        private class PersonConsumer
        {
            public void Consume(Person input)
            {
            }
        }

        private class EmployeeConsumer
        {
            public void Consume(Employee input)
            {
            }
        }

        private class ObjectConsumer
        {
            public void Consume(Object input)
            {
            }
        }

        private class StringConsumer
        {
            public void Consume(String input)
            {
            }
        }

        private class Person
        {
        }

        private class Employee : Person
        {
        }

        public interface IConsumer<T>
        {
            void Consume(T input);
        }
    }
}


