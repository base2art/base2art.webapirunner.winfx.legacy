﻿
namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.IO;
    using System.Linq;
    using Base2art.WebApiRunner.Proxies;
    using NUnit.Framework;
    
    [TestFixture]
    public class AggregateObjectTest
    {

        [Test]
        public void CreateAggregate()
        {
            var sw = new StringWriter();
            var memoryStream = new MemoryStream();
            var streamWriter = new StreamWriter(memoryStream);
            
            var textWriter = new Aggregate<TextWriter>(streamWriter, sw, Console.Out);
            textWriter.AddMapping((x) => x.ToString(), (objects) => sw.GetStringBuilder().ToString());
            
            Assert.IsNotNull(textWriter.Value);
            
            using (textWriter.Value)
            {
                Process(textWriter.Value);
                
                //                TextWriter item = textWriter.Value;
            }
            
            Assert.AreEqual("abc\r\n", sw.GetStringBuilder().ToString());
            Assert.AreEqual("abc\r\n", textWriter.Value.ToString());
            
            
//            Console.WriteLine(textWriter.Value.GetType());
//            Console.WriteLine(textWriter.Value.ToString());
            
            try
            {
                streamWriter.WriteLine("Should Fail");
                Assert.Fail();
            }
            catch (ObjectDisposedException)
            {
            }
            
            try
            {
                textWriter.Value.WriteLine("Should Fail");
                Assert.Fail();
            }
            catch (ObjectDisposedException)
            {
            }
            
            var disposable = new Aggregate<IDisposable>();
            Assert.IsNotNull(disposable.Value);
        }
        
        void Process(TextWriter value)
        {
            value.WriteLine("abc");
        }
    }
    
}
