﻿

namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Internals;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class ComplexObjectDataPopulator_Field_Feature
    {
        Person person;

        Dictionary<string, string> dict;

        [SetUp]
        public void BeforeEach()
        {
            this.person = new Person();
            this.dict = new Dictionary<string, string>();
        }

        [Test]
        public void ShouldPopulate_NoValues()
        {
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            person.Name.Should().Be(null);
            person.SocialSecurityNumber.Should().Be(null);
        }

        [Test]
        public void ShouldPopulate_MismatchedValues()
        {
            dict["person.id"] = "123";
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            person.Name.Should().Be(null);
            person.SocialSecurityNumber.Should().Be(null);
        }

        [Test]
        public void ShouldPopulate_SettersWork()
        {
            dict["person.name"] = "Scott";
            dict["person.SocialSecurityNumber"] = "183-72-4562";
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            person.Name.Should().Be("Scott");
            person.SocialSecurityNumber.Should().Be("183-72-4562");
        }

        [Test]
        public void ShouldPopulate_SettersWork_wrongCase()
        {
            dict["person.Name"] = "Scott";
            dict["person.socialSecurityNumber"] = "183-72-4562";
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            person.Name.Should().Be("Scott");
            person.SocialSecurityNumber.Should().Be("183-72-4562");
        }

        [Test]
        public void ShouldPopulate_SettersWork_Nesting()
        {
            dict["person.Manager.Name"] = "Matt";
            dict["person.SocialSecurityNumber"] = "183-72-4562";
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            person.Manager.Name.Should().Be("Matt");
            person.SocialSecurityNumber.Should().Be("183-72-4562");
        }

        [Test]
        public void ShouldPopulate_SettersWork_Nesting_withDefault()
        {
            dict["person.Manager.SocialSecurityNumber"] = "marker";
            
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            
            dict.Clear();
            
            dict["person.Manager.Name"] = "Matt";
            dict["person.SocialSecurityNumber"] = "183-72-4562";
            
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            person.Manager.Name.Should().Be("Matt");
            person.SocialSecurityNumber.Should().Be("183-72-4562");
            person.Manager.SocialSecurityNumber.Should().Be("marker");
        }

        [Test]
        public void ShouldPopulate_SettersWork_CustomDataTypes()
        {
            dict["person.Manager.BirthDay"] = "1999-01-01";
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            person.Manager.BirthDay.Should().Be(new DateTime(1999, 01, 01));
        }

        private class Person
        {
            private string name;
            
            private string socialSecurityNumber;

            private DateTime birthday;

            private Person manager;
            
            public string SocialSecurityNumber
            {
                get { return socialSecurityNumber; }
            }

            public DateTime BirthDay
            {
                get{ return this.birthday; }
            }
            

            public string Name
            {
                get { return name; }
            }

            public Person Manager
            {
                get { return manager; }
            }
        }
    }
}


