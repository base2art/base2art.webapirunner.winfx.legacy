﻿

namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Internals;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class ComplexObjectDataPopulator_Property_Feature
    {
        Person person;

        Dictionary<string, string> dict;
        
        [SetUp]
        public void BeforeEach()
        {
            this.person = new Person();
            this.dict = new Dictionary<string, string>();
        }
        [Test]
        public void ShouldPopulate_NoValues()
        {
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            
            person.Name.Should().Be(null);
            person.SocialSecurityNumber.Should().Be(null);
        }
        
        [Test]
        public void ShouldPopulate_MismatchedValues()
        {
            dict["person.id"] = "123";
            
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            
            person.Name.Should().Be(null);
            person.SocialSecurityNumber.Should().Be(null);
        }
        
        [Test]
        public void ShouldPopulate_SettersWork()
        {
            dict["person.name"] = "Scott";
            dict["person.SocialSecurityNumber"] = "183-72-4562";
            
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            
            person.Name.Should().Be("Scott");
            person.SocialSecurityNumber.Should().Be("183-72-4562");
        }
        
        [Test]
        public void ShouldPopulate_SettersWork_wrongCase()
        {
            dict["person.Name"] = "Scott";
            dict["person.socialSecurityNumber"] = "183-72-4562";
            
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            
            person.Name.Should().Be("Scott");
            person.SocialSecurityNumber.Should().Be("183-72-4562");
        }
        
        [Test]
        public void ShouldPopulate_SettersWork_Nesting()
        {
            dict["person.Manager.Name"] = "Matt";
            dict["person.SocialSecurityNumber"] = "183-72-4562";
            
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            
            person.Manager.Name.Should().Be("Matt");
            person.SocialSecurityNumber.Should().Be("183-72-4562");
        }
        
        [Test]
        public void ShouldPopulate_SettersWork_Nesting_withDefault()
        {
            dict["person.Manager.Name"] = "Matt";
            dict["person.SocialSecurityNumber"] = "183-72-4562";
            
            person.Manager = new Person();
            person.Manager.SocialSecurityNumber = "marker";
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            
            person.Manager.Name.Should().Be("Matt");
            person.SocialSecurityNumber.Should().Be("183-72-4562");
            person.Manager.SocialSecurityNumber.Should().Be("marker");
        }
        
        [Test]
        public void ShouldPopulate_SettersWork_CustomDataTypes()
        {
            dict["person.Manager.BirthDay"] = "1999-01-01";
            
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            
            person.Manager.BirthDay.Should().Be(new DateTime(1999, 01, 01));
        }
        
        [Test]
        public void ShouldPopulate_SettersWork_CustomDataTypes_badConversion()
        {
            dict["person.Manager.BirthDay"] = "abc";
            
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            
            person.Manager.BirthDay.Should().Be(DateTime.MinValue);
        }
        
        [Test]
        public void ShouldPopulate_SettersWork_CustomDataTypes_badConversion_OfClass()
        {
            dict["person.Manager"] = "abc";
            
            new ComplexObjectDataPopulator("person", person, dict).ProcessData();
            
            person.Manager.Should().BeNull();
        }
        
        private class Person
        {
            private string name;
            
            public string SocialSecurityNumber { get; set; }
            public DateTime BirthDay { get; set; }

            public string Name
            {
                get
                {
                    return name;
                }
            }

            public Person Manager
            {
                get;
                set;
            }
        }
    }
    
    
}
