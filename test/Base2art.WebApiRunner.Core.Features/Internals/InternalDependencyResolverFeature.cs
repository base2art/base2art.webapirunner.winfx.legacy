﻿
namespace Base2art.WebApiRunner.Internals
{
    using System.Linq;
    using FluentAssertions;
    using NUnit.Framework;
    
    [TestFixture]
    public class InternalDependencyResolverFeature
    {
        [Test]
        public void Verify()
        {
            var results = InternalDependencyResolver.GetAllInterfacesImplementedBy<Person>().ToArray();
            results.Length.Should().Be(3);
            results.Should().Contain(typeof(IPerson));
            results.Should().Contain(typeof(IAwareEntity));
            results.Should().Contain(typeof(IMammal));
        }
        
        public class Person : Mammal, IPerson
        {
        }
        
        public class Mammal : IMammal
        {
        }
        
        public interface IPerson : IAwareEntity
        {
        }
        
        public interface IMammal
        {
        }
        
        public interface IAwareEntity
        {
        }
    }
}
