namespace Base2art.WebApiRunner.Features
{
    using Base2art.WebApiRunner.Config;
    using NUnit.Framework;

    [TestFixture]
    public class CoalescingExpressionFeature
    {
        [Test]
        public void ShouldCoalesce()
        {
            var config = new SimpleServerConfiguration();
            Assert.IsNull(config.Endpoints);
            StartupBase.Coalesce(() => config.Endpoints);
            Assert.IsNotNull(config.Endpoints);
            
            Assert.IsNull(config.Endpoints.Urls);
            StartupBase.Coalesce(() => config.Endpoints.Urls);
            Assert.IsNotNull(config.Endpoints.Urls);
        }
        
        private class SimpleServerConfiguration : ServerConfiguration
        {
            public override ConfigIniter Initer()
            {
                throw new System.NotImplementedException();
            }
        }
    }
}
