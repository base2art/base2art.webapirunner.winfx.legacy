namespace Base2art.WebApiRunner.Features
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using NUnit.Framework;

    [TestFixture]
    public class YamlServiceFeature
    {
        [Test]
        public void ShouldSerialize()
        {
            IConfigSerializer service = new YamlService();
            var data = service.Serialize(new
            {
                                             Name = "SjY",
                                             Age = this.BD()
                                         });
            
            Assert.AreEqual(data.Trim(), @"
name: SjY
age: 1984-05-29T00:00:00.0000000
".Trim());
        }
        
        [Test]
        public void ShouldDeserialize()
        {
            IConfigDeserializer service = new YamlService();
            Dictionary<object, object> data = service.Deserialize<Dictionary<object, object>>(@"
name: SjY
age: 1984-05-29T00:00:00.0000000
");
            
            Assert.AreEqual(data["name"], "SjY");
            var date = data["age"];
            Assert.AreEqual(date, this.BD().ToString("o", CultureInfo.InvariantCulture));
        }
        
        [Test]
        public void ShouldDeserialize_strongType()
        {
            IConfigDeserializer service = new YamlService();
            Person data = service.Deserialize<Person>(@"
name: SjY
age: 1984-05-29T00:00:00.0000000
");
            
            Assert.AreEqual(data.Name, "SjY");
            Assert.AreEqual(data.Age, this.BD());
        }
        
        private class Person
        {
            public string Name { get; set; }

            public DateTime Age { get; set; }
        }

        private DateTime BD()
        {
            return new DateTime(1984, 05, 29);
        }
    }
}
