
if (Test-Path ".deploy") {

  rm -Recurse -Force ".deploy"
}


echo "Cleaning..."
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe /t:Clean /p:Configuration=Debug /m

echo "Building..."
C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe /t:Build /p:Configuration=Debug /m


..\..\tools\Base2art.WebApiRunner.Deployer\bin\debug\Base2art.WebApiRunner.Deployer.exe .\environments-sample.json