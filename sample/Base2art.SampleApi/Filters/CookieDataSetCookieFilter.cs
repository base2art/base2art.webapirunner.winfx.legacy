﻿
namespace Base2art.SampleApi.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.Filters;
    using Base2art.SampleApi.Public.Models;
    
    public class CookieDataSetCookieFilter : ISetCookieFilter<CookieData>
    {
        public Task SetCookiesAsync(CookieData content, IList<System.Net.Cookie> cookies)
        {
            if (content != null)
            {
                cookies.Add(new System.Net.Cookie(content.Name, content.Value){ Expires = DateTime.UtcNow.Add(content.Expires) });
            }
            
            return Task.FromResult(true);
        }
    }
}
