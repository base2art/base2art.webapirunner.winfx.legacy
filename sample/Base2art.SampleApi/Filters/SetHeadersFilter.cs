﻿
namespace Base2art.SampleApi.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.Filters;

    public class SetHeadersFilter : ISetHeadersFilter<KeyValuePair<string, string>[]>
    {
        public Task SetHeadersAsync(KeyValuePair<string, string>[] content, ICollection<KeyValuePair<string, string>> headers)
        {
            if (content != null && content.Length > 0)
            {
                Array.ForEach(content, headers.Add);
            }
            
            return Task.FromResult(true);
        }
    }
}




