﻿
namespace Base2art.SampleApi.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Net;
    using System.Threading.Tasks;
    using Base2art.SampleApi.Public.Models;
    using Base2art.WebApiRunner.Filters;

    public class SetRedirectFilter : IRedirectFilter<RedirectData>
    {
        public Task SetRedirectAsync(RedirectData content, Action<HttpStatusCode, string> setRedirect)
        {
            if (content != null)
            {
                setRedirect(
                    content.Permanent ? HttpStatusCode.MovedPermanently : HttpStatusCode.Moved,
                    content.Location);
            }
            
            return Task.FromResult(true);
        }
    }
    
}


