﻿
using System;
using System.Threading.Tasks;
using Base2art.SampleApi.Public.Models;

namespace Base2art.SampleApi.Repositories
{
    public interface IEmployeeRepository
    {
        Task<Employee[]> Find();

        Task<Employee> Get(int employeeId);
        
        Task<Employee> Add(Employee employee);

        Task Clear();
        
        Task Remove(int employeeId);
    }
}
