namespace Base2art.SampleApi.Repositories
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Base2art.SampleApi.Public;

    public class PersonCollection : KeyedCollection<Guid, Person>
    {
        protected override Guid GetKeyForItem(Person item)
        {
            return item.Id;
        }
    }
}
