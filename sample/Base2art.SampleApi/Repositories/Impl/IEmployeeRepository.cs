﻿
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using Base2art.SampleApi.Public.Models;

namespace Base2art.SampleApi.Repositories.Impl
{
    public class InMemoryEmployeeRepo: IEmployeeRepository
    {
        private readonly KeyedCollection<int, Employee> coll = new EmployeeCollection();

        public Task<Employee[]> Find()
        {
            return Task.Run(() =>
                {
                                return this.coll.ToArray();
                });
        }
        
        public Task<Employee> Get(int employeeId)
        {
            return Task.Run(() =>
                {
                    return this.coll[employeeId];
                });
        }
        
        public Task<Employee> Add(Employee employee)
        {
            return Task.Run(() =>
                {
                    coll.Add(employee);
                    return employee;
                });
        }
        
        public Task Clear()
        {
            return Task.Run(() =>
                {
                    coll.Clear();
                });
        }
        
        public Task Remove(int employeeId)
        {
            return Task.Run(() =>
                {
                    coll.Remove(employeeId);
                });
        }
        
        private class EmployeeCollection : KeyedCollection<int, Employee>
        {
            protected override int GetKeyForItem(Employee item)
            {
                return item.EmployeeId;
            }
        }
    }
}
