﻿
namespace Base2art.SampleApi.Public.HealthChecks
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public class SimpleExceptionalAlternateApiHealthCheck
    {
        public async Task ExecuteAsync()
        {
            if (Math.Abs(Environment.TickCount) % 4 < 2)
            {
                throw new InvalidDataException("My Message Exception");
            }
            
            Console.WriteLine("Here");
        }
    }
}


