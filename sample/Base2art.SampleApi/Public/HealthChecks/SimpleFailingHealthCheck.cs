﻿
namespace Base2art.SampleApi.Public.HealthChecks
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Threading.Tasks;

	public class SimpleFailingHealthCheck
	{
		public Task<ErrorEventArgs> ExecuteAsync()
		{
			return Task.FromResult<ErrorEventArgs>(new ErrorEventArgs(new InvalidDataException("sdf")));
		}
	}
}


