﻿
namespace Base2art.SampleApi.Public.HealthChecks
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    
    public class SimpleExceptionalHealthCheck
    {
        public Task<ErrorEventArgs> ExecuteAsync()
        {
            if (Environment.TickCount % 4 < 1)
            {
                throw new InvalidDataException("My Message Exception");
            }
            
            return Task.FromResult<ErrorEventArgs>(new ErrorEventArgs(new InvalidDataException("My Message Standard Fail")));
        }
    }
    
    
}
