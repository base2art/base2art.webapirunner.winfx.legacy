﻿
namespace Base2art.SampleApi.Public.HealthChecks
{
	using System.Collections.Generic;
	using System.IO;
	using System.Threading.Tasks;

	public class SimplePassingHealthCheck
	{
		public Task<ErrorEventArgs> ExecuteAsync()
		{
		    return Task.FromResult<ErrorEventArgs>(null);
		}
	}
}


