﻿
namespace Base2art.SampleApi.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    
    public class CreateFileTask
    {
        private readonly string basePath;

        public CreateFileTask(string basePath)
        {
            this.basePath = basePath;
            
        }
        public async Task ExecuteAsync(Dictionary<string, string> data)
        {
            File.WriteAllText(this.basePath + "CreateFileTask" + data["fname"] + ".txt", Guid.NewGuid().ToString("N"));
        }
    }
    
}


