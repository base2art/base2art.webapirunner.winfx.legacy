﻿
namespace Base2art.SampleApi.Public.Tasks
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner;

    public class TypedCreateFileTaskSimple : ITask
    {
        public async Task ExecuteAsync()
        {
            File.WriteAllText("c:\\Temp\\CreateFileTask-abc-typed.txt", Guid.NewGuid().ToString("N"));
        }
    }
}
