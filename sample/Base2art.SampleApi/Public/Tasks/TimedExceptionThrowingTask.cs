﻿namespace Base2art.SampleApi.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;

    public class TimedExceptionThrowingTask
    {
        public static Type Type;
        
        public async Task ExecuteAsync(Dictionary<string, string> data, TextWriter @out)
        {
            Console.WriteLine("HERE!");
            throw (Exception)Activator.CreateInstance(Type);
        }
    }
}


