﻿namespace Base2art.SampleApi.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    
    public class ExceptionThrowingTask
    {
        public async Task ExecuteAsync(Dictionary<string, string> data, TextWriter @out)
        {
            await @out.WriteLineAsync("Hello");
            
            if (data == null || !data.ContainsKey("type"))
            {
                throw new ApplicationException();
            }
            
            var type = data["type"];
            var t = Type.GetType(type, false, true);
            throw (Exception)Activator.CreateInstance(t);
        }
    }
}
