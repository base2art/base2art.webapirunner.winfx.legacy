﻿
namespace Base2art.SampleApi.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Text;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner;
    
    public class TypedSampleInterfaceObjectDictTask : ILoggedTask<TextWriter>
    {
        public async Task ExecuteAsync(IDictionary<string, object> data, TextWriter @out)
        {
            @out.WriteLine(data);
            await @out.WriteLineAsync("Done");
        }
    }
}








