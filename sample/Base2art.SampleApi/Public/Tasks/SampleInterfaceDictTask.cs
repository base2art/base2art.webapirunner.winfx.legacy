﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
namespace Base2art.SampleApi.Public.Tasks
{
	public class SampleInterfaceDictTask
	{
		public async Task ExecuteAsync(IDictionary<string, string> data, TextWriter @out)
		{
			foreach (var item in data)
			{
				@out.Write(item.Key);
				@out.Write("=");
				@out.Write(item.Value);
				@out.WriteLine();
			}
			await @out.WriteLineAsync("Done");
		}
	}
}

