﻿
namespace Base2art.SampleApi.Public.Tasks
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    
    public class CreateFileTaskSimple
    {
        public async Task ExecuteAsync()
        {
            File.WriteAllText("c:\\Temp\\CreateFileTask-abc.txt", Guid.NewGuid().ToString("N"));
        }
    }
}
