﻿namespace Base2art.SampleApi.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner;
    
    public class TypedExceptionThrowingTask : ILoggedTask<TextWriter>
    {
        public async Task ExecuteAsync(IDictionary<string, object> data, TextWriter @out)
        {
            await @out.WriteLineAsync("Hello");
            throw new ApplicationException();
        }
    }
}
