﻿
namespace Base2art.SampleApi.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner;

    public class TypedCreateFileTask : IParameterizedTask
    {
        public async Task ExecuteAsync(IDictionary<string, object> data)
        {
            File.WriteAllText("c:\\Temp\\CreateFileTask" + (string)data["fname"] + ".txt", Guid.NewGuid().ToString("N"));
        }
    }
}
