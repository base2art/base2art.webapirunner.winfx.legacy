﻿
namespace Base2art.SampleApi.Public.Tasks
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner;
    
    public class TypedPurgeLogsTask : ILoggedTask<TextWriter>
    {
        public async Task ExecuteAsync(IDictionary<string, object> data, TextWriter @out)
        {
            foreach (var item in data)
            {
                @out.Write(item.Key);
                @out.Write("=");
                @out.Write(item.Value);
                @out.WriteLine();
            }
            
            await @out.WriteLineAsync("Done");
        }
    }
}


