﻿
using System;
using System.Collections.Generic;

namespace Base2art.SampleApi.Public.Models
{
    public class Message
    {
        public string To { get; set; }
        public string From { get; set; }
//        public Dictionary<string, object> Data { get; set; }
        public Dictionary<string, object> Data { get; set; }
    }
    public class MessageInterface
    {
        public string To { get; set; }
        public string From { get; set; }
        public IDictionary<string, object> Data { get; set; }
//        public Dictionary<string, object> Data { get; set; }
    }
}
