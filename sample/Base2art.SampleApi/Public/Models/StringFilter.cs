﻿namespace Base2art.SampleApi.Public.Models
{
    public class StringFilter
    {
        public string StartsWith
        {
            get;
            set;
        }

        public string EndsWith
        {
            get;
            set;
        }
    }
}


