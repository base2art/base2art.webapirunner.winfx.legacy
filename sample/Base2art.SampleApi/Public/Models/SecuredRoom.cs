﻿namespace Base2art.SampleApi.Public.Models
{
    public class SecuredRoom
    {
        public string Name { get; set; }
        public int SeatCount { get; set; }
    }
}
