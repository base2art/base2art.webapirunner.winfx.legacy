﻿
namespace Base2art.SampleApi.Public.Models
{
    public class Employee
    {
        public int EmployeeId { get; set; }
        
        public string Name { get; set; }
    }
}
