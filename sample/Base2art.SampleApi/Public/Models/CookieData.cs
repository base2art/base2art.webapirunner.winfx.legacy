﻿
namespace Base2art.SampleApi.Public.Models
{
    using System;
    public class CookieData
    {
        public string Name { get; set; }
        public string Value { get; set; }
        public TimeSpan Expires { get; set; }
    }
    
       
}
