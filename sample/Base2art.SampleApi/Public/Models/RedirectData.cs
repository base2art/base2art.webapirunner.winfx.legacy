﻿
namespace Base2art.SampleApi.Public.Models
{
    using System;

    public class RedirectData
    {
        public string Location
        {
            get;
            set;
        }

        public bool Permanent
        {
            get;
            set;
        }
    }
}


