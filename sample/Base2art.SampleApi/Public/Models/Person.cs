namespace Base2art.SampleApi.Public
{
   using System;
   using System.Linq;

   public class Person
   {
      public Guid Id { get; set; }

      public string Name { get; set; }

      public DateTime BirthDate { get; set; }
   }
}
