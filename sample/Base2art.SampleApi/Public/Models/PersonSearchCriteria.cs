﻿namespace Base2art.SampleApi.Public.Models
{
    using System.Collections.Generic;
    
    public class PersonSearchCriteria
    {
        public bool ThrowException { get; set; }
        public StringFilter Name { get; set; }
        public List<string> Tags { get; set; }
        public List<StringFilter> RandomFilterTest { get; set; }
    }
}
