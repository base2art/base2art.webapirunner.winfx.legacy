﻿namespace Base2art.SampleApi.Public.Models
{
    public class PaginationData
    {
        public int PageIndex { get; set; }
        
        public int PageSize { get; set; }
    }
}
