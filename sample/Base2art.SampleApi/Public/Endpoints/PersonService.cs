namespace Base2art.SampleApi.Public
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Security.Claims;
    using Base2art.SampleApi.Public.Models;
    using Base2art.SampleApi.Repositories;
    using Base2art.WebApiRunner.Security;

    public class PersonService
    {
        private readonly static KeyedCollection<Guid, Person> people = new PersonCollection();
        
        static PersonService()
        {
            people.Add(new Person {
                Id = Guid.NewGuid(),
                Name = "Scott",
                BirthDate = DateTime.UtcNow.AddYears(-32)
            });
            
            people.Add(new Person {
                Id = Guid.NewGuid(),
                Name = "Matt",
                BirthDate = DateTime.UtcNow.AddYears(-38)
            });
        }
        
        public Person[] List()
        {
            return people.ToArray();
        }
        
        // PaginationData paginationData, 
        public Person[] Filter(PaginationData paginationData, PersonSearchCriteria searchCriteria, ClaimsPrincipal principal)
        {
            Console.WriteLine(principal.Identity.IsAuthenticated);
            
            if (searchCriteria.ThrowException)
            {
                throw new NotImplementedException();
            }
            
            IEnumerable<Person> list = this.List();
            
            if (searchCriteria.Name == null)
            {
                searchCriteria.Name = new StringFilter();
            }
            
            if (!string.IsNullOrWhiteSpace(searchCriteria.Name.EndsWith))
            {
                list = list.Where(x=>x.Name.EndsWith(searchCriteria.Name.EndsWith));
            }
            
            if (!string.IsNullOrWhiteSpace(searchCriteria.Name.StartsWith))
            {
                list = list.Where(x=>x.Name.StartsWith(searchCriteria.Name.StartsWith));
            }
            
            return list.Skip(paginationData.PageIndex * paginationData.PageSize).Take(paginationData.PageSize).ToArray();
            //            return people.Where(x=>x.Name.Contains(name)).ToArray();
        }

        // GET api/values/5
        public Person Get(Guid id)
        {
            return people[id];
        }

        // PUT api/values/5
        public void Put(Guid id, Person value)
        {
            if (value.Id == Guid.Empty)
            {
                value.Id = id;
            }
            
            if (value.Id != id)
            {
                throw new InvalidOperationException("Ids must match if present");
            }
            
            if (people.Contains(id))
            {
                people.Remove(id);
            }
            
            people.Add(value);
        }

        // DELETE api/values/5
        public void Delete(Guid id)
        {
            people.Remove(id);
        }
    }
}
