﻿
namespace Base2art.SampleApi.Public.Endpoints
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using System;

    public class ParmsService
    {
        public Task<int[]> GetInts(int[] values)
        {
            return Task.FromResult(values);
        }
        
        public Task<string[]> GetStrings(string[] values)
        {
            return Task.FromResult((values).Union(new []{ DateTime.UtcNow.ToString() }).ToArray());
        }
    }
}

