﻿
namespace Base2art.SampleApi.Public.Endpoints
{
    using System.Threading.Tasks;
    using Base2art.SampleApi.Public.Models;
    
    public class SetCookieService
    {
        public Task<CookieData> SetCookie(CookieData cookieData)
        {
            return Task.FromResult(cookieData);
        }
    }
    
}
