﻿namespace Base2art.SampleApi.Public.Endpoints
{
    using System;
    using System.Threading.Tasks;

    public class EchoService
    {
        private readonly string prefix;

        private readonly string suffix;
        
        public EchoService(string prefix, string suffix)
        {
            this.suffix = suffix;
            this.prefix = prefix;
            
        }
        
        public Task<string> Get(string value)
        {
            return Task.FromResult(string.Concat(this.prefix, value, this.suffix));
        }
    }
}
