﻿
namespace Base2art.SampleApi.Public.Endpoints
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public class HeaderService
    {
        public Task<KeyValuePair<string, string>[]> SetHeaders(KeyValuePair<string, string>[] headerData)
        {
            return Task.FromResult(headerData);
        }
    }

    
}


