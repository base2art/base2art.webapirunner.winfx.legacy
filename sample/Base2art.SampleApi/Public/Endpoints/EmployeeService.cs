﻿
namespace Base2art.SampleApi.Public.Endpoints
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base2art.SampleApi.Public.Models;
    using Base2art.SampleApi.Repositories;
    using Base2art.Tasks;
    using Base2art.WebApiRunner.SystemServices;

    public class EmployeeService
    {
        private readonly IEmployeeRepository repo;

        private readonly IJsonSerializer serializer;
        
        public EmployeeService(IEmployeeRepository repo, IJsonSerializer serializer)
        {
            this.serializer = serializer;
            this.repo = repo;
        }
        
        public async Task<Employee[]> List()
        {
            return await this.repo.Find();
        }
        
        public Task<dynamic[]> ListAsObject()
        {
            return this.repo.Find()
                .Then()
                .Select(x => this.serializer.DeserializeObject<dynamic>(this.serializer.SerializeObject(x)))
                .Then()
                .ToArray();
        }
        
        public Task<Dictionary<string, object>[]> ListAsDict()
        {
            return this.repo.Find()
                .Then()
                .Select(x => this.serializer.DeserializeObject<Dictionary<string, object>>(this.serializer.SerializeObject(x)))
                .Then()
                .ToArray();
        }
        
        public async Task<Employee> Get(int employeeId)
        {
            return await this.repo.Get(employeeId);
        }
        
        public async Task<Employee> Put(Employee employee)
        {
            return await this.repo.Add(employee);
        }
        
        public async Task Clear()
        {
            await this.repo.Clear();
        }
        
        public async Task Delete(int employeeId)
        {
            await this.repo.Remove(employeeId);
        }
    }
}
