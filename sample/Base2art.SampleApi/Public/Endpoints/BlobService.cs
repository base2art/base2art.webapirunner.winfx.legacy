﻿namespace Base2art.SampleApi.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Runtime.Serialization;
    using System.Text;
    using System.Threading.Tasks;
    using System.Xml;
    using Base2art.SampleApi.Public.Models;
    
    public class BlobService
    {
        public Task<string> PutBlob(Dictionary<string, object> data)
        {
            return Task.FromResult(WriteXml(ToXml(data)));
        }
        
        public Task<string> PutBlobInterface(IDictionary<string, object> data)
        {
            return Task.FromResult(WriteXml(ToXml(data)));
        }
        
        public Task<string> PutNestedBlob(Message data)
        {
            return Task.FromResult(WriteXml(ToXml(data)));
        }
        
        public Task<string> PutNestedBlobInterface(MessageInterface data)
        {
            return Task.FromResult(WriteXml(ToXml(data)));
        }
        
        public static string WriteXml(XmlDocument doc)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings {
                Indent = true,
                IndentChars = "  "
            };
            
            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                doc.Save(writer);
            }
            
            return sb.ToString();
        }
        
        public static XmlDocument ToXml<T>(T data)
        {
            var knownTypes = new Type[]{
                typeof(Dictionary<string, object>),
                typeof(Dictionary<string, string>),
                typeof(List<string>)
            };
            
            DataContractSerializer ser = new DataContractSerializer(typeof(T), knownTypes);
            
            using (var ms = new MemoryStream())
            {
                using (var writer = new MyXmlTextWriter(ms))
                {
                    ser.WriteObject(writer, data);
                    writer.Flush();
                    ms.Seek(0L, SeekOrigin.Begin);
                    
                    var xmlDoc = new XmlDocument();
                    xmlDoc.Load(ms);
                    return xmlDoc;
                }
            }
        }

        private class MyXmlTextWriter : XmlTextWriter
        {
            public MyXmlTextWriter(Stream stream)
                : base(stream, null)
            {
            }
            
            public MyXmlTextWriter(TextWriter stream)
                : base(stream)
            {
            }

            public override void WriteStartElement(string prefix, string localName, string ns)
            {
                base.WriteStartElement(null, localName, string.Empty);
            }
        }
    }
}
