﻿using System.Security.Claims;
using Base2art.SampleApi.Public.Models;
using Base2art.WebApiRunner.Security;
namespace Base2art.SampleApi.Public.Endpoints
{
    public class SecuredRoomService
    {
        public string[] List(ClaimsPrincipal principal)
        {
            return new []{ "1", "2" };
        }
        
        public string Get(string key, ClaimsPrincipal principal)
        {
            return "GET " + key;
        }
        
        public string CreateSimple(string key, int count, SecuredRoom room, ClaimsPrincipal principal)
        {
            room = room ?? new SecuredRoom();
            return key + ":" + count + ":" + room.Name;
        }
        
        public SecuredRoom Create(string key, int count, SecuredRoom room, ClaimsPrincipal principal)
        {
            room = room ?? new SecuredRoom();
            room.Name = key + ":" + count + ":" + room.Name;
            return room;
        }
        
        public SecuredRoom Update(string key, SecuredRoom room, ClaimsPrincipal principal)
        {
            return room;
        }
        
        public SecuredRoom[] Merge(SecuredRoom[] rooms, ClaimsPrincipal principal)
        {
            return rooms;
        }
        
        public SecuredRoom AddSpecialRoom(SecuredRoom room, ClaimsPrincipal principal)
        {
            return room;
        }
        
        public void Delete(ClaimsPrincipal principal)
        {
        }
        
        public void DeleteItem(string key, ClaimsPrincipal principal)
        {
        }
        
        
    }
}
