﻿namespace Base2art.SampleApi.Public.Endpoints
{
    using System.Threading.Tasks;
    
    public class UtilService
    {
        public async Task<int> ThrowException()
        {
            await Task.Run(() => {});
            throw new System.Exception("Standard Exception!");
        }
    }
}
