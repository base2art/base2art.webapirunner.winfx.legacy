﻿
namespace Base2art.SampleApi.Public.Endpoints
{
    using System.Threading.Tasks;
    using Base2art.SampleApi.Public.Models;

    public class RedirectService
    {
        public Task<RedirectData> Redirect(RedirectData redirectData)
        {
            return Task.FromResult(redirectData);
        }
    }
    
}
