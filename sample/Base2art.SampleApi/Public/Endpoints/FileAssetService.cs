﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Base2art.SampleApi.Public.Endpoints
{
    public class FileAssetService
    {
        private readonly string basePath;

        private readonly string defaultDocumentResource;
        
        public FileAssetService(string basePath, string defaultDocumentResource)
        {
            this.defaultDocumentResource = defaultDocumentResource;
            this.basePath = basePath;
        }
        
        public FileInfo Get(string anything)
        {
            var parts = anything.Split(new char[] {
                Path.AltDirectorySeparatorChar,
                Path.DirectorySeparatorChar
            }, StringSplitOptions.RemoveEmptyEntries);
            
            var stack = new Stack<string>();
            
            ProcessParts(parts, 0, stack);
            
            List<string> fileParts = new List<string>();
            fileParts.Add(basePath);
            fileParts.AddRange(stack.Reverse());
            
            var path = Path.Combine(fileParts.ToArray());
            
            var di = new DirectoryInfo(path);
            var fi = new FileInfo(path);
            
            if (di.Exists)
            {
                fi = new FileInfo(Path.Combine(path, defaultDocumentResource));
            }
            
            if (fi.Exists)
            {
                return fi;
            }
            
            return null;
        }
        
        public FileInfo Root()
        {
            return new FileInfo(Path.Combine(basePath, defaultDocumentResource));
        }

        private void ProcessParts(string[] parts, int i, Stack<string> stack)
        {
            if (parts.Length <= i)
            {
                return;
            }
            
            var part = parts[i];
            
            if (part == ".")
            {
                this.ProcessParts(parts, i + 1, stack);
                return;
            }
            
            if (part == "..")
            {
                stack.Pop();
                
                this.ProcessParts(parts, i + 1, stack);
                return;
            }
            
            
            stack.Push(part);
            this.ProcessParts(parts, i + 1, stack);
        }
    }
}
