﻿namespace Base2art.SampleApi.Aspects
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using System.Web.Http.Controllers;
    using Base2art.WebApiRunner.Aspects;
    
    public class SampleLogger1 : SimpleMethodInterceptAspect
    {
        public override string Name
        {
            get { return "base2art-tests.FileSystem-Log1"; }
        }

        protected async override Task OnExecuting(MethodInterceptData data, HttpControllerContext controllerContext, HttpActionDescriptor actionDescriptor)
        {
            await Task.Delay(TimeSpan.FromSeconds(1));
            File.WriteAllText("c:\\Temp\\Logs\\" + (Environment.TickCount ) + ".1.log", "TEST-1");
            await base.OnExecuting(data, controllerContext, actionDescriptor);
        }
    }    
}