﻿namespace Base2art.SampleApi.Aspects
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using System.Web.Http.Controllers;
    using Base2art.WebApiRunner.Aspects;

    public class SampleLogger2 : SimpleMethodInterceptAspect
    {
        public override string Name
        {
            get { return "base2art-tests.FileSystem-Log2"; }
        }

        protected async override Task OnExecuting(MethodInterceptData data, HttpControllerContext controllerContext, HttpActionDescriptor actionDescriptor)
        {
            // / 10000L
            await Task.Delay(TimeSpan.FromSeconds(1));
            File.WriteAllText("c:\\Temp\\Logs\\" + (Environment.TickCount) + ".2.log", "TEST-2");
            await base.OnExecuting(data, controllerContext, actionDescriptor);
        }
    }
}

