﻿namespace Base2art.SampleAuthService.WindowsAuth
{
    using System;
    using System.Collections.Generic;
    using System.DirectoryServices.AccountManagement;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    using System.Threading;
    using System.Threading.Tasks;
    using Base2art.SampleAuthService.Repositories;

    public class CurrentPrincipalAuthDisplayNameClaimProvider : IClaimLookup
    {
        public Task<List<Claim>> FindAdditionalClaims(string name)
        {
            var principal = (WindowsPrincipal)Thread.CurrentPrincipal;
            var identity = (WindowsIdentity)principal.Identity;
            List<string> groups = new List<string>();
            PrincipalContext ctx = new PrincipalContext(identity.IsDomain() ? ContextType.Domain : ContextType.Machine);
            UserPrincipal up = UserPrincipal.FindByIdentity(ctx, identity.Name);
            return Task.FromResult(new List<Claim>(){new Claim("name", up.DisplayName ?? "Unknown")});
        }
    }
}




