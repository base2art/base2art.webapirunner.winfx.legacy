﻿namespace Base2art.SampleAuthService.WindowsAuth
{
    using System;
    using System.Linq;
    using System.Security.Principal;

    public static class WindowsIdentityLookup
    {
        public static bool IsDomain(this WindowsIdentity identity)
        {
            string prefix = identity.Name.Split('\\')[0];
            return !string.Equals(prefix, Environment.MachineName, StringComparison.OrdinalIgnoreCase);
        }
    }
}




