﻿namespace Base2art.SampleAuthService.WindowsAuth
{
    using System;
    using System.DirectoryServices.AccountManagement;
    using System.Linq;
    using System.Security.Principal;

    public class CurrentPrincipalWithCredentialsAuthGroupProvider : CurrentPrincipalAuthGroupProvider
    {
        private readonly string domain;

        private readonly string username;

        private readonly string password;

        public CurrentPrincipalWithCredentialsAuthGroupProvider(string domain, string username, string password)
        {
            this.password = password;
            this.username = username;
            this.domain = domain;
        }

        protected override PrincipalContext CreatePrincipalContext(WindowsIdentity identity)
        {
            var contextType = identity.IsDomain() ? ContextType.Domain : ContextType.Machine;
            return new PrincipalContext(contextType, domain, username, password);
        }
    }
}




