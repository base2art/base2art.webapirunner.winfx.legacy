﻿namespace Base2art.SampleAuthService.WindowsAuth
{
    using System;
    using System.Collections.Generic;
    using System.DirectoryServices.AccountManagement;
    using System.Linq;
    using System.Security.Principal;
    using System.Threading;
    using Base2art.Collections.Specialized;
    using Base2art.Security.Membership;
    
    public class CurrentPrincipalAuthGroupProvider : IMembershipProcessor
    {
        protected virtual PrincipalContext CreatePrincipalContext(WindowsIdentity identity)
        {
            return new PrincipalContext(identity.IsDomain() ? ContextType.Domain : ContextType.Machine);
        }

        public string[] UserGroups(string userName)
        {
            var principal = (WindowsPrincipal)Thread.CurrentPrincipal;
            var identity = (WindowsIdentity)principal.Identity;
            
            List<string> groups = new List<string>();
            PrincipalContext ctx = CreatePrincipalContext(identity);

            UserPrincipal up = UserPrincipal.FindByIdentity(
                ctx,
                identity.Name);
            
            return up.GetGroups().Select(x => x.Name).ToArray();
        }

        public IPagedCollection<IGroup> Groups(Page page)
        {
            throw new System.NotImplementedException();
        }

        public IGroup FindGroupByName(string groupName)
        {
            throw new System.NotImplementedException();
        }

        public GroupCreationStatus CreateGroup(GroupData groupData)
        {
            throw new System.NotImplementedException();
        }

        public GroupCreationStatus UpdateGroup(GroupData groupData)
        {
            throw new System.NotImplementedException();
        }

        public GroupAssignmentStatus AssignUserToGroup(string groupName, string userName)
        {
            throw new System.NotImplementedException();
        }

        public GroupAssignmentStatus AssignGroupToGroup(string containerGroupName, string groupName)
        {
            throw new System.NotImplementedException();
        }

        public GroupAssignmentStatus RemoveUserFromGroup(string groupName, string userName)
        {
            throw new System.NotImplementedException();
        }

        public GroupAssignmentStatus RemoveGroupFromGroup(string containerGroupName, string groupName)
        {
            throw new System.NotImplementedException();
        }

        public GroupRemovalStatus RemoveGroup(string groupName)
        {
            throw new System.NotImplementedException();
        }
    }
}


