﻿namespace Base2art.SampleAuthService.WindowsAuth
{
    using System.Threading;
    using Base2art.Security.Authentication;
    using Base2art.Security.Membership;
    
    public class CurrentPrincipalAuthenticationProcessor : IAuthenticationProcessor
    {
        public bool HasUsers()
        {
            return true;
        }
        
        public Base2art.Collections.Specialized.IPagedCollection<IUser> Users(Base2art.Collections.Specialized.Page page)
        {
            throw new System.NotImplementedException();
        }
        
        public IUser FindUserByName(string userName)
        {
            throw new System.NotImplementedException();
        }
        
        public RegisterStatus Register(UserData userData)
        {
            return RegisterStatus.InvalidRequest;
        }
        
        public SignInStatus SignIn(UserData userData)
        {
            var principal = Thread.CurrentPrincipal;
            if (principal == null || !principal.Identity.IsAuthenticated)
            {
                return SignInStatus.UnknownUser;
            }
            
            userData.Name = principal.Identity.Name;
            return SignInStatus.Success;
        }
        
        public ForgotUserNameStatus TriggerForgotUserName(string emailAddress)
        {
            throw new System.NotImplementedException();
        }
        
        public ResetPasswordStatus TriggerResetPassword(UserData userData)
        {
            throw new System.NotImplementedException();
        }
        
        public ResetPasswordStatus ResetPassword(UserData userData)
        {
            throw new System.NotImplementedException();
        }
        
        public VerifyAccountStatus TriggerVerifyAccount(UserData userData)
        {
            throw new System.NotImplementedException();
        }
        
        public VerifyAccountStatus VerifyAccount(UserData userData, System.Guid userVerificationId)
        {
            throw new System.NotImplementedException();
        }
        
        public DeleteAccountStatus DeleteAccount(UserData userData)
        {
            throw new System.NotImplementedException();
        }
    }
}
