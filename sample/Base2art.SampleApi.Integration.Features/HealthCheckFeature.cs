﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Reflection;
    using Base2art.SampleApi.Public.Models;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;

    [TestFixture]
    public class HealthCheckFeature : ServerTest
    {
        [Test]
        public async void RequestPassing()
        {
            using (var client = this.AdminRunner.AdminHttp())
            {
                List<bool> objs = new List<bool>();
                for (int i = 0; i < 20; i++)
                {
                    var response = await client.GetAsync("/healthcheck");
                    Assert.AreEqual(200, (int)response.StatusCode, await response.Content.ReadAsStringAsync());
                    
                    Console.WriteLine(await response.Content.ReadAsStringAsync());
                    var output = await response.Content.ReadAsAsync<dynamic>();
                    Assert.IsNotNull(output);
                    
                    Assert.IsTrue((bool)output.simplePassingCheck.isHealthy);
                    Assert.IsFalse((bool)output.simpleFailingCheck.isHealthy);
                    Assert.IsFalse((bool)output.simpleExceptionalCheck.isHealthy);
                    var value = (bool)output.simpleExceptionalAlternateApiCheck.isHealthy;
                    objs.Add(value);
                }
                
                Assert.IsTrue(objs.Any(x => x));
                Assert.IsTrue(objs.Any(x => !x));
            }
        }
    }
    
}




