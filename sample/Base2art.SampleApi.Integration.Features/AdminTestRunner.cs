﻿namespace Base2art.SampleApi
{
    using System.Net;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Console;
    using Base2art.WebApiRunner.Console.Config;

    public class AdminTestRunner<T> : TestRunner<T> where T : ConsoleServerConfiguration, new()
    {
        public AdminTestRunner(Runner<T> backing, CookieContainer cookies)
            : base(backing, cookies)
        {
        }

    }
}


