﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using Base2art.SampleApi.Bases;
    using Base2art.SampleApi.Public;
    using Base2art.SampleApi.Public.Models;
    using NUnit.Framework;

    [TestFixture]
    public class PostReadMethodBodyFeature : ServerTest
    {
        [Test]
        public async void RequestPublic()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                var id = Guid.NewGuid();
                var person = new Person
                {
                    Id = id,
                    BirthDate = DateTimeOffset.UtcNow.AddYears(-30).DateTime,
                    Name = "Anyone"
                };
                
                var response = await client.PutAsJsonAsync("/api/persons/" + id.ToString("N"), person);
                Assert.AreEqual(204, (int)response.StatusCode);
                
                var response2 = await client.GetAsync("/api/persons");
                Assert.AreEqual(200, (int)response2.StatusCode);
                var item = await response2.Content.ReadAsStringAsync();
                Assert.IsTrue(item.Contains("\"Anyone\""));
            }
        }
        
        [Test]
        public async void RequestPublicObject()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.GetAsync("/api/persons/filtered?paginationData.PageIndex=0&paginationData.PageSize=1");
                Assert.AreEqual(200, (int)response.StatusCode, await response.Content.ReadAsStringAsync());
                
                var item = await response.Content.ReadAsStringAsync();
                
                var rez = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(item);
                Assert.AreEqual(rez.Count, 1);
            }
        }
        
        [Test]
        public async void RequestBlob()
        {
            var data = new Dictionary<string, object>();
            var person = new Dictionary<string, object>();
            var additional = new Dictionary<string, object>();
            person["name"] = "Scott";
            person["id"] = 1;
            person["b-day"] = new DateTime(1985, 06, 07, 11, 12, 13);
            person["additional-info"] = additional;
            additional["prop1"] = 1;
            additional["prop2"] = 2;
            data["person"] = person;
            
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.PutAsJsonAsync("/api/utils/blob-params", data);
                Assert.AreEqual(200, (int)response.StatusCode, await response.Content.ReadAsStringAsync());
                
                var item = await response.Content.ReadAsStringAsync();
                
                Console.WriteLine(item);
                
                var result = @"
<?xml version=\""1.0\"" encoding=\""utf-16\""?>
<ArrayOfKeyValueOfstringanyType xmlns:i=\""http://www.w3.org/2001/XMLSchema-instance\"">
  <KeyValueOfstringanyType>
    <Key>person</Key>
    <Value xmlns:d3p1=\""http://schemas.microsoft.com/2003/10/Serialization/Arrays\"" i:type=\""d3p1:ArrayOfKeyValueOfstringanyType\"">
      <KeyValueOfstringanyType>
        <Key>name</Key>
        <Value xmlns:d5p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d5p1:string\"">Scott</Value>
      </KeyValueOfstringanyType>
      <KeyValueOfstringanyType>
        <Key>id</Key>
        <Value xmlns:d5p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d5p1:long\"">1</Value>
      </KeyValueOfstringanyType>
      <KeyValueOfstringanyType>
        <Key>b-day</Key>
        <Value xmlns:d5p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d5p1:dateTime\"">1985-06-07T11:12:13</Value>
      </KeyValueOfstringanyType>
      <KeyValueOfstringanyType>
        <Key>additional-info</Key>
        <Value i:type=\""d3p1:ArrayOfKeyValueOfstringanyType\"">
          <KeyValueOfstringanyType>
            <Key>prop1</Key>
            <Value xmlns:d7p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d7p1:long\"">1</Value>
          </KeyValueOfstringanyType>
          <KeyValueOfstringanyType>
            <Key>prop2</Key>
            <Value xmlns:d7p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d7p1:long\"">2</Value>
          </KeyValueOfstringanyType>
        </Value>
      </KeyValueOfstringanyType>
    </Value>
  </KeyValueOfstringanyType>
</ArrayOfKeyValueOfstringanyType>
";
                Assert.AreEqual(item, "\"" + result.Trim().Replace("\r\n", "\\r\\n") + "\"");
            }
        }
        
        [Test]
        public async void RequestBlobInterface()
        {
            var data = new Dictionary<string, object>();
            var person = new Dictionary<string, object>();
            var additional = new Dictionary<string, object>();
            person["name"] = "Scott";
            person["id"] = 1;
            person["b-day"] = new DateTime(1985, 06, 07, 11, 12, 13);
            person["additional-info"] = additional;
            additional["prop1"] = 1;
            additional["prop2"] = 2;
            data["person"] = person;
            
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.PutAsJsonAsync("/api/utils/blob-interface-params", data);
                Assert.AreEqual(200, (int)response.StatusCode, await response.Content.ReadAsStringAsync());
                
                var item = await response.Content.ReadAsStringAsync();
                
                Console.WriteLine(item);
                
                var result = @"
<?xml version=\""1.0\"" encoding=\""utf-16\""?>
<ArrayOfKeyValueOfstringanyType xmlns:i=\""http://www.w3.org/2001/XMLSchema-instance\"">
  <KeyValueOfstringanyType>
    <Key>person</Key>
    <Value xmlns:d3p1=\""http://schemas.microsoft.com/2003/10/Serialization/Arrays\"" i:type=\""d3p1:ArrayOfKeyValueOfstringanyType\"">
      <KeyValueOfstringanyType>
        <Key>name</Key>
        <Value xmlns:d5p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d5p1:string\"">Scott</Value>
      </KeyValueOfstringanyType>
      <KeyValueOfstringanyType>
        <Key>id</Key>
        <Value xmlns:d5p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d5p1:long\"">1</Value>
      </KeyValueOfstringanyType>
      <KeyValueOfstringanyType>
        <Key>b-day</Key>
        <Value xmlns:d5p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d5p1:dateTime\"">1985-06-07T11:12:13</Value>
      </KeyValueOfstringanyType>
      <KeyValueOfstringanyType>
        <Key>additional-info</Key>
        <Value i:type=\""d3p1:ArrayOfKeyValueOfstringanyType\"">
          <KeyValueOfstringanyType>
            <Key>prop1</Key>
            <Value xmlns:d7p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d7p1:long\"">1</Value>
          </KeyValueOfstringanyType>
          <KeyValueOfstringanyType>
            <Key>prop2</Key>
            <Value xmlns:d7p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d7p1:long\"">2</Value>
          </KeyValueOfstringanyType>
        </Value>
      </KeyValueOfstringanyType>
    </Value>
  </KeyValueOfstringanyType>
</ArrayOfKeyValueOfstringanyType>
";
                Assert.AreEqual(item, "\"" + result.Trim().Replace("\r\n", "\\r\\n") + "\"");
            }
        }
        
        [Test]
        public async void RequestNestedBlob()
        {
            var data = new Dictionary<string, object>();
            var person = new Dictionary<string, object>();
            var additional = new Dictionary<string, object>();
            person["name"] = "Scott";
            person["id"] = 1;
            person["b-day"] = new DateTime(1985, 06, 07, 11, 12, 13);
            person["additional-info"] = additional;
            additional["prop1"] = 1;
            additional["prop2"] = 2;
            data["person"] = person;
            
            var message = new Message
            {
                To = "George Washington",
                From = "Thomas Jefferson",
                Data = person
            };
            
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.PutAsJsonAsync("/api/utils/nested-blob-params", message);
                Assert.AreEqual(200, (int)response.StatusCode, await response.Content.ReadAsStringAsync());
                
                var item = await response.Content.ReadAsStringAsync();
                
                Console.WriteLine(item);
                var result = @"
<?xml version=\""1.0\"" encoding=\""utf-16\""?>
<Message xmlns:i=\""http://www.w3.org/2001/XMLSchema-instance\"">
  <Data xmlns:d2p1=\""http://schemas.microsoft.com/2003/10/Serialization/Arrays\"">
    <KeyValueOfstringanyType>
      <Key>name</Key>
      <Value xmlns:d4p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d4p1:string\"">Scott</Value>
    </KeyValueOfstringanyType>
    <KeyValueOfstringanyType>
      <Key>id</Key>
      <Value xmlns:d4p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d4p1:long\"">1</Value>
    </KeyValueOfstringanyType>
    <KeyValueOfstringanyType>
      <Key>b-day</Key>
      <Value xmlns:d4p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d4p1:dateTime\"">1985-06-07T11:12:13</Value>
    </KeyValueOfstringanyType>
    <KeyValueOfstringanyType>
      <Key>additional-info</Key>
      <Value i:type=\""d2p1:ArrayOfKeyValueOfstringanyType\"">
        <KeyValueOfstringanyType>
          <Key>prop1</Key>
          <Value xmlns:d6p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d6p1:long\"">1</Value>
        </KeyValueOfstringanyType>
        <KeyValueOfstringanyType>
          <Key>prop2</Key>
          <Value xmlns:d6p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d6p1:long\"">2</Value>
        </KeyValueOfstringanyType>
      </Value>
    </KeyValueOfstringanyType>
  </Data>
  <From>Thomas Jefferson</From>
  <To>George Washington</To>
</Message>
";
                Assert.AreEqual(item, "\"" + result.Trim().Replace("\r\n", "\\r\\n") + "\"");
            }
        }
        
        [Test]
        public async void RequestNestedBlobIDictionary()
        {
            var data = new Dictionary<string, object>();
            var person = new Dictionary<string, object>();
            var additional = new Dictionary<string, object>();
            person["name"] = "Scott";
            person["id"] = 1;
            person["b-day"] = new DateTime(1985, 06, 07, 11, 12, 13);
            person["additional-info"] = additional;
            additional["prop1"] = 1;
            additional["prop2"] = 2;
            data["person"] = person;
            
            var message = new MessageInterface
            {
                To = "George Washington",
                From = "Thomas Jefferson",
                Data = person
            };
            
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.PutAsJsonAsync("/api/utils/nested-blob-interface-params", message);
                Assert.AreEqual(200, (int)response.StatusCode, await response.Content.ReadAsStringAsync());
                
                var item = await response.Content.ReadAsStringAsync();
                
                Console.WriteLine(item);
                var result = @"
<?xml version=\""1.0\"" encoding=\""utf-16\""?>
<MessageInterface xmlns:i=\""http://www.w3.org/2001/XMLSchema-instance\"">
  <Data xmlns:d2p1=\""http://schemas.microsoft.com/2003/10/Serialization/Arrays\"">
    <KeyValueOfstringanyType>
      <Key>name</Key>
      <Value xmlns:d4p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d4p1:string\"">Scott</Value>
    </KeyValueOfstringanyType>
    <KeyValueOfstringanyType>
      <Key>id</Key>
      <Value xmlns:d4p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d4p1:long\"">1</Value>
    </KeyValueOfstringanyType>
    <KeyValueOfstringanyType>
      <Key>b-day</Key>
      <Value xmlns:d4p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d4p1:dateTime\"">1985-06-07T11:12:13</Value>
    </KeyValueOfstringanyType>
    <KeyValueOfstringanyType>
      <Key>additional-info</Key>
      <Value i:type=\""d2p1:ArrayOfKeyValueOfstringanyType\"">
        <KeyValueOfstringanyType>
          <Key>prop1</Key>
          <Value xmlns:d6p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d6p1:long\"">1</Value>
        </KeyValueOfstringanyType>
        <KeyValueOfstringanyType>
          <Key>prop2</Key>
          <Value xmlns:d6p1=\""http://www.w3.org/2001/XMLSchema\"" i:type=\""d6p1:long\"">2</Value>
        </KeyValueOfstringanyType>
      </Value>
    </KeyValueOfstringanyType>
  </Data>
  <From>Thomas Jefferson</From>
  <To>George Washington</To>
</MessageInterface>
";
                Assert.AreEqual(item, "\"" + result.Trim().Replace("\r\n", "\\r\\n") + "\"");
            }
        }
    }
}

