﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Base2art.SampleApi.Public.Models;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Formatters;
    using Base2art.SampleApi.Bases;
    using Base2art.WebApiRunner.Services.SystemServices;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class CsvResponseFeature : ServerTest
    {
        [Test]
        public void CanWrite()
        {
            var settings = Startup.SetupSerializer(new Newtonsoft.Json.JsonSerializerSettings());
            var formatter = new CsvFormatter(new JsonSerializerWrapper(settings));
            Assert.IsFalse(formatter.CanWriteType(typeof(char[])));
            Assert.IsFalse(formatter.CanWriteType(typeof(string)));
            Assert.IsFalse(formatter.CanWriteType(typeof(string[])));
            Assert.IsFalse(formatter.CanWriteType(typeof(Employee)));
            Assert.IsFalse(formatter.CanWriteType(typeof(IList<int>)));
            Assert.IsFalse(formatter.CanWriteType(typeof(System.Collections.IList)));
            Assert.IsFalse(formatter.CanWriteType(typeof(System.Collections.IEnumerable)));
            
            
            Assert.IsTrue(formatter.CanWriteType(typeof(Employee[])));
            Assert.IsTrue(formatter.CanWriteType(typeof(IEnumerable<Employee>)));
            Assert.IsTrue(formatter.CanWriteType(typeof(List<Employee>)));
            Assert.IsTrue(formatter.CanWriteType(typeof(IList<Employee>)));
            Assert.IsTrue(formatter.CanWriteType(typeof(IList<KeyValuePair<string, string>>)));
            Assert.IsTrue(formatter.CanWriteType(typeof(IList<Tuple<string, string>>)));
        }
        
        [TestCase("/api/employees", 1, "sjy", false)]
        [TestCase("/api/employees/dynamic", 2, "cjb", true)]
        [TestCase("/api/employees/dict", 3, "jos", true)]
        public async void RequestPublic(string getUrl, int id, string name, bool ignoreCase)
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                var setupResponse = await client.PutAsJsonAsync("/api/employees", new Employee{EmployeeId = id, Name = name});
                Assert.AreEqual(HttpStatusCode.OK, setupResponse.StatusCode, await setupResponse.Content.ReadAsStringAsync());
                
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, getUrl);
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.SendAsync(request);
                
                var responseContent0 = (await response.Content.ReadAsStringAsync());
                Console.WriteLine(responseContent0);
                Assert.IsTrue(responseContent0.Contains("\"employeeId\": " + id + ","));
                
                
                HttpRequestMessage csvRequest = new HttpRequestMessage(HttpMethod.Get, getUrl);
                csvRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("text/csv"));
                var csvResponse = await client.SendAsync(csvRequest);
                var responseContent = (await csvResponse.Content.ReadAsStringAsync());
                
                Console.WriteLine(responseContent);
                responseContent.Should().ContainEquivalentOf("\"EmployeeId\",\"Name\"");
                Assert.IsTrue(responseContent.Contains("\"" + id + "\",\"" + name + "\""));
                
                
                
                HttpRequestMessage csvRrequest1 = new HttpRequestMessage(HttpMethod.Get, getUrl + "?headers.accept=text/csv");
                var csvResponse1 = await client.SendAsync(csvRrequest1);
                var responseContent1 = (await csvResponse1.Content.ReadAsStringAsync());
                
                Console.WriteLine(responseContent1);
                
                responseContent.Should().ContainEquivalentOf("\"EmployeeId\",\"Name\"");
                Assert.IsTrue(responseContent1.Contains("\"" + id + "\",\"" + name + "\""));
            }
        }
    }
}

