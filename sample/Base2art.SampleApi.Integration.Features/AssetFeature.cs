﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;
    
    [TestFixture]
    public class AssetFeature : ServerTest
    {
        [TestCase("/", "<b>Hello World</b>")]
        [TestCase("/assets/index.html", "<b>Hello World</b>")]
        [TestCase("/assets/test.html", "<b>Hello File World</b>")]
        [TestCase("/assets/sub", "<b>Hello Sub World</b>")]
        [TestCase("/assets/sub/index.html", "<b>Hello Sub World</b>")]
        [TestCase("/assets/sub/test.html", "<b>Hello Sub/File World</b>")]
        [TestCase("/assets/sub/other/../test.html", "<b>Hello Sub/File World</b>")]
        public async void RequestAsset(string path, string expectedContains)
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.GetStringAsync(path);
                Assert.IsTrue(response.Contains(expectedContains));
            }
        }
    }
    
}
