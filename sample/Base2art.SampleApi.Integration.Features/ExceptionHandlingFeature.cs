﻿namespace Base2art.SampleApi
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;

    [TestFixture]
    public class ExceptionHandlingFeature : ServerTest
    {
        [Test]
        public async void RequestTask_WithExceptionHandler()
        {
            using (var client = this.AdminRunner.AdminHttp())
            {
                var response = await client.PostAsJsonAsync<object>("tasks/throw-exception", null);
                
                Assert.AreEqual(response.StatusCode, HttpStatusCode.InternalServerError);
                var str = await response.Content.ReadAsStringAsync();
                Assert.IsTrue(str.Contains("An error has occurred"));
            }
        }
        
        [Test]
        public async void RequestUrl_WithExceptionHandler()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.GetAsync("api/utils/throw-exception");
                
                Assert.AreEqual(response.StatusCode, HttpStatusCode.InternalServerError);
                var str = await response.Content.ReadAsStringAsync();
                Assert.IsTrue(str.Contains("An error has occurred"));
            }
        }
    }
}


