﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;

    [TestFixture]
    public class ArrayParameterFeature : ServerTest
    {
        [TestCase("api/utils/int-parms", "[]")]
        [TestCase("api/utils/int-parms?values=1&values=2", "[ 1, 2]")]
        [TestCase("api/utils/string-parms?values=1+1&values=2", "[ '1 1', '2', {Date}]")]
        public async void RequestPublic(string input, string output)
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                var setupResponse = await client.GetAsync(input);
                var content = (await setupResponse.Content.ReadAsStringAsync());
                
                Assert.AreEqual(HttpStatusCode.OK, setupResponse.StatusCode);
                
                
                
                output = output.Replace("'", "\"")
                    .Replace("{Date}", "\"" + DateTime.UtcNow.ToString() + "\"");
                
                Assert.AreEqual(output, this.Clean(content));
            }
        }

        private string Clean(string content)
        {
            content = content.Replace(Environment.NewLine, "");
            while (content.Contains("  "))
            {
                content = content.Replace("  ", " ");
            }
            
            return content;
        }
    }
}





