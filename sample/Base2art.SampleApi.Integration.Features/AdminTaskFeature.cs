﻿namespace Base2art.SampleApi
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;
    
    [TestFixture]
    public class AdminTaskFeature : ServerTest
    {
        [Test]
        public async void RequestTask()
        {
            using (var client = this.AdminRunner.AdminHttp())
            {
                var gizmo = new { };
                var response = await client.PostAsJsonAsync("tasks/clear-caches", gizmo);

                var result = (await response.Content.ReadAsStringAsync()).Trim();
                Assert.AreEqual(result, "\"Done\\r\\n\"", message:result);
            }
        }
        
        [TestCase("tasks/clear-caches", "\"Done\\r\\n\"", "Done\r\n")]
        [TestCase("tasks/sample-basic", "\"Done\\r\\n\"", "Done\r\n")]
        [TestCase("tasks/sample-interface-dict", "\"Done\\r\\n\"", "Done\r\n")]
        [TestCase("tasks/sample-interface-object-dict", "\"{}\\r\\nDone\\r\\n\"", "{}\r\nDone\r\n")]
        public async void RequestTask_NoBody(string requestPath, string expected, string consoleExpected)
        {
            var old = Console.Out;
            var stringWriter = new StringWriter();
            Console.SetOut(stringWriter);
            using (var client = this.AdminRunner.AdminHttp())
            {
                var response = await client.SendAsync(new HttpRequestMessage(HttpMethod.Post, requestPath));

                var result = (await response.Content.ReadAsStringAsync()).Trim();
                Assert.AreEqual(expected, result);
                // console writing is slow?
                await Task.Delay(TimeSpan.FromMilliseconds(300));
                Assert.AreEqual(consoleExpected, stringWriter.GetStringBuilder().ToString());
            }
            
            Console.SetOut(old);
            
        }
        
        [Test]
        public async void RequestTask_WithSpecificBody()
        {
            using (var client = this.AdminRunner.AdminHttp())
            {
                var response = await client.PostAsJsonAsync<object>("tasks/clear-caches", new Dictionary<string, string>{ {
                                                                            "abc",
                                                                            "123"
                                                                        } });

                var result = (await response.Content.ReadAsStringAsync()).Trim();
                Assert.AreEqual(result, "\"abc=123\\r\\nDone\\r\\n\"");
            }
        }
        
        [Test]
        public async void RequestTask_WithExceptionHandler()
        {
            using (var client = this.AdminRunner.AdminHttp())
            {
                var response = await client.PostAsJsonAsync<object>("tasks/clear-caches", new Dictionary<string, string>{ {
                                                                            "abc",
                                                                            "123"
                                                                        } });

                var result = (await response.Content.ReadAsStringAsync()).Trim();
                Assert.AreEqual(result, "\"abc=123\\r\\nDone\\r\\n\"");
            }
        }

        [Test]
        public async void RequestTask_Parameterized()
        {
            using (var client = this.AdminRunner.AdminHttp())
            {
                int i = new Random().Next();
                
                var item = "c:\\Temp\\CreateFileTask" + i + ".txt";
                
                Assert.IsFalse(File.Exists(item));
                
                var response = await client.PostAsJsonAsync<object>("tasks/create-file", new Dictionary<string, string> {
                                                                        {
                                                                            "fname",
                                                                            i.ToString()
                                                                        }
                                                                    });
                
                var result = (await response.Content.ReadAsStringAsync()).Trim();
                Assert.IsTrue(File.Exists(item));
                File.Delete(item);
            }
        }

        [Test]
        public async void RequestTask_NonParameterized()
        {
            using (var client = this.AdminRunner.AdminHttp())
            {
                int i = new Random().Next();
                
                var item = "c:\\Temp\\CreateFileTask-abc.txt";
                
                if (File.Exists(item))
                {
                    File.Delete(item);
                }
                
                Assert.IsFalse(File.Exists(item));
                
                var response = await client.PostAsJsonAsync<object>("tasks/create-file-simple", new Dictionary<string, string> {
                                                                        {
                                                                            "fname",
                                                                            i.ToString()
                                                                        }
                                                                    });
                var result = (await response.Content.ReadAsStringAsync()).Trim();
                Assert.IsTrue(File.Exists(item));
                File.Delete(item);
            }
        }
    }
}
