﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.IO;
    using System.Linq;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;

    [TestFixture]
    public class ConfigAutoGenerationFeature : ServerTest
    {
        protected override string ConfigFileName
        {
            get
            {
                return "Test--gen";
            }
        }

        [Test]
        public void ShouldCreate()
        {
            var path = HttpClientFactory.ConfigFileToPath(this.ConfigFileName);
            if (File.Exists(path))
            {
                File.Delete(path);
            }
            
            HttpClientFactory.CreateAdminRunner(this.ConfigFileName, new System.Net.CookieContainer());
            
            Assert.IsTrue(File.Exists(path));
        }
    }
}




