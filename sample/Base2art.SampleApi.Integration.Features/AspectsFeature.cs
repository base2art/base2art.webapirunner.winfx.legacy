﻿namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Text;
    using System.Threading.Tasks;
    using Base2art.SampleApi.Bases;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class AspectsFeature : ServerTest
    {
        //        private readonly string key = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImtpZCI6ImxvY2FsaG9zdCJ9.eyJzdWIiOiJzY290dC55b3VuZ2JsdXRAZ21haWwuY29tIiwiaXNzIjoiaHR0cDovL3Nzby5iYXNlMmFydC5jb20vIiwiYXVkIjoiaHR0cDovL2xvY2FsaG9zdC8iLCJleHAiOjE0OTU5MjkyMTAsIm5iZiI6MTQ5NTg0MjgxMH0.CezhNfEEvk_3rD0bH3KYZPa2eLbOENgU2sHUyCzdo4VC-SJqlOf3VvE7VlTjpzkSoiy4htxcQOFsbnfXvxrTGkeENfPrBi_NlqHhVKfHeeju0Bwaq62SPSGhfjyoq0iytSD8gKlJTGkeUGCCB4SADiAP0sDJ6PNp0nPNI-lGLnKb25_DnZLG8YqheGJ-3Pz1F4DsDAcurKoNINOzqz2LtnTTBEthPFpx_Dacw2Lu98_-5fIFIj61okjPCbUYiRlQoKxHGm3SMGVKxpYiEWVlUgswedx6mQoRZ_h8Qfh7LR11wWG4Kt_KoGvvBd7UUS7un_ciV_F9PAluC34W-xGm1A";
        
        private readonly Lazy<string> keyBacking = new Lazy<string>(() => CreateKey());
        
        private string key
        {
            get { return this.keyBacking.Value; }
        }
        
        private static string CreateKey()
        {
            // Create a request for the URL.
            
            var path = Path.Combine(
                Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData),
                "base2art",
                "webapi.runner",
                "tests",
                "sso-user.json");
            
            var allContentString = File.ReadAllText(path);
            var allContent = Encoding.Default.GetBytes(allContentString);
            
            HttpWebRequest request = WebRequest.CreateHttp("https://sso.base2art.com/api/session?app=localhost");
            request.Method = "POST";
            request.ContentLength = allContent.Length;
            request.ContentType = "application/json";
            request.Accept = "application/json";
            
            using(var stream = request.GetRequestStream())
            {
                stream.Write(allContent, 0, allContent.Length);
                stream.Flush();
            }
            
            using (WebResponse response = request.GetResponse())
            {
                using (Stream dataStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(dataStream))
                    {
                        string responseFromServer = reader.ReadToEnd();
                        return (string)(Newtonsoft.Json.JsonConvert.DeserializeObject<dynamic>(responseFromServer).token);
                    }
                }
            }
        }
        
        [TestCase("api/aspect-test1", ".1.")]
        [TestCase("api/aspect-test2", ".2.")]
        public async void RequestPublic(string path, string pattern)
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                var directoryInfo = new DirectoryInfo("c:\\Temp\\Logs");
                
                Directory.CreateDirectory(directoryInfo.FullName);
                
                var files = directoryInfo.GetFiles("*.log");
                foreach (var file in files)
                {
                    file.Delete();
                }
                
                // SET DATA
                var setupResponse = await client.GetAsync(path);
                
                var content = (await setupResponse.Content.ReadAsStringAsync());
                
                var files1 = directoryInfo.GetFiles("*.log");
                
                Console.WriteLine(content);
                files1.Length.Should().Be(2);
                
                files1.OrderByDescending(x => x.Name).First().Name.Should().Contain(pattern);
            }
        }
        
        [Test]
        public async void RequestSecurity_Unauthenticated()
        {
            this.ClearCookies();
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                var setupResponse = await client.GetAsync("api/aspect-test/requireAuth");
                var content = await setupResponse.Content.ReadAsStringAsync();
                Console.WriteLine(content);
                setupResponse.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
            }
        }
        
        [Test]
        public async void RequestSecurity_Authenticated()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                this.Cookies.Add(this.PublicRunner.Uris.FirstOrDefault(), new Cookie("auth_jwt", key));
                var setupResponse = await client.GetAsync("api/aspect-test/requireAuth");
                
                var content = await setupResponse.Content.ReadAsStringAsync();
                Console.WriteLine(content);
                setupResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }
        
        [Test]
        public async void RequestSecurity_Authorization_Access()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                this.Cookies.Add(this.PublicRunner.Uris.FirstOrDefault(), new Cookie("auth_jwt", key));
                var setupResponse = await client.GetAsync("api/aspect-test/requireRole_User");
                
                var content = await setupResponse.Content.ReadAsStringAsync();
                Console.WriteLine(content);
                setupResponse.StatusCode.Should().Be(HttpStatusCode.OK);
            }
        }
        
        [Test]
        public async void RequestSecurity_Authorization_Deny()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                this.Cookies.Add(this.PublicRunner.Uris.FirstOrDefault(), new Cookie("auth_jwt", key));
                var setupResponse = await client.GetAsync("api/aspect-test/requireRole_admin");
                
                var content = await setupResponse.Content.ReadAsStringAsync();
                Console.WriteLine(content);
                setupResponse.StatusCode.Should().Be(HttpStatusCode.Forbidden);
            }
        }
        
        [Test]
        public async void RequestWithCache()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                var setupResponse = await client.GetAsync("api/aspect-test/cache");
                var content = await setupResponse.Content.ReadAsStringAsync();
                
                Console.WriteLine(content);
                
                await Task.Delay(TimeSpan.FromSeconds(2));
                var setupResponse1 = await client.GetAsync("api/aspect-test/cache");
                var content1 = await setupResponse1.Content.ReadAsStringAsync();
                
                await Task.Delay(TimeSpan.FromSeconds(2));
                var setupResponse2 = await client.GetAsync("api/aspect-test/cache?values=s,4");
                var content2 = await setupResponse2.Content.ReadAsStringAsync();
                
                await Task.Delay(TimeSpan.FromSeconds(2));
                var setupResponse3 = await client.GetAsync("api/aspect-test/cache?values=s,4");
                var content3 = await setupResponse3.Content.ReadAsStringAsync();
                
                content1.Should().Be(content);
                content2.Should().NotBe(content);
                content2.Should().Be(content3);
                
                setupResponse.StatusCode.Should().Be(HttpStatusCode.OK);
                setupResponse1.StatusCode.Should().Be(HttpStatusCode.OK);
                setupResponse2.StatusCode.Should().Be(HttpStatusCode.OK);
                setupResponse3.StatusCode.Should().Be(HttpStatusCode.OK);
                
                
//                content1.Should().NotBe(content);
            }
        }
    }
}







