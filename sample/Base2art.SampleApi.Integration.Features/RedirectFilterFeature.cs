﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using Base2art.SampleApi.Public.Models;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;

    [TestFixture]
    public class RedirectFilterFeature : ServerTest
    {
        [Test]
        public async void RequestPublic()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.PostAsJsonAsync("/api/utils/redirect", new RedirectData {
                                                                Location = "http://google.com",
                                                                Permanent = true
                                                            });
                Assert.AreEqual(301, (int)response.StatusCode);
                Assert.AreEqual(new Uri("http://google.com/"), response.Headers.Location);
                
            }
        }
    }
    
}
