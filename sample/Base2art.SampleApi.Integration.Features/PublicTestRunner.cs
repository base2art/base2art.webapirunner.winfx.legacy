﻿namespace Base2art.SampleApi
{
    using System;
    using System.Net;
    using Base2art.WebApiRunner.Console;
    using Base2art.WebApiRunner.Console.Config;

    public class PublicTestRunner<T> : TestRunner<T> where T : ConsoleServerConfiguration, new()
    {
        public PublicTestRunner(Runner<T> backing, CookieContainer cookies)
            : base(backing, cookies)
        {
        }
    }
}


