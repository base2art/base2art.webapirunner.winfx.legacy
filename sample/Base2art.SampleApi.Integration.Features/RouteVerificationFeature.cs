﻿namespace Base2art.SampleApi
{
    using System.Net.Http;
    using Base2art.SampleApi.Public;
    using Base2art.SampleApi.Public.Endpoints;
    using Base2art.SampleApi.Public.Models;
    using Base2art.SampleApi.Public.Tasks;
    using Base2art.WebApiRunner.Api.Routing;
    using NUnit.Framework;
    
    [TestFixture]
    public class RouteVerificationFeature
    {
        [Test]
        public void VerifyRoutes_Combined()
        {
            var verifier = new RouteVerifier(@"..\..\..\..\sample\Base2art.SampleApi\conf\configuration-local.yaml", RoutingType.Combined);
            
            verifier.VerifyRoute(TestHttpMethod.Post, "/abc/124").IsClass(null).IsMethod(null);
            
            verifier.VerifyRoute(TestHttpMethod.Post, "/admin/tasks/sample-basic").IsClass(typeof(SampleTask)).IsMethod("ExecuteAsync");
            verifier.VerifyRoute(TestHttpMethod.Delete, "/api/employees").IsClass(typeof(EmployeeService)).IsMethod("Clear");
            
        }
        
        [Test]
        public void VerifyRoutes_Admin()
        {
            var verifier = new RouteVerifier(@"..\..\..\..\sample\Base2art.SampleApi\conf\configuration-local.yaml", RoutingType.Admin);
            
            verifier.VerifyRoute(TestHttpMethod.Post, "/abc/124").IsClass(null).IsMethod(null);
            
            verifier.VerifyRoute(TestHttpMethod.Post, "/tasks/sample-basic").IsClass(typeof(SampleTask)).IsMethod("ExecuteAsync");
            verifier.VerifyRoute(TestHttpMethod.Delete, "/api/employees").IsClass(null).IsMethod(null);
        }
        
        [Test]
        public void VerifyRoutes_Public()
        {
            var verifier = new RouteVerifier(@"..\..\..\..\sample\Base2art.SampleApi\conf\configuration-local.yaml", RoutingType.Public);
            
            verifier.VerifyRoute(TestHttpMethod.Post, "/abc/124").IsClass(null).IsMethod(null);
            
            verifier.VerifyRoute(TestHttpMethod.Post, "/tasks/sample-basic").IsClass(null).IsMethod(null);
            verifier.VerifyRoute(TestHttpMethod.Post, "/admin/tasks/sample-basic").IsClass(null).IsMethod(null);
            verifier.VerifyRoute(TestHttpMethod.Delete, "/api/employees").IsClass(typeof(EmployeeService)).IsMethod("Clear");
        }
        
        [Test]
        public void VerifyRoutes_Public_TypeAcrossNamespace()
        {
            var verifier = new RouteVerifier(@"..\..\..\..\sample\Base2art.SampleApi\conf\configuration-local-debug-server.yaml", RoutingType.Public);
            
            
            verifier.VerifyRoute(TestHttpMethod.Get, "/api/echo/data")
                .IsClass(typeof(Base2art.SampleApi.Public.Endpoints.EchoService))
                .IsMethod("Get");
            
            verifier.VerifyRoute(TestHttpMethod.Get, "/api/v2/echo/data")
                .IsClass(typeof(Base2art.SampleApi.Public.V2.Endpoints.EchoService))
                .IsMethod("Get");
            
        }
    }
}
