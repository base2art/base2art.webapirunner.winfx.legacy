﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Collections;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Reflection;
    using Base2art.SampleApi.Public.Models;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;

    [TestFixture]
    public class CookieFilterFeature : ServerTest
    {
        [Test]
        public async void RequestPublic()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.PostAsJsonAsync("/api/utils/cookies", new CookieData {
                                                                Name = "abc",
                                                                Value = "def",
                                                                Expires = TimeSpan.FromSeconds(5)
                                                            });
                Assert.AreEqual(200, (int)response.StatusCode);
                var uri = this.PublicRunner.Uris.FirstOrDefault();
                var cookies = this.Cookies.GetCookies(uri);
                var first = cookies.OfType<Cookie>().FirstOrDefault();
                Assert.IsNotNull(first);
            }
        }
    }
    
}


