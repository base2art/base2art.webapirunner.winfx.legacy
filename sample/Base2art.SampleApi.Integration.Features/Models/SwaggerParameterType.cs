﻿namespace Base2art.SampleApi.Models
{
    public enum SwaggerParameterType
    {
        Object,
        String,
        Integer,
        Array
    }
}
