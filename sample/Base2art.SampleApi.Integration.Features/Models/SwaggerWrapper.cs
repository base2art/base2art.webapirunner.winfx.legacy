﻿namespace Base2art.SampleApi.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Newtonsoft.Json.Linq;
    
    public class SwaggerWrapper
    {
        private readonly dynamic swaggerDoc;

        public SwaggerWrapper(dynamic swaggerDoc, bool print)
        {
            this.swaggerDoc = swaggerDoc;
            if (print)
            {
                Console.WriteLine(swaggerDoc);
            }
        }
        
        public SwaggerWrapperPaths Paths
        {
            get { return new SwaggerWrapperPaths(this.swaggerDoc.paths); }
        }
    }
    
    public class SwaggerWrapperPaths
    {
        private readonly IDictionary<string, JToken> paths;

        public SwaggerWrapperPaths(IDictionary<string, JToken> paths)
        {
            this.paths = paths;
        }
        
        public SwaggerWrapperPath this[string path]
        {
            get { return new SwaggerWrapperPath(this.paths[path]); }
        }
    }
    
    public class SwaggerWrapperPath
    {
        private readonly JToken jToken;

        public SwaggerWrapperPath(JToken jToken)
        {
            this.jToken = jToken;
        }
        
        public SwaggerWrapperMethod Method(string methodName)
        {
            return new SwaggerWrapperMethod(this.jToken[methodName]);
        }

        public int MethodCount
        {
            get { return this.jToken.Count(); }
        }
    }
    
    public class SwaggerWrapperMethod
    {
        private readonly JToken jToken;

        public SwaggerWrapperMethod(JToken jToken)
        {
            this.jToken = jToken;
        }
        
        public SwaggerWrapperParameter[] Parameters
        {
            get
            {
                return this.jToken["parameters"]
                    .Select(x => new SwaggerWrapperParameter(x))
                    .ToArray();
            }
        }
    }
    
    public class SwaggerWrapperParameter
    {
        private readonly JToken jToken;

        public SwaggerWrapperParameter(JToken jToken)
        {
            this.jToken = jToken;
        }

        public string Name
        {
            get { return this.jToken["name"].ToString(); }
        }

        public SwaggerParameterIn In
        {
            get { return Get<SwaggerParameterIn>("in").Value; }
        }

        public SwaggerParameterType Type
        {
            get
            {
                var item= Get<SwaggerParameterType>("type");
                if (!item.HasValue)
                {
                    try
                    {
                        return this.jToken["schema"]["type"].ToString() == "array"
                            ? SwaggerParameterType.Array
                            : SwaggerParameterType.Object;
                        
                    }
                    catch
                    {
                        return SwaggerParameterType.Object;
                    }
                }
                
                return item.Value;
            }
        }

        public bool Required
        {
            get { return bool.Parse(this.jToken["required"].ToString()); }
        }
        
        private T? Get<T>(string key)
            where T : struct
        {
            var obj = this.jToken[key];
            if (obj == null)
            {
                return null;
            }
            
            T e;
            if (Enum.TryParse<T>(obj.ToString(), true, out e))
            {
                return e;
            }
            
            return null;
        }
    }
}
