﻿namespace Base2art.SampleApi
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Threading.Tasks;
    using Base2art.SampleApi.Bases;
    using Base2art.SampleApi.Public.Tasks;
    using Base2art.WebApiRunner;
    using NUnit.Framework;

    [TestFixture]
    public class ExceptionLoggerTimedFeature : ServerTest
    {
        protected override string ConfigFileName
        {
            get { return "configuration-task-exceptions"; }
        }
        
        [TestCase(0, typeof(NotImplementedException))]
        [TestCase(0, typeof(ConflictException))]
        [TestCase(0, typeof(UnprocessableEntityException))]
        [TestCase(0, typeof(NotAuthenticatedException))]
        [TestCase(0, typeof(NotAuthorizedException))]
        [TestCase(1, typeof(InvalidDataException))]
        [TestCase(1, typeof(ApplicationException))]
        public async void RequestTaskByInterval_WithExceptionHandlerSuppressed(int expectedMessageCount, Type type)
        {
            HttpClientFactory.DestroyAdminRunner(this.ConfigFileName);
            TimedExceptionThrowingTask.Type = type;
            
            var path = Path.Combine("C:\\Temp\\Errors\\", "Base2art", "sampleApi");
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
            
            Directory.CreateDirectory(path);
            
            Assert.IsTrue(Directory.GetFiles(path, "*.log").Length == 0);
            
            using (var client = this.AdminRunner.AdminHttp())
            {
                var response = await client.GetAsync("/swagger/docs/v1");
                Assert.AreEqual(200, (int)response.StatusCode, await response.Content.ReadAsStringAsync());
                
                await Task.Delay(TimeSpan.FromSeconds(1.0));
            }
            
            Assert.AreEqual(Directory.GetFiles(path, "*.log").Length, expectedMessageCount);
        }
    }
}


