﻿namespace Base2art.SampleApi
{
    using System;
    using System.IO;
    using System.Net.Http;
    using Base2art.SampleApi.Bases;
    using Base2art.WebApiRunner;
    using NUnit.Framework;
    
    [TestFixture]
    public class ExceptionLoggerFeature : ServerTest
    {
        //base2art.MaskException
//        [TestCase("tasks/throw-exception")]
        [TestCase("tasks/throw-exception?base2art.MaskException=true")]
        public async void RequestTask_WithExceptionHandler(string url)
        {
            var path = Path.Combine("C:\\Temp\\Errors\\", "Base2art", "sampleApi");
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
            
            Directory.CreateDirectory(path);
            
            Assert.IsTrue(Directory.GetFiles(path, "*.log").Length == 0);
            
            using (var client = this.AdminRunner.AdminHttp())
            {
                var response = await client.PostAsJsonAsync<object>(url, null);
                
                var result = (await response.Content.ReadAsStringAsync()).Trim();
                //                Assert.AreEqual(result, "\"abc=123\\r\\nDone\\r\\n\"");
            }
            
            Assert.IsTrue(Directory.GetFiles(path, "*.log").Length == 1);
        }
        
        [TestCase(0, typeof(NotImplementedException))]
        [TestCase(0, typeof(ConflictException))]
        [TestCase(0, typeof(UnprocessableEntityException))]
        [TestCase(0, typeof(NotAuthenticatedException))]
        [TestCase(0, typeof(NotAuthorizedException))]
        [TestCase(1, typeof(InvalidDataException))]
        [TestCase(1, typeof(ApplicationException))]
        public async void RequestTask_WithExceptionHandlerSuppressed(int expectedMessageCount, Type type)
        {
            var path = Path.Combine("C:\\Temp\\Errors\\", "Base2art", "sampleApi");
            if (Directory.Exists(path))
            {
                Directory.Delete(path, true);
            }
            
            Directory.CreateDirectory(path);
            
            Assert.IsTrue(Directory.GetFiles(path, "*.log").Length == 0);
            
            using (var client = this.AdminRunner.AdminHttp())
            {
                var message = new {type = type.AssemblyQualifiedName};
                var response = await client.PostAsJsonAsync<object>("tasks/throw-exception", message);
                
                var result = (await response.Content.ReadAsStringAsync()).Trim();
                //                Assert.AreEqual(result, "\"abc=123\\r\\nDone\\r\\n\"");
            }
            
            Assert.IsTrue(Directory.GetFiles(path, "*.log").Length == expectedMessageCount);
        }
    }
}
