﻿namespace Base2art.SampleApi
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Reflection;
//    using System.Threading;
//    using System.Threading.Tasks;
    using System.Web.Http;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Console;
    using Base2art.WebApiRunner.Console.Config;

    public static class HttpClientFactory
    {

        private static Dictionary<string, Tuple<Runner<ConsoleServerConfiguration>, IDisposable>> adminDict = new Dictionary<string, Tuple<Runner<ConsoleServerConfiguration>, IDisposable>>();
        
        private static Dictionary<string, Tuple<Runner<ConsoleServerConfiguration>, IDisposable>> publicDict = new Dictionary<string, Tuple<Runner<ConsoleServerConfiguration>, IDisposable>>();
        
        public static string ConfigFileToPath(string configFileName)
        {
            string localPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            localPath = Path.GetDirectoryName(localPath);
            
            return Path.Combine(
                Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
                "..",
                "..",
                "..",
                "Base2art.SampleApi",
                "conf",
                configFileName + ".yaml");
        }
        
        public static AdminTestRunner<ConsoleServerConfiguration> CreateAdminRunner(string configFileName, CookieContainer cookies)
        {
            return adminDict.CreateRunner(
                true,
                configFileName,
                cookies,
                x => new AdminTestRunner<ConsoleServerConfiguration>(x.Runner, x.Cookies));
        }
        
        public static async System.Threading.Tasks.Task DestroyAdminRunner(string configFileName)
        {
            if (adminDict.ContainsKey(configFileName))
            {
                var current = adminDict[configFileName];
                current.Item2.Dispose();
                adminDict.Remove(configFileName);
            }
        }
        
        public static PublicTestRunner<ConsoleServerConfiguration> CreatePublicRunner(string configFileName, CookieContainer cookies)
        {
            return publicDict.CreateRunner(
                false,
                configFileName,
                cookies,
                x => new PublicTestRunner<ConsoleServerConfiguration>(x.Runner, x.Cookies));
        }
        
        public static void DestroyPublicRunner(string configFileName)
        {
            if (publicDict.ContainsKey(configFileName))
            {
                var current = publicDict[configFileName];
                current.Item2.Dispose();
                publicDict.Remove(configFileName);
            }
        }
        
        public static HttpClient AdminHttp<T>(this AdminTestRunner<T> runner)
            where T : ConsoleServerConfiguration, new()
        {
            var client = CreateClient(runner.Cookies);
            
            var first = runner.Configuration.AdminServerSettings.Http.First();
            client.BaseAddress = new UriBuilder(Uri.UriSchemeHttp, first.Host, first.Port ?? 8081, first.RootPath).Uri;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            
            return client;
        }
        
        public static HttpClient PublicHttp<T>(this PublicTestRunner<T> runner)
            where T : ConsoleServerConfiguration, new()
        {
            var client = CreateClient(runner.Cookies);
            
            var first = runner.Configuration.ServerSettings.Http.First();
            client.BaseAddress = new UriBuilder(Uri.UriSchemeHttp, first.Host, first.Port ?? 8080, first.RootPath).Uri;
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            
            return client;
        }

        public static  HttpClient CreateClient(CookieContainer cookies)
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.CookieContainer = cookies;
            handler.AllowAutoRedirect = false;

            HttpClient client = new HttpClient(handler);
            return client;
        }

        private static Z CreateRunner<Z>(
            this Dictionary<string, Tuple<Runner<ConsoleServerConfiguration>, IDisposable>> lookup,
            bool isAdmin,
            string configFileName,
            CookieContainer cookies,
            Func<TestRunner<ConsoleServerConfiguration>, Z> creator)
            where Z : TestRunner<ConsoleServerConfiguration>
        {
            if (lookup.ContainsKey(configFileName))
            {
                var current = lookup[configFileName];
                return creator(Wrap(current.Item1, cookies));
            }
            
            string localPath = new Uri(Assembly.GetExecutingAssembly().CodeBase).LocalPath;
            localPath = Path.GetDirectoryName(localPath);
            
            var configPath = ConfigFileToPath(configFileName);
            
            var runner = new Runner<ConsoleServerConfiguration>("server", configPath, isAdmin);
            HttpConfiguration local = null;
            var server = runner.Execute(setup: httpConfig => local = httpConfig);
            
            var c = runner.Configuration;
            var tasks = c.JointTasks();
            
            var scheduler = TaskScheduler.Run(local, tasks);
            
            lookup[configFileName] = Tuple.Create(runner, (IDisposable)new AggregateDisposable(server, scheduler));
            
            return creator(Wrap(runner, cookies));
        }

        private static TestRunner<ConsoleServerConfiguration> Wrap(Runner<ConsoleServerConfiguration> runner, CookieContainer cookies)
        {
            return new TestRunner<ConsoleServerConfiguration>(runner, cookies);
        }
        
        private class AggregateDisposable : System.ComponentModel.Component
        {
            private readonly IDisposable[] aggregates;

            public AggregateDisposable(params IDisposable[] aggregates)
            {
                this.aggregates = aggregates ?? new IDisposable[0];
            }
            
            protected override void Dispose(bool disposing)
            {
                base.Dispose(disposing);
                if (disposing)
                {
                    foreach (var element in this.aggregates.Where(x=>x!=null))
                    {
                        using (element)
                        {
                        }
                    }
                }
            }
        }
    }
}


