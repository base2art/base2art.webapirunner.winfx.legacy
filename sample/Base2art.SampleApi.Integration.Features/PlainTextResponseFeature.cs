﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Base2art.SampleApi.Public.Models;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;

    [TestFixture]
    public class PlainTextResponseFeature : ServerTest
    {
        [Test]
        public async void RequestPublic()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                var setupResponse = await client.PutAsJsonAsync("/api/employees", new Employee {
                                                                    EmployeeId = 4,
                                                                    Name = "SjY"
                                                                });
                Console.WriteLine(await setupResponse.Content.ReadAsStringAsync());
                Assert.AreEqual(HttpStatusCode.OK, setupResponse.StatusCode);
                
                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, "/api/employees");
                request.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var response = await client.SendAsync(request);
                Assert.IsTrue((await response.Content.ReadAsStringAsync()).Contains("\"employeeId\": 4,"));
                
                HttpRequestMessage csvRequest = new HttpRequestMessage(HttpMethod.Get, "/api/employees");
                csvRequest.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("text/plain"));
                var csvResponse = await client.SendAsync(csvRequest);
                var responseContent = (await csvResponse.Content.ReadAsStringAsync());
                Console.WriteLine(responseContent);
                Assert.IsTrue(responseContent.Contains("- EmployeeId: 4"));
                Assert.IsTrue(responseContent.Contains("  Name: SjY"));
                
                HttpRequestMessage csvRrequest1 = new HttpRequestMessage(HttpMethod.Get, "/api/employees?headers.accept=text/plain");
                var csvResponse1 = await client.SendAsync(csvRrequest1);
                var responseContent1 = (await csvResponse1.Content.ReadAsStringAsync());
                Console.WriteLine(responseContent1);
                Assert.IsTrue(responseContent1.Contains("- EmployeeId: 4"));
                Assert.IsTrue(responseContent1.Contains("  Name: SjY"));
            }
        }
    }
}



