﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using Base2art.SampleApi.Models;
    using FluentAssertions;
    using Newtonsoft.Json.Linq;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;

    [TestFixture]
    public class SwaggerFeature : ServerTest
    {
        [Test]
        public async void RequestMainPage()
        {
            using (var client = this.AdminRunner.AdminHttp())
            {
                List<bool> objs = new List<bool>();
                for (int i = 0; i < 5; i++)
                {
                    var response = await client.GetAsync("/swagger/ui/index");
                    Assert.AreEqual(200, (int)response.StatusCode, await response.Content.ReadAsStringAsync());
                    Assert.IsTrue((await response.Content.ReadAsStringAsync()).Contains("<body class=\"swagger-section\">"));
                    Assert.IsTrue((await response.Content.ReadAsStringAsync()).Contains("<a id=\"logo\" href=\"http://swagger.io\">swagger</a>"));
                }
            }
        }
        
        [Test]
        public async void RequestJson()
        {
            using (var client = this.AdminRunner.AdminHttp())
            {
                List<bool> objs = new List<bool>();
                for (int i = 0; i < 5; i++)
                {
                    var response = await client.GetAsync("/swagger/docs/v1");
                    Assert.AreEqual(200, (int)response.StatusCode, await response.Content.ReadAsStringAsync());
                    var result = await response.Content.ReadAsAsync<dynamic>();
                    var paths = (IDictionary<string, JToken>)result.paths;
                    Assert.IsTrue(paths.ContainsKey("/healthcheck"));
                    Assert.IsTrue(paths.ContainsKey("/tasks/clear-caches"));
                }
            }
        }
        
        [Test]
        public async void RequestSwagger()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                List<bool> objs = new List<bool>();
                for (int i = 0; i < 5; i++)
                {
                    var response = await client.GetAsync("/swagger/docs/v1");
                    
                    Assert.AreEqual(200, (int)response.StatusCode, await response.Content.ReadAsStringAsync());
                    var swagger = new SwaggerWrapper(await response.Content.ReadAsAsync<dynamic>(), true);
                    
                    var rooms = swagger.Paths["/api/rooms"];
                    rooms.MethodCount.Should().Be(4);
                    
                    rooms.Method("get").Parameters.Length.Should().Be(0);
                    
                    var @post = rooms.Method("post").Parameters;
                    @post.Length.Should().Be(3);
                    
                    @post[0].Name.Should().Be("key");
                    @post[0].In.Should().Be(SwaggerParameterIn.Query);
                    @post[0].Required.Should().Be(true);
                    @post[0].Type.Should().Be(SwaggerParameterType.String);
                    
                    @post[1].Name.Should().Be("count");
                    @post[1].In.Should().Be(SwaggerParameterIn.Query);
                    @post[1].Required.Should().Be(true);
                    @post[1].Type.Should().Be(SwaggerParameterType.Integer);
                    
                    @post[2].Name.Should().Be("room");
                    @post[2].In.Should().Be(SwaggerParameterIn.Body);
                    @post[2].Required.Should().Be(true);
                    @post[2].Type.Should().Be(SwaggerParameterType.Object);
                    
                    var @put = rooms.Method("put").Parameters;
                    @put.Length.Should().Be(1);
                    
                    @put[0].Name.Should().Be("rooms");
                    @put[0].In.Should().Be(SwaggerParameterIn.Body);
                    @put[0].Required.Should().Be(true);
                    @put[0].Type.Should().Be(SwaggerParameterType.Array);
                    
                    
                    var @delete = rooms.Method("delete").Parameters;
                    @delete.Length.Should().Be(0);
                    
                    //------------------------------
                    
                    var roomsParm = swagger.Paths["/api/rooms/{key}"];
                    roomsParm.MethodCount.Should().Be(3);
                    var @getWithKey = roomsParm.Method("get");
                    @getWithKey.Parameters.Length.Should().Be(1);
                    @getWithKey.Parameters[0].Name.Should().Be("key");
                    @getWithKey.Parameters[0].In.Should().Be(SwaggerParameterIn.Path);
                    @getWithKey.Parameters[0].Required.Should().Be(true);
                    @getWithKey.Parameters[0].Type.Should().Be(SwaggerParameterType.String);
                    
                    
                    var @putWithKey = roomsParm.Method("put");
                    @putWithKey.Parameters.Length.Should().Be(2);
                    
                    @putWithKey.Parameters[0].Name.Should().Be("key");
                    @putWithKey.Parameters[0].In.Should().Be(SwaggerParameterIn.Path);
                    @putWithKey.Parameters[0].Required.Should().Be(true);
                    @putWithKey.Parameters[0].Type.Should().Be(SwaggerParameterType.String);
                    
                    @putWithKey.Parameters[1].Name.Should().Be("room");
                    @putWithKey.Parameters[1].In.Should().Be(SwaggerParameterIn.Body);
                    @putWithKey.Parameters[1].Required.Should().Be(true);
                    @putWithKey.Parameters[1].Type.Should().Be(SwaggerParameterType.Object);
                    
                    
                    var @deleteWithKey = roomsParm.Method("delete").Parameters;
                    @deleteWithKey[0].Name.Should().Be("key");
                    @deleteWithKey[0].In.Should().Be(SwaggerParameterIn.Path);
                    @deleteWithKey[0].Required.Should().Be(true);
                    @deleteWithKey[0].Type.Should().Be(SwaggerParameterType.String);
                    
                    //-------------------------------
                    
                    var roomsCountParm = swagger.Paths["/api/rooms/{key}/{count}"];
                    roomsCountParm.MethodCount.Should().Be(1);
                    var method = roomsCountParm.Method("post");
                    method.Parameters.Length.Should().Be(3);
                    
                    method.Parameters[0].Name.Should().Be("key");
                    method.Parameters[0].In.Should().Be(SwaggerParameterIn.Path);
                    method.Parameters[0].Required.Should().Be(true);
                    method.Parameters[0].Type.Should().Be(SwaggerParameterType.String);
                    
                    method.Parameters[1].Name.Should().Be("count");
                    method.Parameters[1].In.Should().Be(SwaggerParameterIn.Path);
                    method.Parameters[1].Required.Should().Be(true);
                    method.Parameters[1].Type.Should().Be(SwaggerParameterType.Integer);
                    
                    method.Parameters[2].Name.Should().Be("room");
                    method.Parameters[2].In.Should().Be(SwaggerParameterIn.Body);
                    method.Parameters[2].Required.Should().Be(true);
                    method.Parameters[2].Type.Should().Be(SwaggerParameterType.Object);
                    
                    var echo = swagger.Paths["/api/echo/{value}"];
                    echo.Should().NotBeNull();
                    echo.MethodCount.Should().Be(1);
                    var echoWith = swagger.Paths["/api/echo-with-fixes/{value}"];
                    echoWith.Should().NotBeNull();
                    echoWith.MethodCount.Should().Be(1);
                    
                    
                    
                    var session1 = swagger.Paths["api/session"];
                    echo.Should().NotBeNull();
                    echo.MethodCount.Should().Be(1);
                    var session2 = swagger.Paths["api/system/remoting/session"];
                    echoWith.Should().NotBeNull();
                    echoWith.MethodCount.Should().Be(1);
                    
                }
            }
        }
    }
}






