﻿namespace Base2art.SampleApi
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.Console;
    using Base2art.WebApiRunner.Console.Config;

    public class TestRunner<T>
        where T : ConsoleServerConfiguration, new()
    {
        private readonly Runner<T> backing;

        private readonly CookieContainer cookies;
        
        public TestRunner(Runner<T> backing, CookieContainer cookies)
        {
            this.cookies = cookies;
            this.backing = backing;
        }

        public Uri[] Uris
        {
            get{ return this.Runner.Uris; }
        }
        public Runner<T> Runner
        {
            get { return backing; }
        }
        
        public T Configuration
        {
            get { return this.backing.Configuration; }
        }
        

        public CookieContainer Cookies
        {
            get
            {
                return cookies;
            }
        }
    }
}
