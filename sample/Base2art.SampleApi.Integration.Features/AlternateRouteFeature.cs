﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Web.Http.ExceptionHandling;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;

    [TestFixture]
    public class AlternateRouteFeature : ServerTest
    {
        [TestCase("/api/echo/test", "test")]
        [TestCase("/api/echo-with-fixes/test", "PrefixtestSuffix")]
        public async void RequestAsset(string path, string expectedContains)
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                string response = null;
                try
                {
                    response = await client.GetStringAsync(path);
                }
                catch(Exception ex)
                {
                    
                }
                
                if (response == null)
                {
                    var responseMessage = await client.GetAsync(path);
                    Console.WriteLine(await responseMessage.Content.ReadAsStringAsync());
                    
                }
                
                Assert.AreEqual("\"" + expectedContains + "\"", response);
                
            }
        }
    }
}


