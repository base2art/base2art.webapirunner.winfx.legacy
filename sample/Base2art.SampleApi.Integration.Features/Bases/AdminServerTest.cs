﻿namespace Base2art.SampleApi.Bases
{
    using System;
    using System.Net;
    using Base2art.WebApiRunner.Console.Config;

    public class ServerTest
    {
        private CookieContainer cookies = new CookieContainer();

        protected CookieContainer Cookies
        {
            get { return this.cookies; }
        }
        
        public void ClearCookies()
        {
            this.cookies = new CookieContainer();
        }
        
        protected virtual string ConfigFileName
        {
            get { return "configuration-local-debug-server"; }
        }

        public AdminTestRunner<ConsoleServerConfiguration> AdminRunner
        {
            get
            {
                return Base2art.SampleApi.HttpClientFactory.CreateAdminRunner(this.ConfigFileName, this.Cookies);
            }
        }

        public PublicTestRunner<ConsoleServerConfiguration> PublicRunner
        {
            get
            {
                return Base2art.SampleApi.HttpClientFactory.CreatePublicRunner(this.ConfigFileName, this.Cookies);
            }
        }
    }
}
