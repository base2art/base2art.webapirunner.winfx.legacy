﻿
namespace Base2art.SampleApi.Integration.Features
{
	using System;
	using System.Collections;
    using System.Collections.Generic;
	using System.Linq;
	using System.Net;
	using System.Net.Http;
	using Base2art.SampleApi.Public.Models;
	using Base2art.SampleApi.Bases;
	using NUnit.Framework;

	[TestFixture]
	public class HeaderSetFilterFeature : ServerTest
	{
		[Test]
		public async void RequestPublic()
		{
			using (var client = this.PublicRunner.PublicHttp())
			{
			    var response = await client.PostAsJsonAsync("/api/utils/headers", new KeyValuePair<string, string>[] {
			                                                    new KeyValuePair<string, string>("Location", "http://google.com"),
			                                                    new KeyValuePair<string, string>("x-Origin-Host", "http://sjy.me"),
					
				});
			    
				Assert.AreEqual(new Uri("http://sjy.me/"), response.Headers.GetValues("x-Origin-Host").First());
				Assert.AreEqual(new Uri("http://google.com/"), response.Headers.Location);
			}
		}
	}
}


