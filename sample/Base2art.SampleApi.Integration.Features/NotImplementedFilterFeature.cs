﻿namespace Base2art.SampleApi
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;

    [TestFixture]
    public class NotImplementedFilterFeature : ServerTest
    {
        [Test]
        public async void RequestTask()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.GetAsync("api/persons/filtered?searchCriteria.throwException=true");
                var result = response.StatusCode;
                Assert.AreEqual(501, (int)result, await response.Content.ReadAsStringAsync());
            }
        }
    }
}


