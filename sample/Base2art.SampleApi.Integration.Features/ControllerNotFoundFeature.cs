﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using Base2art.SampleApi.Bases;
    using NUnit.Framework;
    
    [TestFixture]
    public class ControllerNotFoundFeature : ServerTest
    {
        [Test]
        public async void RequestPublic()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                var response = await client.GetAsync("/path-not-found");
                Assert.AreEqual(404, (int)response.StatusCode);
            }
        }
        
        [Test]
        public async void RequestAdmin()
        {
            using (var client = this.AdminRunner.AdminHttp())
            {
                var response = await client.GetAsync("/path-not-found");
                Assert.AreEqual(404, (int)response.StatusCode);
            }
        }
    }
    
    
}


