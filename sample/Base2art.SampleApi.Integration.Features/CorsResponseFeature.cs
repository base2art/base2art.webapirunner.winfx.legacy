﻿
namespace Base2art.SampleApi.Integration.Features
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using Base2art.SampleApi.Public.Models;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Formatters;
    using Base2art.SampleApi.Bases;
    using Base2art.WebApiRunner.Services.SystemServices;
    using FluentAssertions;
    using NUnit.Framework;

    [TestFixture]
    public class CorsResponseFeature : ServerTest
    {
        [Test]
        public async void RequestPublic()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                
                var message = (new HttpRequestMessage(HttpMethod.Options, "/api/employees"));
                message.Headers.Add("Origin", "https://boofoo.net");
                message.Headers.Accept.Add(new MediaTypeWithQualityHeaderValue("*/*"));
                message.Headers.Add("Access-Control-Request-Method", HttpMethod.Put.Method);
//                message.Headers.Add("Access-Control-Request-Headers", "content-type, accept");
                var setupResponse = await client.SendAsync(message);
                Assert.AreEqual(HttpStatusCode.OK, setupResponse.StatusCode, await setupResponse.Content.ReadAsStringAsync());
                
                var dict = setupResponse.Headers.ToDictionary(x => x.Key, x => string.Join("", x.Value));
                dict.Should().ContainKey("Access-Control-Allow-Origin");
                dict.Should().ContainKey("Access-Control-Allow-Methods");
                dict.Should().ContainKey("Access-Control-Allow-Credentials");
                dict.Should().NotContainKey("Access-Control-Allow-Headers");
                
                dict["Access-Control-Allow-Origin"].Should().Be("https://boofoo.net");
                dict["Access-Control-Allow-Methods"].Should().Be("PUT");
                dict["Access-Control-Allow-Credentials"].Should().Be("true");
//                dict["Access-Control-Allow-Headers"].Should().Be("content-type, accept");
            }
        }
        
        [Test]
        public async void RequestPublic_Headers()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                
                var message = (new HttpRequestMessage(HttpMethod.Options, "/api/employees"));
                message.Headers.Add("Origin", "https://specified-origin.com");
                message.Headers.Add("Access-Control-Request-Method", HttpMethod.Put.Method);
                message.Headers.Add("Access-Control-Request-Headers", "content-type, accept");
                
                var setupResponse = await client.SendAsync(message);
                Assert.AreEqual(HttpStatusCode.OK, setupResponse.StatusCode, await setupResponse.Content.ReadAsStringAsync());
                
                var dict = setupResponse.Headers.ToDictionary(x => x.Key, x => string.Join("", x.Value));
                dict.Should().ContainKey("Access-Control-Allow-Origin");
                dict.Should().ContainKey("Access-Control-Allow-Methods");
//                dict.Should().ContainKey("Access-Control-Allow-Credentials");
                dict.Should().ContainKey("Access-Control-Allow-Headers");
                
                dict["Access-Control-Allow-Origin"].Should().Be("https://specified-origin.com");
                dict["Access-Control-Allow-Methods"].Should().Be("PUT");
//                dict["Access-Control-Allow-Credentials"].Should().Be("true");
                dict["Access-Control-Allow-Headers"].Should().Be("content-type");
            }
        }
        
        [Test]
        public async void RequestPublicWithDefaults()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                
                var message = (new HttpRequestMessage(HttpMethod.Options, "/api/employees"));
                message.Headers.Add("Origin", "http://base2art.com");
                message.Headers.Add("Access-Control-Request-Method", HttpMethod.Put.Method);
                var setupResponse = await client.SendAsync(message);
                Assert.AreEqual(HttpStatusCode.OK, setupResponse.StatusCode, await setupResponse.Content.ReadAsStringAsync());
                
                var dict = setupResponse.Headers.ToDictionary(x => x.Key, x => string.Join("", x.Value));
                dict.Should().ContainKey("Access-Control-Allow-Origin");
                dict.Should().ContainKey("Access-Control-Allow-Methods");
                
                dict["Access-Control-Allow-Origin"].Should().Be("*");
//                dict["Access-Control-Allow-Credentials"].Should().Be("false");
                dict["Access-Control-Allow-Methods"].Should().Be("PUT");
            }
        }
        
        [Test]
        public async void RequestPublicNoOrigin()
        {
            using (var client = this.PublicRunner.PublicHttp())
            {
                // SET DATA
                
                var message = (new HttpRequestMessage(HttpMethod.Options, "/api/employees"));
                // NOT REGISTERED
                message.Headers.Add("Origin", "https://boofoo.com");
                var setupResponse = await client.SendAsync(message);
                Assert.AreEqual(HttpStatusCode.OK, setupResponse.StatusCode, await setupResponse.Content.ReadAsStringAsync());
                
                var dict = setupResponse.Headers.ToDictionary(x => x.Key, x => string.Join("", x.Value));
                dict.Should().NotContainKey("Access-Control-Allow-Origin");
                dict.Should().NotContainKey("Access-Control-Allow-Credentials");
                dict.Should().NotContainKey("Access-Control-Allow-Methods");
                
            }
        }
    }
}



