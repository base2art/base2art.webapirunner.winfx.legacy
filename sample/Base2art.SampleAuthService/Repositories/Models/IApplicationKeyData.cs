﻿
using System;

namespace Base2art.SampleAuthService.Repositories.Models
{
    public interface IApplicationKeyData
    {
        string Id { get; }
        
        byte[] D { get; }
        byte[] DP { get; }
        byte[] DQ { get; }
        byte[] Exponent { get; }
        byte[] InverseQ { get; }
        byte[] Modulus { get; }
        byte[] P { get; }
        byte[] Q { get; }
    }
}
