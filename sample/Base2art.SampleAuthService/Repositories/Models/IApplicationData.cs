﻿namespace Base2art.SampleAuthService.Repositories.Models
{
    public interface IApplicationData
    {
        string Host
        {
            get;
        }

        string Name
        {
            get;
        }
        
        string[] Keys
        {
            get;
        }
    }
}
