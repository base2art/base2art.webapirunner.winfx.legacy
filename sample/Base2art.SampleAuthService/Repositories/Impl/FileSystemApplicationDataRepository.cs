﻿
namespace Base2art.SampleAuthService.Repositories.Impl
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.SystemServices;
    using Base2art.SampleAuthService.Repositories;
    using Base2art.SampleAuthService.Repositories.Models;

    public class FileSystemApplicationDataRepository : IApplicationDataRepository
    {
        private readonly string fileSystemLocation;

        private readonly IApplicationKeyDataRepository keys;

        private readonly IJsonSerializer serializer;
        
        public FileSystemApplicationDataRepository(
            string fileSystemLocation,
            IApplicationKeyDataRepository keys,
            IJsonSerializer serializer)
        {
            this.serializer = serializer;
            this.keys = keys;
            this.fileSystemLocation = fileSystemLocation;
        }

        public async Task<IApplicationData[]> GetApplications()
        {
            var item = await this.GetInternal();
            if (item.Count == 0)
            {
                await this.keys.AddKeyById("localhost");
                await this.AddApplication("localhost", "localhost", new []{ "localhost" });
            }
            
            return (await this.GetInternal()).ToArray();
        }

        public async Task UpdateApplication(string name, string applicationHost, string[] keys)
        {
            var apps = await this.GetInternal();
            apps.RemoveAll(x => string.Equals(x.Name, name, System.StringComparison.OrdinalIgnoreCase));
            await this.AddApplication(name, applicationHost, keys);
        }

        public Task AddApplication(string name, string applicationHost, string[] keys)
        {
            return Task.Run(async () =>
                            {
                                var apps = await this.GetInternal();
                                apps.Add(new JsonApplicationData
                                         {
                                             Name = name,
                                             Host = applicationHost,
                                             Keys = keys
                                         });
                                
                                var content = this.serializer.SerializeObjectFormatted(apps);
                                File.WriteAllText(this.fileSystemLocation, content);
                            });
        }

        private Task<List<JsonApplicationData>> GetInternal()
        {
            if (!File.Exists(this.fileSystemLocation))
            {
                File.WriteAllText(this.fileSystemLocation, "[]");
            }
            
            var content = File.ReadAllText(this.fileSystemLocation);
            
            return this.serializer.DeserializeObjectAsync<List<JsonApplicationData>>(content);
        }
        
        private class JsonApplicationData : IApplicationData
        {
            public string Host{ get; set; }
            public string Name{ get; set; }
            public string[] Keys{ get; set; }
        }
    }
}


