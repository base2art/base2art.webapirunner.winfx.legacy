﻿
namespace Base2art.SampleAuthService.Repositories.Impl
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

    public class NullClaimLookup : IClaimLookup
    {
        public Task<List<Claim>> FindAdditionalClaims(string name)
        {
            return Task.FromResult(new List<Claim>());
        }
    }
}


