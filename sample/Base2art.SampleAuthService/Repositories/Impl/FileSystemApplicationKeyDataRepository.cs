﻿
namespace Base2art.SampleAuthService.Repositories.Impl
{
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.SystemServices;
    using Base2art.SampleAuthService.Repositories.Models;
    
    public class FileSystemApplicationKeyDataRepository : IApplicationKeyDataRepository
    {
        private readonly string fileSystemLocation;

        private readonly IJsonSerializer serializer;
        
        public FileSystemApplicationKeyDataRepository(string fileSystemLocation, IJsonSerializer serializer)
        {
            this.serializer = serializer;
            this.fileSystemLocation = fileSystemLocation;
        }
        
        public async Task<IApplicationKeyData[]> GetKeys()
        {
            var keys = await this.GetKeysInternal();
            return keys.Select<JsonApplicationKeyData, IApplicationKeyData>(x => x).ToArray();
        }
        
        public async Task AddKey(string id, ApplicationKeyData appKeyData)
        {
            var keys = await this.GetKeysInternal();
            keys.Add(new JsonApplicationKeyData
                     {
                         Id = id,
                         D = appKeyData.D,
                         DP = appKeyData.DP,
                         DQ = appKeyData.DQ,
                         Exponent = appKeyData.Exponent,
                         InverseQ = appKeyData.InverseQ,
                         Modulus = appKeyData.Modulus,
                         P = appKeyData.P,
                         Q = appKeyData.Q,
                     });
            
            var formatted = await this.serializer.SerializeObjectFormattedAsync(keys);
            File.WriteAllText(this.fileSystemLocation, formatted);
        }

        private Task<List<JsonApplicationKeyData>> GetKeysInternal()
        {
            Directory.CreateDirectory(Path.GetDirectoryName(this.fileSystemLocation));
            if (!File.Exists(this.fileSystemLocation))
            {
                File.WriteAllText(this.fileSystemLocation, "[]");
            }
            
            var content = File.ReadAllText(this.fileSystemLocation);
            return this.serializer.DeserializeObjectAsync<List<JsonApplicationKeyData>>(content);
        }
        
        private class JsonApplicationKeyData : IApplicationKeyData
        {
            public string Id { get; set; }
            public byte[] D { get; set; }
            public byte[] DP { get; set; }
            public byte[] DQ { get; set; }
            public byte[] Exponent { get; set; }
            public byte[] InverseQ { get; set; }
            public byte[] Modulus { get; set; }
            public byte[] P { get; set; }
            public byte[] Q { get; set; }
        }
    }
}
