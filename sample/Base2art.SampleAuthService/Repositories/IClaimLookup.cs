﻿
namespace Base2art.SampleAuthService.Repositories
{
	using System;
    using System.Collections.Generic;
	using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;

	public interface IClaimLookup
	{
        Task<List<Claim>> FindAdditionalClaims(string name);
	}
}


