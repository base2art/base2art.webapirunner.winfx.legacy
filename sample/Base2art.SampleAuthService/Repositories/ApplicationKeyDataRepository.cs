﻿
namespace Base2art.SampleAuthService.Repositories
{
    using System.Linq;
    using System.Security.Cryptography;
    using System.Threading.Tasks;
    using Base2art.SampleAuthService.Repositories.Models;
    using Base2art.Tasks;
    using Base2art.WebApiRunner;
    
    public static class ApplicationKeyDataRepository
    {
        public static Task<IApplicationKeyData> GetKeyByName(this IApplicationKeyDataRepository repo, string keyId)
        {
            return repo.GetKeys()
                .Then()
                .Where(x => string.Equals(x.Id, keyId))
                .Then()
                .FirstOrDefault();
        }
        
        public static Task AddKeyById(this IApplicationKeyDataRepository repo, string keyId)
        {
            RSACryptoServiceProvider myRSA = new RSACryptoServiceProvider(2048);
            
            var key = myRSA.ExportParameters(true);
            
            return repo.AddKey(keyId, key.Convert());
        }

        public static ApplicationKeyData Convert(this RSAParameters parms)
        {
            return new ApplicationKeyData
            {
                D = parms.D,
                DP = parms.DP,
                DQ = parms.DQ,
                Exponent = parms.Exponent,
                InverseQ = parms.InverseQ,
                Modulus = parms.Modulus,
                P = parms.P,
                Q = parms.Q,
            };
        }
    }
}






