﻿namespace Base2art.SampleAuthService.Repositories
{
    using System.Threading.Tasks;
    using Base2art.SampleAuthService.Repositories.Models;
    
    public interface IApplicationKeyDataRepository
    {
        Task<IApplicationKeyData[]> GetKeys();

        Task AddKey(string id, ApplicationKeyData rSAParameters);
    }
}
