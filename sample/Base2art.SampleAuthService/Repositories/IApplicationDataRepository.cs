﻿
using System.Threading.Tasks;
using Base2art.SampleAuthService.Repositories.Models;
namespace Base2art.SampleAuthService.Repositories
{
    public interface IApplicationDataRepository
    {
        Task<IApplicationData[]> GetApplications();

        Task UpdateApplication(string name, string applicationHost, string[] keys);

        Task AddApplication(string name, string applicationHost, string[] keys);
    }
    
}


