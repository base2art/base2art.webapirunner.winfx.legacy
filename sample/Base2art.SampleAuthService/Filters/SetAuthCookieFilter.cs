﻿
namespace Base2art.SampleAuthService.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.Filters;
    using Base2art.SampleAuthService.Public.Models;

    public class SetAuthCookieFilter : ISetCookieFilter<SignInInfo>
    {
        private readonly string cookieDomain;

        private readonly bool requireSecureCookie;

        private readonly TimeSpan cookieLength;
        
        public SetAuthCookieFilter(string cookieDomain, bool requireSecureCookie, TimeSpan cookieLength)
        {
            this.cookieLength = cookieLength;
            this.requireSecureCookie = requireSecureCookie;
            this.cookieDomain = cookieDomain;
        }
        
        public Task SetCookiesAsync(SignInInfo content, IList<System.Net.Cookie> cookies)
        {
            if (content == null)
            {
                return Task.FromResult(false);
            }
            
            if (string.IsNullOrWhiteSpace(content.Token) || string.IsNullOrWhiteSpace(content.RawToken))
            {
                return Task.FromResult(false);
            }
            
            DateTime expires = DateTime.UtcNow.Add(this.cookieLength);
            if (string.IsNullOrWhiteSpace(cookieDomain))
            {
                cookies.Add(new System.Net.Cookie("auth_jwt", content.Token, "/"){ Secure = requireSecureCookie, Expires = expires });
                cookies.Add(new System.Net.Cookie("auth_info", content.RawToken, "/"){ Secure = false, Expires = expires });
            }
            else
            {
                cookies.Add(new System.Net.Cookie("auth_jwt", content.Token, "/", this.cookieDomain){ Secure = requireSecureCookie, Expires = expires });
                cookies.Add(new System.Net.Cookie("auth_info", content.RawToken, "/", this.cookieDomain){ Secure = false, Expires = expires });
            }
            
            return Task.FromResult(true);
        }
    }
    
    
    
}
