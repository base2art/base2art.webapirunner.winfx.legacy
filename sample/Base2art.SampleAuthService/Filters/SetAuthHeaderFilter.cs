﻿
namespace Base2art.SampleAuthService.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.Filters;
    using Base2art.SampleAuthService.Public.Models;

    public class SetAuthHeaderFilter : ISetHeadersFilter<SignInInfo>, ISetStatusCodeFilter<SignInInfo>
    {
        private readonly string cookieDomain;

        private readonly bool requireSecureCookie;

        public SetAuthHeaderFilter(string cookieDomain, bool requireSecureCookie)
        {
            this.requireSecureCookie = requireSecureCookie;
            this.cookieDomain = cookieDomain;
        }

        public Task SetHeadersAsync(SignInInfo content, ICollection<KeyValuePair<string, string>> headers)
        {
            if (content == null || content.Status != Base2art.Security.Authentication.SignInStatus.UnknownUser)
            {
                return Task.FromResult(true);
            }
            
            headers.Add(new KeyValuePair<string, string>("WWW-Authenticate", "Negotiate"));
            headers.Add(new KeyValuePair<string, string>("WWW-Authenticate", "NTLM"));
            return Task.FromResult(true);
        }
        
        public Task SetStatusCodeAsync(SignInInfo content, Action<System.Net.HttpStatusCode> setRedirect)
        {
            if (content == null || content.Status != Base2art.Security.Authentication.SignInStatus.UnknownUser)
            {
                return Task.FromResult(true);
            }
            
            setRedirect(System.Net.HttpStatusCode.Unauthorized);
            return Task.FromResult(true);
        }
    }
}


