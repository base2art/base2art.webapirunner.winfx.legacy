﻿
namespace Base2art.SampleAuthService.Public.Endpoints
{
    using System;
    using Base2art.Security.Authentication;
    using Base2art.SampleAuthService.Public.Models;
    
    public class UserService
    {
        private readonly IAuthenticationProcessor users;

        public UserService(IAuthenticationProcessor users)
        {
            this.users = users;
        }
        
        public User CreateUser(User user)
        {
            var userData = new UserData();
            userData.Name = user.Name;
            userData.Email = user.Email;
            userData.Password = user.Password;
            var registrationData = this.users.Register(userData);
            if (registrationData == RegisterStatus.Success)
            {
                user.Password = "*************";
                return user;
            }
            
            throw new ArgumentException(registrationData.ToString("G"), "user");
        }
    }
}
