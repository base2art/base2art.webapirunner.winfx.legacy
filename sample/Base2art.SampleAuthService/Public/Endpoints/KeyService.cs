﻿

namespace Base2art.SampleAuthService.Public.Endpoints
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.Tasks;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Security;
    using Base2art.SampleAuthService.Public.Models;
    using Base2art.SampleAuthService.Repositories;
    using Base2art.SampleAuthService.Repositories.Models;

    public class KeyService
    {
        private readonly IApplicationKeyDataRepository repo;
        
        public KeyService(IApplicationKeyDataRepository repo)
        {
            this.repo = repo;
        }
        
        public Task<PublicApplicationKey> Get(string id)
        {
            return this.repo.GetKeys()
                .Then()
                .Where(x => string.Equals(x.Id, id))
                .Then()
                .Select(this.Map)
                .Then()
                .FirstOrDefault();
        }
        
        public Task<PublicApplicationKey> CreateKey(string id, ClaimsPrincipal principal)
        {
            if (!principal.IsInRole("SSO Admin"))
            {
                throw new AccessViolationException();
            }
            
            return this.repo.AddKeyById(id).Then()
                .Return(() =>
                        {
                            return this.repo.GetKeys()
                                .Then()
                                .Where(x => string.Equals(x.Id, id))
                                .Then()
                                .Select(this.Map)
                                .Then()
                                .FirstOrDefault();
                        });
        }
        
        private PublicApplicationKey Map(IApplicationKeyData data)
        {
            return new PublicApplicationKey
            {
                Modulus = data.Modulus,
                Exponent = data.Exponent,
            };
        }
    }
}
