﻿
namespace Base2art.SampleAuthService.Public.Endpoints
{
    using System;
    using System.Collections.Generic;
    using System.IdentityModel.Tokens;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using System.Threading.Tasks;
    using Base2art.Security.Authentication;
    using Base2art.Security.Membership;
    using Base2art.Tasks;
    using Base2art.WebApiRunner.Security;
    using Base2art.SampleAuthService.Public.Models;
    using Base2art.SampleAuthService.Repositories;
    
    public class AuthService
    {
        private readonly IAuthenticationProcessor authentication;

        private readonly IApplicationDataRepository repo;

        private readonly IApplicationKeyDataRepository keys;

        private readonly TimeSpan additionalDuration = TimeSpan.FromDays(1);

        private readonly IMembershipProcessor membership;

        private readonly IClaimLookup[] lookups;
        
        public AuthService(
            IApplicationKeyDataRepository keys,
            IApplicationDataRepository repo,
            IAuthenticationProcessor authentication,
            IMembershipProcessor membership,
            IClaimLookup[] lookups)
        {
            this.lookups = lookups ?? new IClaimLookup[0];
            this.membership = membership;
            this.keys = keys;
            this.repo = repo;
            this.authentication = authentication;
        }
        
        public async Task<SignInInfo> Get(ClaimsPrincipal principal)
        {
            if (!principal.Identity.IsAuthenticated)
            {
                throw new UnauthorizedAccessException();
            }
            
            return new SignInInfo {
                Password = new string(Enumerable.Range(-0, 9).Select(x => '*').ToArray()),
                Name = principal.Identity.Name,
                //                Token = this.CreateToken(info.Name),
                Status = SignInStatus.Success
            };
        }
        
        public async Task<SignInInfo> SignIn(string app, SignInInfo info)
        {
            var userData = new UserData {
                Name = info.Name,
                Password = info.Password ?? ""
            };
            
            var statusIn = this.authentication.SignIn(userData);
            
            if (statusIn == SignInStatus.Success)
            {
                var tokenObject = await this.CreateTokenObject(userData.Name, app);
                if (tokenObject == null)
                {
                    return new SignInInfo {
                        Password = new string((info.Password ?? "").Select(x => '*').ToArray()),
                        Name = userData.Name ?? info.Name,
                        Token = null,
                        Status = SignInStatus.InvalidRequest
                    };
                }
                
                var tokenString = await this.CreateToken(tokenObject);
                var payload = tokenString.Split('.')[1];
                
                return new SignInInfo {
                    Password = new string((info.Password ?? "").Select(x => '*').ToArray()),
                    Name = info.Name,
                    Token = tokenString,
                    RawToken = Base64UrlEncoder.Decode(payload),
                    Status = statusIn
                };
            }
            
            return new SignInInfo {
                Password = new string((info.Password ?? "").Select(x => '*').ToArray()),
                Name = userData.Name ?? info.Name,
                Token = null,
                Status = statusIn
            };
        }
        
        public async Task<SignInInfo> SignOut(SignInInfo info)
        {
            return new SignInInfo {
                Password = new string('*', 10),
                Name = info.Name,
                Token = null,
                Status = SignInStatus.UnknownUser
            };
        }
        
        private Task<JwtSecurityToken> CreateTokenObject(string name, string appName)
        {
            return this.repo.GetApplications()
                .Then()
                .Where(x => x.Name == appName)
                .Then()
                .FirstOrDefault()
                .Then()
                .Return(async x =>
                      {
                          var claimsList = new List<Claim> { new Claim(JwtRegisteredClaimNames.Sub, name) };
                          string[] groups = new string[0];
                          
                          if (membership != null)
                          {
                              groups = membership.UserGroups(name);
                          }
                          
                          claimsList.AddRange(groups.Select(y => new Claim("group", y)));
                          
                          foreach (var claimLookup in this.lookups)
                          {
                              var claims = await claimLookup.FindAdditionalClaims(name);
                              
                              claimsList.AddRange(claims ?? new List<Claim>());
                          }
                          
                          JwtSecurityToken jwtToken = new JwtSecurityToken(
                              "http://sso.base2art.com/",
                              "http://" + x.Host + "/",
                              claimsList,
                              DateTime.UtcNow,
                              DateTime.UtcNow.Add(this.additionalDuration),
                              await this.BuildCredentials(appName));
                          return jwtToken;
                          
                      });
        }

        private Task<string> CreateToken(JwtSecurityToken token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            return Task.FromResult(tokenHandler.WriteToken(token));
        }

        private Task<SigningCredentials> BuildCredentials(string keyName)
        {
            return this.keys.GetKeyByName(keyName)
                .Then()
                .Return(x =>
                      {
                          RSACryptoServiceProvider myRSA = new RSACryptoServiceProvider();
                          myRSA.ImportParameters(new RSAParameters {
                                                     D = x.D,
                                                     DP = x.DP,
                                                     DQ = x.DQ,
                                                     Exponent = x.Exponent,
                                                     InverseQ = x.InverseQ,
                                                     Modulus = x.Modulus,
                                                     P = x.P,
                                                     Q = x.Q,
                                                 });
                          
                          return new SigningCredentials(
                              new RsaSecurityKey(myRSA),
                              SecurityAlgorithms.RsaSha256Signature,
                              SecurityAlgorithms.Sha256Digest,
                              new SecurityKeyIdentifier(new NamedKeySecurityKeyIdentifierClause("kid", keyName)));
                      });
        }
    }
}
