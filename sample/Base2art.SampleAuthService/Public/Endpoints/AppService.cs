﻿
namespace Base2art.SampleAuthService.Public.Endpoints
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using Base2art.SampleAuthService.Public.Models;
    using Base2art.SampleAuthService.Repositories;
    using Base2art.Tasks;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Security;
    
    public class AppService
    {
        private readonly IApplicationDataRepository repo;

        private readonly IApplicationKeyDataRepository keys;
        
        public AppService(IApplicationDataRepository repo, IApplicationKeyDataRepository keys)
        {
            this.keys = keys;
            this.repo = repo;
        }
        
        public Task<Application[]> List(ClaimsPrincipal principal)
        {
            if (!principal.IsInRole("SSO Admin"))
            {
                throw new AccessViolationException();
            }
            
            return this.repo.GetApplications()
                .Then()
                .Select(x =>
                        new Application
                        {
                            Name = x.Name,
                            ApplicationHost = x.Host,
                            Keys = x.Keys
                        })
                .Then()
                .ToArray();
        }
        
        public async Task<Application> CreateOrUpdate(Application application, ClaimsPrincipal principal)
        {
            if (!principal.IsInRole("SSO Admin"))
            {
                throw new AccessViolationException();
            }
            
            var hasItem = await this.repo.GetApplications()
                .Then()
                .Where(x => string.Equals(application.Name, x.Name))
                .Then()
                .FirstOrDefault();
            
            if (hasItem == null)
            {
                if (application.Keys == null || application.Keys.Length == 0)
                {
                    var key = Guid.NewGuid().ToString("N");
                    await this.keys.AddKeyById(key);
                    application.Keys = new string[]{ key };
                }
                
                await this.repo.AddApplication(
                    application.Name,
                    application.ApplicationHost,
                    application.Keys);
            }
            else
            {
                await this.repo.UpdateApplication(
                    application.Name,
                    application.ApplicationHost,
                    application.Keys ?? new string[0]);
            }
            
            return application;
        }
    }
}
