﻿namespace Base2art.SampleAuthService.Public.Tasks
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.SystemServices;

    public class ClearCacheTask : ILoggedTask<TextWriter>
    {
        private readonly ICacheProvider cache;

        public ClearCacheTask(ICacheProvider cache)
        {
            this.cache = cache;
        }

        public Task ExecuteAsync(System.Collections.Generic.IDictionary<string, object> parameters, TextWriter logger)
        {
            this.cache.GetSystemCache().Clear();
            logger.WriteLine(new {Cleared = true});
            return Task.FromResult(true);
        }
    }
}
