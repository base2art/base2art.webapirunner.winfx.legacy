﻿
using Base2art.Security.Authentication;
namespace Base2art.SampleAuthService.Public.Models
{
    public class SignInInfo
    {
        public string Name
        {
            get;
            set;
        }
        
        public string Password
        {
            get;
            set;
        }
        
        public string Token
        {
            get;
            set;
        }
        
        public string RawToken
        {
            get;
            set;
        }
        
        public SignInStatus Status
        {
            get;
            set;
        }
    }
}
