﻿
namespace Base2art.SampleAuthService.Public.Models
{
    public class User
    {
        public string Email
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }
        
        public string Password
        {
            get;
            set;
        }
    }
}
