﻿
namespace Base2art.SampleAuthService.Public.Models
{
    public class PublicApplicationKey
    {
        public byte[] Modulus{get;set;}
        public byte[] Exponent {get;set;}
    }
}
