﻿
namespace Base2art.SampleAuthService.Public.Models
{
    public class Application
    {
        public string ApplicationHost { get; set; }
        
        public string Name { get; set; }
        
        public string[] Keys { get; set; }
    }
}
