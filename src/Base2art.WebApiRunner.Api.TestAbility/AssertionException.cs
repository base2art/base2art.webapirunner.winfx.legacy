﻿
using System;
using System.Runtime.Serialization;

namespace Base2art.WebApiRunner.Api
{
    public class AssertionException : Exception, ISerializable
    {
        public AssertionException()
        {
        }

        public AssertionException(object expected, object actual) : base(CreateMessage(expected, actual))
        {
        }

        public AssertionException(string message) : base(message)
        {
        }

        public AssertionException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected AssertionException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }

        private static string CreateMessage(object expected, object actual)
        {
            return string.Format(
                "Expected : `{0}` Was: `{1}`", 
                expected ?? "<null>", 
                actual ?? "<null>");
        }
    }
}