﻿namespace Base2art.WebApiRunner.Api.Routing
{
    public enum RoutingType
    {
        Combined,
        Public,
        Admin
    }
}
