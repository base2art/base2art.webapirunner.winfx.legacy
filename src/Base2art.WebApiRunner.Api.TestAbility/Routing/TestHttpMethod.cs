﻿namespace Base2art.WebApiRunner.Api.Routing
{
    public enum TestHttpMethod
    {
        Get,
        Post,
        Delete,
        Put,
        Trace,
        Options,
        Head,
        Patch
    }
}
