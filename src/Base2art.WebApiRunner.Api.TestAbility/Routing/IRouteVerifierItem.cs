﻿namespace Base2art.WebApiRunner.Api.Routing
{
    public interface IRouteVerifierItem
    {
        IRouteVerifierItem IsClass(System.Type type);
        
        IRouteVerifierItem IsMethod(string get);
    }
}
