﻿namespace Base2art.WebApiRunner.Api.Routing
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Dispatcher;
    using System.Web.Http.Hosting;
    using System.Web.Http.Routing;
    
    public class RouteVerifierItem : IRouteVerifierItem
    {
        private readonly HttpMethod requestedMethod;

        private readonly string requestedPath;

        private Type type;

        private string method;

        private HttpConfiguration config;

        private HttpRequestMessage request;
        
        private bool inited = false;

        private IHttpControllerSelector controllerSelector;

        private IHttpRouteData httpRouteData;

        private HttpControllerContext controllerContext;
        
        public RouteVerifierItem(HttpMethod requestedMethod, string requestedPath, HttpConfiguration config, HttpRequestMessage request)
        {
            this.request = request;
            this.config = config;
            this.requestedPath = requestedPath;
            this.requestedMethod = requestedMethod;
        }

        public IRouteVerifierItem IsClass(Type type)
        {
            this.type = type;
            var controllerType = this.GetControllerType();
            
            if (this.type != controllerType)
            {
                throw new AssertionException(this.type, controllerType);
            }
            
            return this;
        }

        public IRouteVerifierItem IsMethod(string method)
        {
            this.method = method;
            
            var actionName = this.GetActionName();
            
            if (this.method != actionName)
            {
                throw new AssertionException(this.method, actionName);
            }
            
            return this;
        }

        private void EnsureWireUp()
        {
            if (!this.inited)
            {
                this.WireUp();
                this.inited = true;
            }
        }

        private void WireUp()
        {
            var routeData = this.config.Routes.GetRouteData(this.request);

            if (routeData != null && routeData.Values != null)
            {
                IDictionary<string, object> dictionary = routeData.Values;
                foreach (var pair in dictionary.ToList())
                {
                    if (dictionary[pair.Key] == RouteParameter.Optional)
                    {
                        dictionary.Remove(pair.Key);
                    }
                }
            }

            this.request.Properties[HttpPropertyKeys.HttpRouteDataKey] = routeData;
            this.controllerSelector = this.config.GetService<IHttpControllerSelector>();
            this.httpRouteData = routeData ?? new HttpRouteData(new HttpRoute());
            this.controllerContext = new HttpControllerContext(this.config, this.httpRouteData, this.request);
        }
        
        private string GetActionName()
        {
            this.EnsureWireUp();
            if (this.controllerContext.ControllerDescriptor == null)
            {
                this.GetControllerType();
            }

            IHttpActionSelector actionSelector = this.config.GetService<IHttpActionSelector>();
            var descriptor = actionSelector.SelectAction(this.controllerContext);

            return descriptor == null ? null : descriptor.ActionName;
        }

        private Type GetControllerType()
        {
            this.EnsureWireUp();

            var descriptor = this.controllerSelector.SelectController(this.request);
            if (descriptor == null)
            {
                return null;
            }
            
            this.controllerContext.ControllerDescriptor = descriptor;
            return descriptor.ControllerType;
        }
        
        /*

            public void Uses(Type controller, string action)
            {
                this.EnsureWireUp();

                if (controller == null && action == null)
                {
                    //                    var ctlr =
                    ((Action) (() => this.GetControllerType()))();
                    //                    var actn =
                    ((Action) (() => this.GetActionName()))();
                }
                else if (action == null)
                {
                    var ctlr = this.GetControllerType();
                    //                    var actn =
                    ((Action) (() => this.GetActionName()))();
                    //.ShouldThrow<InvalidOperationException>();
                }
                else
                {
                    var ctlr = this.GetControllerType();
                    //.Should().Be(controller);
                    var actn = this.GetActionName();
                    //.Should().Be(action);
                }
            }
         */
    }
}


