﻿namespace Base2art.WebApiRunner.Api.Routing
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Web.Http;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Internals;
    using Newtonsoft.Json;
    
    public class RouteVerifier
    {
        private readonly HttpConfiguration config;

        private readonly YamlService yaml = new YamlService();
        
        public RouteVerifier(string configPath, RoutingType routingType)
        {
            this.config = new HttpConfiguration();
            this.config.IncludeErrorDetailPolicy = IncludeErrorDetailPolicy.Always;
            
            
            var configuration = new RoutingServerConfiguration();
            this.Init(configuration, configPath);
            
            var startup = new CustomStartup("", configuration, routingType);
            startup.Register(this.config, null, () => null);
            //            WebApiConfig.Register(this._releaseConfig, new PassThroughDelegatingHandler(), new DefaultApplicationSettings(), new DefaultAuthEnvironmentSettings());
            this.config.EnsureInitialized();
        }

        public IRouteVerifierItem VerifyRoute(TestHttpMethod method, string path)
        {
            var meth = new HttpMethod(Map(method));
            var httpContextMock = new HttpRequestMessage(meth, new Uri(new Uri("http://localhost/"), path));

            var router = new RouteVerifierItem(meth, path, config, httpContextMock);
            return router;
            
            //            this.config.VerifyRequestFor(method, path);
        }

        private void Init(RoutingServerConfiguration configuration, string configPath)
        {
            // ConfigIniter
            configuration.Init(configPath);
        }

        private static string Map(TestHttpMethod method)
        {
            switch (method)
            {
                case TestHttpMethod.Delete:
                    return HttpMethod.Delete.Method;
                case TestHttpMethod.Get:
                    return HttpMethod.Get.Method;
                case TestHttpMethod.Head:
                    return HttpMethod.Head.Method;
                case TestHttpMethod.Options:
                    return HttpMethod.Options.Method;
                case TestHttpMethod.Post:
                    return HttpMethod.Post.Method;
                case TestHttpMethod.Patch:
                    return new HttpMethod("PATCH").Method;
                case TestHttpMethod.Put:
                    return HttpMethod.Put.Method;
                case TestHttpMethod.Trace:
                    return HttpMethod.Trace.Method;
            }

            return HttpMethod.Get.Method;
        }
        
        private class CustomStartup : Startup
        {
            private readonly RoutingType routingType;

            public CustomStartup(string configDir, ServerConfiguration config, RoutingType routingType) : base(configDir, config)
            {
                this.routingType = routingType;
            }
            
            protected override System.Collections.Generic.List<TypedMethodConfiguration> CreateMethods(ServerConfiguration config)
            {
                if (this.routingType == RoutingType.Combined)
                {
                    var baseRoutes = base.CreateMethods(config);
                    baseRoutes.AddRange(config.CreateTasksAndHealthCheck("admin/"));
                    return baseRoutes;
                }
                
                return this.routingType == RoutingType.Admin
                    ? config.CreateTasksAndHealthCheck()
                    : base.CreateMethods(config);
                
            }
        }
    }
}
