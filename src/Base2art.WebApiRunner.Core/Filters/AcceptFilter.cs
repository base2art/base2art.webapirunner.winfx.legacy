﻿
namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Web.Http.Filters;

    public class AcceptFilter : ActionFilterBase
    {
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
            
            var response = actionExecutedContext.Response;
            var request = actionExecutedContext.Request;
            
            if (response == null || request == null)
            {
                return;
            }
            
            var oc = response.Content as ObjectContent;
            var uri = request.RequestUri;
            
            if (oc == null || uri == null)
            {
                return;
            }
            
            var query = uri.ParseQueryString();
            var accept = query.Get("headers.accept");
            
            if (string.IsNullOrWhiteSpace(accept))
            {
                return;
            }
            
            var header = new MediaTypeHeaderValue(accept);
            var writer = request.GetConfiguration().Formatters.FindWriter(oc.ObjectType, header);
            if (writer == null)
            {
                return;
            }
            
            response.Content = new ObjectContent(oc.ObjectType, oc.Value, writer, header);
        }
    }
}
