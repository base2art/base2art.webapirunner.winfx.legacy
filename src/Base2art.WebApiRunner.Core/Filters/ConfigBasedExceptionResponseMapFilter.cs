namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Http.Filters;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Models;
    
    public class ConfigBasedExceptionResponseMapFilter : ExceptionFilterBase
    {
        private readonly HttpConfiguration config;

        private readonly ExceptionMapItemConfiguration exceptionMap;
        
        public ConfigBasedExceptionResponseMapFilter(HttpConfiguration config, ExceptionMapItemConfiguration exceptionMap)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }
            
            this.exceptionMap = exceptionMap;
            
            this.config = config;
        }

        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception != null && actionExecutedContext.Response == null)
            {
                var type = Type.GetType(exceptionMap.ExceptionType);
                
                if (type.IsInstanceOfType(actionExecutedContext.Exception))
                {
                    var error = new Error();
                    
                    if (!string.IsNullOrWhiteSpace(exceptionMap.ResponseCode))
                    {
                        error.Code = exceptionMap.ResponseCode;
                    }
                    
                    if (!string.IsNullOrWhiteSpace(exceptionMap.ResponseMessage))
                    {
                        error.Message = exceptionMap.ResponseMessage;
                    }
                    
                    if (!string.IsNullOrWhiteSpace(exceptionMap.ResponseMessageExceptionProperty))
                    {
                        var getter = GetPropGetter<Exception, string>(exceptionMap.ResponseMessageExceptionProperty);
                        
                        error.Message = getter(actionExecutedContext.Exception);
                    }
                    
                    if (string.IsNullOrWhiteSpace(error.Code))
                    {
                        error.Code = actionExecutedContext.Exception.GetType().Name;
                    }
                    
                    if (string.IsNullOrWhiteSpace(error.Message))
                    {
                        error.Message = actionExecutedContext.Exception.Message;
                    }
                    
                    var responseCode = exceptionMap.HttpResponseCode <= 0 ? 500 : exceptionMap.HttpResponseCode;
                    var statusCode = (System.Net.HttpStatusCode)responseCode;
                    
                    actionExecutedContext.Exception.SuppressLogging();
                    actionExecutedContext.Response = actionExecutedContext.Request.CreateResponse(statusCode, error, this.config);
                }
            }

            base.OnException(actionExecutedContext);
        }
        
        public static Func<TObject, TProperty> GetPropGetter<TObject, TProperty>(string propertyName)
        {
            ParameterExpression paramExpression = Expression.Parameter(typeof(TObject), "value");

            Expression propertyGetterExpression = Expression.Property(paramExpression, propertyName);

            Func<TObject, TProperty> result =
                Expression.Lambda<Func<TObject, TProperty>>(propertyGetterExpression, paramExpression).Compile();

            return result;
        }
    }
}
