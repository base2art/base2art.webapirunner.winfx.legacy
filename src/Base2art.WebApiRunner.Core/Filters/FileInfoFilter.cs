﻿
namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.IO;
    using System.Net;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Web.Http.Filters;
    using Base2art.WebApiRunner.Services;
    
    public class FileInfoFilter : ObjectFilterBase<FileInfo>
    {
        private readonly IMediaTypeMap map;

        public FileInfoFilter(IMediaTypeMap map)
        {
            this.map = map;
        }
        
        protected override void Send(HttpActionExecutedContext actionExecutedContext, FileInfo value)
        {
            var ext = this.map.GetMimeType(value.Extension);
            actionExecutedContext.Response.Content = new StreamContent(value.OpenRead());
            actionExecutedContext.Response.Content.Headers.ContentType = new MediaTypeHeaderValue(ext);
            actionExecutedContext.Response.Content.Headers.ContentLength = value.Length;
        }
    }
}
