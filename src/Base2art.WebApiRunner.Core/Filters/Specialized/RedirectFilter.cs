﻿namespace Base2art.WebApiRunner.Filters.Specialized
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http.Filters;
    using Base2art.WebApiRunner.Proxies;

    public class RedirectFilter : GenericDuckTypeActionFilterBase<IRedirectFilter<object>>
    {
        public RedirectFilter(IGenericDuckType<IRedirectFilter<object>> genericDuckType)
            : base(genericDuckType)
        {
        }

        protected override async Task Invoke(HttpActionExecutedContext actionExecutedContext, object content, IRedirectFilter<object> instance)
        {
            HttpStatusCode? statusCode = null;
            string location = null;
            await instance.SetRedirectAsync(content, (x, y) =>
            {
                statusCode = x;
                location = y;
            });
            
            if (!string.IsNullOrWhiteSpace(location))
            {
                var inUri = actionExecutedContext.Request.RequestUri;
                var @outUri = new Uri(inUri, location);
                actionExecutedContext.Response.Headers.Location = @outUri;
            }
            
            if (statusCode.HasValue)
            {
                actionExecutedContext.Response.StatusCode = statusCode.Value;
            }
        }
    }

    
}


