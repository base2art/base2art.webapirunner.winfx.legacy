﻿namespace Base2art.WebApiRunner.Filters.Specialized
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using System.Web.Http.Filters;
    using Base2art.WebApiRunner.Proxies;

    public class HeaderFilter : GenericDuckTypeActionFilterBase<ISetHeadersFilter<object>>
    {
        public HeaderFilter(IGenericDuckType<ISetHeadersFilter<object>> genericDuckType)
            : base(genericDuckType)
        {
        }

        protected override async Task Invoke(HttpActionExecutedContext actionExecutedContext, object content, ISetHeadersFilter<object> instance)
        {
            var headers = actionExecutedContext.Response.Headers;
            await instance.SetHeadersAsync(content, new HeadersWrapper(headers));
        }
        
        private class HeadersWrapper : ICollection<KeyValuePair<string, string>>
        {
            private readonly HttpResponseHeaders headers;

            public HeadersWrapper(HttpResponseHeaders headers)
            {
                this.headers = headers;
            }

            public ICollection<string> Keys
            {
                get { return this.headers.Select(x => x.Key).ToList(); }
            }
            
            public void Add(KeyValuePair<string, string> item)
            {
                this.headers.Add(item.Key, item.Value);
            }
            
            public void Clear()
            {
                this.headers.Clear();
            }
            
            public bool Contains(KeyValuePair<string, string> item)
            {
                return this.headers.Contains(new KeyValuePair<string, IEnumerable<string>>(item.Key, new[]{ item.Value }));
            }
            
            public void CopyTo(KeyValuePair<string, string>[] array, int arrayIndex)
            {
                
            }
            
            public bool Remove(KeyValuePair<string, string> item)
            {
                return this.headers.Remove(item.Key);
            }
            
            public int Count
            {
                get { return this.headers.Count(); }
            }
            
            public bool IsReadOnly
            {
                get { return false; }
            }
            
            public bool TryGetValue(string key, out string value)
            {
                IEnumerable<string> values;
                var ret = this.headers.TryGetValues(key, out values);
                value = null;
                if (ret)
                {
                    value = string.Join("", values ?? new string[0]);
                }
                
                return ret;
            }
            
            public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
            {
                foreach (var element in this.Keys)
                {
                    string value;
                    this.TryGetValue(element, out value);
                    yield return new KeyValuePair<string, string>(element, value);
                }
            }
            
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }
        }
    }
}

/*
 
            private readonly HttpResponseHeaders headers;

            public HeadersWrapper(HttpResponseHeaders headers)
            {
                this.headers = headers;
            }

            public bool ContainsKey(string key)
            {
                return this.headers.Contains(key);
            }
            
            public void Add(string key, string value)
            {
                this.headers.Add(key, value);
            }
            
            public bool Remove(string key)
            {
                return this.headers.Remove(key);
            }
            
            public bool TryGetValue(string key, out string value)
            {
                IEnumerable<string> values;
                var ret = this.headers.TryGetValues(key, out values);
                value = null;
                if (ret)
                {
                    value = string.Join("", values ?? new string[0]);
                }
                
                return ret;
            }
            
            public string this[string key]
            {
                get
                {
                    string val;
                    this.TryGetValue(key, out val);
                    return val;
                }
                
                set
                {
                    this.Add(key, value);
                }
            }
            public ICollection<string> Keys
            {
                get { return this.headers.Select(x => x.Key).ToList(); }
            }
            
            public ICollection<string> Values
            {
                get { return this.headers.Select(x => string.Join("", x.Value ?? new string[0])).ToList(); }
            }
            
            public void Add(KeyValuePair<string, string> item)
            {
                this.Add(item.Key, item.Value);
            }
            
            public void Clear()
            {
                this.headers.Clear();
            }
            
            public bool Contains(KeyValuePair<string, string> item)
            {
                return this.headers.Contains(new KeyValuePair<string, IEnumerable<string>>(item.Key, new[]{ item.Value }));
            }
            
            public void CopyTo(KeyValuePair<string, string>[] array, int arrayIndex)
            {
                
            }
            
            public bool Remove(KeyValuePair<string, string> item)
            {
                return this.headers.Remove(item.Key);
            }
            
            public int Count
            {
                get { return this.headers.Count(); }
            }
            
            public bool IsReadOnly
            {
                get { return false; }
            }
            
            public IEnumerator<KeyValuePair<string, string>> GetEnumerator()
            {
                foreach (var element in this.Keys)
                {
                    string value;
                    this.TryGetValue(element, out value);
                    yield return new KeyValuePair<string, string>(element, value);
                }
            }
            
            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }
 */


