﻿namespace Base2art.WebApiRunner.Filters.Specialized
{
    using System;
    using System.Net;
    using System.Threading.Tasks;
    using System.Web.Http.Filters;
    using Base2art.WebApiRunner.Proxies;

    public class StatusCodeFilter : GenericDuckTypeActionFilterBase<ISetStatusCodeFilter<object>>
    {
        public StatusCodeFilter(IGenericDuckType<ISetStatusCodeFilter<object>> genericDuckType) : base(genericDuckType)
        {
        }

        protected override async Task Invoke(HttpActionExecutedContext actionExecutedContext, object content, ISetStatusCodeFilter<object> instance)
        {
            HttpStatusCode? statusCode = null;
            await instance.SetStatusCodeAsync(content, x =>
                                              {
                                                  statusCode = x;
                                              });
            if (statusCode.HasValue)
            {
                actionExecutedContext.Response.StatusCode = statusCode.Value;
            }
        }
    }
}




