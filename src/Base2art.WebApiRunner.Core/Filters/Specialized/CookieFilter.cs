﻿namespace Base2art.WebApiRunner.Filters.Specialized
{
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Threading.Tasks;
    using System.Web.Http.Filters;
    using Base2art.WebApiRunner.Proxies;
    
    public class CookieFilter : GenericDuckTypeActionFilterBase<ISetCookieFilter<object>>
    {
        public CookieFilter(IGenericDuckType<ISetCookieFilter<object>> genericDuckType)
            : base(genericDuckType)
        {
        }
        
        protected override async Task Invoke(HttpActionExecutedContext actionExecutedContext, object content, ISetCookieFilter<object> instance)
        {
            List<System.Net.Cookie> cookies = new List<System.Net.Cookie>();
            await instance.SetCookiesAsync(content, cookies);
            
            foreach (var cookie in cookies)
            {
                var cookieHeaderValue = new CookieHeaderValue(cookie.Name, cookie.Value) {
                    Expires = cookie.Expires,
                    HttpOnly = cookie.HttpOnly,
                    Secure = cookie.Secure
                };
                
                if (!string.IsNullOrWhiteSpace(cookie.Domain))
                {
                    cookieHeaderValue.Domain = cookie.Domain;
                }
                
                cookieHeaderValue.Path = cookie.Path;
                if (string.IsNullOrWhiteSpace(cookie.Path))
                {
                    cookieHeaderValue.Path = "/";
                }
                
                actionExecutedContext.Response.Headers.AddCookies(new[] {
                                                                      cookieHeaderValue
                                                                  });
            }
        }
    }
    
    
    
}
