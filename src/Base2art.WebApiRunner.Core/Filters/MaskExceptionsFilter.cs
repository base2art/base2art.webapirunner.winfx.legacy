namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Web.Http;
    using Base2art.WebApiRunner.Models;

    public class MaskExceptionsFilter : System.Web.Http.ExceptionHandling.ExceptionHandler
    {
        private readonly HttpConfiguration config;

        private readonly System.Web.Http.ExceptionHandling.IExceptionHandler handler;
        
        public MaskExceptionsFilter(HttpConfiguration config, System.Web.Http.ExceptionHandling.IExceptionHandler handler)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            this.config = config;
            this.handler = handler;
        }
        
        public async override System.Threading.Tasks.Task HandleAsync(
            System.Web.Http.ExceptionHandling.ExceptionHandlerContext context,
            System.Threading.CancellationToken cancellationToken)
        {
            if (handler != null)
            {
                await this.handler.HandleAsync(context, cancellationToken);
            }
            
            await base.HandleAsync(context, cancellationToken);
            if (context.ExceptionContext.Response == null)
            {
                if (!context.Request.IsLocal() || this.MaskException(context.Request))
                {
                    if (context.Exception != null && context.CatchBlock == null)
                    {
                        context.ExceptionContext.Response = context.Request.CreateResponse(
                            System.Net.HttpStatusCode.InternalServerError, new Error {
                                Message = "Unhandled Exception",
                                Code = "UNKNOWN_ERROR"
                            }, this.config);
                    }
                }
            }
        }
//
        //        public async System.Threading.Tasks.Task HandleAsync(
        //            System.Web.Http.ExceptionHandling.ExceptionHandlerContext context,
        //            System.Threading.CancellationToken cancellationToken)
        //        {
        ////            base.HandleAsync(context, cancellationToken);
//
        //            if (this.handler != null)
        //            {
        //                await this.handler.HandleAsync(context, cancellationToken);
        //            }
//
//
        //        }
        
        //        public override void OnException(System.Web.Http.Filters.HttpActionExecutedContext actionExecutedContext)
        //        {
        //            base.OnException(actionExecutedContext);
        //
        //        }

        private bool MaskException(HttpRequestMessage request)
        {
            #if !DEBUG
            return false;
            #endif
            
            var queryString = request.GetQueryNameValuePairs();
            var comp = StringComparison.OrdinalIgnoreCase;
            return queryString.Any(x => string.Equals(x.Key, "base2art.MaskException", comp) && string.Equals(x.Value, bool.TrueString, comp));
        }
    }
}
