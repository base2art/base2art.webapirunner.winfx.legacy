﻿
namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Net.Http;
    using System.Web.Http.Filters;

    public abstract class ObjectFilterBase<T> : ActionFilterBase 
        where T : class
    {
        ///<summary>Occurs after the action method is invoked.</summary>
        ///<param name="actionExecutedContext">The action executed context.</param>
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
            
            if (actionExecutedContext.Response == null)
            {
                return;
            }
            
            var oc = actionExecutedContext.Response.Content as ObjectContent;
            if (oc == null)
            {
                return;
            }
            
            var fi = oc.Value as T;
            if (fi == null)
            {
                return;
            }
            
            this.Send(actionExecutedContext, fi);
        }

        protected abstract void Send(HttpActionExecutedContext actionExecutedContext, T value);
    }
}


