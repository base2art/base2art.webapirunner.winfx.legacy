namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Filters;

    public class NotImplementedExceptionFilter : ExceptionFilterBase
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is NotImplementedException)
            {
                actionExecutedContext.Exception.SuppressLogging();
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                    HttpStatusCode.NotImplemented, "Operation not implemented");
            }
        }
    }
}
