﻿namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Filters;

    public class ConflictExceptionFilter : ExceptionFilterBase
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is ConflictException)
            {
                actionExecutedContext.Exception.SuppressLogging();
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(HttpStatusCode.Unauthorized, actionExecutedContext.Exception.Message ?? "Object could not be parsed.");
            }
        }
    }
}






