﻿namespace Base2art.WebApiRunner.Filters
{
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http.Filters;
    using Base2art.WebApiRunner.Proxies;

    public abstract class GenericDuckTypeActionFilterBase<TInterface> : ActionFilterBase
    {
        private readonly IGenericDuckType<TInterface> genericDuckType;

        protected GenericDuckTypeActionFilterBase(IGenericDuckType<TInterface> genericDuckType)
        {
            this.genericDuckType = genericDuckType;
        }

        public override async Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            var resp = actionExecutedContext.Response;
            
            if (resp == null)
            {
                await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
                return;
            }
            
            var oc = resp.Content as ObjectContent;
            if (oc == null)
            {
                await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
                return;
            }
            
            if (this.genericDuckType.SupportsType(oc.ObjectType))
            {
                var fi = oc.Value;
                if (fi == null)
                {
                    await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
                    return;
                }
                
                var instance = this.genericDuckType.Instance;
                if (instance != null)
                {
                    await this.Invoke(actionExecutedContext, fi, instance);
                }
            }
            
            await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
        }

        protected abstract Task Invoke(HttpActionExecutedContext actionExecutedContext, object content, TInterface instance);
    }
}


