﻿namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Filters;

    public class NotAuthenticatedExceptionFilter : ExceptionFilterBase
    {
        public override void OnException(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception is NotAuthenticatedException)
            {
                actionExecutedContext.Exception.SuppressLogging();
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(
                    HttpStatusCode.Unauthorized, actionExecutedContext.Exception.Message ?? "You are not authenticated. Or are using an incorrect authenication mechanism");
            }
        }
    }
}


