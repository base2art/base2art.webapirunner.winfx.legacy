namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Linq;
    using System.Net.Http;
    using System.Runtime.ExceptionServices;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    public abstract class ActionFilterBase : IActionFilter
    {
        public bool AllowMultiple
        {
            get { return true; }
        }

        /// <summary>Occurs before the action method is invoked.</summary>
        /// <param name="actionContext">The action context.</param>
        public virtual void OnActionExecuting(HttpActionContext actionContext)
        {
        }

        /// <summary>Occurs after the action method is invoked.</summary>
        /// <param name="actionExecutedContext">The action executed context.</param>
        public virtual void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
        }

        public virtual Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            try
            {
                this.OnActionExecuting(actionContext);
            }
            catch (Exception exception)
            {
                return TaskHelper.FromError(exception);
            }

            return TaskHelper.Complete();
        }

        public virtual Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            try
            {
                this.OnActionExecuted(actionExecutedContext);
            }
            catch (Exception exception)
            {
                return TaskHelper.FromError(exception);
            }

            return TaskHelper.Complete();
        }

        /// <summary>Executes the filter action asynchronously.</summary>
        /// <returns>The newly created task for this operation.</returns>
        /// <param name="actionContext">The action context.</param>
        /// <param name="cancellationToken">The cancellation token assigned for this task.</param>
        /// <param name="continuation">The delegate function to continue after the action method is invoked.</param>
        Task<HttpResponseMessage> IActionFilter.ExecuteActionFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            if (actionContext == null)
            {
                throw new ArgumentNullException("actionContext");
            }
            
            if (continuation == null)
            {
                throw new ArgumentNullException("continuation");
            }
            
            return this.ExecuteActionFilterAsyncCore(actionContext, cancellationToken, continuation);
        }

        private async Task<HttpResponseMessage> ExecuteActionFilterAsyncCore(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            await this.OnActionExecutingAsync(actionContext, cancellationToken);
            return actionContext.Response ?? await this.CallOnActionExecutedAsync(actionContext, cancellationToken, continuation);
        }

        private async Task<HttpResponseMessage> CallOnActionExecutedAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            cancellationToken.ThrowIfCancellationRequested();
            HttpResponseMessage response = null;
            ExceptionDispatchInfo exceptionDispatchInfo = null;
            try
            {
                response = await continuation();
            }
            catch (Exception source)
            {
                exceptionDispatchInfo = ExceptionDispatchInfo.Capture(source);
            }

            Exception ex = exceptionDispatchInfo == null ? null : exceptionDispatchInfo.SourceException;
            HttpActionExecutedContext httpActionExecutedContext = new HttpActionExecutedContext(actionContext, ex)
            {
                Response = response
            };
            try
            {
                await this.OnActionExecutedAsync(httpActionExecutedContext, cancellationToken);
            }
            catch
            {
                actionContext.Response = null;
                throw;
            }

            if (httpActionExecutedContext.Response != null)
            {
                return httpActionExecutedContext.Response;
            }

            Exception exception = httpActionExecutedContext.Exception;
            if (exception != null)
            {
                if (exception != ex)
                {
                    throw exception;
                }

                exceptionDispatchInfo.Throw();
            }

            throw new InvalidOperationException("ActionFilterAttribute_MustSupplyResponseOrException" + this.GetType().Name);
        }
    }
}
