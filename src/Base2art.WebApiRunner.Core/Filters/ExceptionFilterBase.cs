namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http.Filters;

    public abstract class ExceptionFilterBase : IExceptionFilter, IFilter
    {
        public bool AllowMultiple
        {
            get { return true; }
        }

        /// <summary>Raises the exception event.</summary>
        /// <param name="actionExecutedContext">The context for the action.</param>
        public virtual void OnException(HttpActionExecutedContext actionExecutedContext)
        {
        }

        public virtual Task OnExceptionAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            try
            {
                this.OnException(actionExecutedContext);
            }
            catch (Exception exception)
            {
                return TaskHelper.FromError(exception);
            }

            return TaskHelper.Complete();
        }

        /// <summary>Asynchronously executes the exception filter.</summary>
        /// <returns>The result of the execution.</returns>
        /// <param name="actionExecutedContext">The context for the action.</param>
        /// <param name="cancellationToken">The cancellation context.</param>
        Task IExceptionFilter.ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            if (actionExecutedContext == null)
            {
                throw new ArgumentNullException("actionExecutedContext");
            }
            
            return this.ExecuteExceptionFilterAsyncCore(actionExecutedContext, cancellationToken);
        }

        private async Task ExecuteExceptionFilterAsyncCore(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            await this.OnExceptionAsync(actionExecutedContext, cancellationToken);
        }
    }
}
