﻿namespace Base2art.WebApiRunner.Function
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Remoting.Messaging;
    using System.Runtime.Remoting.Proxies;

    internal class MethodProxy<T> : RealProxy
    {
        private readonly Delegate method;

        public MethodProxy(Delegate method)
            : base(typeof(T))
        {
            this.method = method;
        }

        public T Value
        {
            get
            {
                return (T)this.GetTransparentProxy();
            }
        }

        public override IMessage Invoke(IMessage msg)
        {
            var type = typeof(T);
            IMethodCallMessage methodCall = msg as IMethodCallMessage;
            try
            {
                if (((MethodInfo)methodCall.MethodBase).ReturnType == typeof(void))
                {
                    method.DynamicInvoke(methodCall.InArgs);
                    return new ReturnMessage(null, null, 0, methodCall.LogicalCallContext, methodCall);
                }
                else
                {
                    var result = method.DynamicInvoke(methodCall.InArgs);
                    return new ReturnMessage(result, null, 0, methodCall.LogicalCallContext, methodCall);
                }
            }
            catch (TargetInvocationException invocationException)
            {
                var exception = invocationException.InnerException;
                return new ReturnMessage(exception, methodCall);
            }
        }
    }
}


