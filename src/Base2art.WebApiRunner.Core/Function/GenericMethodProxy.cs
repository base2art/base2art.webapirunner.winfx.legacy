namespace Base2art.WebApiRunner.Function
{
    using System;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Remoting.Messaging;
    using System.Runtime.Remoting.Proxies;

    internal class GenericMethodProxy<T> : RealProxy
    {
        private readonly Delegate method;

        public GenericMethodProxy(Delegate method) : base(typeof(T))
        {
            this.method = method;
        }

        public T Value
        {
            get { return (T)this.GetTransparentProxy(); }
        }

        public override IMessage Invoke(IMessage msg)
        {
            var type = typeof(T);
            IMethodCallMessage methodCall = msg as IMethodCallMessage;
            MethodInfo info = ((MethodInfo)methodCall.MethodBase);
            try
            {
                var args = methodCall.InArgs;
                if (info.IsGenericMethod)
                {
                    var count = info.GetGenericArguments().Length;
                    args = new object[methodCall.InArgs.Length + count];
                    
                    Array.Copy(info.GetGenericArguments(), 0, args, 0, count);
                    
                    Array.Copy(methodCall.InArgs, 0, args, count, methodCall.InArgs.Length);
                }
                
                if (info.ReturnType == typeof(void))
                {
                    method.DynamicInvoke(args);
                    return new ReturnMessage(null, null, 0, methodCall.LogicalCallContext, methodCall);
                }
                else
                {
                    var result = method.DynamicInvoke(args);
                    return new ReturnMessage(result, null, 0, methodCall.LogicalCallContext, methodCall);
                }
            }
            catch (TargetInvocationException invocationException)
            {
                var exception = invocationException.InnerException;
                return new ReturnMessage(exception, methodCall);
            }
        }
    }
}
