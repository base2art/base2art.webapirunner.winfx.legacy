namespace Base2art.WebApiRunner.Function
{
    using System;

    public class SmpBuilder<T>
    {
        public T Func1<TIn, TResult>(Func<TIn, TResult> input)
        {
            return new MethodProxy<T>(input).Value;
        }
        
        public T GenericFunc1<TIn, TResult>(Func<Type, TIn, TResult> input)
        {
            return new GenericMethodProxy<T>(input).Value;
        }
    }
}
