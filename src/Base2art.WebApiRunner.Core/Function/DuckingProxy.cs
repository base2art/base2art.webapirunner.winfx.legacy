﻿//namespace Base2art.WebApiRunner.Function
//{
//    using System;
//    using System.Linq;
//    using System.Reflection;
//    using System.Runtime.Remoting.Messaging;
//    using System.Runtime.Remoting.Proxies;
//
//    internal class DuckingProxy<T> : RealProxy
//    {
//        private readonly object backing;
//
//        public DuckingProxy(object backing)
//            : base(typeof(T))
//        {
//            this.backing = backing;
//        }
//
//        public T Value
//        {
//            get
//            {
//                return (T)this.GetTransparentProxy();
//            }
//        }
//
//        public override IMessage Invoke(IMessage msg)
//        {
//            var type = typeof(T);
//            IMethodCallMessage methodCall = msg as IMethodCallMessage;
//            try
//            {
//                var item = backing.GetType().GetMethods().Where(x => x.Name == methodCall.MethodName).First();
//                
//                var result = item.Invoke(this.backing, new object[]{});
//                return new ReturnMessage(result, null, 0, methodCall.LogicalCallContext, methodCall);
//            }
//            catch (TargetInvocationException invocationException)
//            {
//                var exception = invocationException.InnerException;
//                return new ReturnMessage(exception, methodCall);
//            }
//        }
//    }
//}
//
//
//
//
