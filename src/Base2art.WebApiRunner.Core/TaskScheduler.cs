﻿
namespace Base2art.WebApiRunner
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Runtime.ExceptionServices;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.ExceptionHandling;
    using System.Web.Http.Routing;
    using Base2art.WebApiRunner.Config;
    using Quartz;
    using Quartz.Impl;
    using Quartz.Spi;
    using Base2art.WebApiRunner.Internals.Controllers;

    public class TaskScheduler : Component
    {

        IScheduler scheduler;

        TasksConfiguration[] tasks;

        HttpConfiguration config;
        
        public TaskScheduler(HttpConfiguration config, TasksConfiguration[] tasks)
        {
            this.config = config;
            this.tasks = tasks;
        }

        public static IDisposable Run(HttpConfiguration config, TasksConfiguration[] tasks)
        {
            var ts = new TaskScheduler(config, tasks);
            ts.Start();
            return ts;
        }
        
        public void Start()
        {
            
            Common.Logging.LogManager.Adapter = new Common.Logging.Simple.ConsoleOutLoggerFactoryAdapter { Level = Common.Logging.LogLevel.Info };
            
            // Grab the Scheduler instance from the Factory
            scheduler = StdSchedulerFactory.GetDefaultScheduler();

            scheduler.JobFactory = new InjectingJobFactory(config);
            
            // and start it off
            scheduler.Start();
            
            foreach (var item in this.tasks.SelectMany(x => x.Items ?? new List<TaskConfiguration>()).Where(x=>x.Interval.TotalSeconds > 0))
            {
                IJobDetail job = JobBuilder.Create<TaskRunner>()
                    .UsingJobData("Name", item.Name)
                    .UsingJobData("ClassName", item.Type.Value)
                    .Build();
                

                // Trigger the job to run now, and then repeat every 10 seconds
                var delay = item.Delay;
                if (delay.TotalMilliseconds < 10)
                {
                    delay = item.Interval;
                }
                
                ITrigger trigger = TriggerBuilder.Create()
                    .StartAt(DateTimeOffset.Now.Add(delay))
                    .WithSimpleSchedule(x => x
                                        .WithInterval(item.Interval)
                                        .RepeatForever())
                    .Build();
                
                
                // Tell quartz to schedule the job using our trigger
                scheduler.ScheduleJob(job, trigger);
            }
        }
        
        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                if (scheduler != null)
                {
                    scheduler.Shutdown();
                }
            }
        }
        
        private class InjectingJobFactory : IJobFactory
        {
            private readonly HttpConfiguration config;

            public InjectingJobFactory(HttpConfiguration config)
            {
                this.config = config;
            }

            public IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
            {
                return (IJob)Activator.CreateInstance(bundle.JobDetail.JobType, new object[]{ config });
            }

            public void ReturnJob(IJob job)
            {
            }
        }
        
        private class TaskRunner : IJob
        {
            private readonly HttpConfiguration config;
            private System.Web.Http.ExceptionHandling.IExceptionHandler _exceptionHandler;
            private IEnumerable<System.Web.Http.ExceptionHandling.IExceptionLogger> _exceptionLogger;
            
            internal System.Web.Http.ExceptionHandling.IExceptionHandler ExceptionHandler
            {
                get
                {
                    if (this._exceptionHandler == null)
                    {
                        this._exceptionHandler = this.config.Services.GetExceptionHandler();
                    }
                    
                    return this._exceptionHandler;
                }
                
                set
                {
                    this._exceptionHandler = value;
                }
            }

            // System.Web.Http.Dispatcher.HttpControllerDispatcher
            internal IEnumerable<System.Web.Http.ExceptionHandling.IExceptionLogger> ExceptionLogger
            {
                get
                {
                    if (this._exceptionLogger == null)
                    {
                        this._exceptionLogger = this.config.Services.GetExceptionLoggers() ?? new System.Web.Http.ExceptionHandling.IExceptionLogger[0];
                    }
                    
                    return this._exceptionLogger;
                }
                set
                {
                    this._exceptionLogger = value;
                }
            }
            
            public TaskRunner(HttpConfiguration config)
            {
                this.config = config;
            }
            
            public async void Execute(IJobExecutionContext context)
            {
                var message = new HttpRequestMessage();
                message.Method = HttpMethod.Post;
                var controllerSelector = this.config.Services.GetHttpControllerSelector();
                var controllerActivator = this.config.Services.GetHttpControllerActivator();
                var actionSelector = this.config.Services.GetActionSelector();
                
                var item = config.Routes.FirstOrDefault(x => GetFirstMatch(x, context.JobDetail.JobDataMap));
                var data = new HttpRouteData(item, new HttpRouteValueDictionary(item.Defaults));
                
                message.SetConfiguration(config);
                message.SetRouteData(data);
                
                var controller = controllerSelector.SelectController(message);
                var controllerContext = new HttpControllerContext(config, data, message);
                
                controllerContext.Controller = controllerActivator.Create(message, controller, controller.ControllerType);
                controllerContext.ControllerDescriptor = controller;
                var action = actionSelector.SelectAction(controllerContext);
                
                var parms = new Dictionary<string, object>();
                
                var actionParms = action.GetParameters();
                foreach (var parm in actionParms)
                {
                    if (parm.ParameterType.IsAssignableFrom(typeof(IDictionary<string, string>)))
                    {
                        parms[parm.ParameterName] = new Dictionary<string, string>();
                    }
                }
                
                var cancellationToken = CancellationToken.None;
                ExceptionDispatchInfo exceptionDispatchInfo;
                try
                {
                    await controllerContext.Controller.ExecuteAsync(controllerContext, cancellationToken);
                    return;
                }
                catch (OperationCanceledException)
                {
                    return;
                }
                catch (HttpResponseException ex)
                {
                    return;
                }
                catch (Exception source)
                {
                    exceptionDispatchInfo = ExceptionDispatchInfo.Capture(source);
                }
                
                ExceptionContext exceptionContext = new ExceptionContext(exceptionDispatchInfo.SourceException, ExceptionCatchBlocks.HttpServer, message)
                {
                    ControllerContext = controllerContext,
                };
                
                foreach (var logger in  this.ExceptionLogger)
                {
                    await logger.LogAsync(exceptionContext, cancellationToken);
                }
                
                var handler = this.ExceptionHandler;
                HttpResponseMessage httpResponseMessage = await handler.HandleAsync(exceptionContext, cancellationToken);
                if (httpResponseMessage == null)
                {
                    return;
                }
            }

            private bool GetFirstMatch(IHttpRoute x, JobDataMap jobDataMap)
            {
                var defaults = x.Defaults;
                
                if (defaults.ContainsKey("controllerClassName"))
                {
                    var routeClassName = defaults["controllerClassName"] as string;
                    var jobClassName = jobDataMap.Get("ClassName") as string;
                    var isMatch = string.Equals(routeClassName, jobClassName, StringComparison.OrdinalIgnoreCase);
                    
                    // FOR DEBUGGING... BOy this is lame-o.
                    if (isMatch) 
                    {
                        return true;
                    }
                    
                    return false;
                }
                
                return false;
            }
        }
    }
}
