namespace Base2art.WebApiRunner
{
    using System;
    using System.Threading.Tasks;

    internal static class TaskHelper
    {
        public static Task Complete()
        {
            return Task.FromResult<AsyncVoid>(default(AsyncVoid));
        }

        public static Task Complete<TResult>(TResult result)
        {
            return Task.FromResult<TResult>(result);
        }

        public static Task FromError(Exception exception)
        {
            return FromError<AsyncVoid>(exception);
        }

        public static Task<TResult> FromError<TResult>(Exception exception)
        {
            TaskCompletionSource<TResult> taskCompletionSource = new TaskCompletionSource<TResult>();
            taskCompletionSource.SetException(exception);
            return taskCompletionSource.Task;
        }

        public static async Task<object> Coerce<T>(this Task<T> task)
        {
            return await task;
        }


        public static async Task<object> CastToObject(this Task task)
        {
            await task;
            return null;
        }

        [System.Runtime.InteropServices.StructLayoutAttribute(System.Runtime.InteropServices.LayoutKind.Sequential, Size = 1)]
        private struct AsyncVoid
        {
        }
    }
}
