﻿
namespace Base2art.WebApiRunner.Models
{
	public class HeathCheckResultItem
	{
        public System.Collections.IDictionary Data
        {
            get;
            set;
        }
        
        public string Message
        {
            get;
            set;
        }

		public bool IsHealthy
		{
			get;
			set;
		}
	}
}


