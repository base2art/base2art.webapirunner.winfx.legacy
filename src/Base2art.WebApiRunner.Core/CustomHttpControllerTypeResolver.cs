namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http.Dispatcher;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Reflection;
    
    public class CustomHttpControllerTypeResolver : IHttpControllerTypeResolver
    {
        private readonly List<TypedMethodConfiguration> methods;
        
        public CustomHttpControllerTypeResolver(List<TypedMethodConfiguration> methods)
        {
            this.methods = methods;
        }
        
        private bool IsHttpEndpoint(Type x)
        {
            return x != null;
        }
        
        public ICollection<Type> GetControllerTypes(IAssembliesResolver assembliesResolver)
        {
            var asms = assembliesResolver.GetAssemblies();
            
            // ci
            return this.methods.GroupBy(x => x.MethodConfiguration.Type.Value)
                // return this.methods.GroupBy(x => x.MethodConfiguration.ClassName)
                .Where(x => !string.IsNullOrWhiteSpace(x.Key))
                .Select(x => asms.LoadType(x))
                .Where(x => IsHttpEndpoint(x))
                .ToArray();
        }
    }
}
