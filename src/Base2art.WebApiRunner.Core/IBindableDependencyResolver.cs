﻿using System;
using System.Collections.Generic;
namespace Base2art.WebApiRunner
{
    public interface IBindableDependencyResolver
    {
        void Register(Type requested, Type fullfilling, Dictionary<string, object> parameters, Dictionary<string, object> properties, bool isSingleton);
        
        void RegisterInstance<T>(T instance, bool registerImplementedInterfaces);
    }
}
