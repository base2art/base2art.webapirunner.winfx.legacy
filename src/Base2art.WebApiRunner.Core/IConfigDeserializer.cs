namespace Base2art.WebApiRunner
{
    public interface IConfigDeserializer
    {
       T Deserialize<T>(string input);
    }
}
