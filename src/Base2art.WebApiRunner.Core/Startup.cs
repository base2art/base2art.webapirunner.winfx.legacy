namespace Base2art.WebApiRunner
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Dependencies;
    using System.Web.Http.Description;
    using System.Web.Http.Dispatcher;
    using System.Web.Http.Filters;
    using System.Web.Http.ModelBinding;
    using System.Web.Http.ModelBinding.Binders;
    using System.Web.Http.Routing;
    using Base2art.WebApiRunner.Config.TypeConverters;
    using Base2art.WebApiRunner.Internals.Bindings;
    using Base2art.WebApiRunner.SystemServices;
    using Base2art.WebApiRunner.SystemServices.DefaultServiceImplementations;
    using Newtonsoft.Json;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Filters;
    using Base2art.WebApiRunner.Filters.Specialized;
    using Base2art.WebApiRunner.Formatters;
    using Base2art.WebApiRunner.Internals;
    using Base2art.WebApiRunner.Internals.Cors;
    using Base2art.WebApiRunner.Middleware;
    using Base2art.WebApiRunner.Proxies;
    using Base2art.WebApiRunner.Reflection;
    using Newtonsoft.Json.Serialization;
    using Swashbuckle.Application;
    using Base2art.WebApiRunner.Internals.Swagger;
    using Base2art.WebApiRunner.Services;
    using Base2art.WebApiRunner.Services.Internals;
    using Base2art.WebApiRunner.Services.SystemServices;

    // http://www.strathweb.com/2013/02/but-i-dont-want-to-call-web-api-controllers-controller/
    public class Startup : StartupBase
    {
        private readonly Lazy<List<TypedMethodConfiguration>> methods;

        private readonly string configDir;
        
        public Startup(string configDir, ServerConfiguration config)
            : base(config)
        {
            this.configDir = configDir;
            methods = new Lazy<List<TypedMethodConfiguration>>(() => this.CreateMethods(config));
        }
        
        protected virtual string ApiName
        {
            get { return "Public API"; }
        }
        
        protected List<TypedMethodConfiguration> Methods
        {
            get
            {
                return this.methods.Value;
            }
        }

        /// <summary>
        /// This method configures the serializers...
        /// </summary>
        /// <param name="serializerSettings"></param>
        /// <returns></returns>
        public static JsonSerializerSettings SetupSerializer(JsonSerializerSettings serializerSettings)
        {
            serializerSettings.ContractResolver = new Newtonsoft.Json.Serialization.CamelCasePropertyNamesContractResolver();
            
            serializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
            
            serializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Ignore;
            serializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
            serializerSettings.Converters.Add(new Newtonsoft.Json.Converters.VersionConverter());
            serializerSettings.Converters.Add(new Newtonsoft.Json.Converters.KeyValuePairConverter());
            serializerSettings.Converters.Add(new DictionaryConverter());
            
            serializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
            serializerSettings.DateFormatHandling = DateFormatHandling.IsoDateFormat;
            serializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.RoundtripKind;
            return serializerSettings;
        }

        protected virtual List<TypedMethodConfiguration> CreateMethods(ServerConfiguration config)
        {
            var items = config.JointEndpoints();
            
            return items.SelectMany(y => y.Urls.Select(x => new TypedMethodConfiguration {
                                                           MethodConfiguration = new MethodConfiguration {
                                                               Aspects = (x.Aspects ?? new AspectConfiguration[0]).Union(items.SelectMany(z => z.Aspects ?? new List<AspectConfiguration>())).ToArray(),
                                                               Method = x.Method,
                                                               Parameters = x.Parameters,
                                                               Properties = x.Properties,
                                                               Type = x.Type,
                                                               Url = x.Url,
                                                               Verb = x.Verb
                                                           },
                                                           ControllerType = ControllerType.Service,
                                                       })).ToList();
        }
        
        protected override void RegisterDependencyResolver(HttpConfiguration config)
        {
            string resolverTypeName = this.ServerConfig.DependencyResolverTypeName;
            if (string.IsNullOrWhiteSpace(resolverTypeName))
            {
                resolverTypeName = typeof(InternalDependencyResolver).FullName;
            }
            
            var type = Type.GetType(resolverTypeName);
            config.DependencyResolver = (IDependencyResolver)Activator.CreateInstance(type);
        }
        
        protected override void RegisterFormatters(HttpConfiguration config)
        {
            config.Formatters.ReturnJsonByDefault();
            var serializerSettings = config.Formatters.JsonFormatter.SerializerSettings;
            
            SetupSerializer(serializerSettings);
            
            config.Formatters.Add(new CsvFormatter(config.DependencyResolver.GetService<IJsonSerializer>()));
            
            config.Formatters.Add(new PlainTextFormatter(config.DependencyResolver.GetService<IObjectStringifier>()));
        }
        
        protected override void RegisterSystemServices(HttpConfiguration config)
        {
            var configuredAssembliesResolver = new ConfiguredAssembliesResolver(this.configDir, this.ServerConfig);
            config.Services.Replace(typeof(IAssembliesResolver), configuredAssembliesResolver);
            config.Services.Replace(typeof(IHttpControllerTypeResolver), new CustomHttpControllerTypeResolver(this.Methods));
            config.Services.Replace(typeof(IHttpControllerSelector), new ConfiguredControllerSelector(config, this.ServerConfig, this.Methods));
            config.Services.Replace(typeof(IHttpControllerActivator), new CustomHttpControllerActivator());
            config.Services.Replace(typeof(IHttpActionSelector), new ConfiguredActionSelector(config, this.Methods));
            config.Services.Replace(typeof(IApiExplorer), new Base2art.WebApiRunner.Internals.ApiExploration.WrappedApiExplorer(config));
            
            
            //            var handler = config.Services.GetExceptionHandler();
            //            config.Services.Replace(typeof(IExceptionHandler), new ConfiguredExceptionHandler(handler));
            
            var mediaTypeMap = CreateMediaTypeMap();
            
            var bindableResolver = config.DependencyResolver as IBindableDependencyResolver;
            
            if (bindableResolver != null)
            {
                var serializer = new JsonSerializerWrapper(config.Formatters.JsonFormatter.SerializerSettings);
                bindableResolver.RegisterInstance(this.ServerConfig, true);
                bindableResolver.RegisterInstance(mediaTypeMap, true);
                bindableResolver.RegisterInstance(new SystemCacheProvider(), true);
                bindableResolver.RegisterInstance(serializer, true);
                bindableResolver.RegisterInstance(new ObjectStringifier(), true);
                bindableResolver.RegisterInstance(new HashService(), true);
                bindableResolver.RegisterInstance(new CustomMethodInterceptAspectProvider(configuredAssembliesResolver, config), true);
            }
        }
        
        protected override void RegisterUserServices(HttpConfiguration config)
        {
            if (!this.ServerConfig.JointInjections().SelectMany(x => x.Items).Any())
            {
                return;
            }
            
            var bindableResolver = config.DependencyResolver as IBindableDependencyResolver;
            if (bindableResolver == null)
            {
                throw new InvalidOperationException("In order to use the built-in injection mechanism you must make your `config.DependencyResolver` implement `IBindableDependencyResolver`");
            }
            
            var asms = config.Services.GetAssembliesResolver().GetAssemblies();
            foreach (var injection in this.ServerConfig.JointInjections().SelectMany(x => x.Items))
            {
                string requestedType = injection.RequestedType;
                string fullfillingType = injection.FulfillingType;
                if (string.IsNullOrWhiteSpace(fullfillingType))
                {
                    fullfillingType = requestedType;
                }
                
                var requestedTypeLoaded = asms.LoadType(requestedType);
                var fulfillingTypeLoaded = asms.LoadType(fullfillingType);
                
                if (requestedTypeLoaded == null)
                {
                    throw new InvalidOperationException("Cannot find Type `" + requestedType + "`");
                }
                
                if (fulfillingTypeLoaded == null)
                {
                    throw new InvalidOperationException("Cannot find Type `" + fullfillingType + "`");
                }
                
                bindableResolver.Register(requestedTypeLoaded, fulfillingTypeLoaded, injection.Parameters, injection.Properties, injection.Singleton);
            }
        }
        
        protected override void RegisterSystemServicesPostUserServiceRegistration(HttpConfiguration config)
        {
            var exceptions = this.ServerConfig.JointApplications().Select(x => x.Exceptions ?? new ExceptionConfiguration()).ToArray();
            
            TrySetupExceptions<Base2art.WebApiRunner.IExceptionLogger, System.Web.Http.ExceptionHandling.IExceptionLogger>(config)
                (exceptions.Where(x => x.Loggers != null).SelectMany(x => x.Loggers).ToArray(), x => new WrappedExceptionLogger(x));
            
            TrySetupExceptions<Base2art.WebApiRunner.IExceptionHandler, System.Web.Http.ExceptionHandling.IExceptionHandler>(config)
                (exceptions.Where(x => x.Handlers != null).SelectMany(x => x.Handlers).ToArray(), x => new WrappedExceptionHandler(x));
            
            
            var apps = this.ServerConfig.JointApplications();
            if (!apps.Any(x => (x.Exceptions ?? new ExceptionConfiguration()).AllowStackTraceInOutput))
            {
                //                TrySetupExceptions<Base2art.WebApiRunner.IExceptionHandler, System.Web.Http.ExceptionHandling.IExceptionHandler>(config)
                //                    (new InstanceConfiguration[]{masker}, x => new WrappedExceptionHandler(x));
                //                config.Services.Replace(serviceType, func(logger.Instance));
                var type = typeof(System.Web.Http.ExceptionHandling.IExceptionHandler);
                var handler = config.Services.GetExceptionHandler();
                config.Services.Replace(type, new MaskExceptionsFilter(config, handler));
            }
        }
        
        protected override void RegisterSwagger(HttpConfiguration config)
        {
            config
                .EnableSwagger(c =>
                               {
                                   c.GroupActionsBy(this.GetSwaggerActionGrouper(config));
                                   c.IgnoreObsoleteProperties();
                                   c.IgnoreObsoleteActions();
                                   c.UseFullTypeNameInSchemaIds();
                                   c.OperationFilter<AddDefaultResponse>();
                                   c.DescribeAllEnumsAsStrings();
                                   c.SingleApiVersion("v1", this.ApiName);
                               })
                
                .EnableSwaggerUi();
        }

        protected override void RegisterMiddleware(HttpConfiguration config)
        {
            config.RunMiddleware(
                this.ServerConfig.JointMiddleware(),
                typeof(IMiddleware<>),
                (x) => new object []{ config, x });
        }
        
        protected override void RegisterBindings(HttpConfiguration config)
        {
            var items = this.ServerConfig.JointApplications()
                .SelectMany(x => x.ModelBindings ?? new List<ModelBindingConfiguration>())
                .Where(x => !string.IsNullOrWhiteSpace(x.Type))
                .Where(x => !string.IsNullOrWhiteSpace(x.BoundType))
                .ToList();
            var asms = config.Services.GetAssembliesResolver().GetAssemblies();
            
            foreach (var element in items)
            {
                var binderType = asms.LoadType(element.Type);
                var boundType = asms.LoadType(element.BoundType);
                
                Func<IModelBinder> create = () => (IModelBinder)binderType.CreateFrom(config.DependencyResolver, element.Parameters, element.Properties);
                
                this.BindModel(config, boundType, create);
            }
            
            this.BindModel<TextWriter>(config, () => new PrinterWriterModelBinder());
            
            this.BindModel<string[]>(config, () => new SpecializedArrayModelBinder<string>());
            
            this.BindModel<Guid[]>(config, () => new SpecializedArrayModelBinder<Guid>());
            this.BindModel<long[]>(config, () => new SpecializedArrayModelBinder<long>());
            this.BindModel<int[]>(config, () => new SpecializedArrayModelBinder<int>());
            this.BindModel<DateTime[]>(config, () => new SpecializedArrayModelBinder<DateTime>());
            this.BindModel<DateTimeOffset[]>(config, () => new SpecializedArrayModelBinder<DateTimeOffset>());
            this.BindModel<TimeSpan[]>(config, () => new SpecializedArrayModelBinder<TimeSpan>());
            this.BindModel<short[]>(config, () => new SpecializedArrayModelBinder<short>());
            this.BindModel<decimal[]>(config, () => new SpecializedArrayModelBinder<decimal>());
            this.BindModel<float[]>(config, () => new SpecializedArrayModelBinder<float>());
            this.BindModel<double[]>(config, () => new SpecializedArrayModelBinder<double>());
            this.BindModel<bool[]>(config, () => new SpecializedArrayModelBinder<bool>());
            
            this.BindModel<Guid?[]>(config, () => new SpecializedArrayModelBinder<Guid?>());
            this.BindModel<long?[]>(config, () => new SpecializedArrayModelBinder<long?>());
            this.BindModel<int?[]>(config, () => new SpecializedArrayModelBinder<int?>());
            this.BindModel<DateTime?[]>(config, () => new SpecializedArrayModelBinder<DateTime?>());
            this.BindModel<DateTimeOffset?[]>(config, () => new SpecializedArrayModelBinder<DateTimeOffset?>());
            this.BindModel<TimeSpan?[]>(config, () => new SpecializedArrayModelBinder<TimeSpan?>());
            this.BindModel<short?[]>(config, () => new SpecializedArrayModelBinder<short?>());
            this.BindModel<decimal?[]>(config, () => new SpecializedArrayModelBinder<decimal?>());
            this.BindModel<float?[]>(config, () => new SpecializedArrayModelBinder<float?>());
            this.BindModel<double?[]>(config, () => new SpecializedArrayModelBinder<double?>());
            this.BindModel<bool?[]>(config, () => new SpecializedArrayModelBinder<bool?>());
        }
        
        protected override void RegisterFilters(HttpConfiguration config)
        {
            var map = config.DependencyResolver.GetService<IMediaTypeMap>();
            
            // These are executed backward...
            
            config.Filters.Add(new FileInfoFilter(map ?? this.CreateMediaTypeMap()));
            config.Filters.Add(new AcceptFilter());
            
            var apps = this.ServerConfig.JointApplications();
            if (apps.Any(x => (x.Exceptions ?? new ExceptionConfiguration()).RegisterCommonHandlers))
            {
                config.Filters.Add(new NotImplementedExceptionFilter());
                config.Filters.Add(new NotAuthenticatedExceptionFilter());
                config.Filters.Add(new NotAuthorizedExceptionFilter());
                config.Filters.Add(new ConflictExceptionFilter());
                config.Filters.Add(new UnprocessableEntityExceptionFilter());
            }
            
            foreach (var item in apps.SelectMany(x => (x.Exceptions?? new ExceptionConfiguration()).ResponseMapItems ?? new List<ExceptionMapItemConfiguration>()))
            {
                config.Filters.Add(new ConfigBasedExceptionResponseMapFilter(config, item));
            }
            
            var asms = config.Services.GetAssembliesResolver().GetAssemblies();
            
            var filterLookupList = new Func<object, IFilter>[] {
                (x) => this.ConvertableDuck<ISetCookieFilter<object>, IFilter>(x, duck => new CookieFilter(duck)),
                (x) => this.ConvertableDuck<IRedirectFilter<object>, IFilter>(x, duck => new RedirectFilter(duck)),
                (x) => this.ConvertableDuck<ISetHeadersFilter<object>, IFilter>(x, duck => new HeaderFilter(duck)),
                (x) => this.ConvertableDuck<ISetStatusCodeFilter<object>, IFilter>(x, duck => new StatusCodeFilter(duck)),
            };
            
            
            config.Filters.AddRange(
                apps.SelectMany(y => (y.Filters ?? new List<InstanceConfiguration>()).Select(x => asms.LoadType(x.Type).ProxiedCreateFrom(
                    config.DependencyResolver,
                    x.Parameters,
                    x.Properties,
                    filterLookupList))));
        }
        
        protected override void RegisterCors(HttpConfiguration config)
        {
            if (this.ServerConfig.JointCors().Any(x => x.Enabled))
            {
                var provider = new CorsPolicyFactory(this.ServerConfig.JointCors().SelectMany(x => x.Items ?? new List<CorsItemConfiguration>()).ToList());
                //                config.SetCorsPolicyProviderFactory(provider);
                config.MessageHandlers.Add(new AllowOptionsHandler(provider));
                // config.SetCorsPolicyProviderFactory(provider);
                // config.SetCorsEngine(config.GetCorsEngine());
                //                config.EnableCors(provider.Provider);
            }
        }
        
        protected override void RegisterRoutes(HttpConfiguration config)
        {
            foreach (var itemContainer in this.Methods)
            {
                var item = itemContainer.MethodConfiguration;
                var verb = string.IsNullOrWhiteSpace(item.Verb) ? HttpMethod.Get : new HttpMethod(item.Verb.ToUpper());
                
                string name = item.Url + ":" + item.Type + "." + item.Method;
                try
                {
                    config.Routes.MapHttpRoute(
                        name: name,
                        routeTemplate: item.Url,
                        
                        constraints: new
                        {
                            httpMethod = new HttpMethodConstraint(verb)
                        },
                        defaults: new
                        {
                            controller = ConfiguredControllerSelector.Controller(item.Type, this.ServerConfig.JointApplications().SelectMany(x => x.ControllerSuffixes ?? new List<string>()).ToList()),
                            controllerClassName = item.Type.ToString(),
                            controllerProperties = item.Properties,
                            controllerParameters = item.Parameters,
                            controllerType = itemContainer.ControllerType,
                            action = item.Method,
                            actionVerb = verb,
                        });
                }
                catch (ArgumentException)
                {
                    Console.WriteLine("Duplicate Route Name: " + name);
                    throw;
                }
            }
        }

        protected virtual Func<System.Web.Http.Description.ApiDescription, string> GetSwaggerActionGrouper(HttpConfiguration configuration)
        {
            Func<IHttpRoute, ControllerType, bool> routeIs = (z, type) => z.Defaults.ContainsKey("controllerType") && type.Equals(z.Defaults["controllerType"]);
            
            Func<string, string> parse = (x) =>
            {
                var url = new string(x.TakeWhile(y => y != '{').ToArray());
                url = url.TrimEnd(new char[]{ '/' });
                
                return url;
            };
            
            
            Func<Dictionary<string, string>> lookup = () =>
            {
                
                var map = configuration.Routes.Where(x => routeIs(x, ControllerType.Service))
                    .Select(x => x.RouteTemplate)
                    .Select(parse)
                    .ToList();
                
                
                var urlSet = new Dictionary<string, string>();
                
                foreach (var mapItem in map)
                {
                    urlSet[mapItem] = mapItem;
                }
                
                foreach (var mapItem in map)
                {
                    var parts = mapItem.Split(new char[]{ '/', '\\' }, StringSplitOptions.RemoveEmptyEntries);
                    for (int i = 1; i < parts.Length; i++)
                    {
                        var newMapItem = string.Join("/", parts.Take(i));
                        if (urlSet.ContainsKey(newMapItem))
                        {
                            urlSet[mapItem] = newMapItem;
                        }
                    }
                }
                
                return urlSet;
            };
            
            
            return apiDesc =>
            {
                var urlSet = lookup();
                var route = apiDesc.Route;
                if (routeIs(route, ControllerType.Task))
                {
                    return "Tasks";
                }
                
                if (routeIs(route, ControllerType.HealthCheck))
                {
                    return "Healthchecks";
                }
                
                var parsed = parse(route.RouteTemplate);
                return urlSet.ContainsKey(parsed) ? urlSet[parsed] : parsed;
            };
        }
        
        private void BindModel<T>(HttpConfiguration config, Func<IModelBinder> binder)
        {
            BindModel(config, typeof(T), binder);
        }

        private void BindModel(HttpConfiguration config, Type bouldType, Func<IModelBinder> binder)
        {
            var twProvider = new SimpleModelBinderProvider(bouldType, binder);
            config.Services.Insert(typeof(ModelBinderProvider), 0, twProvider);
            
            config.ParameterBindingRules.Insert(0, bouldType, (param) => param.BindWithModelBinding(binder()));
        }
        
        private Action<InstanceConfiguration[], Func<T, TOut>> TrySetupExceptions<T, TOut>(HttpConfiguration config)
        {
            Action<InstanceConfiguration[], Func<T, TOut>> serviceLoader = (cns, func) =>
            {
                foreach (var cn in cns)
                {
                    if (cn != null)
                    {
                        var serviceType = typeof(TOut);
                        
                        var asms = config.Services.GetAssembliesResolver().GetAssemblies();
                        var type = asms.LoadType(cn.Type);
                        var item = type.CreateFrom(config.DependencyResolver, cn.Parameters, cn.Properties);
                        
                        if (serviceType.IsAssignableFrom(type))
                        {
                            config.Services.Replace(serviceType, item);
                        }
                        else
                        {
                            var logger = item.AsDuck<T>();
                            if (logger.IsDuck)
                            {
                                config.Services.Replace(serviceType, func(logger.Instance));
                            }
                        }
                    }
                }
            };
            
            return serviceLoader;
        }

        private IMediaTypeMap CreateMediaTypeMap()
        {
            var map = new MediaTypeMap();
            var mediaTypes = this.ServerConfig.JointApplications()
                .SelectMany(z => (z.MediaTypes ?? new Dictionary<string, string>()).Where(x => !string.IsNullOrWhiteSpace(x.Key))
                            .Where(x => !string.IsNullOrWhiteSpace(x.Value)));
            
            foreach (var mediaType in mediaTypes)
            {
                map.AddMapping(mediaType.Key, mediaType.Value);
            }
            
            return map;
        }

        private TOut ConvertableDuck<TIn, TOut>(object instance, Func<IGenericDuckType<TIn>, TOut> inOut)
            where TOut : class
        {
            var duck = instance.AsGenericDuck<TIn>();
            if (duck.IsDuck)
            {
                return inOut(duck);
            }
            
            return null;
        }
    }
}

