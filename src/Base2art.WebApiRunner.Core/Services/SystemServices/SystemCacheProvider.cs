﻿
namespace Base2art.WebApiRunner.Services.SystemServices
{
    using System;
    using System.Runtime.Caching;
    using Base2art.WebApiRunner.SystemServices;
    
    public class SystemCacheProvider : ICacheProvider
    {
        private static object padLock = new object();

        private static MemoryCache defaultCache;

        public static MemoryCache Default
        {
            get
            {
                if (SystemCacheProvider.defaultCache == null)
                {
                    object obj = SystemCacheProvider.padLock;
                    lock (obj)
                    {
                        if (SystemCacheProvider.defaultCache == null)
                        {
                            SystemCacheProvider.defaultCache = new MemoryCache("Default-" + Guid.NewGuid().ToString("N"));
                        }
                    }
                }
                
                return SystemCacheProvider.defaultCache;
            }
        }
        
        public ICache<string, object> GetSystemCache()
        {
            return new SystemCache<string, object>(TimeSpan.FromMinutes(10), () => SystemCacheProvider.Default);
        }

        public ICache<TKey, TValue> CreateCache<TKey, TValue>(TimeSpan defaultKeepForDuration)
        {
            return new SystemCache<TKey, TValue>(defaultKeepForDuration);
        }
        
        private class SystemCache<TKey, TValue> : ICache<TKey, TValue>
        {
            private Lazy<MemoryCache> memoryCache;

            private readonly TimeSpan defaultKeepForDuration;
            
            private readonly Func<MemoryCache> cache;

            public SystemCache(TimeSpan defaultKeepForDuration)
            {
                this.defaultKeepForDuration = defaultKeepForDuration;
                this.cache = () => new MemoryCache(DateTime.UtcNow.Ticks + ":" + typeof(TValue).FullName);
                this.Init();
            }
            
            public SystemCache(TimeSpan defaultKeepForDuration, Func<MemoryCache> memoryCache)
            {
                if (memoryCache == null)
                {
                    throw new ArgumentNullException("memoryCache");
                }

                this.defaultKeepForDuration = defaultKeepForDuration;
                this.cache = memoryCache;
                this.Init();
            }

            protected MemoryCache Cache
            {
                get
                {
                    return this.memoryCache.Value;
                }
            }

            public TValue GetItem(TKey key)
            {
                var cacheItem = this.Cache.GetCacheItem(this.ToKey(key));
                if (cacheItem != null)
                {
                    return (TValue)cacheItem.Value;
                }

                return default(TValue);
            }

            public bool HasItem(TKey key)
            {
                return this.Cache.Contains(this.ToKey(key));
            }

            public void Upsert(TKey key, TValue value)
            {
                this.Cache.Set(this.ToKey(key), value, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.Add(this.defaultKeepForDuration) });
            }

            public void Upsert(TKey key, TValue value, TimeSpan keepForDuration)
            {
                this.Cache.Set(this.ToKey(key), value, new CacheItemPolicy { AbsoluteExpiration = DateTimeOffset.Now.Add(keepForDuration) });
            }
            
            public void Clear()
            {
                var oldCache = this.Cache;
                
                if(SystemCacheProvider.defaultCache == oldCache)
                {
                    SystemCacheProvider.defaultCache = null;
                }
                
                this.Init();
                
                if (oldCache != null)
                {
                    oldCache.Dispose();
                }
            }
            
            protected virtual string ToKey(TKey key)
            {
                return key.ToString();
            }

            private void Init()
            {
                this.memoryCache = new Lazy<MemoryCache>(this.cache);
            }
        }
    }
}
