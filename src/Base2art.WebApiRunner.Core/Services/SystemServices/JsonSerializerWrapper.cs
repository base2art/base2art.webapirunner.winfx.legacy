﻿
namespace Base2art.WebApiRunner.Services.SystemServices
{
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.SystemServices;
    
    public class JsonSerializerWrapper : IJsonSerializer
    {
        private readonly ILazy<Newtonsoft.Json.JsonSerializer> indentedFormatter;
        private readonly ILazy<Newtonsoft.Json.JsonSerializer> nondentedFormatter;
        
        public JsonSerializerWrapper(Newtonsoft.Json.JsonSerializerSettings settings)
        {
            this.indentedFormatter = new RetryLazy<Newtonsoft.Json.JsonSerializer>(() => CreateSerializer(true, settings));
            this.nondentedFormatter = new RetryLazy<Newtonsoft.Json.JsonSerializer>(() => CreateSerializer(false, settings));
        }
        
        public Task<string> SerializeObjectFormattedAsync<T>(T data)
        {
            return this.SerializeObjectAsyncInternal(data, typeof(T), true);
        }

        public Task<string> SerializeObjectAsync<T>(T data)
        {
            return this.SerializeObjectAsyncInternal(data, typeof(T), false);
        }

        public Task<string> SerializeObjectFormattedAsync(object data, System.Type type)
        {
            return this.SerializeObjectAsyncInternal(data, type, true);
        }
        
        public Task<string> SerializeObjectAsync(object data, System.Type type)
        {
            return this.SerializeObjectAsyncInternal(data, type, false);
        }
        
        public async Task<T> DeserializeObjectAsync<T>(string data)
        {
            return (T)await this.DeserializeObjectAsync(data, typeof(T));
        }

        public async Task<object> DeserializeObjectAsync(string data, System.Type parameterType)
        {
            var serializer = GetSerializer(false);
            
            using (MemoryStream ms = new MemoryStream())
            {
                using (var writer = new StreamWriter(ms))
                {
                    await writer.WriteLineAsync(data);
                    
                    await writer.FlushAsync();
                    ms.Seek(0L, SeekOrigin.Begin);
                    
                    using (var sr = new StreamReader(ms))
                    {
                        using (var reader = new Newtonsoft.Json.JsonTextReader(sr))
                        {
                            return serializer.Deserialize(reader, parameterType);
                        }
                    }
                }
            }
        }
        
        public string SerializeObjectFormatted<T>(T data)
        {
            return this.SerializeObjectInternal(data, typeof(T), true);
        }

        public string SerializeObject<T>(T data)
        {
            return this.SerializeObjectInternal(data, typeof(T), false);
        }

        public string SerializeObjectFormatted(object data, System.Type type)
        {
            return this.SerializeObjectInternal(data, type, true);
        }
        
        public string SerializeObject(object data, System.Type type)
        {
            return this.SerializeObjectInternal(data, type, false);
        }
        
        public T DeserializeObject<T>(string data)
        {
            return (T)this.DeserializeObject(data, typeof(T));
        }

        public object DeserializeObject(string data, System.Type parameterType)
        {
            var serializer = GetSerializer(false);
            
            using (MemoryStream ms = new MemoryStream())
            {
                using (var writer = new StreamWriter(ms))
                {
                    writer.WriteLine(data);
                    
                    writer.Flush();
                    ms.Seek(0L, SeekOrigin.Begin);
                    
                    using (var sr = new StreamReader(ms))
                    {
                        using (var reader = new Newtonsoft.Json.JsonTextReader(sr))
                        {
                            return serializer.Deserialize(reader, parameterType);
                        }
                    }
                }
            }
        }

        private string SerializeObjectInternal(object data, System.Type type, bool formatting)
        {
            var serializer = GetSerializer(formatting);
            
            using (MemoryStream ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms))
                {
                    using (var writer = new Newtonsoft.Json.JsonTextWriter(sw))
                    {
                        serializer.Serialize(writer, data, type);
                        writer.Flush();
                        ms.Seek(0L, SeekOrigin.Begin);
                        
                        using (var reader = new StreamReader(ms))
                        {
                            return reader.ReadToEnd();
                        }
                    }
                }
            }
        }

        private Task<string> SerializeObjectAsyncInternal(object data, System.Type type, bool formatting)
        {
            var serializer = GetSerializer(formatting);
            
            using (MemoryStream ms = new MemoryStream())
            {
                using (var sw = new StreamWriter(ms))
                {
                    using (var writer = new Newtonsoft.Json.JsonTextWriter(sw))
                    {
                        serializer.Serialize(writer, data, type);
                        writer.Flush();
                        ms.Seek(0L, SeekOrigin.Begin);
                        
                        using (var reader = new StreamReader(ms))
                        {
                            return reader.ReadToEndAsync();
                        }
                    }
                }
            }
        }
        
        private Newtonsoft.Json.JsonSerializer GetSerializer(bool indenting)
        {
            return indenting ? this.indentedFormatter.Value : this.nondentedFormatter.Value;
        }
        
        private static Newtonsoft.Json.JsonSerializer CreateSerializer(bool indenting, Newtonsoft.Json.JsonSerializerSettings settings)
        {
            var serializer = Newtonsoft.Json.JsonSerializer.Create(settings); 
             serializer.Formatting =  indenting 
                 ? Newtonsoft.Json.Formatting.Indented
                 : Newtonsoft.Json.Formatting.None;
            
            return serializer;
        }
    }
}