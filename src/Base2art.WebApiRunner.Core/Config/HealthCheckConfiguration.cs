﻿namespace Base2art.WebApiRunner.Config
{
    using System;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class HealthCheckConfiguration : InstanceConfiguration
    {
        public string Name { get; set; }
    }
}



