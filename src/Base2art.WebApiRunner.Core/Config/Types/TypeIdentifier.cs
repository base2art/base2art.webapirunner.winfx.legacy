﻿namespace Base2art.WebApiRunner.Config.Types
{
    using System.ComponentModel;
    using Base2art.WebApiRunner.Config.TypeConverters;

    /// <summary>
    /// A string that represents a full class name.
    /// </summary>
    /// <example>
    /// System.Data.SqlClient.SqlConnection
    /// </example>
    /// <example>
    /// System.Data.SqlClient.SqlConnection, System.Data
    /// </example>
    [InspectableAttribute(true, false)]
    [TypeConverter(typeof(TypeIdentifierTypeConverter))]
    public class TypeIdentifier
    {
        private readonly string value;

        /// <summary>
        /// Initializes a new instance of the TypeIdentifier class.
        /// </summary>
        /// <param name="value">The actual value.</param>
        public TypeIdentifier(string value)
        {
            this.value = value;
        }

        /// <summary>
        /// The backing value.
        /// </summary>
        public string Value
        {
            get { return value; }
        }

        
        ///
        ///      <summary>Returns a <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.</summary>
        ///      <returns>A <see cref="T:System.String" /> that represents the current <see cref="T:System.Object" />.</returns>
        ///      <filterpriority>2</filterpriority>
        ///
        public override string ToString()
        {
            return this.value;
        }

        /// <summary>
        /// Converts the object to a string.
        /// </summary>
        /// <param name="d"></param>
        /// <returns></returns>
        public static implicit operator string(TypeIdentifier d)
        {
            return d == null ? null : d.ToString();
        }
    }
}
