﻿namespace Base2art.WebApiRunner.Config.Types
{
    using System;

    [AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
    public class InspectableAttribute : Attribute
    {
        private readonly bool isInspectable;

        private readonly bool areChildrenInspectable;
        
        public InspectableAttribute(bool isInspectable, bool areChildrenInspectable)
        {
            this.areChildrenInspectable = areChildrenInspectable;
            this.isInspectable = isInspectable;
        }

        public bool IsInspectable
        {
            get { return this.isInspectable; }
        }

        public bool AreChildrenInspectable
        {
            get { return this.areChildrenInspectable; }
        }
    }
}
