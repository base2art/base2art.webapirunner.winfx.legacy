﻿namespace Base2art.WebApiRunner.Config.Types
{
    using System.Collections.Generic;
    
    /// <summary>
    /// A list of application-root-relative paths, to other onfiguration files.
    /// </summary>
    [InspectableAttribute(true, false)]
    public class PathList : List<string>
    {
    }
}
