namespace Base2art.WebApiRunner.Config
{
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class ApplicationConfiguration
    {
        public List<InstanceConfiguration> Filters { get; set; }

        public List<ModelBindingConfiguration> ModelBindings { get; set; }
        
        public Dictionary<string,string> MediaTypes { get; set; }
        
        public ExceptionConfiguration Exceptions { get; set; }
        
        public List<string> ControllerSuffixes { get; set; }
    }
}
