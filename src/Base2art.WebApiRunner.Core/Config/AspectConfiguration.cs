﻿namespace Base2art.WebApiRunner.Config
{
    using System;
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class AspectConfiguration
    {
        public string Name { get; set; }
        public Dictionary<string, object> Parameters { get; set; }
    }
}


