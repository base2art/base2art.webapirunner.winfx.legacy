﻿namespace Base2art.WebApiRunner.Config
{
    using System;
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class TasksConfiguration
    {
        public List<TaskConfiguration> Items { get; set; }
        
        public AspectsConfiguration Aspects { get; set; }
    }
}
