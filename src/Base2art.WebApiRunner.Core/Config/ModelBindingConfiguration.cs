﻿
namespace Base2art.WebApiRunner.Config
{
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class ModelBindingConfiguration : InstanceConfiguration
    {
        // ci
        public TypeIdentifier BoundType { get; set; }
    }
}
