﻿namespace Base2art.WebApiRunner.Config
{
    using System;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class TaskConfiguration : InstanceConfiguration
    {
        // ci
        
        public string Name { get; set; }
        
        public TimeSpan Delay { get; set; }
        
        public TimeSpan Interval { get; set; }
        
        public AspectsConfiguration Aspects { get; set; }
    }
}

