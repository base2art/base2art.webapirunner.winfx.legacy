﻿namespace Base2art.WebApiRunner.Config
{
    using System;
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, false)]
    public class AspectsConfiguration : List<AspectConfiguration>
    {
    }
}
