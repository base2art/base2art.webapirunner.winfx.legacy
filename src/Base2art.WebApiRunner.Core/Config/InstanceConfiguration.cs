﻿
namespace Base2art.WebApiRunner.Config
{
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class InstanceConfiguration
    {
        // ci
        public TypeIdentifier Type { get; set; }

        public Dictionary<string, string> Parameters { get; set; }

        public Dictionary<string, object> Properties { get; set; }
    }
}




