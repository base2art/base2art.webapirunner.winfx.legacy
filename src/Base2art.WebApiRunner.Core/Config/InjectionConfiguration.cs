﻿namespace Base2art.WebApiRunner.Config
{
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class InjectionConfiguration
    {
        public List<InjectionItemConfiguration> Items { get; set; }
    }
}
