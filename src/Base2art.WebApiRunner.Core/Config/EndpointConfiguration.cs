namespace Base2art.WebApiRunner.Config
{
    using System;
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class EndpointConfiguration
    {
        public List<MethodConfiguration> Urls { get; set; }
        
        public AspectsConfiguration Aspects { get; set; }
    }
}