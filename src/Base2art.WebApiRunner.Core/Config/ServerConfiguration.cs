namespace Base2art.WebApiRunner.Config
{
    using System;
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public abstract class ServerConfiguration
    {
        
        public abstract ConfigIniter Initer();

        public EndpointConfiguration Endpoints { get; set; }
        
        public PathList EndpointsConfigurationItems { get; set; }

        public TasksConfiguration Tasks { get; set; }
        
        public PathList TasksConfigurationItems { get; set; }

        public HealthChecksConfiguration HealthChecks { get; set; }
        
        public PathList HealthChecksConfigurationItems { get; set; }
        
        public ApplicationConfiguration Application { get; set; }
        
        public PathList ApplicationConfigurationItems { get; set; }
        
        public InjectionConfiguration Injection { get; set; }
        
        public PathList InjectionConfigurationItems { get; set; }
        
        // ci
        public TypeIdentifier DependencyResolverTypeName  { get; set; }
        
        public List<MiddlewareConfiguration> Middleware { get; set; }
        
        public PathList MiddlewareConfigurationItems { get; set; }
        
        public List<string> Assemblies { get; set; }
        
        public PathList AssembliesConfigurationItems { get; set; }

        public List<string> AssemblySearchPaths { get; set; }
        
        public PathList AssemblySearchPathsConfigurationItems { get; set; }

        public CorsConfiguration Cors { get; set; }
        
        public PathList CorsConfigurationItems { get; set; }
    }
}
