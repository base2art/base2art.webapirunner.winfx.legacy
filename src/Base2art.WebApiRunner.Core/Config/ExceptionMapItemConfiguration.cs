namespace Base2art.WebApiRunner.Config
{
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    
    public class ExceptionMapItemConfiguration
    {
        // ci
        public TypeIdentifier ExceptionType { get; set; }
        
        public int HttpResponseCode { get; set; }
        
        public string ResponseCode { get; set; }
        
        public string ResponseMessage { get; set; }
        
        public string ResponseMessageExceptionProperty { get; set; }
    }
}
