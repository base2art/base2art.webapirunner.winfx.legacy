﻿
namespace Base2art.WebApiRunner.Config
{
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class InjectionItemConfiguration
    {
        // ci
        public TypeIdentifier RequestedType { get; set; }

        public TypeIdentifier FulfillingType { get; set; }

        public Dictionary<string, object> Parameters { get; set; }
        
        public Dictionary<string, object> Properties { get; set; }

        public bool Singleton { get; set; }
    }
}


