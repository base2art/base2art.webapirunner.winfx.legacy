﻿namespace Base2art.WebApiRunner.Config
{
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class CorsConfiguration
    {
        public bool Enabled { get; set; }
        
        public List<CorsItemConfiguration> Items { get; set; }
    }
}
