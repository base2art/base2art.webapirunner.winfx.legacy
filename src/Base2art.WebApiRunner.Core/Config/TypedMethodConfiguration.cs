﻿namespace Base2art.WebApiRunner.Config
{
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(false, false)]
    public class TypedMethodConfiguration
    {
        public MethodConfiguration MethodConfiguration { get; set; }

        public ControllerType ControllerType { get; set; }
    }
}
