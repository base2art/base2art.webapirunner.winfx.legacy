﻿namespace Base2art.WebApiRunner.Config
{
    using System;
    using System.Collections.Generic;
    using Base2art.WebApiRunner.Config.Types;

    [InspectableAttribute(true, true)]
    public class ExceptionConfiguration
    {
        public bool RegisterCommonHandlers { get; set; }
        
        public bool AllowStackTraceInOutput { get; set; }
        
        public List<InstanceConfiguration> Handlers { get; set; }
        
        public List<InstanceConfiguration> Loggers { get; set; }
        
        public List<ExceptionMapItemConfiguration> ResponseMapItems { get; set; }
    }
}
