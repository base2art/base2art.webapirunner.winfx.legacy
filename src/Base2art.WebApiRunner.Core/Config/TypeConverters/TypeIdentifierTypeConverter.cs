﻿
namespace Base2art.WebApiRunner.Config.TypeConverters
{
    using System;
    using System.ComponentModel;
    using Base2art.WebApiRunner.Config.Types;
    
    // ci
    public class TypeIdentifierTypeConverter: TypeConverter
    {
        public override bool CanConvertFrom(ITypeDescriptorContext context, Type sourceType)
        {
            if (sourceType == typeof(string))
            {
                return true;
            }
            
            return base.CanConvertFrom(context, sourceType);
        }
        
        public override object ConvertFrom(ITypeDescriptorContext context, System.Globalization.CultureInfo culture, object value)
        {
            if (value is string)
            {
                string s = value.ToString();
                return new TypeIdentifier(s);
            }
            return base.ConvertFrom(context, culture, value);
        }
    }
}
