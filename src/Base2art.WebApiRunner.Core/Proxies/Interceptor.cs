﻿
using System;
using System.Linq;
using System.Linq.Expressions;
namespace Base2art.WebApiRunner.Proxies
{
    public static class Interceptor
    {
        public static InterceptingProxy<TResult> Proxy<TResult>(Expression<Func<TResult>> callback)
        {
            return new InterceptingProxy<TResult>(callback);
        }
        
        public static InterceptingProxy<TIn1, TResult> Proxy<TIn1, TResult>(Expression<Func<TIn1, TResult>> callback)
        {
            return new InterceptingProxy<TIn1, TResult>(callback);
        }
    }
    
    public interface IInterceptor
    {
        
    }
    
    public class InterceptingProxy<T>
    {
        private readonly Expression callback;
        private readonly IInterceptor[] interceptors;

        public InterceptingProxy(Expression callback)
        {
            this.callback = callback;
            this.interceptors = new IInterceptor[0];
        }

        protected InterceptingProxy(InterceptingProxy<T> parent, IInterceptor interceptor)
        {
            this.interceptors = parent.interceptors.Union(new []{ interceptor }).ToArray();
            this.callback = parent.callback;
        }
        
        public InterceptingProxy<T> With<TNew>()
            where TNew : IInterceptor, new()
        {
            return new InterceptingProxy<T>(this, new TNew());
        }

        public T Invoke()
        {
            return (T)Expression.Invoke(this.callback).Expression();
//            return this.callback.Compile().Invoke();
        }
    }
    
    public class InterceptingProxy<TIn, TResult>
    {
        private readonly Expression<Func<TIn, TResult>> callback;
        private readonly IInterceptor[] interceptors;

        public InterceptingProxy(Expression<Func<TIn, TResult>> callback)
        {
            this.callback = callback;
            this.interceptors = new IInterceptor[0];
        }
    }
}
