﻿
namespace Base2art.WebApiRunner.Proxies
{
    using System.Threading.Tasks;
    using System.Web.Http.ExceptionHandling;

    public class WrappedExceptionLogger : ExceptionLogger
    {
        private readonly Base2art.WebApiRunner.IExceptionLogger obj;

        public WrappedExceptionLogger(Base2art.WebApiRunner.IExceptionLogger obj)
        {
            this.obj = obj;
        }

        public async override Task LogAsync(ExceptionLoggerContext context, System.Threading.CancellationToken cancellationToken)
        {
            if (this.obj != null)
            {
                await this.obj.LogAsync(context.Exception);
            }
            
            await base.LogAsync(context, cancellationToken);
            
            
        }
    }
}


