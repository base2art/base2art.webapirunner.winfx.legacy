﻿
namespace Base2art.WebApiRunner.Proxies
{
    using System;
    using System.Linq;
    using Base2art.WebApiRunner.Proxies.Internals;
    
    public static class Ducker
    {
        public static IDuckType<TOut> AsDuck<TOut>(this object input)
        {
            return new ProxyDuckType<TOut>(input);
        }
        
        public static IGenericDuckType<TOut> AsGenericDuck<TOut>(this object input)
        {
            return new GenericProxyDuckType<TOut>(input);
        }
    }
    
}
