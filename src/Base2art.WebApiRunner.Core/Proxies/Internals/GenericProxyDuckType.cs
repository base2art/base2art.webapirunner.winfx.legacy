﻿
namespace Base2art.WebApiRunner.Proxies.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public class GenericProxyDuckType<T> : ProxyDuckType<T>, IGenericDuckType<T>
    {
        public GenericProxyDuckType(object backing)
            : base(backing)
        {
        }
        
        protected override System.Runtime.Remoting.Proxies.RealProxy CreateProxy(object backingValue, IDictionary<MethodInfo, MethodInfo> methodMap)
        {
            return new GenericProxy<T>(backingValue, methodMap);
        }
        
        protected override Type GetTargetType<TIn>()
        {
            return base.GetTargetType<TIn>().GetGenericTypeDefinition();
        }

        public bool SupportsType<TIn>()
        {
            return this.SupportsType(typeof(TIn));
        }
        
        public bool SupportsType(Type type)
        {
            if (!this.IsDuck)
            {
                return false;
            }
            
            var currentMatches = this.MethodMap;
            foreach (var match in currentMatches)
            {
                var @in = match.Key;
                var @out = match.Value;
                
                if (@in.ReturnType.IsGenericParameter)
                {
                    // compare to implementor!
                    var returns = this.GetGenericTypes(@in, @in.ReturnType, @out.ReturnType);
                    if (!returns.All(z => z.IsAssignableFrom(type)))
                    {
                        return false;
                    }
                }
                
                var inParms = @in.GetParameters();
                var outParms = @out.GetParameters();
                for (int i = 0; i < inParms.Length; i++)
                {
                    var inParm = @inParms[i];
                    var outParm = @outParms[i];
                    
                    if (inParm.ParameterType.IsGenericParameter)
                    {
                        // compare to implementor!
                        var returns = this.GetGenericTypes(@in, inParm.ParameterType, outParm.ParameterType);
                        if (!returns.All(z => z.IsAssignableFrom(type)))
                        {
                            return false;
                        }
                    }
                }
            }
            
            return true;
        }
    }
}




