﻿
namespace Base2art.WebApiRunner.Proxies.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Runtime.Remoting.Proxies;
    
    public class ProxyDuckType<T> : IDuckType<T>
    {
        private readonly object backing;

        private readonly Lazy<Dictionary<MethodInfo, MethodInfo>> matches;

        private readonly Lazy<T> proxy;
        
        public ProxyDuckType(object backing)
        {
            this.backing = backing;
            this.matches = new Lazy<Dictionary<MethodInfo, MethodInfo>>(this.CalculateMatches);
            this.proxy = new Lazy<T>(() => (T)this.CreateProxy(this.backing, this.matches.Value).GetTransparentProxy());
        }

        public bool IsDuck
        {
            get
            {
                
                return this.matches.Value != null;
            }
        }

        public T Instance
        {
            get
            {
                if (this.matches.Value == null)
                {
                    return default(T);
                }
                
                if (this.backing == null)
                {
                    return default(T);
                }
                
                return this.proxy.Value;
            }
        }

        protected IReadOnlyDictionary<MethodInfo, MethodInfo> MethodMap
        {
            get { return this.matches.Value; }
        }

        protected virtual RealProxy CreateProxy(object backingValue, IDictionary<MethodInfo, MethodInfo> methodMap)
        {
            return new SimpleProxy<T>(backingValue, methodMap);
        }

        protected virtual Type GetTargetType<TIn>()
        {
            return typeof(TIn);
        }
        
        protected bool NamesMatch(MethodInfo x, MethodInfo y)
        {
            return x.Name == y.Name;
        }

        protected bool ReturnsMatch(MethodInfo x, MethodInfo y)
        {
            var types = this.GetGenericTypes(y, y.ReturnType, y.ReturnType);
            
            return types.All(z => z.IsAssignableFrom(x.ReturnType));
        }

        protected bool InputsMatch(MethodInfo x, MethodInfo y)
        {
            var backingParms = x.GetParameters().Select(pi => pi.ParameterType).ToArray();
            var targetParms = y.GetParameters().Select(pi => pi.ParameterType).ToArray();
            if (backingParms.Length != targetParms.Length)
            {
                return false;
            }
            
            var @in = x;
            var @out = y;
            
            for (int i = 0; i < targetParms.Length; i++)
            {
                // compare to implementor!
                var tp = targetParms[i];
                var bp = backingParms[i];
                var returns = this.GetGenericTypes(@in, tp, bp);
                if (!returns.All(z => z.IsAssignableFrom(bp)))
                {
                    return false;
                }
                
                //                if (!targetParms[i].IsAssignableFrom(backingParms[i]))
                //                {
                //                    return false;
                //                }
            }
            return true;
        }

        protected Type[] GetGenericTypes(MethodInfo target, Type interfaceDeclared, Type concreteDeclared)
        {
            if (!interfaceDeclared.IsGenericParameter)
            {
                return new Type[]{ interfaceDeclared };
            }
            
            if (target.DeclaringType.IsGenericType)
            {
                
                var args = target.DeclaringType.GetGenericArguments()
                    .First(x => x.Name == interfaceDeclared.Name);
                //                Console.WriteLine(args);
                //                foreach (var item in args)
                //                {
                //                    var constriants = item.GetGenericParameterConstraints();
                //                    Console.WriteLine(constriants);
                //                }

                var items = args.GetGenericParameterConstraints();
                if (items.Length == 0)
                {
                    return new Type[]{ interfaceDeclared == concreteDeclared ? typeof(object) : concreteDeclared };
                }
                
                return items;
            }
            
            return new Type[]{ concreteDeclared };
        }
        
        private Dictionary<MethodInfo, MethodInfo> CalculateMatches()
        {
            var map = new Dictionary<MethodInfo, MethodInfo>();
            if (this.backing == null)
            {
                return map;
            }
            
            var backingType = backing.GetType();
            var targetType = this.GetTargetType<T>();
            var targetMethods = targetType.GetMethods();
            var backingMethods = backingType.GetMethods();
            
            foreach (var targetMethod in targetMethods)
            {
                var @base = targetMethod.GetBaseDefinition();
                //                var newMeth = targetMethod.GetGenericMethodDefinition();
                var foundMatches = backingMethods.Where(x => NamesMatch(x, targetMethod) && ReturnsMatch(x, targetMethod) && InputsMatch(x, targetMethod));
                if (!foundMatches.Any())
                {
                    return null;
                }
                
                map[targetMethod] = foundMatches.FirstOrDefault();
            }
            
            return map;
        }
    }
    
    
    
    
}


