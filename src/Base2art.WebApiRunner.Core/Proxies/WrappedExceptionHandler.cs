﻿
namespace Base2art.WebApiRunner.Proxies
{
    using System.Threading.Tasks;
    using System.Web.Http.ExceptionHandling;
    
    public class WrappedExceptionHandler : ExceptionHandler
    {
        private readonly Base2art.WebApiRunner.IExceptionHandler obj;

        public WrappedExceptionHandler(Base2art.WebApiRunner.IExceptionHandler obj)
        {
            this.obj = obj;
        }
        
        public async override Task HandleAsync(ExceptionHandlerContext context, System.Threading.CancellationToken cancellationToken)
        {
            if (obj != null)
            {
                await this.obj.HandleAsync(context.Exception);
            }
            
            await base.HandleAsync(context, cancellationToken);
        }
    }
}
