﻿
namespace Base2art.WebApiRunner.Proxies
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Runtime.Remoting.Messaging;
    using System.Runtime.Remoting.Proxies;

    public class Aggregate<T> : RealProxy
    {
        private readonly T[] backingItems;

        private readonly IDictionary<MethodBase, Func<T[], object>> mappings = new Dictionary<MethodBase, Func<T[], object>>();

        private readonly IDictionary<MethodBase, Action<T[]>> actionMappings = new Dictionary<MethodBase, Action<T[]>>();

        public Aggregate(params T[] backingItems)
            : base(typeof(T))
        {
            this.backingItems = backingItems;
        }

        public T Value
        {
            get
            {
                return (T)this.GetTransparentProxy();
            }
        }
        
        public void AddMapping<TResult>(Expression<Func<T, TResult>> expr, Func<T[], object> mapping)
        {
            var exprCall = expr.Body as MethodCallExpression;
            if (exprCall != null)
            {
                this.mappings[exprCall.Method] = mapping;
            }
        }
        
        public void AddActionMapping(Expression<Action<T>> expr, Action<T[]> mapping)
        {
            var exprCall = expr.Body as MethodCallExpression;
            if (exprCall != null)
            {
                this.actionMappings[exprCall.Method] = mapping;
            }
        }

        public override IMessage Invoke(IMessage msg)
        {
            var methodCall = msg as IMethodCallMessage;
            if (methodCall != null)
            {
                if (this.mappings.ContainsKey(methodCall.MethodBase))
                {
                    var result = this.mappings[methodCall.MethodBase](this.backingItems.ToList().ToArray());
                    return new ReturnMessage(result, null, 0, methodCall.LogicalCallContext, methodCall);
                }
                
                if (this.actionMappings.ContainsKey(methodCall.MethodBase))
                {
                    this.actionMappings[methodCall.MethodBase](this.backingItems.ToList().ToArray());
                    return new ReturnMessage(null, null, 0, methodCall.LogicalCallContext, methodCall);
                }
                
                return HandleMethodCall(methodCall);
            }
            
            return null;
        }

        private IMessage HandleMethodCall(IMethodCallMessage methodCall)
        {
            try
            {
                var mo = methodCall.MethodBase;
                object result = null;
                foreach (var item in this.backingItems)
                {
                    result = mo.Invoke(item, methodCall.InArgs);
                }
                
                return new ReturnMessage(result, null, 0, methodCall.LogicalCallContext, methodCall);
            }
            catch (TargetInvocationException invocationException)
            {
                var exception = invocationException.InnerException;
                return new ReturnMessage(exception, methodCall);
            }
        }
    }
}


