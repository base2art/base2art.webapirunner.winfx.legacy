﻿namespace Base2art.WebApiRunner.Proxies
{
    using System.IO;
    using Newtonsoft.Json;
    
    public class ObjectTextWriterWrapper : TextWriter
    {
        private readonly TextWriter writer;

        public ObjectTextWriterWrapper(TextWriter writer)
        {
            this.writer = writer;
        }
            
        public override System.Text.Encoding Encoding
        {
            get
            {
                return this.writer.Encoding;
            }
        }
        
        public override void WriteLine(object value)
        {
            this.WriteLine(JsonConvert.SerializeObject(value, Formatting.Indented));
        }
        
        public override void Write(object value)
        {
            this.Write(JsonConvert.SerializeObject(value, Formatting.Indented));
        }

        public override void Write(char[] buffer, int index, int count)
        {
            this.writer.Write(buffer, index, count);
        }

        public override void Write(string value)
        {
            this.writer.Write(value);
        }

        public override void WriteLine()
        {
            this.writer.WriteLine();
        }

        public override void WriteLine(string value)
        {
            this.writer.WriteLine(value);
        }
        
        public override void Flush()
        {
            base.Flush();
            this.writer.Flush();
        }
        
        public override System.Threading.Tasks.Task FlushAsync()
        {
            return this.writer.FlushAsync();
        }
    }
}
