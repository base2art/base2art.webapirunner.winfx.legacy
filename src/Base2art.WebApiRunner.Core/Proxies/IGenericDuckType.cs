﻿namespace Base2art.WebApiRunner.Proxies
{
    using System;

    public interface IGenericDuckType<T> : IDuckType<T>
    {
        bool SupportsType<TIn>();

        bool SupportsType(Type t);
    }
}
