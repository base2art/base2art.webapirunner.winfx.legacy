﻿namespace Base2art.WebApiRunner.Proxies
{
    using System;
    
    public interface IDuckType<T>
    {
        bool IsDuck
        {
            get;
        }

        T Instance
        {
            get;
        }
    }
}


