﻿namespace Base2art.WebApiRunner.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Http.Dependencies;
    using Base2art.WebApiRunner.Config;

    public static class TypeHelper
    {
        public static bool IsNullableValueType(this Type type)
        {
            return type.IsValueType && (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>));
        }
        
        public static bool IsGenericTypeOf(this Type t, Type genericDefinition)
        {
            return t.IsGenericType && genericDefinition.IsGenericType && t.GetGenericTypeDefinition() == genericDefinition.GetGenericTypeDefinition();
        }
        
        public static Type LoadType(this ICollection<Assembly> asms, string typeName)
        {
            return Type.GetType(typeName) ?? asms
                .Select(a => a.GetType(typeName))
                .FirstOrDefault(t => t != null);
        }
        
        public static Array GetServicesArray(this IDependencyScope resolver, Type type)
        {
            var optionsObjs = resolver.GetServices(type).ToArray();
            var newArray = Array.CreateInstance(type, optionsObjs.Length);
            Array.Copy(optionsObjs, newArray, optionsObjs.Length);
            return newArray;
        }

        public static void RunMiddleware(
            this HttpConfiguration config,
            IEnumerable<MiddlewareConfiguration> middlewareConfig,
            Type middlewareType,
            Func<Array, object[]> getParmsFromOptions)
        {
            var asms = config.Services.GetAssembliesResolver().GetAssemblies();
            foreach (var element in middlewareConfig)
            {
                var type = asms.LoadType(element.Type);
                
                var item = type.GetInterfaces().FirstOrDefault(x => x.IsGenericTypeOf(middlewareType));
                if (item == null)
                {
                    throw new InvalidProgramException("All Middleware must implement `" +middlewareType +"`");
                }
                
                var optionsType = item.GetGenericArguments()[0];
                
                var middleware = config.DependencyResolver.GetService(type);
                
                if (middleware == null)
                {
                    middleware = type.CreateFrom(config.DependencyResolver, element.Parameters, element.Properties);
                }
                
                item.GetMethod("Run")
                    .Invoke(middleware, getParmsFromOptions(config.DependencyResolver.GetServicesArray(optionsType)));
            }
        }
        
        internal static Type LoadType(this ICollection<Assembly> asms, IGrouping<string, TypedMethodConfiguration> x)
        {
            return LoadType( asms, x.Key);
        }
    }
}


