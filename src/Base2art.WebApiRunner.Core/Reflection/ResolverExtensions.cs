﻿
namespace Base2art.WebApiRunner.Reflection
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Globalization;
    using System.Linq;
    using System.Reflection;
    using System.Web.Http.Dependencies;
    using Base2art.ComponentModel;
    using Base2art.WebApiRunner.Config;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;

    public static class ResolverExtensions
    {
        public static TProxyType ProxiedCreateFrom<TProxyType>(
            this Type type,
            IDependencyScope config,
            IDictionary<string, string> inputParms,
            IDictionary<string, object> properties,
            params Func<object, TProxyType>[] lookupList)
            where TProxyType : class
        {
            var instance = type.CreateFrom(config, inputParms, properties);
            var filter = instance as TProxyType;
            if (filter != null)
            {
                return filter;
            }
            
            foreach (var item in lookupList)
            {
                var val = item(instance);
                if (val == null)
                {
                    continue;
                }
                
                return val;
            }
            
            throw new InvalidCastException(type + " is not a valid " + typeof(TProxyType) + " type", 0);
        }
        
        public static T CreateFrom<T>(this IDependencyScope config, IDictionary<string, object> inputParms)
        {
            if (inputParms == null)
            {
                inputParms = new Dictionary<string, object>();
            }
            
            return (T)CreateFromInternal(
                typeof(T),
                config,
                s => inputParms.ContainsKey(s.Name),
                (s) => inputParms[s.Name]);
        }
        
        public static T CreateFrom<T>(
            this IDependencyScope config,
            IDictionary<string, object> inputParms,
            IDictionary<string, object> properties)
        {
            if (inputParms == null)
            {
                inputParms = new Dictionary<string, object>();
            }
            
            if (properties == null)
            {
                properties = new Dictionary<string, object>();
            }
            
            var obj = (T)CreateFromInternal(
                typeof(T),
                config,
                s => inputParms.ContainsKey(s.Name),
                s => Convert(inputParms, s));
            SetProperties(properties, obj);
            return obj;
        }
        
        public static object CreateFrom(
            this Type type,
            IDependencyScope config,
            IDictionary<string, string> inputParms,
            IDictionary<string, object> properties)
        {
            if (inputParms == null)
            {
                inputParms = new Dictionary<string, string>();
            }
            
            if (properties == null)
            {
                properties = new Dictionary<string, object>();
            }
            
            
            var obj = CreateFromInternal(
                type,
                config,
                s => inputParms.ContainsKey(s.Name),
                s => TypeDescriptor.GetConverter(s.ParameterType).ConvertFromInvariantString(inputParms[s.Name]));
            
            SetProperties(properties, obj);
            return obj;
        }
        
        private static object CreateFromInternal(
            this Type t,
            IDependencyScope config,
            Func<ParameterInfo, bool> hasParam,
            Func<ParameterInfo, object> getParam)
        {
            
            ConstructorInfo[] ctors = t.GetConstructors();
            if (ctors.Length == 0)
            {
                return Activator.CreateInstance(t);
            }
            
            List<ConstructorInfo> items = (from x in ctors
                                           orderby x.GetParameters().Length
                                           select x).ToList<ConstructorInfo>();
            ConstructorInfo ctor = items[0];
            ParameterInfo[] parms = ctor.GetParameters();
            int length = parms.Length;
            if (length == 0)
            {
                return Activator.CreateInstance(t);
            }
            object[] obj = new object[length];
            for (int i = 0; i < obj.Length; i++)
            {
                var parameterInfo = parms[i];
                if (hasParam(parameterInfo))
                {
                    obj[i] = getParam(parameterInfo);
                }
                else
                {
                    if (parameterInfo.ParameterType == typeof(string))
                    {
                        throw new MissingFieldException(t.FullName, parameterInfo.Name);
                    }
                    
                    if (parameterInfo.ParameterType.IsPrimitive)
                    {
                        throw new MissingFieldException(t.FullName, parameterInfo.Name);
                    }
                    
                    if (parameterInfo.ParameterType.IsNullableValueType())
                    {
                        throw new MissingFieldException(t.FullName, parameterInfo.Name);
                    }
                    
                    try
                    {
                        var parmType = parameterInfo.ParameterType;
                        if (parmType.IsArray)
                        {
                            var innerParmType = parmType.GetElementType();
                            //                            config.GetServices(innerParmType).ToArray();
                            obj[i] = config.GetServicesArray(innerParmType);
                        }
                        else
                        {
                            obj[i] = config.GetService(parmType);
                        }
                    }
                    catch (CircularDependencyException e)
                    {
                        string errorMessage = string.Format(CultureInfo.InvariantCulture, "{0} in {1}", new object[] {
                                                                parameterInfo.ParameterType,
                                                                t
                                                            });
                        throw new CircularDependencyException(errorMessage, e);
                    }
                }
            }
            
            return (object)Activator.CreateInstance(t, obj);
        }

        private static void SetProperties(IDictionary<string, object> properties, object obj)
        {
            if (properties == null)
            {
                return;
            }
            
            if (properties.Count > 0)
            {
                var objType = obj.GetType();
                foreach (var element in properties)
                {
                    var prop = objType.GetProperty(element.Key);
                    var propSet = prop.GetSetMethod();
                    if (TypeDescriptor.GetConverter(prop.PropertyType).CanConvertFrom(typeof(string)))
                    {
                        var value = TypeDescriptor.GetConverter(prop.PropertyType).ConvertFromInvariantString(element.Value.ToString());
                        propSet.Invoke(obj, new object[] {
                                           value
                                       });
                    }
                    else
                    {
                        var serializer = new Newtonsoft.Json.JsonSerializer();
                        var objString = JsonConvert.SerializeObject(element.Value);
                        var value = JsonConvert.DeserializeObject(objString, prop.PropertyType);
                        propSet.Invoke(obj, new object[] {
                                           value
                                       });
                    }
                }
            }
        }

        private static object Convert(IDictionary<string, object> inputParms, ParameterInfo s)
        {
            var value = inputParms[s.Name];
            var type = s.ParameterType;
            var converter = TypeDescriptor.GetConverter(type);
            if (value == null)
            {
                return null;
            }
            
            if (converter.CanConvertFrom(value.GetType()))
            {
                return converter.ConvertFrom(value);
            }
            
            if (type == typeof(Dictionary<string, List<string>>))
            {
                var dict = new Dictionary<string, List<string>>();
                
                IEnumerable<KeyValuePair<string, JToken>> typedValue = value as IEnumerable<KeyValuePair<string, JToken>>;
                if (typedValue == null)
                {
                    return dict;
                }
                
                foreach (var item in typedValue)
                {
                    var list = new List<string>();
                    dict[item.Key] = list;
                    var array = item.Value as JArray;
                    list.AddRange(array.Select(x => (string)x));
                }
                
                return dict;
            }
            
            var jToken = value as JToken;
            if (jToken != null)
            {
                try
                {
                    return jToken.ToObject(type);
                }
                catch (JsonException)
                {
                    return null;
                }
            }
            
            return null;
        }
    }
}


