﻿using System.Web.Http;
namespace Base2art.WebApiRunner.Middleware
{
    public interface IMiddleware<TOptions>
    {
        void Run(HttpConfiguration config, TOptions[] options);
    }
}
