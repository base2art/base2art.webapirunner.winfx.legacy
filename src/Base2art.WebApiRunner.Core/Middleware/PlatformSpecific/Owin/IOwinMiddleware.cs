﻿

namespace Base2art.WebApiRunner.Middleware.PlatformSpecific.Owin
{
    using System;
    using System.Collections.Generic;
    using System.Web.Http;
    
    public interface IOwinMiddleware<TOptions>
    {
        void Run(
            HttpConfiguration config,
            TOptions[] options,
            IDictionary<string, object> properties,
            Action<object, object[]> useSetup);
        
    }
}
