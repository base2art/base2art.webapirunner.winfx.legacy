﻿namespace Base2art.WebApiRunner
{
    using System;
    using System.Web.Http;

    public static class HttpConfigurationExtensions
    {
        public static T GetService<T>(this HttpConfiguration config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("config");
            }

            return  (T) config.Services.GetService(typeof (T));
        }
    }
}
