namespace Base2art.WebApiRunner
{
    using System;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Reflection;
    using System.Web.Http;
    using System.Web.Http.Dependencies;
    using Base2art.WebApiRunner.Config;

    public abstract class StartupBase
    {
        private readonly ServerConfiguration config;

        protected StartupBase(ServerConfiguration config)
        {
            this.config = config;
            
            Coalesce(() => this.ServerConfig.AssembliesConfigurationItems);
            Coalesce(() => this.ServerConfig.Assemblies);
            
            Coalesce(() => this.ServerConfig.AssemblySearchPathsConfigurationItems);
            Coalesce(() => this.ServerConfig.AssemblySearchPaths);
            
            Coalesce(() => this.ServerConfig.InjectionConfigurationItems);
            Coalesce(() => this.ServerConfig.Injection);
            Coalesce(() => this.ServerConfig.Injection.Items);
            
            Coalesce(() => this.ServerConfig.HealthChecksConfigurationItems);
            Coalesce(() => this.ServerConfig.HealthChecks);
            Coalesce(() => this.ServerConfig.HealthChecks.Items);
            Coalesce(() => this.ServerConfig.HealthChecks.Aspects);
            
            Coalesce(() => this.ServerConfig.EndpointsConfigurationItems);
            Coalesce(() => this.ServerConfig.Endpoints);
            Coalesce(() => this.ServerConfig.Endpoints.Urls);
            Coalesce(() => this.ServerConfig.Endpoints.Aspects);
            
            Coalesce(() => this.ServerConfig.ApplicationConfigurationItems);
            Coalesce(() => this.ServerConfig.Application);
            Coalesce(() => this.ServerConfig.Application.Exceptions);
            Coalesce(() => this.ServerConfig.Application.Exceptions.ResponseMapItems);
            
            Coalesce(() => this.ServerConfig.Application.Filters);
            
            Coalesce(() => this.ServerConfig.Application.MediaTypes);
            Coalesce(() => this.ServerConfig.Application.ModelBindings);
            Coalesce(() => this.ServerConfig.Application.ControllerSuffixes);
            
            
            Coalesce(() => this.ServerConfig.MiddlewareConfigurationItems);
            Coalesce(() => this.ServerConfig.Middleware);
            
            Coalesce(() => this.ServerConfig.CorsConfigurationItems);
            Coalesce(() => this.ServerConfig.Cors);
            Coalesce(() => this.ServerConfig.Cors.Items);
            
            
            Coalesce(() => this.ServerConfig.TasksConfigurationItems);
            Coalesce(() => this.ServerConfig.Tasks);
            Coalesce(() => this.ServerConfig.Tasks.Items);
            Coalesce(() => this.ServerConfig.Tasks.Aspects);
            
        }

        protected ServerConfiguration ServerConfig
        {
            get { return this.config; }
        }

        public void Register(HttpConfiguration config, Action<HttpConfiguration> setup, Func<IDependencyResolver> resolver)
        {
            if (resolver == null)
            {
                this.RegisterDependencyResolver(config);
            }
            else
            {
                var resolverValue = resolver();
                if (resolverValue == null)
                {
                    this.RegisterDependencyResolver(config);
                }
                else
                {
                    config.DependencyResolver = resolverValue;
                }
            }
            
            this.RegisterSystemServices(config);
            this.RegisterUserServices(config);
            this.RegisterSystemServicesPostUserServiceRegistration(config);
            this.RegisterFormatters(config);
            this.RegisterFilters(config);
            this.RegisterBindings(config);
            this.RegisterCors(config);
            this.RegisterSwagger(config);
            this.RegisterMiddleware(config);
            this.RegisterRoutes(config);
            
            if (setup != null)
            {
                setup(config);
            }
        }

        protected abstract void RegisterDependencyResolver(HttpConfiguration config);
        
        protected abstract void RegisterSystemServices(HttpConfiguration config);
        
        protected abstract void RegisterUserServices(HttpConfiguration config);
        
        protected abstract void RegisterSystemServicesPostUserServiceRegistration(HttpConfiguration config);

        protected abstract void RegisterFilters(HttpConfiguration config);

        protected abstract void RegisterBindings(HttpConfiguration config);

        protected abstract void RegisterFormatters(HttpConfiguration config);

        protected abstract void RegisterCors(HttpConfiguration config);

        protected abstract void RegisterSwagger(HttpConfiguration config);

        protected abstract void RegisterMiddleware(HttpConfiguration config);

        protected abstract void RegisterRoutes(HttpConfiguration config);

        public static void Coalesce<T>(Expression<Func<T>> expr) where T : class, new()
        {
            var item = expr.Compile().Invoke();
            
            if (item != null)
            {
                return;
            }
            
            var memberExpression = (MemberExpression)expr.Body;
            var property = (PropertyInfo)memberExpression.Member;
            var setMethod = property.GetSetMethod();

            var parameterTProperty = Expression.Parameter(typeof(T), "y");
            var newExpression =
                Expression.Lambda<Action<T>>(
                    Expression.Call(memberExpression.Expression, setMethod, parameterTProperty),
                    parameterTProperty);

            newExpression.Compile()(new T());
        }
    }
}
