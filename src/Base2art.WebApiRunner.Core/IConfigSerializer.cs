namespace Base2art.WebApiRunner
{
   public interface IConfigSerializer
   {
      string Serialize<T>(T value);
   }
}
