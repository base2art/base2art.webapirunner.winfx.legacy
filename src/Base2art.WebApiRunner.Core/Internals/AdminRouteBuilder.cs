﻿namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Config.Types;

    public static class AdminRouteBuilder
    {
        public static List<TypedMethodConfiguration> CreateTasksAndHealthCheck(this ServerConfiguration config, string adminPrefix = "")
        {
            if (!string.IsNullOrWhiteSpace(adminPrefix) && !adminPrefix.EndsWith("/"))
            {
                adminPrefix = adminPrefix + "/";
            }
            
            var tasks = config.JointTasks().SelectMany(x => x.Items ?? new List<TaskConfiguration>());
            var tasksAspects = config.JointTasks().SelectMany(x => x.Aspects ?? new List<AspectConfiguration>());
            var healthAspects = config.JointHealthChecks().SelectMany(x => x.Aspects ?? new List<AspectConfiguration>());
            
            var list = tasks.Select(x => new TypedMethodConfiguration {
                                        ControllerType = ControllerType.Task,
                                        MethodConfiguration = new MethodConfiguration {
                                            // ci
                                            Type = x.Type,
                                            Properties = x.Properties,
                                            Parameters = x.Parameters,
                                            //                                            ClassName = new ClassIdentifier(x.ClassName),
                                            Method = "ExecuteAsync",
                                            Verb = HttpMethod.Post.Method,
                                            Url = adminPrefix + "tasks/" + x.Name,
                                            Aspects = tasksAspects.Union(x.Aspects ?? new AspectsConfiguration()).ToArray()
                                        }
                                    }).ToList();
            
            list.Add(new TypedMethodConfiguration {
                         ControllerType = ControllerType.HealthCheck,
                         MethodConfiguration = new MethodConfiguration {
                             // ci
                             // ClassName = typeof(Internals.Controllers.InternalHealthCheckController).FullName,
                             Type = new TypeIdentifier(typeof(Internals.Controllers.InternalHealthCheckController).FullName),
                             
                             Method = "Get",
                             Verb = HttpMethod.Get.Method,
                             Url = adminPrefix + "healthcheck",
                             Aspects = healthAspects.ToArray()
                         }
                     });
            
            return list;
        }
    }
}
