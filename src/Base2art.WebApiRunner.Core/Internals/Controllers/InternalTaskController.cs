﻿namespace Base2art.WebApiRunner.Internals.Controllers
{
    using System;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.ExceptionHandling;
    using System.Web.Http.Filters;

    public class InternalTaskController : Component, IHttpController, IInstanceHolder
    {
        private readonly object backing;
        
        public InternalTaskController(object backing)
        {
            this.backing = backing;
        }

        public object Instance
        {
            get { return this.backing; }
        }

        public Task<System.Net.Http.HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, System.Threading.CancellationToken cancellationToken)
        {
            
            if (controllerContext.Request != null)
            {
                controllerContext.Request.RegisterForDispose(this);
            }
            
            HttpControllerDescriptor controllerDescriptor = controllerContext.ControllerDescriptor;
            ServicesContainer services = controllerDescriptor.Configuration.Services;
            
            // Refactor Later...
            // Nest the IHttpActionSelector to return this...
            HttpActionDescriptor httpActionDescriptor = services.GetActionSelector().SelectAction(controllerContext);
            HttpActionContext _actionContext = new HttpActionContext(controllerContext, httpActionDescriptor);
            _actionContext.ActionDescriptor = httpActionDescriptor;
            
            FilterGrouping filterGrouping = GetFilterGrouping(httpActionDescriptor);
            IActionFilter[] actionFilters = filterGrouping.ActionFilters;
            IAuthenticationFilter[] authenticationFilters = filterGrouping.AuthenticationFilters;
            IAuthorizationFilter[] authorizationFilters = filterGrouping.AuthorizationFilters;
            IExceptionFilter[] exceptionFilters = filterGrouping.ExceptionFilters;
            IHttpActionResult httpActionResult = new ActionFilterResult(
                httpActionDescriptor.ActionBinding,
                _actionContext,
                services,
                actionFilters);
            
            //            if (authorizationFilters.Length > 0)
            //            {
            //                httpActionResult = new AuthorizationFilterResult(this.ActionContext, authorizationFilters, httpActionResult);
            //            }
            //            if (authenticationFilters.Length > 0)
            //            {
            //                httpActionResult = new AuthenticationFilterResult(this.ActionContext, this, authenticationFilters, httpActionResult);
            //            }
            
            if (exceptionFilters.Length > 0)
            {
                IExceptionLogger logger = ExceptionServices.GetLogger(services);
                IExceptionHandler handler = ExceptionServices.GetHandler(services);
                httpActionResult = new ExceptionFilterResult(_actionContext, exceptionFilters, logger, handler, httpActionResult);
            }
            
            return httpActionResult.ExecuteAsync(cancellationToken);
        }

        private FilterGrouping _filterGrouping;

        private Collection<FilterInfo> _filterPipelineForGrouping;

        internal FilterGrouping GetFilterGrouping(HttpActionDescriptor descriptor)
        {
            Collection<FilterInfo> filterPipeline = descriptor.GetFilterPipeline();
            if (this._filterGrouping == null || this._filterPipelineForGrouping != filterPipeline)
            {
                this._filterGrouping = new FilterGrouping(filterPipeline);
                this._filterPipelineForGrouping = filterPipeline;
            }

            return this._filterGrouping;
        }
    }
}
