﻿
namespace Base2art.WebApiRunner.Internals.Controllers
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Web.Http;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Function;
    using Base2art.WebApiRunner.Models;
    using Base2art.WebApiRunner.Proxies;
    using Base2art.WebApiRunner.Reflection;
    
    public class InternalHealthCheckController : ApiController
    {
        private readonly ServerConfiguration config;

        public InternalHealthCheckController(ServerConfiguration config)
        {
            this.config = config;
        }
        
        public async Task<HeathCheckResult> Get()
        {
            var asms = this.Configuration.Services.GetAssembliesResolver().GetAssemblies();
            var serviceContainer = this.Configuration.DependencyResolver;
            
            var filterLookupList = new Func<object, IHealthCheck>[] {
                (x) => this.ConvertableDuck<IHealthCheck, IHealthCheck>(x, duck => new HealthCheckWrapper1(duck)),
                (x) => this.ConvertableDuck<ISimpleHealthCheck, IHealthCheck>(x, duck => new HealthCheckWrapper(duck))
            };
            
            var items = this.config.JointHealthChecks().SelectMany(x => x.Items)
                .Select(x => Tuple.Create(x.Name, asms.LoadType(x.Type), x))
                .Select(x => Tuple.Create(x.Item1, x.Item2.ProxiedCreateFrom(serviceContainer, x.Item3.Parameters, x.Item3.Properties, filterLookupList)))
                        //                .Select(x => Tuple.Create(x.Item1, x.Item2.AsDuck<IHealthCheck>().Instance))
                .Select(x => Tuple.Create(x.Item1, x.Item2.ExecuteAsync()))
                .ToArray();
            
            await Task.WhenAll(items.Select(x => x.Item2));
            var result = new HeathCheckResult();
            
            
            
            foreach (var check in items)
            {
                result[check.Item1] = this.Map(check.Item2.Result);
            }
            
            return result;
        }

        private HeathCheckResultItem Map(System.IO.ErrorEventArgs item2)
        {
            var item = new HeathCheckResultItem();
            
            if (item2 == null || item2.GetException() == null)
            {
                item.IsHealthy = true;
            }
            else
            {
                item.Message = item2.GetException().Message;
                item.Data = item2.GetException().Data;
            }
            
            return item;
        }

        private TOut ConvertableDuck<TIn, TOut>(object instance, Func<IDuckType<TIn>, TOut> inOut)
            where TOut : class
        {
            var duck = instance.AsDuck<TIn>();
            if (duck.IsDuck)
            {
                return inOut(duck);
            }
            
            return null;
        }
        
        private class HealthCheckWrapper : IHealthCheck
        {
            private IDuckType<ISimpleHealthCheck> duck;

            public HealthCheckWrapper(IDuckType<ISimpleHealthCheck> duck)
            {
                this.duck = duck;
            }

            public async Task<System.IO.ErrorEventArgs> ExecuteAsync()
            {
                try
                {
                    await this.duck.Instance.ExecuteAsync();
                }
                catch (Exception ex)
                {
                    return new System.IO.ErrorEventArgs(ex);
                }
                
                return null;
            }
        }
        
        private class HealthCheckWrapper1 : IHealthCheck
        {
            private IDuckType<IHealthCheck> duck;

            public HealthCheckWrapper1(IDuckType<IHealthCheck> duck)
            {
                this.duck = duck;
            }

            public async Task<System.IO.ErrorEventArgs> ExecuteAsync()
            {
                try
                {
                    return await this.duck.Instance.ExecuteAsync();
                }
                catch (Exception ex)
                {
                    return new System.IO.ErrorEventArgs(ex);
                }
            }
        }
    }
}
