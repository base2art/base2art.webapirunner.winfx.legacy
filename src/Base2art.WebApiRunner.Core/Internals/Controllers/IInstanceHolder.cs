﻿namespace Base2art.WebApiRunner.Internals.Controllers
{
    public interface IInstanceHolder
    {
        object Instance { get; }
    }
}
