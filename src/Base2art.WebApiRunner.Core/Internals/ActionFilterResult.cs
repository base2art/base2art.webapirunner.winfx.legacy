namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    internal class ActionFilterResult : IHttpActionResult
    {
        private struct ActionInvoker
        {
            private readonly HttpActionContext _context;

            private readonly CancellationToken _cancellationToken;

            private readonly ServicesContainer _controllerServices;

            public ActionInvoker(HttpActionContext context, CancellationToken cancellationToken, ServicesContainer controllerServices)
            {
                this._context = context;
                this._cancellationToken = cancellationToken;
                this._controllerServices = controllerServices;
            }

            public Task<HttpResponseMessage> InvokeActionAsync()
            {
                return this._controllerServices.GetActionInvoker().InvokeActionAsync(this._context, this._cancellationToken);
            }
        }

        private readonly HttpActionBinding _binding;

        private readonly HttpActionContext _context;

        private readonly ServicesContainer _services;

        private readonly IActionFilter[] _filters;

        public ActionFilterResult(HttpActionBinding binding, HttpActionContext context, ServicesContainer services, IActionFilter[] filters)
        {
            this._binding = binding;
            this._context = context;
            this._services = services;
            this._filters = filters;
        }

        public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
        {
            await this._binding.ExecuteBindingAsync(this._context, cancellationToken);
            ActionFilterResult.ActionInvoker state = new ActionFilterResult.ActionInvoker(this._context, cancellationToken, this._services);
            HttpResponseMessage result;
            if (this._filters.Length == 0)
            {
                result = await state.InvokeActionAsync();
            }
            else
            {
                Func<ActionFilterResult.ActionInvoker, Task<HttpResponseMessage>> innerAction = (ActionFilterResult.ActionInvoker innerInvoker) => innerInvoker.InvokeActionAsync();
                result = await ActionFilterResult.InvokeActionWithActionFilters<ActionFilterResult.ActionInvoker>(this._context, cancellationToken, this._filters, innerAction, state)();
            }

            return result;
        }

        public static Func<Task<HttpResponseMessage>> InvokeActionWithActionFilters(HttpActionContext actionContext, CancellationToken cancellationToken, IActionFilter[] filters, Func<Task<HttpResponseMessage>> innerAction)
        {
            Func<Task<HttpResponseMessage>> func = innerAction;
            for (int i = filters.Length - 1; i >= 0; i--)
            {
                IActionFilter arg = filters[i];
                Func<Func<Task<HttpResponseMessage>>, IActionFilter, Func<Task<HttpResponseMessage>>> func2 = (Func<Task<HttpResponseMessage>> continuation, IActionFilter innerFilter) => () => innerFilter.ExecuteActionFilterAsync(actionContext, cancellationToken, continuation);
                func = func2(func, arg);
            }

            return func;
        }

        private static Func<Task<HttpResponseMessage>> InvokeActionWithActionFilters<T>(HttpActionContext actionContext, CancellationToken cancellationToken, IActionFilter[] filters, Func<T, Task<HttpResponseMessage>> innerAction, T state)
        {
            return ActionFilterResult.InvokeActionWithActionFilters(actionContext, cancellationToken, filters, () => innerAction(state));
        }
    }
}
