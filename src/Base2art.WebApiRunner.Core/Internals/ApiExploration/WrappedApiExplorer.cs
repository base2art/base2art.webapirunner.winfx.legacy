﻿namespace Base2art.WebApiRunner.Internals.ApiExploration
{
    using System.Collections.Generic;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Description;
    using System.Web.Http.Routing;
    
    internal class WrappedApiExplorer : ApiExplorer
    {
        private readonly HashSet<string> actions = new HashSet<string>();
        
        public WrappedApiExplorer(HttpConfiguration config) : base(config)
        {
        }
        
        /// <summary>
        /// Determines whether the action should be considered for <see cref="ApiExplorer.ApiDescriptions"/> generation. Called when initializing the <see cref="ApiExplorer.ApiDescriptions"/>.
        /// </summary>
        /// <param name="actionVariableValue">The action variable value from the route.</param>
        /// <param name="actionDescriptor">The action descriptor.</param>
        /// <param name="route">The route.</param>
        /// <returns><c>true</c> if the action should be considered for <see cref="ApiExplorer.ApiDescriptions"/> generation, <c>false</c> otherwise.</returns>
        public override bool ShouldExploreAction(string actionVariableValue, HttpActionDescriptor actionDescriptor, IHttpRoute route)
        {
            var baseResult = base.ShouldExploreAction(actionVariableValue, actionDescriptor, route);
            if (baseResult)
            {
                return !this.HasProcessed(actionDescriptor, route.RouteTemplate, (route.Defaults["actionVerb"] ?? string.Empty).ToString());
            }
            
            return false;
        }
        
        private bool HasProcessed(HttpActionDescriptor contro, string route, string method)
        {
            var key = method + "::" + contro.ControllerDescriptor.ControllerName + "::" + contro.ActionName + "::" + route;
            bool has = actions.Contains(key);
            actions.Add(key);
            return has;
        }
    }
}
