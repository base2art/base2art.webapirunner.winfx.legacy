﻿namespace Base2art.WebApiRunner.Internals.Cors
{
    using System;
    using System.Net;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Cors;
    using System.Web.Http;
    using System.Web.Http.Cors;
    
    public class AllowOptionsHandler : DelegatingHandler
    {
        private readonly CorsPolicyFactory provider;

        public AllowOptionsHandler(CorsPolicyFactory provider)
        {
            this.provider = provider;
        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            var response = await base.SendAsync(request, cancellationToken);

            var policyProvider = this.provider.GetCorsPolicyProvider(request);
            
            if (policyProvider == null)
            {
                return response;
            }
            
            var policy = await policyProvider.GetCorsPolicyAsync(request, cancellationToken);
            if (policy == null)
            {
                return response;
            }
            
            ICorsEngine corsEngine = request.GetConfiguration().GetCorsEngine();
            
            if (request.Method == HttpMethod.Options)
            {
                if (response.StatusCode == HttpStatusCode.NotFound || response.StatusCode == HttpStatusCode.BadRequest || response.StatusCode == HttpStatusCode.MethodNotAllowed)
                {
                    var newResponse = request.CreateResponse(HttpStatusCode.OK);
                    
                    Func<string, bool> hasHeader = request.Headers.Contains;
                    if (hasHeader("Origin"))
                    {
                        var corsContext = request.GetCorsRequestContext();
                        if (corsContext != null)
                        {
                            var corsResult = corsEngine.EvaluatePolicy(corsContext, policy);
                            CorsHttpResponseMessageExtensions.WriteCorsHeaders(newResponse, corsResult);
                            response = newResponse;
                        }
                    }
                    else
                    {
                        var corsContext = request.GetCorsRequestContext();
                        if (corsContext != null)
                        {
                            var corsResult = corsEngine.EvaluatePolicy(corsContext, policy);
                            CorsHttpResponseMessageExtensions.WriteCorsHeaders(newResponse, corsResult);
                            response = newResponse;
                        }
                    }
                    
                    // if (request.IsLocal() ||  && hasHeader("Access-Control-Request-Headers") && hasHeader("Access-Control-Request-Method"))
                    // {
                    //     //                        CorsPolicyFactory item;
                    //     //                        var provider = item.GetCorsPolicyProvider(request);
                    //     //                        provider.GetCorsPolicyAsync(request, cancellationToken);
                    //     return request.CreateResponse(HttpStatusCode.MethodNotAllowed);
                    // }
                }
            }
            else
            {
                var corsContext = request.GetCorsRequestContext();
                if (corsContext != null && policy != null)
                {
                    CorsHttpResponseMessageExtensions.WriteCorsHeaders(response, corsEngine.EvaluatePolicy(corsContext, policy));
                }
            }

            return response;
        }
    }
}
