﻿namespace Base2art.WebApiRunner.Internals.Cors
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Cors;
    using System.Web.Http.Cors;
    using Base2art.WebApiRunner.Config;
    
    public class CorsPolicyFactory : ICorsPolicyProviderFactory
    {
        private readonly ICorsPolicyProvider provider;
        
        public CorsPolicyFactory(List<CorsItemConfiguration> corsItems)
        {
            this.provider = new MyCorsPolicyProvider(corsItems);
        }
        
        public ICorsPolicyProvider Provider
        {
            get { return this.provider; }
        }
        
        public ICorsPolicyProvider GetCorsPolicyProvider(HttpRequestMessage request)
        {
            return provider;
        }
        
        public class MyCorsPolicyProvider : ICorsPolicyProvider
        {
            private readonly List<CorsItemConfiguration> corsItems;
            
            public MyCorsPolicyProvider(List<CorsItemConfiguration> corsItems)
            {
                this.corsItems = corsItems;
            }
            
            public Task<CorsPolicy> GetCorsPolicyAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                var origin = this.Header(request, "Origin");
                
                Uri uri;
                if (!Uri.TryCreate(origin, UriKind.Absolute, out uri))
                {
                    return Task.FromResult(new CorsPolicy());
                }
                
                var item = this.corsItems.FirstOrDefault(x => string.Equals(uri.Host, x.RequestHost, StringComparison.OrdinalIgnoreCase));
                if (item == null)
                {
                    return Task.FromResult(new CorsPolicy());
                }
                
                var policy = new CorsPolicy();

                policy.AllowAnyOrigin = item.AllowAnyOrigin;
                policy.AllowAnyMethod = item.AllowAnyMethod;
                policy.AllowAnyHeader = item.AllowAnyHeader;
                policy.PreflightMaxAge = item.PreflightMaxAge;
                policy.SupportsCredentials = item.SupportsCredentials;
                
                Action<List<string>, IList<string>> map = (@from, @to) =>
                {
                    foreach (var element in @from ?? new List<string>())
                    {
                        @to.Add(element);
                    }
                };
                
                map(item.ExposedHeaders, policy.ExposedHeaders);
                map(item.Headers, policy.Headers);
                map(item.Methods, policy.Methods);
                map(item.Origins, policy.Origins);
                
                if (policy.Origins.Count == 0 && !policy.AllowAnyOrigin)
                {
                    policy.Origins.Add(origin);
                }
                
                if (policy.Methods.Count == 0 && !policy.AllowAnyMethod)
                {
                    this.AddHeader(request, "Access-Control-Request-Method", policy.Methods);
                }
                
                if (policy.Headers.Count == 0 && !policy.AllowAnyHeader)
                {
                    this.AddHeader(request, "Access-Control-Request-Headers", policy.Headers);
                }
                
                return Task.FromResult(policy);
            }

            private void AddHeader(HttpRequestMessage request, string header, IList<string> coll)
            {
                var headerValue = this.Header(request, header);
                if (!string.IsNullOrWhiteSpace(headerValue))
                {
                    coll.Add(headerValue);
                }
            }

            private string Header(HttpRequestMessage request, string header)
            {
                IEnumerable<string> originValues;
                if (!request.Headers.TryGetValues(header, out originValues))
                {
                    return string.Empty;
                }
                
                var origin = string.Join(string.Empty, originValues ?? new string[0]);
                return origin;
            }
        }
        
        //        private string Header()
        //        {
        //
        //        }
    }
}
