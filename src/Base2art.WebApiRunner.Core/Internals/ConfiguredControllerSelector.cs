﻿
namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Dispatcher;
    using System.Web.Http.Routing;
    using Base2art.WebApiRunner.Config;
    
    public class ConfiguredControllerSelector : IHttpControllerSelector
    {
        private readonly List<TypedMethodConfiguration> methods;

        private readonly HttpConfiguration config;

        private readonly ServerConfiguration serverConfig;
        
        public ConfiguredControllerSelector(HttpConfiguration config, ServerConfiguration serverConfig, List<TypedMethodConfiguration> methods)
        {
            this.serverConfig = serverConfig;
            this.config = config;
            this.methods = methods;
        }
        
        public virtual string GetControllerName(HttpRequestMessage request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            
            IHttpRouteData routeData = request.GetRouteData();
            if (routeData == null)
            {
                return null;
            }
            
            object result = null;
            routeData.Values.TryGetValue("controller", out result);
            return result as string;
        }

        
        public HttpControllerDescriptor SelectController(System.Net.Http.HttpRequestMessage request)
        {
            var mapping = this.GetControllerMapping();
            var controllerName = this.GetControllerName(request);
            
            if (controllerName != null && mapping.ContainsKey(controllerName))
            {
                return mapping[controllerName];
            }
            
            return null;
        }
        
        public System.Collections.Generic.IDictionary<string, HttpControllerDescriptor> GetControllerMapping()
        {
            var resolver = config.Services.GetHttpControllerTypeResolver();
            
            var suffixes = this.serverConfig.JointApplications().SelectMany(z => z.ControllerSuffixes ?? new List<string>()).ToList();
            return resolver.GetControllerTypes(config.Services.GetAssembliesResolver())
                .GroupBy(x => Controller(x.FullName, suffixes))
                .Select(x => new {
                            //                            Key = x.Key.AssemblyQualifiedName,
                            ControllerName = x.Key,
                            Type = x.FirstOrDefault()})
                .ToDictionary(x => x.ControllerName, x => new HttpControllerDescriptor(config, x.ControllerName, x.Type));
        }
        
        public static string Controller(string className, List<string> configuredSuffixes)
        {
            IEnumerable<string> configuredSuffixes1 = (configuredSuffixes ?? new List<string>())
                .Union(new[] { "Service", "Task", "Endpoint" });
            var parts = className.Split(new char[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            var last = parts.Last();
            
            return string.Join(".", parts.Reverse().Skip(1).Reverse()) + "." + StripPrefix(StripSuffix(last, configuredSuffixes1)).ToUpperInvariant();
        }

        public static string StripSuffix(string name, IEnumerable<string> configuredSuffixes)
        {
            foreach (var sufix in configuredSuffixes)
            {
                if (name.EndsWith(sufix, StringComparison.Ordinal))
                {
                    name = name.Substring(0, name.Length - sufix.Length);
                }
            }
            
            return name;
        }

        public static string StripPrefix(string name)
        {
            foreach (var sufix in new[] { "Internal" })
            {
                if (name.StartsWith(sufix, StringComparison.Ordinal))
                {
                    name = name.Substring(sufix.Length);
                }
            }
            
            return name;
        }
    }
}
