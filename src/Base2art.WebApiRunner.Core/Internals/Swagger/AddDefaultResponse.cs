﻿
using System;
using System.IO;
using System.Linq;
using Swashbuckle.Swagger;
namespace Base2art.WebApiRunner.Internals.Swagger
{
    public class AddDefaultResponse : IOperationFilter
    {
        [CLSCompliant(false)]
        public void Apply(Operation operation, SchemaRegistry schemaRegistry, System.Web.Http.Description.ApiDescription apiDescription)
        {
            if (apiDescription.ParameterDescriptions.Count == 0)
            {
                return;
            }
            
            var parms = apiDescription.ParameterDescriptions
                                      .Where(x => x.ParameterDescriptor == null || x.ParameterDescriptor.ParameterType.IsAbstract || x.ParameterDescriptor.ParameterType.IsInterface)
                                      .ToArray();
            
            foreach (var parm in parms)
            {
                var name = parm.Name;
                
                var items = operation.parameters.Where(x => x.name.StartsWith(name + ".", StringComparison.Ordinal)).ToArray();
                
                
                foreach (var item in items)
                {
                    operation.parameters.Remove(item);
                }
            }
        }
    }
}
