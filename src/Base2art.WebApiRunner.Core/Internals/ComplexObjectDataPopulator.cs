﻿namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    
    public class ComplexObjectDataPopulator
    {
        private readonly string baseName;

        private readonly object instance;

        private readonly IEnumerable<KeyValuePair<string, string>> requestData;

        private readonly StringComparison comp = StringComparison.OrdinalIgnoreCase;
        
        public ComplexObjectDataPopulator(
            string baseName,
            object instance,
            IEnumerable<KeyValuePair<string, string>> requestData)
        {
            this.requestData = requestData;
            this.instance = instance;
            this.baseName = baseName;
        }

        public void ProcessData()
        {
            foreach (var pair in requestData.Where(x => x.Key.StartsWith(baseName + ".", comp)))
            {
                var key = pair.Key.Substring(baseName.Length + 1);
                
                this.HandleProperty(instance, key, pair.Value);
            }
        }

        private void HandleProperty(object value, string key, string finalValue)
        {
            //                        if (key.Length == 0)
            //                        {
            //                            this.SetValue(value);
            //                            return;
            //                        }
            
            if (key[0] == '[')
            {
                this.HandleArray(value, key, finalValue);
                return;
            }
            
            if (char.IsLetterOrDigit(key[0]) || key[0] == '_')
            {
                this.HandlePropertyValue(value, key, finalValue);
                return;
            }
            
            throw new InvalidOperationException();
        }

        void HandlePropertyValue(object value, string key, string finalValue)
        {
            var parts = key.TakeWhile(x => char.IsLetterOrDigit(x) || x == '_').ToArray();

            var subKey = key;
            var useFinal = true;
            if (parts.Length != key.Length)
            {
                subKey = new string(parts);
                useFinal = false;
            }
            
            var itemType = value.GetType();
            var allMembers = itemType.GetMembers(BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic);
            var members = allMembers.Where(x => string.Equals(x.Name, subKey, comp));
            
            
            foreach (var member in members)
            {
                var next = this.SetValueForMember(member, value, key, finalValue, useFinal);
                if (next != null)
                {
                    if (!useFinal)
                    {
                        var newKey = key.Substring(subKey.Length);
                        if (newKey.StartsWith("."))
                        {
                            newKey = newKey.Substring(1);
                        }
                        
                        this.HandleProperty(next, newKey, finalValue);
                    }
                }
            }
            
            
        }
        
        private void HandleArray(object value, string key, string finalValue)
        {
            
        }

        private object SetValueForMember(MemberInfo member, object value, string key, string finalValue, bool useFinal)
        {
            if (member.MemberType == MemberTypes.Property)
            {
                return this.SetValueForProperty(member as PropertyInfo, value, finalValue, useFinal);
            }
            
            if (member.MemberType == MemberTypes.Field)
            {
                return this.SetValueForField(member as FieldInfo, value, finalValue, useFinal);
            }
            
            // return null;
            throw new InvalidOperationException("UnsupportedMemberType");
        }

        private object SetValueForField(FieldInfo field, object value, string finalValue, bool useFinal)
        {
            return SetValueForWithDelegates(
                () => field != null,
                () => field != null,
                () => field.GetValue(value),
                () => field.FieldType,
                () => field.FieldType,
                (x) => field.SetValue(value, x),
                finalValue,
                useFinal);
        }
        
        private object SetValueForProperty(
            PropertyInfo prop,
            object value,
            string finalValue,
            bool useFinal)
        {
            return SetValueForWithDelegates(
                () => prop != null && prop.GetMethod != null,
                () => prop != null && prop.SetMethod != null,
                () => prop.GetMethod.Invoke(value, new object[]{ }),
                () => prop.GetMethod.ReturnType,
                () => prop.SetMethod.GetParameters()[0].ParameterType,
                (x) => prop.SetMethod.Invoke(value, new object[]{ x }),
                finalValue,
                useFinal);
        }

        private object SetValueForWithDelegates(
            Func<bool> hasGetAccess,
            Func<bool> hasSetAccess,
            Func<object> getValue,
            Func<Type> getReturnType,
            Func<Type> getSetType,
            Action<object> setValue,
            string finalValue,
            bool useFinal)
        {
            object newValue = null;
            
            if (!useFinal)
            {
                if (hasGetAccess())
                {
                    var next = getValue();
                    if (next != null)
                    {
                        return next;
                    }
                    
                    try
                    {
                        newValue = Activator.CreateInstance(getReturnType());
                    }
                    catch
                    {
                        return null;
                    }
                }
            }
            
            if (hasSetAccess())
            {
                if (useFinal)
                {
                    var foo = TypeDescriptor.GetConverter(getSetType());
                    if (!foo.CanConvertFrom(typeof(string)))
                    {
                        return null;
                    }
                    
                    try
                    {
                        newValue = foo.ConvertFromInvariantString(finalValue);   
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                
                setValue(newValue);
                
                return newValue;
            }
            
            return null;
        }
    }
}
