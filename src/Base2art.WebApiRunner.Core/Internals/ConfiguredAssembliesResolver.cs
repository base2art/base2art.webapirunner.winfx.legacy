﻿
using System;

using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Http.Dispatcher;
using Base2art.Collections;
using Base2art.WebApiRunner.Config;

namespace Base2art.WebApiRunner.Internals
{
    public class ConfiguredAssembliesResolver : IAssembliesResolver
    {
        private readonly ServerConfiguration configuration;

        private readonly string configDir;
        
        public ConfiguredAssembliesResolver(string configDir, ServerConfiguration configuration)
        {
            this.configDir = configDir;
            this.configuration = configuration;
            
            AppDomain.CurrentDomain.AssemblyResolve += this.AppDomain_CurrentDomain_AssemblyResolve;
        }
        
        public ICollection<Assembly> GetAssemblies()
        {
            var asms = new List<string>(configuration.JointAssemblies());
            
            Func<Assembly, string> asmKeyLookup = (x) => {
                var name = x.GetName();
                return name.Name.ToUpperInvariant() + "." + name.Version;
            };
            
            KeyedCollection<string, Assembly> asmSet = new KeyedCollection<string, Assembly>(asmKeyLookup);
            
            foreach (var known in AppDomain.CurrentDomain.GetAssemblies())
            {
                if (!asmSet.Contains(known))
                {
                    asmSet.Add(known);
                }
            }
            
            Load(asmSet, asms);
            var knownAsms =  new[]{ 
                "Base2art.WebApiRunner.ServerContrib",  
                "Base2art.WebApiRunner.Contrib", 
                "Base2art.WebApiRunner.Api", 
                "Base2art.WebApiRunner.Server.Api" };
            
            Load(asmSet,knownAsms);
            
            return asmSet
                .Where(x => x != null)
                .ToArray();
        }

        private void Load(KeyedCollection<string, Assembly> asmSet, IEnumerable<string> arr)
        {
            foreach (var item in arr)
            {
                this.LoadItem(asmSet, item);
            }
        }

        private void LoadItem(KeyedCollection<string, Assembly> asmSet, string item)
        {
            try
            {
                //                if (asmSet.Contains(item.ToUpperInvariant()))
                //                {
                //                    return;
                //                }
                
                var asm = Assembly.Load(item);
                if (asm == null)
                {
                    return;
                }
                
                if (asmSet.Contains(asm))
                {
                    return;
                }
                
                asmSet.Add(asm);
                var asms = asm.GetReferencedAssemblies();
                foreach (var asmName in asms)
                {
                    if (asmName.Name.StartsWith("Microsoft.", StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }
                    
                    if (asmName.Name.StartsWith("mscorlib", StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }
                    
                    if (asmName.Name.StartsWith("System.", StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }
                    
                    if (asmName.Name.Equals("System", StringComparison.InvariantCultureIgnoreCase))
                    {
                        continue;
                    }
                    
                    LoadItem(asmSet, asmName.Name);
                }
            }
            catch (FileNotFoundException)
            {
            }
        }
        
        private Assembly AppDomain_CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            var name = args.Name;
            
            name = name.Split(new[] { ',' })[0];
            
            IEnumerable<string> items = this.configuration.JointAssemblySearchPaths();
            if (!string.IsNullOrWhiteSpace(this.configDir))
            {
                items = items.Union(this.configuration.JointAssemblySearchPaths().Select(x => Path.Combine(this.configDir, x)));
            }
            
            Func<string> itemDll = () => items.Select(x => Path.Combine(x, name + ".dll"))
                .FirstOrDefault(File.Exists);
            Func<string> itemExe = () => items.Select(x => Path.Combine(x, name + ".exe"))
                .FirstOrDefault(File.Exists);
            
            var data = itemDll() ?? itemExe();
            if (data == null)
            {
                return null;
            }

            return Assembly.LoadFile(new FileInfo(data).FullName);
        }
    }
}
