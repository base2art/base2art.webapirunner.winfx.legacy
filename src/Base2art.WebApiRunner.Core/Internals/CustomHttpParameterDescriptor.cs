﻿namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.ObjectModel;
    using System.Net.Http;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Metadata;
    
    public class CustomHttpParameterDescriptor : HttpParameterDescriptor
    {
        private readonly string parentName;

        private readonly PropertyInfo info;
        
        public CustomHttpParameterDescriptor(
            HttpActionDescriptor descriptor,
            HttpConfiguration configuration,
            string parentName,
            PropertyInfo info)
        {
            this.info = info;
            this.parentName = parentName;
            this.ActionDescriptor = descriptor;
            this.Configuration = configuration;
            this.ParameterBinderAttribute = new CustomModelBinder(this.ParameterType);
        }
        
        public override string ParameterName
        {
            get { return string.Concat(parentName, ".", this.info.Name); }
        }
        
        public sealed override Type ParameterType
        {
            get
            {
                return this.info.PropertyType;
            }
        }
        
        //        public override string Prefix
        //        {
        //            get
        //            {
        //                return this.backing.Prefix;
        //            }
        //        }
        
        public sealed override ParameterBindingAttribute ParameterBinderAttribute
        {
            get;
            set;
        }
        
        //        public override object DefaultValue
        //        {
        //            get
        //            {
        //                return this.ParameterType;
        //            }
        //        }
        
        public override bool IsOptional
        {
            get
            {
                return true;
            }
        }
        
        public override System.Collections.ObjectModel.Collection<T> GetCustomAttributes<T>()
        {
            return new Collection<T>();
            //            return this.backing.GetCustomAttributes<T>();
        }
        
        private class CustomModelBinder : ParameterBindingAttribute
        {
            private readonly Type type;

            public CustomModelBinder(Type type)
            {
                this.type = type;
            }

            public override HttpParameterBinding GetBinding(HttpParameterDescriptor parameter)
            {
                return new Binding(type, parameter);
            }
        }
        
        private class Binding : HttpParameterBinding
        {
            private readonly Type type;

            private readonly HttpParameterDescriptor parameter;
            
            public Binding(Type type, HttpParameterDescriptor parameter)
                : base(parameter)
            {
                this.parameter = parameter;
                this.type = type;
            }
            
            /// <summary>Asynchronously executes parameter binding.</summary>
            /// <returns>The binded parameter.</returns>
            /// <param name="metadataProvider">The metadata provider.</param>
            /// <param name="actionContext">The action context.</param>
            /// <param name="cancellationToken">The cancellation token.</param>
            public override Task ExecuteBindingAsync(ModelMetadataProvider metadataProvider, HttpActionContext actionContext, CancellationToken cancellationToken)
            {
                string parameterName = base.Descriptor.ParameterName;
                HttpRequestMessage request = actionContext.ControllerContext.Request;
                actionContext.ActionArguments.Add(parameterName, request);
                return Task.FromResult(true);
            }
        }
    }
}
