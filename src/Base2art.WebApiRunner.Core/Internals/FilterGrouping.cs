namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http.Filters;

    internal class FilterGrouping
    {
        private IActionFilter[] _actionFilters;

        private IAuthorizationFilter[] _authorizationFilters;

        private IAuthenticationFilter[] _authenticationFilters;

        private IExceptionFilter[] _exceptionFilters;

        public IActionFilter[] ActionFilters
        {
            get { return this._actionFilters; }
        }

        public IAuthorizationFilter[] AuthorizationFilters
        {
            get { return this._authorizationFilters; }
        }

        public IAuthenticationFilter[] AuthenticationFilters
        {
            get { return this._authenticationFilters; }
        }

        public IExceptionFilter[] ExceptionFilters
        {
            get { return this._exceptionFilters; }
        }

        public FilterGrouping(IEnumerable<FilterInfo> filters)
        {
            List<FilterInfo> list = filters.ToList<FilterInfo>();
            List<FilterInfo> overrideFilters = (from f in list
                                                where f.Instance is IOverrideFilter
                                                select f).ToList<FilterInfo>();
            FilterScope overrideFiltersBeforeScope = FilterGrouping.SelectLastOverrideScope<IActionFilter>(overrideFilters);
            FilterScope overrideFiltersBeforeScope2 = FilterGrouping.SelectLastOverrideScope<IAuthorizationFilter>(overrideFilters);
            FilterScope overrideFiltersBeforeScope3 = FilterGrouping.SelectLastOverrideScope<IAuthenticationFilter>(overrideFilters);
            FilterScope overrideFiltersBeforeScope4 = FilterGrouping.SelectLastOverrideScope<IExceptionFilter>(overrideFilters);
            this._actionFilters = FilterGrouping.SelectAvailable<IActionFilter>(list, overrideFiltersBeforeScope);
            this._authorizationFilters = FilterGrouping.SelectAvailable<IAuthorizationFilter>(list, overrideFiltersBeforeScope2);
            this._authenticationFilters = FilterGrouping.SelectAvailable<IAuthenticationFilter>(list, overrideFiltersBeforeScope3);
            this._exceptionFilters = FilterGrouping.SelectAvailable<IExceptionFilter>(list, overrideFiltersBeforeScope4);
        }

        private static T[] SelectAvailable<T>(List<FilterInfo> filters, FilterScope overrideFiltersBeforeScope)
        {
            return (from f in filters
                    where f.Scope >= overrideFiltersBeforeScope && f.Instance is T
                    select (T)((object)f.Instance)).ToArray<T>();
        }

        private static FilterScope SelectLastOverrideScope<T>(List<FilterInfo> overrideFilters)
        {
            FilterInfo filterInfo = (from f in overrideFilters
                                     where ((IOverrideFilter)f.Instance).FiltersToOverride == typeof(T)
                                     select f).LastOrDefault<FilterInfo>();
            if (filterInfo == null)
            {
                return FilterScope.Global;
            }

            return filterInfo.Scope;
        }
    }
}
