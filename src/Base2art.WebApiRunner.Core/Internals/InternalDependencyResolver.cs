﻿
namespace Base2art.WebApiRunner
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using System.Reflection;
    using System.Web.Http.Dependencies;
    using Base2art.ComponentModel;
    using Base2art.WebApiRunner.Reflection;

    public class InternalDependencyResolver : Component, IDependencyResolver, IBindableDependencyResolver
    {
        private readonly IBindableAndSealableServiceLoaderInjector loader;

        public InternalDependencyResolver()
        {
            this.loader = ServiceLoader.CreateLoader();
        }
        
        public static IEnumerable<Type> GetAllInterfacesImplementedBy<T>()
        {
            return typeof(T).GetInterfaces();
        }

        public void RegisterInstance<T>(T instance, bool registerImplementedInterfaces)
        {
            var items = new List<Tuple<Type, object>>();
            items.Add(Tuple.Create(typeof(T), (object)instance));
            
            if (registerImplementedInterfaces)
            {
                foreach (var item in GetAllInterfacesImplementedBy<T>())
                {
                    items.Add(Tuple.Create(item, (object)instance));
                }
            }
            
            foreach (var item in items)
            {
                var parm1 = ServiceLoaderBindingType.Singleton;
                
                MethodInfo method1 = typeof(InternalDependencyResolver).GetMethod("Register1NonGeneric");
                MethodInfo generic1 = method1.MakeGenericMethod(item.Item1);
                MethodInfo method2 = typeof(InternalDependencyResolver).GetMethod("Register3NonGeneric");
                MethodInfo generic2 = method2.MakeGenericMethod(item.Item1);
                
                var rez = generic1.Invoke(this, new object[]{ parm1 });
                generic2.Invoke(this, new object[]{ rez, item.Item2  });
            }
        }
        
        public void Register(
            Type requested, 
            Type fullfilling, 
            Dictionary<string, object> parameters,
            Dictionary<string, object> properties,
            bool isSingleton)
        {
            var parm1 = isSingleton ? ServiceLoaderBindingType.Singleton : ServiceLoaderBindingType.Instance;
            
            MethodInfo method1 = typeof(InternalDependencyResolver).GetMethod("Register1NonGeneric");
            MethodInfo generic1 = method1.MakeGenericMethod(requested);
            MethodInfo method2 = typeof(InternalDependencyResolver).GetMethod("Register2NonGeneric");
            MethodInfo generic2 = method2.MakeGenericMethod(requested, fullfilling);
            
            var item = generic1.Invoke(this, new object[]{ parm1 });
            generic2.Invoke(this, new object[]{ item, parameters ?? new Dictionary<string, object>(), properties ?? new Dictionary<string, object>()  });
        }
        
        public IServiceLoaderBinding<T> Register1NonGeneric<T>(ServiceLoaderBindingType bindingType)
        {
            return this.loader.Bind<T>()
                .As(bindingType);
        }
        
        public void Register2NonGeneric<T, TConcrete>(IServiceLoaderBinding<T> binding, IDictionary<string, object> inputParms, IDictionary<string, object> properties)
            where TConcrete : T
        {
            binding.To((injector) => ResolverExtensions.CreateFrom<TConcrete>(this, inputParms, properties));
        }
        
        public void Register3NonGeneric<T>(IServiceLoaderBinding<T> binding, T instance)
        {
            binding.To(instance);
        }
        
        IDependencyScope IDependencyResolver.BeginScope()
        {
            return new Scope(this.loader);
        }

        object IDependencyScope.GetService(System.Type serviceType)
        {
            var result = this.loader.Resolve(serviceType, true);
            if (result != null)
            {
                return result;
            }
            
            return null;
        }

        IEnumerable<object> IDependencyScope.GetServices(System.Type serviceType)
        {
            return this.loader.ResolveAll(serviceType, true);
        }
        
        private class Scope : Component, IDependencyScope
        {
            private readonly IBindableAndSealableServiceLoaderInjector loader;

            public Scope(IBindableAndSealableServiceLoaderInjector loader)
            {
                this.loader = loader;
            }

            object IDependencyScope.GetService(System.Type serviceType)
            {
                var result = this.loader.Resolve(serviceType, true);
                if (result != null)
                {
                    return result;
                }
                
//                if (serviceType.IsClass && !serviceType.IsAbstract)
//                {
//                    return serviceType.CreateFrom(this, new Dictionary<string, string>(), new Dictionary<string, object>());
//                }
                
                return null;
            }

            IEnumerable<object> IDependencyScope.GetServices(System.Type serviceType)
            {
                return this.loader.ResolveAll(serviceType, true);
            }
        }
    }
}
