﻿namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.Aspects;

    public class MethodInterceptAspectInvoker// : IMethodInterceptAspect
    {
        private readonly List<Tuple<IMethodInterceptAspect, Dictionary<string, object>>> intercepts;
        
        public MethodInterceptAspectInvoker(List<Tuple<IMethodInterceptAspect, Dictionary<string, object>>> intercepts)
        {
            if (intercepts == null)
            {
                throw new ArgumentNullException("intercepts");
            }
            
            this.intercepts = intercepts.ToArray().Reverse().ToList();
        }

        
        public void Intercept(
            Func<object[], Task<object>> next,
            MethodInterceptData data,
            System.Web.Http.Controllers.HttpControllerContext controllerContext,
            System.Web.Http.Controllers.HttpActionDescriptor actionDescriptor)
        {
            // BUILD CHAIN;
            List<Func<object[], Task<object>>> calls = new List<Func<object[], Task<object>>>();
            for (int i = 0; i < intercepts.Count; i++)
            {
                var intercep = intercepts[i];
                calls.Add(this.InvokeNext(data, controllerContext, actionDescriptor, intercep, i + 1, calls));
            }
            
            calls.Add(next);
            data.ReturnValue = calls.First()(data.InputParameters);
        }

        private Func<object[], Task<object>> InvokeNext(
            MethodInterceptData data,
            System.Web.Http.Controllers.HttpControllerContext controllerContext,
            System.Web.Http.Controllers.HttpActionDescriptor actionDescriptor,
            Tuple<IMethodInterceptAspect, Dictionary<string, object>> intercep,
            int j,
            List<Func<object[], Task<object>>> calls)
        {
//            return x => NewMethod(data, controllerContext, actionDescriptor, intercep, j, calls);
//            
//            /*
            return async x => await NewMethod(data, controllerContext, actionDescriptor, intercep, j, calls);
//             */
        }

        //        static Task<object> NewMethod(MethodInterceptData data, System.Web.Http.Controllers.HttpControllerContext controllerContext, System.Web.Http.Controllers.HttpActionDescriptor actionDescriptor, Tuple<IMethodInterceptAspect, Dictionary<string, object>> intercep, int j, List<Func<object[], Task<object>>> calls)
        //        {
        //                intercep.Item1.Intercept(calls[j], data, intercep.Item2, controllerContext, actionDescriptor);
        //                return data.ReturnValue;
        //        }

        static Task<object> NewMethod(MethodInterceptData data, System.Web.Http.Controllers.HttpControllerContext controllerContext, System.Web.Http.Controllers.HttpActionDescriptor actionDescriptor, Tuple<IMethodInterceptAspect, Dictionary<string, object>> intercep, int j, List<Func<object[], Task<object>>> calls)
        {
            IMethodInterceptAspect interceptor = intercep.Item1;
            interceptor.Intercept(calls[j], data, intercep.Item2, controllerContext, actionDescriptor);
            return data.ReturnValue;
        }
    }
}
