﻿
namespace Base2art.WebApiRunner.Internals.Bindings
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Linq.Expressions;
    using System.Text;
    using System.Web.Http.ModelBinding;
    using Base2art.WebApiRunner.Proxies;
    using Newtonsoft.Json;

    public class PrinterWriterModelBinder : IModelBinder
    {
        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var items = actionContext.ActionDescriptor.ActionBinding.ParameterBindings;
            
            var ow = new ObjectWriter();
            var consoleWrapper = new ObjectTextWriterWrapper(Console.Out);
            
            var aggregate = new Aggregate<TextWriter>(consoleWrapper, ow);
            
            
            aggregate.AddMapping(x => x.ToString(), x =>
                {
                    var sb = new StringBuilder();
                    foreach (var element in ow.Objects)
                    {
                        if (element == null)
                        {
                            sb.AppendLine();
                        }
                        else
                        {
                                             
                            var obj = JsonConvert.SerializeObject(element);
                            var obj1 = JsonConvert.DeserializeObject<dynamic>(obj);
                            sb.Append(obj1.ToString());
                        }
                    }
                    return sb.ToString();
                });
            
            aggregate.AddActionMapping(x => x.Dispose(), x =>
                {
                });
            
            bindingContext.Model = aggregate.Value;
            return true;
        }
        
        private class ObjectWriter : TextWriter
        {
            private readonly List<object> objs = new List<object>();
            
            public override System.Text.Encoding Encoding
            {
                get
                {
                    return System.Text.Encoding.Default;
                }
            }

            public object[] Objects
            {
                get{ return this.objs.ToArray(); }
            }
            
            public override void Write(char[] buffer, int index, int count)
            {
                this.objs.Add(new String(buffer, index, count));
            }

            public override void Write(object value)
            {
                this.objs.Add(value);
            }
            
            public override void Write(string value)
            {
                this.objs.Add(value);
            }

            public override void WriteLine()
            {
                this.objs.Add(null);
            }

            public override void WriteLine(string value)
            {
                this.objs.Add(value);
                this.objs.Add(null);
            }

            public override void WriteLine(object value)
            {
                this.objs.Add(value);
                this.objs.Add(null);
            }
        }
    }
    
    
}
