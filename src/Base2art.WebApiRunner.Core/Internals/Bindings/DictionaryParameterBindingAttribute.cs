﻿
namespace Base2art.WebApiRunner.Internals.Bindings
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Net.Http.Formatting;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.ModelBinding;
    using System.Web.Http.Validation;
    
    public class DictionaryParameterBindingAttribute : ParameterBindingAttribute
    {
        public override HttpParameterBinding GetBinding(HttpParameterDescriptor parameter)
        {
            HttpConfiguration configuration = parameter.Configuration;
            IEnumerable<MediaTypeFormatter> formatters = configuration.Formatters;
            IBodyModelValidator bodyModelValidator = configuration.Services.GetBodyModelValidator();
            
            return new DictionaryModelBinder(parameter, formatters, bodyModelValidator);
        }
        
        private class DictionaryModelBinder : FormatterParameterBinding
        {
            public DictionaryModelBinder(
                HttpParameterDescriptor descriptor, 
                IEnumerable<MediaTypeFormatter> formatters, 
                IBodyModelValidator bodyModelValidator)
                : base(descriptor, formatters, bodyModelValidator)
            {
            }
            
            public override Task<object> ReadContentAsync(
                HttpRequestMessage request,
                Type type,
                IEnumerable<MediaTypeFormatter> formatters,
                IFormatterLogger formatterLogger,
                CancellationToken cancellationToken)
            {
                return base.ReadContentAsync(request, type, formatters, formatterLogger, cancellationToken)
                    .ContinueWith(x => Create(x, type), TaskContinuationOptions.OnlyOnRanToCompletion);
            }

            private static object Create(Task<object> x, Type t)
            {
                if(x.Result != null)
                {
                    return x.Result;
                }
                
                if (t == typeof(IDictionary<string, string>) || t == typeof(Dictionary<string, string>))
                {
                    return new Dictionary<string, string>();
                }
                
                if (t == typeof(IDictionary<string, object>) || t == typeof(Dictionary<string, object>))
                {
                    return new Dictionary<string, object>();
                }
                
                return Activator.CreateInstance(t);
            }
        }
    }
}





