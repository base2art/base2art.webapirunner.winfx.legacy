﻿namespace Base2art.WebApiRunner.Internals.Bindings
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http.Controllers;
    using System.Web.Http.ModelBinding;
    using System.Web.Http.ModelBinding.Binders;

    public class SpecializedArrayModelBinder<TElement>  : CollectionModelBinder<TElement>
    {
        public override bool BindModel(HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            if (bindingContext.ModelMetadata.IsReadOnly)
            {
                return false;
            }

            var valueProvider = bindingContext.ValueProvider;
            var modelName = bindingContext.ModelName;
            var rez = valueProvider.ContainsPrefix(modelName);
            if (!rez)
            {
                return this.CreateOrReplaceCollection(actionContext, bindingContext, new List<TElement>());
            }
            
            return base.BindModel(actionContext, bindingContext);
        }

        protected override bool CreateOrReplaceCollection(HttpActionContext actionContext, ModelBindingContext bindingContext, IList<TElement> newCollection)
        {
            bindingContext.Model = newCollection.ToArray<TElement>();
            return true;
        }
    }
}
