namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.ComponentModel;
    using System.Diagnostics.CodeAnalysis;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Reflection;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;
    using System.Web.Http.ModelBinding.Binders;
    using Base2art.WebApiRunner.Aspects;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Internals.Bindings;
    using Base2art.WebApiRunner.Reflection;

    public class CustomReflectedHttpActionDescriptor : HttpActionDescriptor
    {
        private readonly ReflectedHttpActionDescriptor backing;

        private Lazy<ActionExecutor> _actionExecutor;
        
        private readonly List<TypedMethodConfiguration> methods;
        
        private static readonly Type TaskGenericType = typeof(Task<>);

        private readonly ControllerType controllerType;

        private readonly IEnumerable<IMethodInterceptAspect> knownAspects;
        
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomReflectedHttpActionDescriptor"/> class.
        /// </summary>
        /// <remarks>The default constructor is intended for use by unit testing only.</remarks>
        public CustomReflectedHttpActionDescriptor(
            HttpControllerDescriptor controllerDescriptor,
            List<TypedMethodConfiguration> methods,
            MethodInfo item,
            ControllerType controllerType,
            IEnumerable<IMethodInterceptAspect> knownAspects)
            : base(controllerDescriptor)
        {
            this.knownAspects = knownAspects;
            this.controllerType = controllerType;
            this.backing = new ReflectedHttpActionDescriptor(controllerDescriptor, item);
            this.methods = methods;
            
            this._actionExecutor = new Lazy<ActionExecutor>(() => InitializeActionExecutor(MethodInfo));
            
        }
        
        public bool IsTask
        {
            get
            {
                return this.controllerType == ControllerType.Task;
            }
        }
        
        public override IActionResultConverter ResultConverter
        {
            get
            {
                return this.IsTask
                    ? new ValueResultConverter<string>()
                    : this.backing.ResultConverter;
            }
        }
        
        public override string ActionName
        {
            get { return this.backing.ActionName; }
        }
        
        public TypedMethodConfiguration Method
        {
            get
            {
                return this.methods.First(x =>
                                          x.MethodConfiguration.Type == this.ControllerDescriptor.ControllerType.FullName &&
                                          string.Equals(x.MethodConfiguration.Url, this.GetRouteTemplate(), StringComparison.OrdinalIgnoreCase) &&
                                          x.MethodConfiguration.Method == this.ActionName);
            }
        }

        public override Collection<HttpMethod> SupportedHttpMethods
        {
            get
            {
                var method = this.Method;
                var verb = string.IsNullOrWhiteSpace(method.MethodConfiguration.Verb) ? HttpMethod.Get : new HttpMethod(method.MethodConfiguration.Verb.ToUpper());
                
                return new Collection<HttpMethod>(){ verb };
            }
        }

        public MethodInfo MethodInfo
        {
            get { return (this.backing).MethodInfo; }
            //            set { this.backing.MethodInfo = (value); }
        }
        
        public override Collection<T> GetCustomAttributes<T>(bool inherit)
        {
            return this.backing.GetCustomAttributes<T>(inherit);
        }
        
        public override Collection<FilterInfo> GetFilterPipeline()
        {
            return this.backing.GetFilterPipeline();
        }

        /// <inheritdoc/>
        public override Type ReturnType
        {
            get
            {
                return this.IsTask ? typeof(string) : this.backing.ReturnType;
            }
        }

        public override Collection<T> GetCustomAttributes<T>()
        {
            return this.backing.GetCustomAttributes<T>();
        }

        /// <inheritdoc/>
        public override Task<object> ExecuteAsync(
            HttpControllerContext controllerContext,
            IDictionary<string, object> arguments,
            CancellationToken cancellationToken)
        {
            if (controllerContext == null)
            {
                throw new ArgumentNullException("controllerContext");
            }

            if (arguments == null)
            {
                throw new ArgumentNullException("arguments");
            }
            
            
            Func<object[], Task<object>> runner = argumentValues =>
            {
                var item = _actionExecutor.Value.Execute(controllerContext.Controller, argumentValues);
                if (this.IsTask)
                {
                    return this.GetAsyncTextWriterOutput(item, argumentValues);
                }
                
                return item;
            };
            
            //                                            Aspects = tasksAspects.Union(x.Aspects ?? new AspectsConfiguration()).ToArray()
            var plugins = this.Method.MethodConfiguration.Aspects ?? new AspectConfiguration[0];
            var comp = StringComparison.OrdinalIgnoreCase;
            var aspects = plugins.Select(x =>
                                         {
                                             var name = x.Name;
                                             var first = this.knownAspects.FirstOrDefault(y => string.Equals(y.Name, name, comp));
                                             return Tuple.Create(first, x.Parameters);
                                         })
                .ToList();
            
            var aspect = new MethodInterceptAspectInvoker(aspects);
            
            Func<Task<object>> runWith = () =>
            {
                object[] argumentValues = PrepareParameters(arguments, controllerContext);
                
                var data = new MethodInterceptData(this.MethodInfo, argumentValues);
                
                aspect.Intercept(runner, data, controllerContext, this);
                return data.ReturnValue;
            };
            
            
            return Task.Run(runWith, cancellationToken);
        }

        public override Collection<IFilter> GetFilters()
        {
            return this.backing.GetFilters();
        }

        public override Collection<HttpParameterDescriptor> GetParameters()
        {
            var parms = this.backing.GetParameters();
            if (!this.IsTask)
            {
                if (this.SupportedHttpMethods.Count > 1)
                {
                    throw new InvalidOperationException("Bad Count");
                }
                
                var method = this.SupportedHttpMethods[0];
                
                if (method == HttpMethod.Get)
                {
                    List<HttpParameterDescriptor> rez = new List<HttpParameterDescriptor>();
                    
                    foreach (var el in parms)
                    {
                        if (el.ParameterType.IsArray || this.CanConvertFrom(el.ParameterType))
                        {
                            rez.Add(el);
                        }
                        else
                        {
                            if (el.ParameterBinderAttribute == null)
                            {
                                var vals = this.Configuration.Services.GetModelBinderProviders();
                                var firstBinder = vals.Select(x => x.GetBinder(this.Configuration, el.ParameterType))
                                    .FirstOrDefault(x => x != null);
                                if (firstBinder != null && !(firstBinder is TypeMatchModelBinder))
                                {
                                    //                                    el.ParameterBinderAttribute = new WrappedBindingAttribute(firstBinder);
                                    rez.Add(el);
                                }
                                else
                                {
                                    foreach (var prop in el.ParameterType.GetProperties())
                                    {
                                        rez.Add(new CustomHttpParameterDescriptor(
                                            this,
                                            this.Configuration,
                                            el.ParameterName,
                                            prop));
                                    }
                                }
                            }
                        }
                    }
                    
                    return new Collection<HttpParameterDescriptor>(rez);
                }
                
                return parms;
            }
            
            var items = parms.Where(x => x.ParameterType.GetInterfaces().Union(new []{ x.ParameterType })
                                    .Any(y => y.IsGenericTypeOf(typeof(IDictionary<,>))));
            
            foreach (var item in items)
            {
                item.ParameterBinderAttribute = new DictionaryParameterBindingAttribute();
            }
            
            return parms;
        }

        private object[] PrepareParameters(IDictionary<string, object> parameters, HttpControllerContext controllerContext)
        {
            // This is on a hotpath, so a quick check to avoid the allocation if we have no parameters.
            if (this.GetParameters().Count == 0)
            {
                return new object[0];
            }
            
            Dictionary<string, object> parameterCache = new Dictionary<string, object>();
            return MethodInfo.GetParameters()
                .Select(parameterInfo => ExtractParameterFromDictionary(parameterInfo, parameters, parameterCache, controllerContext))
                .ToArray();
        }

        [SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Caller is responsible for disposing of response instance.")]
        private object ExtractParameterFromDictionary(
            ParameterInfo parameterInfo,
            IDictionary<string, object> parameters,
            IDictionary<string, object> parameterCache,
            HttpControllerContext controllerContext)
        {
            object value = null;
            if (!parameters.TryGetValue(parameterInfo.Name, out value))
            {
                if (!parameterInfo.ParameterType.IsClass && parameterInfo.ParameterType != typeof(string))
                {
                    // the key should always be present, even if the parameter value is null
                    throw new HttpResponseException(controllerContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Bad request", new FormatException(string.Format("{0} {1} {2} {3}", parameterInfo.Name, parameterInfo.ParameterType, MethodInfo, MethodInfo.DeclaringType))));
                }
                
                var vals = controllerContext.Configuration.Services.GetModelBinderProviders();
                
                var type = parameterInfo.ParameterType;
                
                var firstBinder = vals.Select(x => x.GetBinder(controllerContext.Configuration, type)).FirstOrDefault(x => x != null);
                
                
                if ((firstBinder == null || firstBinder is TypeMatchModelBinder) && (!type.IsArray))
                {
                    if (parameterCache.ContainsKey(parameterInfo.Name))
                    {
                        value = parameterCache[parameterInfo.Name];
                    }
                    else
                    {
                        value = Activator.CreateInstance(parameterInfo.ParameterType);
                        parameterCache[parameterInfo.Name] = value;
                    }
                    
                    var parmProps = parameterInfo.ParameterType.GetProperties();
                    
                    foreach (var parmProp in parmProps)
                    {
                        var key = string.Concat(parameterInfo.Name, ".", parmProp.Name);
                        var parameter = parameters
                            .Select(x => Tuple.Create(x.Key, x.Value))
                            .FirstOrDefault(x => string.Equals(x.Item1, key, StringComparison.OrdinalIgnoreCase));
                        
                        if (parameter != null)
                        {
                            var data = parameter.Item2 as HttpRequestMessage;
                            if (data != null)
                            {
                                var pairs = data.GetQueryNameValuePairs();
                                var pair = pairs.Select(x => Tuple.Create(x.Key, x.Value))
                                    .FirstOrDefault(x => string.Equals(x.Item1, key, StringComparison.OrdinalIgnoreCase));
                                if (pair != null)
                                {
                                    
                                    var foo = TypeDescriptor.GetConverter(parmProp.PropertyType);
                                    
                                    parmProp.SetValue(value, foo.ConvertFromInvariantString(pair.Item2));
                                }
                            }
                        }
                    }
                }
            }
            
            if (value == null && !TypeAllowsNullValue(parameterInfo.ParameterType))
            {
                // tried to pass a null value for a non-nullable parameter type
                throw new HttpResponseException(controllerContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Bad request", new FormatException(string.Format("{0} {1} {2} {3}", parameterInfo.Name, parameterInfo.ParameterType, MethodInfo, MethodInfo.DeclaringType))));
            }

            
            var param = this.GetParameters().FirstOrDefault(x => x.ParameterName == parameterInfo.Name);
            if (value == null && param != null)
            {
                Func<bool> canPopulateDefaultValueFromQueryString = () =>
                {
                    if (!CanPopulateDefaultValue(parameterInfo))
                    {
                        return false;
                    }
                    
                    if (controllerContext.Request.Method != HttpMethod.Get)
                    {
                        return false;
                    }
                    
                    return true;
                };
                
                if (canPopulateDefaultValueFromQueryString())
                {
                    value = Activator.CreateInstance(parameterInfo.ParameterType);
                    new ComplexObjectDataPopulator(param.ParameterName, value, controllerContext.Request.GetQueryNameValuePairs()).ProcessData();
                }
            }

            if (value != null && !parameterInfo.ParameterType.IsInstanceOfType(value))
            {
                // value was supplied but is not of the proper type
                throw new HttpResponseException(controllerContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, "Bad request", new FormatException(string.Format("{0} {1} {2} {3} {4}", parameterInfo.Name, MethodInfo, MethodInfo.DeclaringType, value.GetType(), parameterInfo.ParameterType))));
            }

            return value;
        }

        private static ActionExecutor InitializeActionExecutor(MethodInfo methodInfo)
        {
            if (methodInfo.ContainsGenericParameters)
            {
                throw new InvalidOperationException(string.Format("{0} {1}", methodInfo, methodInfo.ReflectedType.FullName));
            }
            
            return new ActionExecutor(methodInfo);
        }
        
        private static bool CanPopulateDefaultValue(ParameterInfo parameterInfo)
        {
            if (!parameterInfo.ParameterType.IsClass)
            {
                return false;
            }
            
            if (parameterInfo.IsOptional)
            {
                return false;
            }
            
            if (parameterInfo.ParameterType.GetConstructor(Type.EmptyTypes) == null)
            {
                return false;
            }
            
            return true;
        }
        
        internal static Type GetTaskInnerTypeOrNull(Type type)
        {
            if (type.IsGenericType && !type.IsGenericTypeDefinition)
            {
                Type genericTypeDefinition = type.GetGenericTypeDefinition();
                if (TaskGenericType == genericTypeDefinition)
                {
                    return type.GetGenericArguments()[0];
                }
            }

            return null;
        }

        private async Task<object> GetAsyncTextWriterOutput(Task<object> item, object[] argumentValues)
        {
            var x = await item;
            
            var sws = argumentValues.OfType<TextWriter>().ToArray();
            if (sws == null || sws.Length != 1)
            {
                return x;
            }
            
            var sw = sws[0];
            sw.Flush();
            
            var value = sw.ToString();
            try
            {
                sw.Dispose();
            }
            catch (Exception)
            {
            }
            
            return value;
        }
        
        internal static bool TypeAllowsNullValue(Type type)
        {
            return !type.IsValueType || IsNullableValueType(type);
        }
        
        internal static bool IsNullableValueType(Type type)
        {
            return Nullable.GetUnderlyingType(type) != null;
        }

        private bool CanConvertFrom(Type parameterType)
        {
            var foo = TypeDescriptor.GetConverter(parameterType);
            return foo.CanConvertFrom(typeof(string));
        }
    }
}
