﻿
using System;
using System.Web.Http.Controllers;

namespace Base2art.WebApiRunner.Internals
{
    public static class ActionDescriptor
    {
        public static string GetRouteTemplate(this HttpActionDescriptor descriptor)
        {
            return (descriptor.Properties["base2art.routeTemplate"] as string) ?? string.Empty;
        }
        
        public static void SetRouteTemplate(this HttpActionDescriptor descriptor, string value)
        {
            descriptor.Properties["base2art.routeTemplate"] = value;
        }
    }
}
