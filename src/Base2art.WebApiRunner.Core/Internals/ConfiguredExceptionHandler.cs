﻿namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Threading.Tasks;
    using System.Web.Http.ExceptionHandling;

    public class ConfiguredExceptionHandler : IExceptionHandler
    {
        private readonly IExceptionHandler oldHandler;

        public ConfiguredExceptionHandler(IExceptionHandler oldHandler)
        {
            this.oldHandler = oldHandler;
        }

        public  async Task HandleAsync(ExceptionHandlerContext context, System.Threading.CancellationToken cancellationToken)
        {
            if (this.oldHandler != null)
            {
                await this.oldHandler.HandleAsync(context, cancellationToken);
            }
        }
    }
}
