namespace Base2art.WebApiRunner.Internals
{
   using System;
   using System.Net.Http;
    using System.Runtime.ExceptionServices;
   using System.Threading;
   using System.Threading.Tasks;
   using System.Web.Http;
   using System.Web.Http.Controllers;
    using System.Web.Http.ExceptionHandling;
   using System.Web.Http.Filters;

   internal class ExceptionFilterResult : IHttpActionResult
   {
      private readonly HttpActionContext _context;

      private readonly IExceptionFilter[] _filters;

      private readonly IExceptionLogger _exceptionLogger;

      private readonly IExceptionHandler _exceptionHandler;

      private readonly IHttpActionResult _innerResult;

      public ExceptionFilterResult(HttpActionContext context, IExceptionFilter[] filters, IExceptionLogger exceptionLogger, IExceptionHandler exceptionHandler, IHttpActionResult innerResult)
      {
         this._context = context;
         this._filters = filters;
         this._exceptionLogger = exceptionLogger;
         this._exceptionHandler = exceptionHandler;
         this._innerResult = innerResult;
      }

      public async Task<HttpResponseMessage> ExecuteAsync(CancellationToken cancellationToken)
      {
         HttpResponseMessage result;
         ExceptionDispatchInfo exceptionDispatchInfo;
         try
         {
            result = await this._innerResult.ExecuteAsync(cancellationToken);
            return result;
         }
         catch (Exception source)
         {
            exceptionDispatchInfo = ExceptionDispatchInfo.Capture(source);
         }
         
         Exception sourceException = exceptionDispatchInfo.SourceException;
         bool flag = sourceException is OperationCanceledException;
         ExceptionContext context = new ExceptionContext(sourceException, ExceptionCatchBlocks.IExceptionFilter, this._context);
         if (!flag)
         {
            await this._exceptionLogger.LogAsync(context, cancellationToken);
         }
         
         HttpActionExecutedContext httpActionExecutedContext = new HttpActionExecutedContext(this._context, sourceException);
         for (int i = this._filters.Length - 1; i >= 0; i--)
         {
            IExceptionFilter exceptionFilter = this._filters[i];
            await exceptionFilter.ExecuteExceptionFilterAsync(httpActionExecutedContext, cancellationToken);
         }

         if (httpActionExecutedContext.Response == null && !flag)
         {
            httpActionExecutedContext.Response = await this._exceptionHandler.HandleAsync(context, cancellationToken);
         }

         if (httpActionExecutedContext.Response == null)
         {
            if (sourceException == httpActionExecutedContext.Exception)
            {
                exceptionDispatchInfo.Throw();
            }

            throw httpActionExecutedContext.Exception;
         }

         result = httpActionExecutedContext.Response;
         return result;
      }
   }
}
