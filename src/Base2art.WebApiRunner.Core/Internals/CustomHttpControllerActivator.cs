namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Net.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Dispatcher;
    using System.Web.Http.Routing;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Internals.Controllers;
    using Base2art.WebApiRunner.Reflection;
    
    public class CustomHttpControllerActivator : IHttpControllerActivator
    {
        public IHttpController Create(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            var httpController = request.GetDependencyScope().GetService(controllerType);
            
            if (httpController == null)
            {
                httpController = this.CreateItem(request, controllerDescriptor, controllerType);
            }
            
            var apiController = httpController as IHttpController;
            if (apiController != null)
            {
                return apiController;
            }
            
            var type = this.GetMethodType(request);
            
            if (type == ControllerType.Task)
            {
                return this.ExecuteForTask(request, controllerDescriptor, controllerType, httpController);
            }
            
            return this.ExecuteForService(request, controllerDescriptor, controllerType, httpController);
        }
        
        public virtual ControllerType GetMethodType(HttpRequestMessage request)
        {
            if (request == null)
            {
                throw new ArgumentNullException("request");
            }
            
            IHttpRouteData routeData = request.GetRouteData();
            if (routeData == null)
            {
                return ControllerType.Service;
            }
            
            object result = null;
            routeData.Values.TryGetValue("controllerType", out result);
            return result as ControllerType? ?? ControllerType.Service;
        }
        

        private IHttpController ExecuteForService(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType, object httpController)
        {
            var backing = httpController;
            if (httpController == null)
            {
                return null;
            }
            
            return request.GetDependencyScope().CreateFrom<InternalHttpController>(new Dictionary<string, object> { { "backing", backing } });
        }

        private IHttpController ExecuteForTask(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType, object httpController)
        {
            var backing = httpController;
            if (httpController == null)
            {
                return null;
            }
            
            return request.GetDependencyScope().CreateFrom<InternalTaskController>(new Dictionary<string, object> { { "backing", backing } });
        }

        private object CreateItem(HttpRequestMessage request, HttpControllerDescriptor controllerDescriptor, Type controllerType)
        {
            /*
                controllerProperties = item.Properties,
                controllerParameters = item.Parameters,
             */
            var data = request.GetRouteData().Route.Defaults;
            Dictionary<string, object> props = null;
            Dictionary<string, string> parms = null;
            if (data.ContainsKey("controllerProperties"))
            {
                props = data["controllerProperties"] as Dictionary<string, object>;
            }
            
            if (data.ContainsKey("controllerParameters"))
            {
                parms = data["controllerParameters"] as Dictionary<string, string>;
            }
            
            return controllerType.CreateFrom(controllerDescriptor.Configuration.DependencyResolver, parms, props);
        }
    }
}
