﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Web.Http;
using System.Web.Http.Controllers;
using System.Web.Http.Routing;
using Base2art.WebApiRunner.Aspects;
using Base2art.WebApiRunner.Config;

namespace Base2art.WebApiRunner.Internals
{
    public class ConfiguredActionSelector : IHttpActionSelector
    {
        private readonly HttpConfiguration config;

        private readonly List<TypedMethodConfiguration> methods;

        private readonly Lazy<IMethodInterceptAspect[]> aspects;
        
        public ConfiguredActionSelector(HttpConfiguration config, List<TypedMethodConfiguration> methods)
        {
            this.methods = methods;
            this.config = config;
            this.aspects = new Lazy<IMethodInterceptAspect[]>(() =>
            {
                var provider = (IMethodInterceptAspectProvider)config.DependencyResolver.GetService(typeof(IMethodInterceptAspectProvider));
                var items = provider.Get();
                return items.ToArray();
            });
        }
        
        public HttpActionDescriptor SelectAction(HttpControllerContext controllerContext)
        {
            IHttpRouteData routeData = controllerContext.Request.GetRouteData();
            if (routeData == null)
            {
                return null;
            }
            
            object result = null;
            routeData.Values.TryGetValue("action", out result);
            var action = result as string;
            
            if (string.IsNullOrWhiteSpace(action))
            {
                return null;
            }
            
            var lookup = action;
            var results = this.GetActionMapping(controllerContext.ControllerDescriptor)[lookup].ToList();
            
            if (results.Count > 1)
            {
                foreach (var descriptor in results)
                {
                    var routeTemplate = descriptor.GetRouteTemplate();
                    if (string.Equals(routeTemplate, routeData.Route.RouteTemplate, StringComparison.OrdinalIgnoreCase))
                    {
                        return descriptor;
                    }
                }
            }
            
            
            return results.First();
        }

        public System.Linq.ILookup<string, HttpActionDescriptor> GetActionMapping(HttpControllerDescriptor controllerDescriptor)
        {
            var filtered = methods.Where(x => x.MethodConfiguration.Type == controllerDescriptor.ControllerType.FullName);
            
            var lookup = filtered.ToLookup(x => x.MethodConfiguration.Method, x => this.Get(controllerDescriptor, x));
            return lookup;
        }
        
        private HttpActionDescriptor Get(HttpControllerDescriptor controllerDescriptor, TypedMethodConfiguration x)
        {
            var item = GetMethods(controllerDescriptor.ControllerType)
                .FirstOrDefault(y => x.MethodConfiguration.Method == y.Name);
            
            if (item == null)
            {
                item = GetMethods(GetExplicitType(controllerDescriptor.ControllerType))
                    .FirstOrDefault(y => x.MethodConfiguration.Method == y.Name);
            }
            
            var knownAspects = this.aspects.Value.Select(y => y).ToArray();
            
            var descriptor = new CustomReflectedHttpActionDescriptor(
                                 controllerDescriptor,
                                 methods,
                                 item,
                                 x.ControllerType,
                                 knownAspects);
            
            descriptor.SetRouteTemplate(x.MethodConfiguration.Url);
            return descriptor;
        }

        private Type GetExplicitType(Type controllerType)
        {
            var explicitInterfaces = new[] {
                //                typeof(IAuthenticatedLoggedTask<,>),
                typeof(IHealthCheck),
                //                typeof(ILoggedTask<>),
                typeof(IParameterizedTask),
                typeof(ISimpleHealthCheck),
                typeof(IStandardTask),
                typeof(ITask),
            };
            
            return controllerType.GetInterfaces().FirstOrDefault(x => explicitInterfaces.Any(y => y == x))
            ?? controllerType;
        }
        
        private IEnumerable<MethodInfo> GetMethods(Type controllerType)
        {
            if (controllerType == typeof(object))
            {
                yield break;
            }
            
            foreach (var item  in controllerType.GetMethods().Union(GetMethods(controllerType.BaseType)))
            {
                yield return item;
            }
        }
    }
}
