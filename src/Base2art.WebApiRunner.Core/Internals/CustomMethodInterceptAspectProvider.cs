﻿
namespace Base2art.WebApiRunner.Internals
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.Dispatcher;
    using Base2art.WebApiRunner.Aspects;
    using Base2art.WebApiRunner.Reflection;

    public class CustomMethodInterceptAspectProvider : IMethodInterceptAspectProvider
    {
        private readonly IAssembliesResolver assemblies;

        private readonly HttpConfiguration config;
        
        public CustomMethodInterceptAspectProvider(IAssembliesResolver assemblies, HttpConfiguration config)
        {
            this.config = config;
            this.assemblies = assemblies;
        }
        
        public IMethodInterceptAspect[] Get()
        {
            var assemblies = this.assemblies.GetAssemblies();
            var interType = typeof(IMethodInterceptAspect);
            var invoker = typeof(MethodInterceptAspectInvoker);
            
            return assemblies.SelectMany(x => x.GetTypes())
                .Where(interType.IsAssignableFrom)
                .Where(x => invoker != x)
                .Where(x => !x.IsAbstract && !x.IsInterface && x.IsClass)
                .Select(x => this.CreateInstance(x))
                .Where(x => x != null)
                .ToArray();
        }

        private IMethodInterceptAspect CreateInstance(Type x)
        {
            var resolver = this.config.DependencyResolver;
            var input = new Dictionary<string, string>();
            var props = new Dictionary<string, object>();
            return (IMethodInterceptAspect)x.CreateFrom(resolver, input, props);
        }
    }
}
