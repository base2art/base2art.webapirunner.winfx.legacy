﻿
namespace Base2art.WebApiRunner.Formatters
{
    using System;
    using System.Collections;
    using System.Data;
    using System.Linq;
    
    public static class DataSetConverter
    {
//        public static DataSet ToDataSet<T>(this System.Collections.Generic.IEnumerable<T> list)
//        {
//            return list.ToDataSet(typeof(T));
//        }
        
        public static DataSet ToDataSet(this System.Collections.Generic.IEnumerable<System.Collections.Generic.IDictionary<string, object>> list)
        {
            DataSet ds = new DataSet();
            DataTable t = new DataTable();
            ds.Tables.Add(t);

            //add a column to table for each public property on T
            foreach (var propInfo in list)
            {
                foreach (string key in propInfo.Keys)
                {
                    if (t.Columns.Cast<DataColumn>().All(x => x.ColumnName != key))
                    {
                        t.Columns.Add(key);
                    }
                }
            }

            //go through each property on T and add each value to the table
            foreach (IDictionary item in list)
            {
                DataRow row = t.NewRow();

                foreach (var propInfo in t.Columns.Cast<DataColumn>())
                {
                    row[propInfo.ColumnName] = item[propInfo.ColumnName] ?? DBNull.Value;
                }

                t.Rows.Add(row);
            }

            return ds;
        }
        
        public static DataSet ToDataSet(this IEnumerable list, Type elementType)
        {
            var dictMatch = typeof(System.Collections.Generic.IDictionary<string, object>);
            if (dictMatch.IsAssignableFrom(elementType))
            {
                return list.Cast<System.Collections.Generic.IDictionary<string, object>>().ToDataSet();
            }
            
            DataSet ds = new DataSet();
            DataTable t = new DataTable();
            ds.Tables.Add(t);

            //add a column to table for each public property on T
            foreach (var propInfo in elementType.GetProperties())
            {
                Type colType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;

                if (colType.IsEnum)
                {
                    colType = typeof(object);
                }
                
                t.Columns.Add(propInfo.Name, colType);
            }

            //go through each property on T and add each value to the table
            foreach (object item in list)
            {
                DataRow row = t.NewRow();

                foreach (var propInfo in elementType.GetProperties())
                {
                    var data = propInfo.GetValue(item, null);
                    row[propInfo.Name] = data ?? DBNull.Value;
                }

                t.Rows.Add(row);
            }

            return ds;
        }
    }
}
