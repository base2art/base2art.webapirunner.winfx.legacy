﻿namespace Base2art.WebApiRunner.Formatters
{
    using System;
    using System.Linq;

    //    public class MarkdownTableFormatter : TableFormatterBase
    //    {
    //        public MarkdownTableFormatter(JsonSerializerSettings serializer) : base(serializer, "text/markdown")
    //        {
    //        }
//
    //        protected override bool DataTableToCSV(DataTable table, StreamWriter writer, bool includeHeader)
    //        {
    //            if (table == null || writer == null)
    //            {
    //                return false;
    //            }
    //            if (includeHeader)
    //            {
    //                string[] columnNames = table.Columns.Cast<DataColumn>().Select(column => "\"" + column.ColumnName.Replace("\"", "\"\"") + "\"").ToArray<string>();
    //                writer.WriteLine(String.Join(",", columnNames));
    //                writer.Flush();
    //            }
    //            foreach (DataRow row in table.Rows)
    //            {
    //                string[] fields = row.ItemArray.Select(field =>
    //                                                       {
    //                                                           var data = field as string;
    //                                                           if (data == null)
    //                                                           {
    //                                                               data = JsonConvert.SerializeObject(field, this.Serializer);
    //                                                               if (data.StartsWith("\"", StringComparison.OrdinalIgnoreCase) && data.EndsWith("\"", StringComparison.OrdinalIgnoreCase))
    //                                                               {
    //                                                                   data = data.Substring(1, data.Length - 2);
    //                                                               }
    //                                                           }
    //                                                           return "\"" + data.Replace("\"", "\"\"") + "\"";
    //                                                       }).ToArray<string>();
    //                writer.WriteLine(String.Join(",", fields));
    //                writer.Flush();
    //            }
    //            return true;
    //        }
//
    //        public static DataSet ToDataSet(IEnumerable list, Type elementType)
    //        {
    //            //            Type elementType = typeof(T);
    //            DataSet ds = new DataSet();
    //            DataTable t = new DataTable();
    //            ds.Tables.Add(t);
    //            //add a column to table for each public property on T
    //            foreach (var propInfo in elementType.GetProperties())
    //            {
    //                Type colType = Nullable.GetUnderlyingType(propInfo.PropertyType) ?? propInfo.PropertyType;
    //                if (colType.IsEnum)
    //                {
    //                    colType = typeof(object);
    //                }
    //                t.Columns.Add(propInfo.Name, colType);
    //            }
    //            //go through each property on T and add each value to the table
    //            foreach (object item in list)
    //            {
    //                DataRow row = t.NewRow();
    //                foreach (var propInfo in elementType.GetProperties())
    //                {
    //                    var data = propInfo.GetValue(item, null);
    //                    row[propInfo.Name] = data ?? DBNull.Value;
    //                }
    //                t.Rows.Add(row);
    //            }
    //            return ds;
    //        }
    //    }
}
/*
            List<int> minimumLenghts = Enumerable.Range(0, table.Columns.Count).Select(x => 0).ToList();
            
            for (int colNum = 0; colNum < minimumLenghts.Count; colNum++)
            {
                if (includeHeader)
                {
                    var colName = table.Columns[colNum].ColumnName ?? "";
                    if (colName.Length > minimumLenghts[colNum])
                    {
                        minimumLenghts[colNum] = colName.Length;
                    }
                }
                
                
                foreach (DataRow row in table.Rows)
                {
                    var colData = row.ItemArray[colNum];
                    
                    var data = colData as string;
                    if (data == null)
                    {
                        data = JsonConvert.SerializeObject(colData, this.Serializer);
                        if (data.StartsWith("\"", StringComparison.OrdinalIgnoreCase) && data.EndsWith("\"", StringComparison.OrdinalIgnoreCase))
                        {
                            data = data.Substring(1, data.Length - 2);
                        }
                    }
                    
                    if (data.Length > minimumLenghts[colNum])
                    {
                        minimumLenghts[colNum] = data.Length;
                    }
                    
                    
                    //                    var data = field as string;
                    //                        if (data == null)
                    //                        {
                    //                            data = JsonConvert.SerializeObject(field, this.Serializer);
                    //                            if (data.StartsWith("\"", StringComparison.OrdinalIgnoreCase) && data.EndsWith("\"", StringComparison.OrdinalIgnoreCase))
                    //                            {
                    //                                data = data.Substring(1, data.Length - 2);
                    //                            }
                    //                        }
                }
            }
 */
