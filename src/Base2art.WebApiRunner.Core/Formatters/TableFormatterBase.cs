﻿namespace Base2art.WebApiRunner.Formatters
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using System.Net.Http.Headers;
    using Base2art.WebApiRunner.SystemServices;
    using Newtonsoft.Json;

    public abstract class TableFormatterBase : OutputFormatter<IEnumerable>
    {
        private readonly IJsonSerializer serializer;

        private readonly string mimeType;

        protected TableFormatterBase(IJsonSerializer serializer, string mimeType) : base(mimeType)
        {
            this.mimeType = mimeType;
            this.serializer = serializer;
        }

        public IJsonSerializer Serializer
        {
            get { return serializer; }
        }
        
        public string MimeType
        {
            get
            {
                return this.mimeType;
            }
        }

        public override bool CanWriteType(Type type)
        {
            var componentType = this.GetElementType(type);
            if (componentType == null)
            {
                return false;
            }
            return this.ArgTypeConforms(componentType);
        }

        public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
        {
            base.SetDefaultContentHeaders(type, headers, mediaType);
            headers.ContentType = new MediaTypeHeaderValue(this.mimeType);
        }
        
        protected override void WriteValueToStream(IEnumerable enumerable, Type type, Stream writeStream)
        {
            var elementType = this.GetElementType(type);
            if (elementType != null && elementType != typeof(object))
            {
                using (var writer = new StreamWriter(writeStream))
                {
                    var @set = enumerable.ToDataSet(elementType);
                    @set.Tables.OfType<DataTable>().ToList().ForEach(x => DataTableToCSV(x, writer, true));
                }
            }
            else
            {
                var array = enumerable.Cast<object>().ToArray();
                if (array.Length == 0)
                {
                }
                else
                {
                    var items = array.Select(x => this.serializer.DeserializeObject<Dictionary<string, object>>(this.serializer.SerializeObject(x)));
                    using (var writer = new StreamWriter(writeStream))
                    {
                        var @set = items.ToDataSet();
                        @set.Tables.OfType<DataTable>().ToList().ForEach(x => DataTableToCSV(x, writer, true));
                    }
                }
            }
        }
        
        protected abstract void DataTableToCSV(DataTable table, TextWriter writer, bool includeHeaders);

        private bool ArgTypeConforms(Type type)
        {
            if (type.IsGenericType)
            {
                var gtd = type.GetGenericTypeDefinition();
                if (typeof(KeyValuePair<, >) == gtd)
                {
                    return true;
                }
            }
            return !type.IsValueType && type != typeof(string);
        }

        private Type GetElementType(Type type)
        {
            if (type.IsArray)
            {
                return type.GetElementType();
            }
            
            var enumType = typeof(IEnumerable);
            var interfaces = type.GetInterfaces();
            if (type.IsInterface)
            {
                interfaces = interfaces.Union(new[] { type }).ToArray();
            }
            
            var @interface = interfaces.OrderByDescending(x => x.IsGenericType).FirstOrDefault(enumType.IsAssignableFrom);
            if (@interface == null)
            {
                return null;
            }
            
            if (!@interface.IsGenericType)
            {
                return null;
            }
            
            return @interface.GetGenericArguments()[0];
        }
    }
}


