namespace Base2art.WebApiRunner.Formatters
{
   using System;
   using System.Linq;
   using System.Net.Http.Formatting;

   public static class BrowserJsonFormatterRegistrar
   {
      public static void ReturnJsonByDefault(this MediaTypeFormatterCollection config)
      {
         config.Add(new BrowserJsonFormatter());
      }
   }
}
