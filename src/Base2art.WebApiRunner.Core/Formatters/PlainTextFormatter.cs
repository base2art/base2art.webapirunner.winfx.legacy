﻿
namespace Base2art.WebApiRunner.Formatters
{
    using System;
    using System.IO;
    using Base2art.WebApiRunner.SystemServices;

    public class PlainTextFormatter : OutputFormatter<object>
    {
        private readonly IObjectStringifier stringifier;

        public PlainTextFormatter(IObjectStringifier stringifier) : base("text/plain")
        {
            this.stringifier = stringifier;
        }
        
        protected override void WriteValueToStream(object realValue, Type declaredType, Stream writeStream)
        {
            var str = this.stringifier.PrettyPrint(realValue);
            var sw = new StreamWriter(writeStream);
            sw.Write(str);
            sw.Flush();
        }
    }
}
