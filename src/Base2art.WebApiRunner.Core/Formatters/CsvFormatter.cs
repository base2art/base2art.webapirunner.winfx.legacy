﻿namespace Base2art.WebApiRunner.Formatters
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.IO;
    using System.Linq;
    using Base2art.WebApiRunner.SystemServices;
    using Newtonsoft.Json;
    
    public class CsvFormatter : TableFormatterBase
    {
        public CsvFormatter(IJsonSerializer serializer)
            : base(serializer, "text/csv")
        {
        }
        
        protected override void DataTableToCSV(DataTable table, TextWriter writer, bool includeHeaders)
        {
            if (table == null)
            {
                throw new ArgumentNullException("table");
            }
            
            if (writer == null)
            {
                throw new ArgumentNullException("writer");
            }
            
            if (includeHeaders)
            {
                string[] columnNames = table.Columns.Cast<DataColumn>().Select(column => "\"" + column.ColumnName.Replace("\"", "\"\"") + "\"").ToArray<string>();
                writer.WriteLine(String.Join(",", columnNames));
                writer.Flush();
            }

            foreach (DataRow row in table.Rows)
            {
                string[] fields = row.ItemArray.Select(
                                      field =>
                    {
                        var data = field as string;
                        if (field != null && data == null && field != DBNull.Value)
                        {
                            data = this.Serializer.SerializeObject(field);
                            if (data.StartsWith("\"", StringComparison.OrdinalIgnoreCase) && data.EndsWith("\"", StringComparison.OrdinalIgnoreCase))
                            {
                                data = data.Substring(1, data.Length - 2);
                            }
                        }
                        
                        
                        if (data == null)
                        {
                            data = string.Empty;
                        }
                        
                        return "\"" + data.Replace("\"", "\"\"") + "\"";
                    }).ToArray<string>();
                writer.WriteLine(String.Join(",", fields));
                writer.Flush();
            }
        }
    }
}
