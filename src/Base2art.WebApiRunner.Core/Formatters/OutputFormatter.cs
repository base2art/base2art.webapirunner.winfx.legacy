﻿namespace Base2art.WebApiRunner.Formatters
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Net.Http.Formatting;
    using System.Net.Http.Headers;

    public abstract class OutputFormatter<T> : BufferedMediaTypeFormatter where T : class
    {
        private readonly string mimeType;

        protected OutputFormatter(string mimeType)
        {
            this.mimeType = mimeType;
            this.SupportedMediaTypes.Add(new MediaTypeHeaderValue(mimeType));
        }

        public override bool CanReadType(Type type)
        {
            return false;
        }

        public override bool CanWriteType(Type type)
        {
            return !type.IsValueType;
        }

        public override void SetDefaultContentHeaders(Type type, HttpContentHeaders headers, MediaTypeHeaderValue mediaType)
        {
            base.SetDefaultContentHeaders(type, headers, mediaType);
            headers.ContentType = new MediaTypeHeaderValue(this.mimeType);
        }

        public override void WriteToStream(Type type, object value, Stream writeStream, System.Net.Http.HttpContent content)
        {
            var realValue = value as T;
            if (realValue == null)
            {
                base.WriteToStream(type, value, writeStream, content);
                return;
            }
            
            this.WriteValueToStream(realValue, type, writeStream);
        }

        protected abstract void WriteValueToStream(T realValue, Type declaredType, Stream writeStream);
    }
}




