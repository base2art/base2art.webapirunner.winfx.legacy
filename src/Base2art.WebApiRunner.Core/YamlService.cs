namespace Base2art.WebApiRunner
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Reflection;
    using Base2art.WebApiRunner.Function;

    public class YamlService : IConfigSerializer, IConfigDeserializer
    {
        private const string YamlName = "YamlDotNet, Version=3.9.0.0, Culture=neutral, PublicKeyToken=null";
        
        private readonly Lazy<Assembly> asm = new Lazy<Assembly>(() => Assembly.Load(YamlName));
        
        private readonly Lazy<object> callingConventions;
        
        T IConfigDeserializer.Deserialize<T>(string input)
        {
            return this.Deserializer().Deserialize<T>(input);
        }

        string IConfigSerializer.Serialize<T>(T value)
        {
            return this.Serializer().Serialize(value);
        }

        static YamlService()
        {
            InitYaml();
        }
        
        public YamlService()
        {
            this.callingConventions = new Lazy<object>(() =>
                                                       {
                                                           var conventionsType = this.asm.Value.GetType("YamlDotNet.Serialization.NamingConventions.CamelCaseNamingConvention");
                                                           return Activator.CreateInstance(conventionsType, new object[0]);
                                                       });
        }
        
        protected virtual IConfigDeserializer Deserializer()
        {
            var type = this.asm.Value.GetType("YamlDotNet.Serialization.Deserializer");
            var @params = new object[]
            {
                null,
                this.callingConventions.Value,
                false,
                null
            };
            dynamic deserializer = type.GetConstructors().First().Invoke(@params);
            
            //            var deserializer = new YamlDotNet.Serialization.Deserializer(namingConvention: new CamelCaseNamingConvention());

            return SMP.Of<IConfigDeserializer>()
                .GenericFunc1<string, object>((gtype, x) =>
                                      {
                                          using (var sr = new StringReader(x))
                                          {
                                              return deserializer.Deserialize(sr, gtype);
                                          }
                                      });
        }
        
        protected virtual IConfigSerializer Serializer()
        {
            var type = this.asm.Value.GetType("YamlDotNet.Serialization.Serializer");
            dynamic serializer = type.GetConstructors().First().Invoke(new object[] { 0, this.callingConventions.Value, null });
            
            // var deserializer = new YamlDotNet.Serialization.Deserializer(namingConvention: new CamelCaseNamingConvention());

            return SMP.Of<IConfigSerializer>()
                .Func1<object, string>(x =>
                                      {
                                          using (MemoryStream ms = new MemoryStream())
                                          {
                                              using (var sw = new StreamWriter(ms))
                                              {
                                                  serializer.Serialize(sw, x);
                                                  sw.Flush();
                                                  ms.Seek(0L, SeekOrigin.Begin);
                                                  
                                                  using (var sr = new StreamReader(ms))
                                                  {
                                                      return sr.ReadToEnd();
                                                  }
                                              }
                                          }
                                      });
        }
        
        private static void InitYaml()
        {
            AppDomain.CurrentDomain.AssemblyResolve += (object sender, ResolveEventArgs args) =>
            {
                if (args.Name == YamlName)
                {
                    const string resourcepath = "Base2art.WebApiRunner.Resources.YamlDotNet.dll";
                    using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(resourcepath))
                    {
                        if (stream != null)
                        {
                            using (stream)
                            {
                                byte[] data = new byte[stream.Length];
                                stream.Read(data, 0, data.Length);
                                return Assembly.Load(data);
                            }
                        }
                    }
                }
                
                return null;
            };
        }
    }
}
