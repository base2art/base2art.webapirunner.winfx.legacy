﻿namespace Base2art.WebApiRunner
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Config.Types;
    using Newtonsoft.Json;
    
    public class ConfigIniter
    {
        private static readonly Lazy<YamlService> yaml = new Lazy<YamlService>(() => new YamlService());

        private readonly string configPath;
        private readonly string configDirectoryPath;
        
        public ConfigIniter(string configPath)
        {
            this.configPath = configPath;
            this.configDirectoryPath = Path.GetDirectoryName(configPath);
        }

        public void Write<T>(T item)
        {
            var result = Serializer().Serialize(item);
                
            using (var input = File.OpenWrite(this.configPath))
            {
                using (var sr = new StreamWriter(input))
                {
                    sr.Write(result);
                }
            }
        }
        
        public void Init<T>(T item)
        {
            this.InitInternal(item, this.configPath);
        }
        
        public void Init<T>(T item, string alternatePath)
        {
            this.InitInternal(item, Path.Combine(this.configDirectoryPath, alternatePath));
        }
        
        public void InitInternal<T>(T item, string path)
        {
            using (var input = File.OpenRead(path))
            {
                using (var sr = new StreamReader(input))
                {
                    var data = sr.ReadToEnd();
                    
                    var dataObj = Deserializer().Deserialize<object>(data);
                    var serialiser = new JsonSerializer();
                    var content = JsonConvert.SerializeObject(dataObj, Formatting.Indented);
                    JsonConvert.PopulateObject(content, item);
                }
            }
        }
        
        public T LoadConfigAs<T>(string filePath)
            where T : new()
        {
            var config = new T();
            this.Init(config, filePath);
            return config;
        }

        private static IConfigSerializer Serializer()
        {
            return yaml.Value;
        }

        private static IConfigDeserializer Deserializer()
        {
            return yaml.Value;
        }
    }
    
    public static class ConfigIniterHelper
    {
        public static EndpointConfiguration[] JointEndpoints(this ServerConfiguration config)
        {
            return config.Endpoints.JoinTo(config.Initer(), config.EndpointsConfigurationItems);
        }
        
        public static TasksConfiguration[] JointTasks(this ServerConfiguration config)
        {
            return config.Tasks.JoinTo(config.Initer(), config.TasksConfigurationItems);
        }
        
        public static HealthChecksConfiguration[] JointHealthChecks(this ServerConfiguration config)
        {
            return config.HealthChecks.JoinTo(config.Initer(), config.HealthChecksConfigurationItems);
        }
        
        public static ApplicationConfiguration[] JointApplications(this ServerConfiguration config)
        {
            return config.Application.JoinTo(config.Initer(), config.ApplicationConfigurationItems);
        }
        
        public static InjectionConfiguration[] JointInjections(this ServerConfiguration config)
        {
            return config.Injection.JoinTo(config.Initer(), config.InjectionConfigurationItems);
        }
        
        public static MiddlewareConfiguration[] JointMiddleware(this ServerConfiguration config)
        {
            return config.Middleware.JoinTos(config.Initer(), config.MiddlewareConfigurationItems);
        }
        
        public static string[] JointAssemblies(this ServerConfiguration config)
        {
            return config.Assemblies.JoinTos(config.Initer(), config.AssembliesConfigurationItems);
        }
        
        public static string[] JointAssemblySearchPaths(this ServerConfiguration config)
        {
            return config.AssemblySearchPaths.JoinTos(config.Initer(), config.AssemblySearchPathsConfigurationItems);
        }
        
        public static CorsConfiguration[] JointCors(this ServerConfiguration config)
        {
            return config.Cors.JoinTo(config.Initer(), config.CorsConfigurationItems);
        }
        
        private static T[] JoinTo<T>(this T item, ConfigIniter initer, PathList paths)
            where T : class, new()
        {
            if (item == null)
            {
                return paths.Select(x => initer.LoadConfigAs<T>(x)).ToArray();
            }
            
            return new T[]{ item }.Union(paths.Select(x => initer.LoadConfigAs<T>(x))).ToArray();
        }
        
        private static T[] JoinTos<T>(this IEnumerable<T> items, ConfigIniter initer, PathList paths)
            where T : class
        {
            return items.Union(paths.SelectMany(x => initer.LoadConfigAs<List<T>>(x))).Where(x=>x != null).ToArray();
        }
        
//        public static T ParseConfigAs<T>(string filePath)
//            where T : new()
//        {
//            var config = new T();
//            ConfigIniter.Init(filePath, config);
//            return config;
//        }
    }
}
