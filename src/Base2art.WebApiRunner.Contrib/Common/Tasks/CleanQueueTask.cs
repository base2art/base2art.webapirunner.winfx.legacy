﻿namespace Base2art.WebApiRunner.Common.Tasks
{
    using Base2art.WebApiRunner.MessageQueue;
    
    public class CleanQueueTask : DelegateTask
    {
        public CleanQueueTask(IMessageQueueManager manager)
            : base(manager.TruncateCompleted)
        {
        }
    }
    
}
