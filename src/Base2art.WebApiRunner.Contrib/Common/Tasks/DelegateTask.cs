﻿
namespace Base2art.WebApiRunner.Common.Tasks
{
    using System;
    using System.Threading.Tasks;
    
    public class DelegateTask : ITask
    {
        private readonly Func<Task> action;

        public DelegateTask(Func<Task> action)
        {
            this.action = action;
        }

        Task ITask.ExecuteAsync()
        {
            return action();
        }
    }
}


