﻿namespace Base2art.WebApiRunner.Common.Tasks
{
    using Base2art.WebApiRunner.MessageQueue;

    public class SetupQueueRetriesTask : DelegateTask
    {
        public SetupQueueRetriesTask(IMessageQueueManager manager) : base(manager.SetupRetries)
        {
        }
    }
}
