﻿
namespace Base2art.WebApiRunner.Security
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Security.Principal;
    
    public class JwtIdentity : IIdentity
    {
        private readonly string subject;

        private readonly string displayName;

        private readonly string issuer;

        private readonly string[] groups;

        private readonly List<Claim> claims;

        private readonly string rawPayload;

        private readonly bool isAuthenticated;
        
        private JwtIdentity()
        {
            this.subject = "";
            this.displayName = "Unknown";
            this.issuer = "N/A";
            this.groups = new string[0];
            this.claims = new List<Claim>();
            this.rawPayload = "";
            this.isAuthenticated = false;
        }
        
        public JwtIdentity(string subject, string name, string issuer, Claim[] groups, Claim[] claims, string rawPayload)
        {
            this.rawPayload = rawPayload;
            this.subject = subject;
            this.displayName = name;
            this.issuer = issuer;
            this.groups = (groups ?? new Claim[0]).Select(x => x.Value).ToArray();
            this.claims = new List<Claim>(claims ?? new Claim[0]);
            this.isAuthenticated = true;
        }

        public string Subject
        {
            get
            {
                return this.subject;
            }
        }

        public string Name
        {
            get
            {
                return this.Subject;
            }
        }

        public string AuthenticationType
        {
            get
            {
                return "JWT";
            }
        }

        public List<Claim> Claims
        {
            get
            {
                return claims;
            }
        }

        public bool IsAuthenticated
        {
            get
            {
                return this.isAuthenticated;
            }
        }

        public string DisplayName
        {
            get
            {
                return this.displayName;
            }
        }
        
        public string[] Groups
        {
            get
            {
                return new List<string>(this.groups ?? new string[0]).ToArray();
            }
        }
        
        public static JwtIdentity Null()
        {
            return new JwtIdentity();
        }
    }
}


