﻿namespace Base2art.WebApiRunner.Security
{
    using System.Linq;
    using System.Security.Claims;
    
    public static class Principal
    {
        public static string[] Groups(this ClaimsPrincipal principal)
        {
            if (principal == null)
            {
                return new string[0];
            }
            
            return principal.Claims.Where(x => string.Equals(x.Type, "group", System.StringComparison.OrdinalIgnoreCase))
                .Union(principal.Claims.Where(x => (x.Type ?? string.Empty).EndsWith("/group", System.StringComparison.OrdinalIgnoreCase)))
                .Select(x => x.Value).ToArray();
        }
        
        public static string Token(this ClaimsPrincipal principal)
        {
            if (principal == null)
            {
                return string.Empty;
            }
            
            return principal.Claims.Where(x => string.Equals(x.Type, "token", System.StringComparison.OrdinalIgnoreCase))
                .Select(x => x.Value)
                .FirstOrDefault();
        }
    }
}
