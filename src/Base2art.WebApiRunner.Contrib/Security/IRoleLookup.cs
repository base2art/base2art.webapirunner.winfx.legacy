﻿namespace Base2art.WebApiRunner.Security
{
    using System.Collections.Generic;
    using System.Security.Claims;
    
    public interface IRoleLookup
    {
        HashSet<string> GetRoles(ClaimsPrincipal principal);
    }
}
