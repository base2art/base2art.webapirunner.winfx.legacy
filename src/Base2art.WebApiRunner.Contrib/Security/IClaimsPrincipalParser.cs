﻿namespace Base2art.WebApiRunner.Security
{
    using System.Security.Claims;

    public interface IClaimsPrincipalParser
    {
        ClaimsPrincipal CreateValidated(string token);

        ClaimsPrincipal CreateNonValidated(string token);
    }
}


