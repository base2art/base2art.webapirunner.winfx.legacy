﻿
namespace Base2art.WebApiRunner.Security
{
    using System.Security.Principal;
    
    public class JwtPrincipal : IPrincipal
    {
        private readonly JwtIdentity identity;

        private readonly IRoleLookup roleLookup;
        
        public JwtPrincipal(IRoleLookup roleLookup, JwtIdentity identity)
        {
            this.roleLookup = roleLookup;
            this.identity = identity;
        }
        
        public JwtIdentity Identity
        {
            get{ return this.identity; }
        }
        
        IIdentity IPrincipal.Identity
        {
            get
            {
                return this.Identity;
            }
        }
        
        public bool IsInRole(string role)
        {
            
            return this.roleLookup.GetRoles(identity.Subject, identity.Groups).Contains(role);
        }
    }
}
