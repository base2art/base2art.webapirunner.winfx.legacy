﻿namespace Base2art.WebApiRunner.Security
{
    public interface IJwtPrincipalParser
    {
        JwtPrincipal CreateValidated(string token);
        
        JwtPrincipal CreateNonValidated(string token);
    }
}
