﻿namespace Base2art.WebApiRunner.Console
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Console.Config;
    using Base2art.WebApiRunner.Internals;

    public class AdminStartup : ConsoleStartup
    {
        public AdminStartup(string configDir, ConsoleServerConfiguration config)
            : base(configDir, config)
        {
        }
        
        protected override string ApiName
        {
            get { return "Admin API"; }
        }
        
        protected override List<TypedMethodConfiguration> CreateMethods(ServerConfiguration config)
        {
            return config.CreateTasksAndHealthCheck();
        }
        
        protected override void RegisterMiddleware(System.Web.Http.HttpConfiguration config)
        {
        }
        
        protected override void RegisterUserServices(System.Web.Http.HttpConfiguration config)
        {
        }
    }
}
