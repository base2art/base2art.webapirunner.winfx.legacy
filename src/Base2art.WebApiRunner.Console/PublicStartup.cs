namespace Base2art.WebApiRunner.Console
{
    using System;
    using Base2art.WebApiRunner.Console.Config;
    
    public class PublicStartup : ConsoleStartup
    {
        public PublicStartup(string configDir, ConsoleServerConfiguration config)
            : base(configDir, config)
        {
        }
        
        protected override string ApiName
        {
            get { return "Public API"; }
        }
    }
}
