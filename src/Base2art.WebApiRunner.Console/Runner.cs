namespace Base2art.WebApiRunner.Console
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.IO;
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.Dependencies;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Middleware.PlatformSpecific.Owin;
    using Base2art.WebApiRunner.Console.Config;
    using Base2art.WebApiRunner.Reflection;
    using Microsoft.Owin.Hosting;

    public class Runner<T>
        where T : ConsoleServerConfiguration, new()
    {
        private readonly string verb;

        private readonly Lazy<T> config;

        private readonly Lazy<Tuple<ConsoleStartup, StartOptions>> startup;
        
        private readonly Lazy<IReadOnlyList<Uri>> listenerUris;
        private readonly Lazy<IReadOnlyList<string>> listenerUriStrings;

        private readonly bool isAdmin;
        
        public Runner(string verb, string configPath, bool isAdmin)
        {
            this.isAdmin = isAdmin;
            this.verb = verb;
            this.listenerUris = new Lazy<IReadOnlyList<Uri>>(() => this.GetUris());
            this.listenerUriStrings = new Lazy<IReadOnlyList<string>>(() => this.GetUriStrings());
            
            this.config = new Lazy<T>(() =>
            {
                var configuration = new T();
                configuration.Init(configPath);
                return configuration;
            });
            
            var configDir = Path.GetDirectoryName(configPath);
            this.startup = new Lazy<Tuple<ConsoleStartup, StartOptions>>(
                () =>
                {
                    var options = new StartOptions();
                    
                    PrepareUrls(options);

                    // Start OWIN host
                    var s = isAdmin ? (ConsoleStartup)new AdminStartup(configDir, this.Configuration) : new PublicStartup(configDir, this.Configuration);
                    return Tuple.Create(s, options);
                });
        }
        
        
        public T Configuration
        {
            get { return this.config.Value; }
        }
        
        public Uri[] Uris
        {
            get { return this.listenerUris.Value.ToArray(); }
        }
        
        public IDisposable Execute(
            IDependencyResolver resolver = null,
            Action<HttpConfiguration> setup = null)
        {
            return WebApp.Start(
                this.startup.Value.Item2,
                app =>
                {
                    Action<HttpConfiguration> newSetup = (x) =>
                    {
                        if (setup != null)
                        {
                            setup(x);
                        }
                        
                        var middleware = this.isAdmin
                            ? this.Configuration.OwinAdminMiddleware
                            : this.Configuration.OwinMiddleware;
                        middleware = middleware ?? new List<MiddlewareConfiguration>();
                        
                        if (middleware.Count > 0)
                        {
                            Action<object, object[]> user = (obj, parms) => app.Use(obj, parms);
                            x.RunMiddleware(
                                middleware,
                                typeof(IOwinMiddleware<>),
                                (y) => new object[] {
                                    x,
                                    y,
                                    app.Properties,
                                    user
                                });
                        }
                    };
                    
                    this.startup.Value.Item1.Configure(app, newSetup, resolver);
                });
        }

        public void Verify(HttpConfiguration httpConfig)
        {
            var asms = httpConfig.Services.GetAssembliesResolver().GetAssemblies();
            var scope = httpConfig.DependencyResolver.BeginScope();
            
            foreach (var injection in this.Configuration.JointInjections().SelectMany(x => x.Items))
            {
                Console.WriteLine("Loading `{0}`...", injection.RequestedType);
                var type = asms.LoadType(injection.RequestedType);
                Assert(
                    type != null,
                    "Type Not Found",
                    "[RequestedType=`{0}` FulfillingType=`{1}`]",
                    injection.RequestedType,
                    injection.FulfillingType);
                
                var obj = scope.GetService(type);
                Assert(
                    obj != null,
                    "Object Not Found",
                    "[RequestedType=`{0}` FulfillingType=`{1}`]",
                    injection.RequestedType,
                    injection.FulfillingType);
            }
            
            foreach (var url in this.Configuration.JointEndpoints().SelectMany(x => x.Urls))
            {
                Console.WriteLine("Loading `{0}`...", url.Type);
                
                var type = asms.LoadType(url.Type);
                Assert(
                    type != null,
                    "Url Type Not Found",
                    "[ClassName=`{0}`]",
                    url.Type);
                
                var obj = this.GetServiceOrCreate(scope, type, url.Parameters, url.Properties);
                Assert(
                    obj != null,
                    "Url Object Not Found",
                    "[ClassName=`{0}`]",
                    url.Type);
            }
            
            foreach (var task in this.Configuration.JointTasks().SelectMany(x=>x.Items ?? new List<TaskConfiguration>()))
            {
                Console.WriteLine("Loading `{0}`...", task.Type);
                
                var type = asms.LoadType(task.Type);
                Assert(
                    type != null,
                    "Task Type Not Found",
                    "[ClassName=`{0}`]",
                    task.Type);
                
                var obj = this.GetServiceOrCreate(scope, type, task.Parameters, task.Properties);
                Assert(
                    obj != null,
                    "Task Object Not Found",
                    "[ClassName=`{0}`]",
                    task.Type);
            }
        }
        
        private IReadOnlyList<Uri> GetUris()
        {
            var urls = new List<Uri>();
            
            var serverSettingsConfiguration = (this.isAdmin ? this.Configuration.AdminServerSettings : this.Configuration.ServerSettings) ?? new ServerSettingsConfiguration();
            foreach (var http in serverSettingsConfiguration.Http ?? new List<HttpServerConfiguration>())
            {
                string host = http.Host ?? "localhost";
                
                if (string.Equals("*", host, StringComparison.OrdinalIgnoreCase))
                {
                    host = "localhost";
                }
                
                UriBuilder builder = new UriBuilder();
                builder.Scheme = Uri.UriSchemeHttp;
                builder.Host = host;
                builder.Port = http.Port ?? 8080;
                builder.Path = http.RootPath ?? "/";
                urls.Add(builder.Uri);
            }
            
            foreach (var https in serverSettingsConfiguration.Https ?? new List<HttpsServerConfiguration>())
            {
                string host = https.Host ?? "localhost";
                
                if (string.Equals("*", host, StringComparison.OrdinalIgnoreCase))
                {
                    host = "localhost";
                }
                
                UriBuilder builder = new UriBuilder();
                builder.Scheme = Uri.UriSchemeHttps;
                builder.Host = host;
                builder.Port = https.Port ?? 8443;
                builder.Path = https.RootPath ?? "/";
                urls.Add(builder.Uri);
            }
            
            if (urls.Count == 0)
            {
                UriBuilder builder = new UriBuilder();
                builder.Scheme = Uri.UriSchemeHttp;
                builder.Host = "localhost";
                builder.Port = (this.isAdmin ? 8081 : 8080);
                builder.Path = "/";
                urls.Add(builder.Uri);
            }
            
            return urls;
        }
        
        
        private IReadOnlyList<string> GetUriStrings()
        {
            var urls = new List<string>();
            
            var serverSettingsConfiguration = (this.isAdmin ? this.Configuration.AdminServerSettings : this.Configuration.ServerSettings) ?? new ServerSettingsConfiguration();
            foreach (var http in serverSettingsConfiguration.Http ?? new List<HttpServerConfiguration>())
            {
                string host = http.Host ?? "localhost";
                
                var port = http.Port ?? 8080;
                var path = http.RootPath ?? "/";
                urls.Add(string.Format("{0}://{1}:{2}{3}", Uri.UriSchemeHttp, host, port, path));
            }
            
            foreach (var https in serverSettingsConfiguration.Https ?? new List<HttpsServerConfiguration>())
            {
                string host = https.Host ?? "localhost";
                
                var port = https.Port ?? 8080;
                var path = https.RootPath ?? "/";
                urls.Add(string.Format("{0}://{1}:{2}{3}", Uri.UriSchemeHttps, host, port, path));
            }
            
            if (urls.Count == 0)
            {
                urls.Add(string.Format("{0}://{1}:{2}{3}", Uri.UriSchemeHttp, "localhost", (this.isAdmin ? 8081 : 8080), "/"));
            }
            
            return urls;
        }
        
        private void PrepareUrls(StartOptions options)
        {
            foreach (var builder in this.listenerUriStrings.Value)
            {
                options.Urls.Add(builder);
            }
        }

        public static void Assert(bool condition, string message, string detailMessageFormat, params object[] args)
        {
            if (condition)
            {
                return;
            }
            
            var detailedMessage = string.Format(detailMessageFormat, args);
            
            Console.WriteLine(message);
            Console.WriteLine(string.Format("  {0}", detailedMessage));
            throw new TypeAccessException(message, new TypeAccessException(detailedMessage));
        }

        private object GetServiceOrCreate(IDependencyScope scope, Type type, Dictionary<string, string> parameters, Dictionary<string, object> properties)
        {
            var item = scope.GetService(type);
            if (item != null)
            {
                return item;
            }
            
            return type.CreateFrom(scope, parameters, properties);
        }
    }
}
