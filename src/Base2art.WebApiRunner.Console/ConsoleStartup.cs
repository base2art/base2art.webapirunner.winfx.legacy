﻿namespace Base2art.WebApiRunner.Console
{
    using System;
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.Dependencies;
    using Base2art.WebApiRunner.Console.Config;
    using Owin;

    public class ConsoleStartup : Startup
    {
        public ConsoleStartup(string configDir, ConsoleServerConfiguration config) : base(configDir, config)
        {
        }

        // This code configures Web API. The Startup class is specified as a type
        // parameter in the WebApp.Start method.
        public void Configure(IAppBuilder appBuilder, Action<HttpConfiguration> setup, IDependencyResolver resolver)
        {
            // Configure Web API for self-host.
            HttpConfiguration config = new HttpConfiguration();
            base.Register(config, setup, () => resolver);
            appBuilder.UseWebApi(config);
        }
    }
}

