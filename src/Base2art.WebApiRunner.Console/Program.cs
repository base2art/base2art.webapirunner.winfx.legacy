namespace Base2art.WebApiRunner.Console
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Web.Http;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Console.Config;

    public static class Program
    {
        public static int Main(string[] args)
        {
            
            Action<string> write = System.Console.WriteLine;
            Action usage = () =>
            {
                write("Usage Base2art.WebApiRunner.Console.exe verb configPath");
                write(string.Empty);
                write("verb := 'server' | 'check-health' | 'server-with-verify'");
                write("configPath := <System_Path> # to yaml file");
            };
            
            Console.WriteLine("Starting App in {0}", Environment.CurrentDirectory);
            
            if (args.Length < 2)
            {
                usage();
                return -1;
            }
            
            HashSet<string> commands = new HashSet<string> {
                "server",
                "check-health",
                "server-with-verify"
            };
            
            if (!commands.Contains(args[0]))
            {
                usage();
                return -2;
            }
            
            var configPath = Path.Combine(Environment.CurrentDirectory, args[1]);
            
            var publicRunner = new Runner<ConsoleServerConfiguration>(args[0], configPath, false);
            
            var adminRunner = new Runner<ConsoleServerConfiguration>(args[0], configPath, true);
            
            HttpConfiguration publicConfig = null;
            HttpConfiguration adminConfig = null;
            
            using (publicRunner.Execute(setup: config => publicConfig = config))
            {
                using (adminRunner.Execute(publicConfig.DependencyResolver, config => adminConfig = config))
                {
                    var c = adminRunner.Configuration;
                    var tasks = c.JointTasks();
                        
                    var scheduler = TaskScheduler.Run(adminConfig, tasks);
                    using (scheduler)
                    {
                        if (args[0] == "server-with-verify")
                        {
                            publicRunner.Verify(publicConfig);
                            adminRunner.Verify(adminConfig);
                        }
                        
                        Console.WriteLine("Press enter to exit");
                        Console.ReadLine();
                        return 0;
                    }
                }
            }
        }
    }
}
