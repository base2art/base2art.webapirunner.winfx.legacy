namespace Base2art.WebApiRunner.Console.Config
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using Base2art.WebApiRunner.Config;

    public class ConsoleServerConfiguration : ServerConfiguration
    {
        private ConfigIniter initer;
        
        public ServerSettingsConfiguration ServerSettings { get; set; }
        
        public List<MiddlewareConfiguration> OwinMiddleware { get; set; }
        
        public List<MiddlewareConfiguration> OwinAdminMiddleware { get; set; }
        
        public ServerSettingsConfiguration AdminServerSettings { get; set; }

        public override ConfigIniter Initer()
        {
            return this.initer;
        }
        
        public void Init(string configPath)
        {
            this.initer = new ConfigIniter(configPath);
            
            if (!File.Exists(configPath))
            {
                this.ServerSettings = new ServerSettingsConfiguration();
                this.ServerSettings.Http = new[]
                {
                    new HttpServerConfiguration
                    {
                        Host = "localhost",
                        Port = 9000,
                        RootPath = "/"
                    }
                }.ToList();
                
                this.AdminServerSettings = new ServerSettingsConfiguration();
                this.AdminServerSettings.Http = new[]
                {
                    new HttpServerConfiguration
                    {
                        Host = "localhost",
                        Port = 9001,
                        RootPath = "/"
                    }
                }.ToList();
                
                this.initer.Write(this);
            }
            
            this.initer.Init(this);
        }
    }
}
