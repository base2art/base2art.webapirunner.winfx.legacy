namespace Base2art.WebApiRunner.Console.Config
{
   public class HttpsServerConfiguration
   {
      public string Host { get; set; }

      public string RootPath { get; set; }

      public int? Port { get; set; }
   }
}
