namespace Base2art.WebApiRunner.Console.Config
{
    using System.Collections.Generic;
    using System.Web.Http;

    public class ServerSettingsConfiguration
    {
        public List<HttpServerConfiguration> Http { get; set; }

        public List<HttpsServerConfiguration> Https { get; set; }
    }
}
