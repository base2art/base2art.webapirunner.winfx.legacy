﻿
//[assembly: Microsoft.Owin.OwinStartupAttribute(typeof(Base2art.WebApiRunner.IIS.AppBuilderStartup))]
namespace Base2art.WebApiRunner.IIS
{
    using System;
    using System.Security;
    using System.Web.Http;
    using Owin;
    using System.Web;
    using System.IO;
//    using Microsoft.Owin.Extensions;
    
    public class AppBuilderStartup
    {
        private static IDisposable scheduler;
        
        public void Configuration(IAppBuilder app)
        {
            
            var file = System.Configuration.ConfigurationManager.AppSettings["configFile"];
            if (string.IsNullOrWhiteSpace(file))
            {
                file = "configuration.config";
            }
            
            try
            {
                Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            }
            catch (SecurityException)
            {
            }
            
            var config = new IISServerConfiguration();
            config.Init(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file));
            
            
            GlobalConfiguration.Configure(x => new IISStartup(config).Register(x, (y) =>
            {
                                                                                   scheduler = TaskScheduler.Run(x, config.JointTasks());
                                                                                   app.UseWebApi(GlobalConfiguration.DefaultServer);
            }, null));
            
        }
    }
}
