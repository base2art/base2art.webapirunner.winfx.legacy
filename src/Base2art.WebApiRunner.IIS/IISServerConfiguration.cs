﻿namespace Base2art.WebApiRunner.IIS
{
    using System;
    using System.IO;
    using Base2art.WebApiRunner.Config;

    public class IISServerConfiguration : ServerConfiguration
    {
        private ConfigIniter initer;

        public override ConfigIniter Initer()
        {
            return this.initer;
        }
        
        public void Init(string configPath)
        {
            this.initer = new ConfigIniter(configPath);
            
            if (!File.Exists(configPath))
            {
                this.initer.Write(this);
            }
            
            this.initer.Init(this);
        }
    }
}
