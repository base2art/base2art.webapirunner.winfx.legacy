﻿namespace Base2art.WebApiRunner.IIS
{
    using System;
    using System.Linq;
    using Base2art.WebApiRunner.Config;
    using Base2art.WebApiRunner.Internals;

    public class IISStartup : Startup
    {
        public IISStartup(IISServerConfiguration config) : base(null, config)
        {
        }

        protected override System.Collections.Generic.List<TypedMethodConfiguration> CreateMethods(ServerConfiguration config)
        {
            var baseRoutes = base.CreateMethods(config);
            baseRoutes.AddRange(config.CreateTasksAndHealthCheck("admin/"));
            return baseRoutes;
        }
    }
}


