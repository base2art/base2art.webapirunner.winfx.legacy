﻿namespace Base2art.WebApiRunner.IIS
{
    using System;
    using System.ComponentModel;
    using System.IO;
    using System.Linq;
    using System.Security;
    using System.Web;
    using System.Web.Http;
    using Base2art.WebApiRunner.Config;
    
    public class StartupModule : Component, IHttpModule
    {
        private static bool hasInited = false;

        private static IDisposable scheduler;

        private static readonly object padlock = new object();
        
        public void Init(HttpApplication context)
        {
            if (!hasInited)
            {
                lock (padlock)
                {
                    
                    if (!hasInited)
                    {
//                        this.InitInternal();
                        hasInited = true;
                    }
                }
            }
        }

        private void InitInternal()
        {
            var file = System.Configuration.ConfigurationManager.AppSettings["configFile"];
            if (string.IsNullOrWhiteSpace(file))
            {
                file = "configuration.config";
            }
            
            try
            {
                Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;
            }
            catch (SecurityException)
            {
            }
            
            var config = new IISServerConfiguration();
            config.Init(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, file));
            
            
            GlobalConfiguration.Configure(x => new IISStartup(config).Register(x, (y) =>
            {
                                                                                   scheduler = TaskScheduler.Run(x, config.JointTasks());
            }, null));
        }
    }
}
