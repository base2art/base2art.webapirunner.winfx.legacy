﻿namespace Base2art.WebApiRunner
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class UnprocessableEntityException : Exception, ISerializable
    {
        public UnprocessableEntityException()
        {
        }

        public UnprocessableEntityException(string message) : base(message)
        {
        }

        public UnprocessableEntityException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected UnprocessableEntityException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

