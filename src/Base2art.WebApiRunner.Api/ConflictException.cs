﻿namespace Base2art.WebApiRunner
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class ConflictException : Exception, ISerializable
    {
        public ConflictException()
        {
        }

        public ConflictException(string message) : base(message)
        {
        }

        public ConflictException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected ConflictException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

