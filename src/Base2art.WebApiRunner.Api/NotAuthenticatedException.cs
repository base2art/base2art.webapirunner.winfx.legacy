﻿namespace Base2art.WebApiRunner
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class NotAuthenticatedException : Exception, ISerializable
    {
        public NotAuthenticatedException()
        {
        }

        public NotAuthenticatedException(string message) : base(message)
        {
        }

        public NotAuthenticatedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected NotAuthenticatedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}