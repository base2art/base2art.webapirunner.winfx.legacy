﻿
namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Threading.Tasks;

    public interface ISetStatusCodeFilter<T>
    {
        Task SetStatusCodeAsync(T content, Action<System.Net.HttpStatusCode> setRedirect);
    }
}
