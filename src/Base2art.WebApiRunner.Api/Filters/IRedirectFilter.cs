﻿
namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Threading.Tasks;

    public interface IRedirectFilter<T>
    {
        Task SetRedirectAsync(T content, Action<System.Net.HttpStatusCode, string> setRedirect);
    }
}


