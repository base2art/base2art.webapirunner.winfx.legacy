﻿
namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    
    public interface ISetCookieFilter<T>
    {
        Task SetCookiesAsync(T content, IList<System.Net.Cookie> cookies);
    }   
}
