﻿namespace Base2art.WebApiRunner.Filters
{
    public static class ExceptionFilter
    {
        public static void SuppressLogging(this System.Exception exception)
        {
            if (exception == null || exception.Data == null)
            {
                return;
            }
            
            exception.Data["Base2art.Logged"] = true;
        }
        
        public static bool ShouldLog(this System.Exception exception)
        {
            if (exception == null || exception.Data == null)
            {
                return true;
            }
            
            return !exception.Data.Contains("Base2art.Logged");
        }
    }
}
