﻿
namespace Base2art.WebApiRunner.Filters
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ISetHeadersFilter<T>
    {
        Task SetHeadersAsync(T content, ICollection<KeyValuePair<string, string>> headers);
    }
}

