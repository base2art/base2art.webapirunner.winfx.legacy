﻿
namespace Base2art.WebApiRunner
{
    using System;
    using System.Threading.Tasks;

    public interface ISimpleHealthCheck
    {
        Task ExecuteAsync();
    }
}


