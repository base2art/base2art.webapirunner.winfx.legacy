﻿

namespace Base2art.WebApiRunner
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IStandardTask
    {
        Task<string> ExecuteAsync(IDictionary<string, object> parameters);
    }
}


