﻿
namespace Base2art.WebApiRunner
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public interface IHealthCheck
    {
        Task<ErrorEventArgs> ExecuteAsync();
    }
}
