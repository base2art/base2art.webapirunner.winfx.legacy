﻿

namespace Base2art.WebApiRunner
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface IAuthenticatedLoggedTask<TLogger, TAuth>
    {
        Task<string> ExecuteAsync(IDictionary<string, object> parameters, TLogger logger, TAuth auth);
    }
}


