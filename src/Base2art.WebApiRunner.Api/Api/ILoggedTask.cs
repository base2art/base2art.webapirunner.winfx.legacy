﻿

namespace Base2art.WebApiRunner
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    public interface ILoggedTask<TLogger>
    {
        Task ExecuteAsync(IDictionary<string, object> parameters, TLogger logger);
    }
}


