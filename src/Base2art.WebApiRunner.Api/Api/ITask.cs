﻿

namespace Base2art.WebApiRunner
{
    using System.Threading.Tasks;
    
    public interface ITask
    {
        Task ExecuteAsync();
    }
}
