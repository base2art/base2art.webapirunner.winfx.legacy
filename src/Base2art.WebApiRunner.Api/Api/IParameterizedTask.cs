﻿
namespace Base2art.WebApiRunner
{
    using System.Collections.Generic;
    using System.Threading.Tasks;
    public interface IParameterizedTask
    {
        Task ExecuteAsync(IDictionary<string, object> parameters);
    }
}


