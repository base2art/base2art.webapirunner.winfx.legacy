﻿namespace Base2art.WebApiRunner.SystemServices
{
    using System.Threading.Tasks;
    
    public interface IObjectStringifier
    {
        string PrettyPrint(object value);
            
        Task<string> PrettyPrintAsync(object value);
    }
}
