﻿
namespace Base2art.WebApiRunner.SystemServices
{
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    
    public static class Cache
    {
        public static CacheLookupWrapper<T> As<T>(this ICache<string, object> cache)
        {
            return new CacheLookupWrapper<T>(cache);
        }
        
        /* ****************** SYNC API ****************/
        public static TResult TryGet<TResult>(this ICache<string, object> cache, Func<TResult> getter)
        {
            return cache.TryGet<object, TResult>(getter);
        }
        
        public static TActualResult TryGet<TCacheValue, TActualResult>(this ICache<string, TCacheValue> cache, Func<TActualResult> getter)
            where TActualResult : TCacheValue
        {
            return cache.TryGetGeneric<TCacheValue, TActualResult>(
                getter.Method,
                new KeyValuePair<Type, object>[]{ },
                getter);
        }
        
        public static TResult TryGet<TIn1, TResult>(this ICache<string, object> cache, TIn1 in1, Func<TIn1, TResult> getter)
        {
            return cache.TryGet<TIn1, object, TResult>(in1, getter);
        }
        
        public static TActualResult TryGet<TIn1, TCacheValue, TActualResult>(this ICache<string, TCacheValue> cache, TIn1 in1, Func<TIn1, TActualResult> getter)
            where TActualResult : TCacheValue
        {
            return cache.TryGetGeneric<TCacheValue, TActualResult>(
                getter.Method,
                new []{ new KeyValuePair<Type, object>(typeof(TIn1), in1) },
                () => getter(in1));
        }
        
        /* ****************** ASYNC API ****************/
        public static Task<TResult> TryGetAsync<TResult>(this ICache<string, object> cache, Func<Task<TResult>> getter)
        {
            return cache.TryGetAsync<object, TResult>(getter);
        }
        
        public static Task<TActualResult> TryGetAsync<TCacheValue, TActualResult>(this ICache<string, TCacheValue> cache, Func<Task<TActualResult>> getter)
            where TActualResult : TCacheValue
        {
            return cache.TryGetGenericAsync<TCacheValue, TActualResult>(
                getter.Method,
                new KeyValuePair<Type, object>[]{ },
                getter);
        }
        
        public static Task<TResult> TryGetAsync<TIn1, TResult>(this ICache<string, object> cache, TIn1 in1, Func<TIn1, Task<TResult>> getter)
            where TResult : class
        {
            return cache.TryGetAsync<TIn1, object, TResult>(in1, getter);
        }
        
        public static Task<TActualResult> TryGetAsync<TIn1, TCacheValue, TActualResult>(this ICache<string, TCacheValue> cache, TIn1 in1, Func<TIn1, Task<TActualResult>> getter)
            where TActualResult : TCacheValue
        {
            return cache.TryGetGenericAsync<TCacheValue, TActualResult>(
                getter.Method,
                new []{ new KeyValuePair<Type, object>(typeof(TIn1), in1) },
                () => getter(in1));
        }
        
        private static TActualResult TryGetGeneric<TCacheValue, TActualResult>(
            this ICache<string, TCacheValue> cache,
            MethodInfo method,
            KeyValuePair<Type, object>[] arguments,
            Func<TActualResult> callback)
            where TActualResult : TCacheValue
        {
            var key = BuildCacheKey(method, arguments);
            if (string.IsNullOrWhiteSpace(key))
            {
                return callback();
            }
            
            var item = cache.GetItem(key);
            if (item != null)
            {
                return (TActualResult)item;
            }
            
            var value = callback();
            if (value != null)
            {
                cache.Upsert(key, value);
            }
            
            return value;
        }
        
        private async static Task<TActualResult> TryGetGenericAsync<TCacheValue, TActualResult>(
            this ICache<string, TCacheValue> cache,
            MethodInfo method,
            KeyValuePair<Type, object>[] arguments,
            Func<Task<TActualResult>> callback)
            where TActualResult : TCacheValue
        {
            var key = BuildCacheKey(method, arguments);
            if (string.IsNullOrWhiteSpace(key))
            {
                return await callback();
            }
            
            var item = cache.GetItem(key);
            if (item != null)
            {
                return (TActualResult)item;
            }
            
            var value = await callback();
            if (value != null)
            {
                cache.Upsert(key, value);
            }
            
            return value;
        }
        
        
        private static string BuildCacheKey(MethodBase method, params KeyValuePair<Type, object>[] arguments)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(method.DeclaringType.FullName);
            sb.Append(".");
            sb.Append(method.Name);
            sb.Append('(');
            
            const string separator = "^✓$✓^";
            
            for (int i = 0; i < arguments.Length; i++)
            {
                var argument = arguments[i];
                if (argument.Key != typeof(string) && !argument.Key.IsValueType && !argument.Key.IsEnum)
                {
                    return null;
                }
                
                string argumentKey = argument.Value == null ? "__NULL__" : argument.ToString();
                if (argumentKey.Contains(separator))
                {
                    return null;
                }
                
                sb.Append(argumentKey);
                
                if (i >= 1)
                {
                    sb.Append(separator);
                }
            }

            sb.Append(')');
            
            return sb.ToString();
        }
    }
}


//        public static TResult TryGet<TResult>(this ICache<string, TResult> cache, Func<TResult> getter)
//            where TResult : class
//        {
//            var method = getter.Method;
//            var key = BuildCacheKey(method);
//            if (string.IsNullOrWhiteSpace(key))
//            {
//                return getter();
//            }
//
//            var item = cache.GetItem(key);
//            if (item != null)
//            {
//                return item;
//            }
//
//            var value = getter();
//            if (value != null)
//            {
//                cache.Upsert(key, value);
//            }
//
//            return value;
//        }
//
//        public static TResult TryGet<TIn1, TResult>(this ICache<string, TResult> cache, TIn1 in1, Func<TIn1, TResult> getter)
//            where TResult : class
//        {
//            var method = getter.Method;
//            var key = BuildCacheKey(method, new KeyValuePair<Type, object>(typeof(TIn1), in1));
//            if (string.IsNullOrWhiteSpace(key))
//            {
//                return getter(in1);
//            }
//
//            var item = cache.GetItem(key);
//            if (item != null)
//            {
//                return item;
//            }
//
//            var value = getter(in1);
//            if (value != null)
//            {
//                cache.Upsert(key, value);
//            }
//
//            return value;
//        }