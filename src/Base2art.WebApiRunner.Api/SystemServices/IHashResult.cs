﻿namespace Base2art.WebApiRunner.SystemServices
{
    using System;

    public interface IHashResult
    {
        string AsString();

        byte[] AsByteArray();
    }
}
