﻿
namespace Base2art.WebApiRunner.SystemServices
{
    using System;
    
    public interface ICacheProvider
    {
        ICache<string, object> GetSystemCache();

        ICache<TKey, TValue> CreateCache<TKey, TValue>(TimeSpan defaultKeepForDuration);
    }
}
