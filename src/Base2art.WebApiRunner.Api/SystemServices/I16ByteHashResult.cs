﻿namespace Base2art.WebApiRunner.SystemServices
{
    using System;

    public interface I16ByteHashResult : IHashResult
    {
        Guid AsGuid();
    }
}


