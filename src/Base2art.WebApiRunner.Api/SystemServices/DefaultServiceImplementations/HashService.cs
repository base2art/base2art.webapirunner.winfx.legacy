﻿namespace Base2art.WebApiRunner.SystemServices.DefaultServiceImplementations
{
    using System;
    using System.Globalization;
    using System.Security.Cryptography;
    using System.Text;
    
    public class HashService : IHashService
    {
        public IHashResult Hash<T>(string value)
            where T : HashAlgorithm, new()
        {
            return Create<T, HashResult>(value, x => new HashResult(x));
        }
        
        public I16ByteHashResult HashAsMD5(string value)
        {
            return Create<MD5CryptoServiceProvider, Md5HashResult>(value, x => new Md5HashResult(x));
        }
        
        private TResult Create<T, TResult>(string value, Func<byte[], TResult> creator)
            where T : HashAlgorithm, new()
            where TResult : HashResult
        {
            byte[] inputBytes = Encoding.ASCII.GetBytes(value ?? string.Empty);
            using (var hashAlgorithm = new T())
            {
                return creator(hashAlgorithm.ComputeHash(inputBytes));
            }
        }

        private class HashResult : IHashResult
        {
            private readonly byte[] hashedBytes;

            public HashResult(byte[] hashedBytes)
            {
                this.hashedBytes = hashedBytes;
            }

            protected byte[] HashedBytes
            {
                get { return this.hashedBytes; }
            }
            
            public byte[] AsByteArray()
            {
                return this.hashedBytes;
            }
            
            public string AsString()
            {
                var hash = this.hashedBytes;
                
                // step 2, convert byte array to hex string
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < hash.Length; i++)
                {
                    sb.Append(hash[i].ToString("X2", CultureInfo.InvariantCulture));
                }
                
                return sb.ToString();
            }
        }
        
        private class Md5HashResult : HashResult, I16ByteHashResult
        {
            public Md5HashResult(byte[] hashedBytes) : base(hashedBytes)
            {
            }

            public Guid AsGuid()
            {
                var hashedBytes = this.HashedBytes;
                var bytes = new byte[16];
                bytes[0] = hashedBytes[3];
                bytes[1] = hashedBytes[2];
                bytes[2] = hashedBytes[1];
                bytes[3] = hashedBytes[0];
                bytes[4] = hashedBytes[5];
                bytes[5] = hashedBytes[4];
                bytes[6] = hashedBytes[7];
                bytes[7] = hashedBytes[6];
                bytes[8] = hashedBytes[8];
                bytes[9] = hashedBytes[9];
                bytes[10] = hashedBytes[10];
                bytes[11] = hashedBytes[11];
                bytes[12] = hashedBytes[12];
                bytes[13] = hashedBytes[13];
                bytes[14] = hashedBytes[14];
                bytes[15] = hashedBytes[15];
                return new Guid(bytes);
            }
        }
    }
}
