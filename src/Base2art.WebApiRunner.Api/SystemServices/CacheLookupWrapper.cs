﻿
namespace Base2art.WebApiRunner.SystemServices
{
    using System;
    using System.Threading.Tasks;

    public class CacheLookupWrapper<T>
    {
        private readonly ICache<string, object> cache;

        public CacheLookupWrapper(ICache<string, object> cache)
        {
            this.cache = cache;
        }

        public TResult TryGet<TResult>(Func<TResult> getter) where TResult : class
        {
            return this.cache.TryGet(getter) as TResult;
        }

        public TResult TryGet<TIn1, TResult>(TIn1 in1, Func<TIn1, TResult> getter) where TResult : class
        {
            return this.cache.TryGet(in1, getter) as TResult;
        }

        public async Task<TResult> TryGetAsync<TResult>(Func<Task<TResult>> getter) where TResult : class
        {
            return await this.cache.TryGetAsync<object, TResult>(getter) as TResult;
        }

        public async Task<TResult> TryGetAsync<TIn1, TResult>(TIn1 in1, Func<TIn1, Task<TResult>> getter) where TResult : class
        {
            return await this.cache.TryGetAsync(in1, getter) as TResult;
        }
    }
}

