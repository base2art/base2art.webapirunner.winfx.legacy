﻿namespace Base2art.WebApiRunner.SystemServices
{
    using System;
    using System.Security.Cryptography;
    
    public interface IHashService
    {
        IHashResult Hash<T>(string value)
            where T : HashAlgorithm, new();
        
        I16ByteHashResult HashAsMD5(string value);
    }
}
