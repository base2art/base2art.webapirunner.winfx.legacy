﻿
namespace Base2art.WebApiRunner.SystemServices
{
    using System;
    
    public interface ICache<TKey, TValue>
    {
        TValue GetItem(TKey key);

        bool HasItem(TKey key);

        void Upsert(TKey key, TValue value);

        void Upsert(TKey key, TValue value, TimeSpan keepForDuration);

        void Clear();
    }
}
