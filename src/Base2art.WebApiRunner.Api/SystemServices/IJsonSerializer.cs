﻿using System.Threading.Tasks;
namespace Base2art.WebApiRunner.SystemServices
{
    public interface IJsonSerializer
    {
        string SerializeObjectFormatted<T>(T data);
        
        string SerializeObject<T>(T data);
        
        string SerializeObjectFormatted(object data, System.Type type);
        
        string SerializeObject(object data, System.Type type);
        
        T DeserializeObject<T>(string data);
        
        object DeserializeObject(string data, System.Type type);
        
        
        Task<string> SerializeObjectFormattedAsync<T>(T data);
        
        Task<string> SerializeObjectAsync<T>(T data);
        
        Task<string> SerializeObjectFormattedAsync(object data, System.Type type);
        
        Task<string> SerializeObjectAsync(object data, System.Type type);
        
        Task<T> DeserializeObjectAsync<T>(string data);
        
        Task<object> DeserializeObjectAsync(string data, System.Type type);
    }
}
