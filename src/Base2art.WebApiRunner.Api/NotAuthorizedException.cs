﻿namespace Base2art.WebApiRunner
{
    using System;
    using System.Runtime.Serialization;

    [Serializable]
    public class NotAuthorizedException : Exception, ISerializable
    {
        public NotAuthorizedException()
        {
        }

        public NotAuthorizedException(string message) : base(message)
        {
        }

        public NotAuthorizedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        // This constructor is needed for serialization.
        protected NotAuthorizedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}

