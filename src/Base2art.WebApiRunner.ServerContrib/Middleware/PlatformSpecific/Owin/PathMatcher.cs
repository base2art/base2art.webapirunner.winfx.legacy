﻿
namespace Base2art.WebApiRunner.Middleware.PlatformSpecific.Owin
{
    using System.Collections.Generic;
    using System.Net;

    public class PathMatcher
    {
        public string Verb
        {
            get;
            set;
        }
        
        public string Path
        {
            get;
            set;
        }

        public StringMatchType MatchType
        {
            get;
            set;
        }
    }
}


