﻿
namespace Base2art.WebApiRunner.Middleware.PlatformSpecific.Owin
{
    public enum StringMatchType
    {
        EqualsIgnoreCase,
        Equals,
        StartsWithIgnoreCase,
        StartsWith,
        RegexIgnoreCase,
        Regex
    }
}


