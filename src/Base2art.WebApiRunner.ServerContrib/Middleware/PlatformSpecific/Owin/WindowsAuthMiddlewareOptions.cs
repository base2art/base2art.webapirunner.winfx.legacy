﻿
namespace Base2art.WebApiRunner.Middleware.PlatformSpecific.Owin
{
    using System.Collections.Generic;
    using System.Net;
    
    public class WindowsAuthMiddlewareOptions
    {
        public AuthenticationSchemes Scheme { get; set; }
        public List<PathMatcher> Urls { get; set; }
    }
}
