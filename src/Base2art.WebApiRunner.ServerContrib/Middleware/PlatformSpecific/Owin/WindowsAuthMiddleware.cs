﻿
namespace Base2art.WebApiRunner.Middleware.PlatformSpecific.Owin
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net;
    using System.Text.RegularExpressions;
    
    public class WindowsAuthMiddleware : IOwinMiddleware<WindowsAuthMiddlewareOptions>
    {
        private readonly IDictionary<StringMatchType, Func<string, string, bool>> compLookup;
        
        public WindowsAuthMiddleware()
        {
            compLookup = new Dictionary<StringMatchType, Func<string, string, bool>>
            {
                
                { StringMatchType.Equals, (paq, y) => string.Equals(paq, y, System.StringComparison.Ordinal) },
                { StringMatchType.EqualsIgnoreCase, (paq, y) => string.Equals(paq, y, System.StringComparison.OrdinalIgnoreCase) },
                
                { StringMatchType.StartsWith, (paq, y) => paq.StartsWith(y, StringComparison.Ordinal) },
                { StringMatchType.StartsWithIgnoreCase, (paq, y) => paq.StartsWith(y, StringComparison.OrdinalIgnoreCase) },
                
                { StringMatchType.Regex, (paq, y) => Regex.IsMatch(paq, y, RegexOptions.None) },
                { StringMatchType.RegexIgnoreCase, (paq, y) => Regex.IsMatch(paq, y, RegexOptions.IgnoreCase) },
            };
        }
        
        public void Run(System.Web.Http.HttpConfiguration config, WindowsAuthMiddlewareOptions[] options, System.Collections.Generic.IDictionary<string, object> properties, System.Action<object, object[]> useSetup)
        {
            HttpListener listener = (HttpListener)properties["System.Net.HttpListener"];
            if (listener == null)
            {
                return;
            }

            if (options.Length == 0)
            {
                listener.AuthenticationSchemes = AuthenticationSchemes.Ntlm;
            }
            
            var defaultSchemeContainer = options.FirstOrDefault(x => this.Coalesce(x.Urls).Count == 0);
            
            var urlSchemes = options.Where(x => this.Coalesce(x.Urls).Count >= 0).ToArray();
            
            var defaultScheme = defaultSchemeContainer == null ? AuthenticationSchemes.Anonymous : defaultSchemeContainer.Scheme;
            
            if (urlSchemes.Length == 0 && defaultSchemeContainer != null)
            {
                listener.AuthenticationSchemes = defaultScheme;
            }
            else
            {
                var lookup = new List<KeyValuePair<PathMatcher, AuthenticationSchemes>>();
                
                foreach (var urlScheme in urlSchemes)
                {
                    foreach (var url in this.Coalesce(urlScheme.Urls))
                    {
                        lookup.Add(new KeyValuePair<PathMatcher, AuthenticationSchemes>(url, urlScheme.Scheme));
                    }
                }
                
                listener.AuthenticationSchemeSelectorDelegate = delegate(HttpListenerRequest s)
                {
                    foreach (var option in lookup)
                    {
                        var matcher = option.Key;
                        
                        if (this.compLookup[matcher.MatchType](s.Url.AbsolutePath, matcher.Path))
                        {
                            if (string.IsNullOrWhiteSpace(matcher.Verb))
                            {
                                return option.Value;
                            }
                            
                            if (string.Equals(matcher.Verb, s.HttpMethod, StringComparison.OrdinalIgnoreCase))
                            {
                                return option.Value;
                            }
                        }
                    }
                    
                    return defaultScheme;
                };
                
            }
        }

        private List<T> Coalesce<T>(List<T> urls)
        {
            return urls ?? new List<T>();
        }
    }
}
