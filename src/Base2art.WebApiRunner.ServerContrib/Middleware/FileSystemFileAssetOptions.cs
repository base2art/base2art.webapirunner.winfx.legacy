﻿
using System;
namespace Base2art.WebApiRunner.Middleware
{
    public class FileSystemFileAssetOptions
    {
        public FileSystemFileAssetServeType ServeType
        {
            get;
            set;
        }
        
        public TimeSpan? CacheTime
        {
            get;
            set;
        }
        
        public FileSystemFileAssetCacheMode CacheMode
        {
            get;
            set;
        }
        
        public string BasePath
        {
            get;
            set;
        }
        
        public string DiskPath
        {
            get;
            set;
        }

        public string DefaultDocumentPath
        {
            get;
            set;
        }

        public bool ServeDefaultDocumentPathOnFileNotFound
        {
            get;
            set;
        }
    }
}
