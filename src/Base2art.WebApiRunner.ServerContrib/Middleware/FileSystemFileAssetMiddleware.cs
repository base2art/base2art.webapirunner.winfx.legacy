﻿
namespace Base2art.WebApiRunner.Middleware
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Net.Http;
    using System.Net.Mime;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Web.Http;
    using Base2art.WebApiRunner.Reflection;
    using Base2art.WebApiRunner.Services;
    using Base2art.WebApiRunner.SystemServices;
    using Base2art.WebApiRunner.SystemServices.DefaultServiceImplementations;
    
    public class FileSystemFileAssetMiddleware : IMiddleware<FileSystemFileAssetOptions>
    {
        private HashSet<Guid> cacheFiles = new HashSet<Guid>();
        
        public void Run(HttpConfiguration config, FileSystemFileAssetOptions[] options)
        {
            Array.ForEach(options, option =>
                          {
                              var path = option.ServeType == FileSystemFileAssetServeType.File
                                  ? option.BasePath
                                  : option.BasePath + "/{*filePath}";
                              config.Routes.MapHttpRoute(
                                  "assets" + option.BasePath,
                                  path,
                                  null,
                                  new object(),
                                  new CustomMessageHandler(option, new HashService()));
                          });
        }
        
        private class CustomMessageHandler : HttpMessageHandler
        {
            private readonly FileSystemFileAssetOptions options;

            private readonly IHashService service;
            public CustomMessageHandler(FileSystemFileAssetOptions options, IHashService service)
            {
                this.service = service;
                this.options = options;
            }
            
            protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
            {
                //                var cd = Environment.CurrentDirectory;
                //                cd.ToString();
                var routeData = request.GetRouteData();
                var file = new FileInfo(this.options.DiskPath);
                
                if (this.options.ServeType == FileSystemFileAssetServeType.File)
                {
                    if (!file.Exists)
                    {
                        file = null;
                    }
                    
                    return this.SendFileAsync(request, file);
                }
                
                var partialPath = routeData.Values["filePath"] as string;
                if (routeData.Values["filePath"] == null)
                {
                    partialPath = this.options.DefaultDocumentPath;
                }
                
                var output = this.Correct(this.options.DiskPath, partialPath);
                if (output == null && this.options.ServeDefaultDocumentPathOnFileNotFound)
                {
                    output = this.Correct(this.options.DiskPath, this.options.DefaultDocumentPath);
                }
                
                return this.SendFileAsync(request, output);
            }
            
            private FileInfo Correct(string basePath, string anything)
            {
                var parts = anything.Split(new char[]
                                           {
                                               Path.AltDirectorySeparatorChar,
                                               Path.DirectorySeparatorChar
                                           }, StringSplitOptions.RemoveEmptyEntries);
                
                var stack = new Stack<string>();
                
                ProcessParts(parts, 0, stack);
                
                List<string> fileParts = new List<string>();
                fileParts.Add(basePath);
                fileParts.AddRange(stack.Reverse());
                
                var path = Path.Combine(fileParts.ToArray());
                
                var di = new DirectoryInfo(path);
                var fi = new FileInfo(path);
                
                if (di.Exists)
                {
                    if (string.IsNullOrWhiteSpace(this.options.DefaultDocumentPath))
                    {
                        return null;
                    }
                    
                    fi = new FileInfo(Path.Combine(path, this.options.DefaultDocumentPath));
                }
                
                if (fi.Exists)
                {
                    return fi;
                }
                
                return null;
            }

            private void ProcessParts(string[] parts, int i, Stack<string> stack)
            {
                if (parts.Length <= i)
                {
                    return;
                }
                
                var part = parts[i];
                
                if (part == ".")
                {
                    this.ProcessParts(parts, i + 1, stack);
                    return;
                }
                
                if (part == "..")
                {
                    stack.Pop();
                    
                    this.ProcessParts(parts, i + 1, stack);
                    return;
                }
                
                
                stack.Push(part);
                this.ProcessParts(parts, i + 1, stack);
            }

            public Task<HttpResponseMessage> SendFileAsync(HttpRequestMessage request, FileInfo fullName)
            {
                if (fullName == null)
                {
                    return Task.FromResult(request.CreateResponse(
                        System.Net.HttpStatusCode.NotFound,
                        new Models.Error{ Message = "File Not Found" }));
                }
                
                var id = this.service.HashAsMD5(fullName.FullName).AsGuid();
                if (request.Headers.IfNoneMatch.Any(x => ToGuid(x) == id))
                {
                    var response304 = request.CreateResponse(System.Net.HttpStatusCode.NotModified);
                    SetHeaders(response304, id);
                    return Task.FromResult(response304);
                }
                
                var map = request.GetConfiguration().DependencyResolver.GetService<IMediaTypeMap>();
                
                var response = request.CreateResponse(System.Net.HttpStatusCode.OK);
                response.Content = new StreamContent(fullName.OpenRead());
                response.Content.Headers.ContentType = new System.Net.Http.Headers.MediaTypeHeaderValue(map.GetMimeType(fullName.Extension));
                
                if (this.options.CacheMode == FileSystemFileAssetCacheMode.FullPath)
                {
                    SetHeaders(response, id);
                }
                
                return Task.FromResult(response);
            }

            private void SetHeaders(HttpResponseMessage response, Guid id)
            {
                if (this.options.CacheTime.HasValue)
                {
                    var cacheControl = new System.Net.Http.Headers.CacheControlHeaderValue();
                    cacheControl.MaxAge = this.options.CacheTime.Value;
                    response.Headers.CacheControl = cacheControl;
                }
                
                response.Headers.ETag = new System.Net.Http.Headers.EntityTagHeaderValue("\"" + id.ToString("N") + "\"");
            }

            private static Guid ToGuid(System.Net.Http.Headers.EntityTagHeaderValue x)
            {
                var tag = x.Tag;
                var comp = StringComparison.OrdinalIgnoreCase;
                if (tag.StartsWith("\"", comp) && tag.EndsWith("\"", comp))
                {
                    tag = tag.Substring(1, tag.Length - 2);
                }
                
                Guid g;
                Guid.TryParse(tag, out g);
                return g;
            }
        }
    }
}
/*
 
 
        private readonly string basePath;

        private readonly string defaultDocumentResource;
        
        public FileAssetService(string basePath, string defaultDocumentResource)
        {
            this.defaultDocumentResource = defaultDocumentResource;
            this.basePath = basePath;
        }
        
        public FileInfo Root()
        {
            return new FileInfo(Path.Combine(basePath, defaultDocumentResource));
        }

        private void ProcessParts(string[] parts, int i, Stack<string> stack)
        {
            if (parts.Length <= i)
            {
                return;
            }
            
            var part = parts[i];
            
            if (part == ".")
            {
                this.ProcessParts(parts, i + 1, stack);
                return;
            }
            
            if (part == "..")
            {
                stack.Pop();
                
                this.ProcessParts(parts, i + 1, stack);
                return;
            }
            
            
            stack.Push(part);
            this.ProcessParts(parts, i + 1, stack);
        }
 
 */