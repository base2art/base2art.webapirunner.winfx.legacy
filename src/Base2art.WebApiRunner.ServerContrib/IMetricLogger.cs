﻿namespace Base2art.WebApiRunner
{
    using System;
    using System.Reflection;
    using System.Threading.Tasks;
    
    public interface IMetricLogger
    {
        Task LogAsync(string method, Uri url, MethodInfo methodInfo, object[] parms, TimeSpan span);
    }
}
