﻿namespace Base2art.WebApiRunner.ExceptionHandling
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Filters;

    public abstract class LoggingExceptionHandlerBase : IExceptionHandler
    {
        public Task HandleAsync(Exception exception)
        {
            if (exception.ShouldLog())
            {
                return this.HandleExceptionAsync(exception);
            }
            
            return Task.FromResult(true);
        }

        protected abstract Task HandleExceptionAsync(Exception exception);
    }
}


