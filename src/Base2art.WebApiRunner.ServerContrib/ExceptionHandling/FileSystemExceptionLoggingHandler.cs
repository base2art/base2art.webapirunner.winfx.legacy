﻿namespace Base2art.WebApiRunner.ExceptionHandling
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Filters;
    using Base2art.WebApiRunner.SystemServices.DefaultServiceImplementations;
    
    public class FileSystemExceptionLoggingHandler : LoggingExceptionHandlerBase
    {
        private string basePath;
        
        private readonly string companyName;
        
        private readonly string appName;
        
        public FileSystemExceptionLoggingHandler(string basePath, string companyName, string appName)
        {
            this.appName = appName;
            this.companyName = companyName;
            this.basePath = basePath;
        }
        
        protected override Task HandleExceptionAsync(Exception exception)
        {
            if (string.IsNullOrWhiteSpace(this.basePath))
            {
                this.basePath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            }
            
            if (string.IsNullOrWhiteSpace(basePath) || File.Exists(basePath))
            {
                this.basePath = "c:\\";
            }
            
            Func<string, string> coalesce = x => string.IsNullOrWhiteSpace(x) ? "default" : x;
            
            try
            {
                var path = Path.Combine(basePath, coalesce(this.companyName), coalesce(this.appName));
                
                Directory.CreateDirectory(path);
                var ticks = DateTime.UtcNow.Ticks;
                File.WriteAllText(Path.Combine(path, ticks + ".log"), this.Serialize(exception));
            }
            catch (Exception)
            {
            }
            
            return Task.FromResult(true);
        }

        protected virtual string Serialize(Exception e)
        {
            return new ObjectStringifier().PrettyPrint(e);
        }
    }
}
