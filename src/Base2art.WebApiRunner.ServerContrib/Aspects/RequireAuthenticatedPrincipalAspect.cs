﻿namespace Base2art.WebApiRunner.Aspects
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Web.Http.Controllers;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Aspects;

    public class RequireAuthenticatedPrincipalAspect : SimpleMethodInterceptAspect
    {
        public override string Name
        {
            get { return "base2art.requireAuthenticated"; }
        }
        
        protected override async System.Threading.Tasks.Task<object> InvokeWithEvents(MethodInterceptData data, HttpControllerContext controllerContext, HttpActionDescriptor actionDescriptor, Func<System.Threading.Tasks.Task<object>> next)
        {
            var model = TypedAspect.Parse<ClaimsPrincipal>(controllerContext, actionDescriptor, () => new NotAuthenticatedException());
            
            if (model == null || !model.Identity.IsAuthenticated)
            {
                throw new NotAuthenticatedException();
            }
            
            return await base.InvokeWithEvents(data, controllerContext, actionDescriptor, next);
        }
    }
}


