﻿namespace Base2art.WebApiRunner.Aspects
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Reflection;
    using System.Text;
    using System.Threading.Tasks;
    using Base2art.WebApiRunner.Aspects;
    using Base2art.WebApiRunner.SystemServices;
    
    internal static class AspectCacher
    {
        public static void Invoke(
            this MethodInfo methodInfo,
            ICache<string, object> cacheService,
            IList<object> args,
            Action baseCall,
            MethodInterceptData valueHolder,
            TimeSpan keepForDuration)
        {
            if (typeof(Task).IsAssignableFrom(methodInfo.ReturnType))
            {
                ProcessTask(cacheService, methodInfo, args, valueHolder, baseCall, keepForDuration);
            }
            
            if (!typeof(Task).IsAssignableFrom(methodInfo.ReturnType))
            {
                ProcessNonTask(cacheService, methodInfo, args, valueHolder, baseCall, keepForDuration);
            }
        }
        
        private static object ContinueStash1(
            Type returnType,
            object task,
            ICache<string, object> cacheService,
            string cacheKey,
            TimeSpan keepForDuration)
        {
            
            if (returnType != typeof(object) && task.GetType() == typeof(Task<object>))
            {
                return ContinueStash3((Task<object>)task, cacheService, cacheKey, keepForDuration);
            }
            
            var method = typeof(AspectCacher).GetMethod("ContinueStash2");
            var methodTyped = method.MakeGenericMethod(returnType);
            
            methodTyped.Invoke(null, new [] {
                                   task,
                                   cacheService,
                                   cacheKey,
                                   keepForDuration
                               });
            
            return methodTyped;
        }
        
        private static async Task<object> ContinueStash3(Task<object> task, ICache<string, object> cacheService, string cacheKey, TimeSpan keepForDuration)
        {
            var result = await task;
            
            Stash(cacheService, cacheKey, result, keepForDuration);
            
            return result;
        }
        
        public static async Task<T> ContinueStash2<T>(Task<T> task, ICache<string, object> cacheService, string cacheKey, TimeSpan keepForDuration)
        {
            var result = await task;
            
            Stash(cacheService, cacheKey, result, keepForDuration);
            
            return result;
        }

        private static void ProcessTask(
            ICache<string, object> cache,
            MethodInfo method,
            IList<object> args,
            MethodInterceptData valueHolder,
            Action baseCall,
            TimeSpan keepForDuration)
        {
            ProcessBase(
                cache,
                method,
                args,
                valueHolder,
                baseCall,
                x => x.ReturnType.GetGenericArguments()[0],
                (returnType, x) => CreateTask(x, returnType),
                (returnType, cacheService, cacheKey, x) => ContinueStash1(returnType, x, cacheService, cacheKey, keepForDuration));
        }
        
        private static void ProcessNonTask(
            ICache<string, object> cache,
            MethodInfo method,
            IList<object> args,
            MethodInterceptData valueHolder,
            Action baseCall,
            TimeSpan keepForDuration)
        {
            ProcessBase(
                cache,
                method,
                args,
                valueHolder,
                baseCall,
                x => x.ReturnType,
                (returnType, x) => x,
                (returnType, cacheService, cacheKey, x) => Stash(cacheService, cacheKey, x, keepForDuration));
        }

        private static void ProcessBase(
            ICache<string, object> cacheService,
            MethodInfo methodInfo,
            IList<object> args,
            MethodInterceptData valueHolder,
            Action baseCall,
            Func<MethodInfo, Type> getReturnType,
            Func<Type, object, object> wrap,
            Action<Type, ICache<string, object>, string, object> stash)
        {
            var returnType = getReturnType(methodInfo);
            
            var cacheKey = CreateKey(args, methodInfo);
            
            Debug.WriteLine("Key: " + cacheKey);
            
            var cachedAttribute = cacheService.GetItem(cacheKey);
            Debug.WriteLine("ValueFromCache: " + cachedAttribute);
            
            if (cachedAttribute != null)
            {
                if (((returnType.IsArray) && ((Array)cachedAttribute).Length == 0) ||
                    ((typeof(System.Collections.ICollection)).IsAssignableFrom(returnType) && ((System.Collections.ICollection)cachedAttribute).Count == 0))
                {
                    baseCall();
                    
                    var valueToCache = valueHolder.ReturnValue;
                    
                    Debug.WriteLine("Key: " + valueToCache);
                    stash(returnType, cacheService, cacheKey, valueToCache);
                    
                    Debug.WriteLine("Cached: " + valueToCache);
                }
                else
                {
                    // Use Value From Cache...
                    var obj = wrap(returnType, cachedAttribute);
                    valueHolder.ReturnValue = (Task<object>)obj;
                }
                
            }
            else
            {
                baseCall();
                
                var valueToCache = valueHolder.ReturnValue;
                Debug.WriteLine("Key: " + valueToCache);
                stash(returnType, cacheService, cacheKey, valueToCache);
                
                Debug.WriteLine("Cached: " + valueToCache);
            }
        }

        private static void Stash(ICache<string, object> cacheService, string cacheKeyLocal, object valueToCache, TimeSpan keepForDuration)
        {
            if (keepForDuration == TimeSpan.Zero && valueToCache != null)
            {
                cacheService.Upsert(cacheKeyLocal, valueToCache, TimeSpan.FromMinutes(20));
            }
            else
                if (valueToCache != null)
            {
                cacheService.Upsert(cacheKeyLocal, valueToCache, keepForDuration);
            }
        }
        
        private static string CreateKey(IList<object> args, MethodInfo method)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(method.DeclaringType.Name);
            sb.Append(".");
            sb.Append(method.Name);
            sb.Append("(");
            
            var parms = method.GetParameters();

            for (int i = 0; i < args.Count; i++)
            {
                var argument = args[i];
                var argumentType = parms[i].ParameterType;
                
                if (typeof(System.Collections.ICollection).IsAssignableFrom(argumentType))
                {
                    foreach (var element in (System.Collections.IEnumerable)argument)
                    {
                        sb.Append(element);
                        sb.Append(":✓#✓:");
                    }
                }
                else
                {
                    sb.Append(argument);
                }
                //Added this seperator ":✓#✓:" to make sure that no arguments contains the seperator character.
                sb.Append(":_✓#✓_:");
            }

            sb.Append(")");
            return sb.ToString();
        }
        
        private static object CreateTask(object t, Type type)
        {
            var task = typeof(Task);
            var method = task.GetMethod("FromResult");
            var methodItems = method.MakeGenericMethod(typeof(object));
            
            return methodItems.Invoke(null, new []{ t });
        }
    }
}
