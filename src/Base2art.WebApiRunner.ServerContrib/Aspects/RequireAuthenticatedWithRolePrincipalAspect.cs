﻿namespace Base2art.WebApiRunner.Aspects
{
    using System;
    using System.Linq;
    using System.Security.Claims;
    using System.Web.Http.Controllers;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Aspects;

    public class RequireAuthenticatedWithRolePrincipalAspect : ParameterizedMethodInterceptAspect<RequireAuthenticatedWithRolePrincipalAspect.Parameters>
    {
        public override string Name
        {
            get { return "base2art.requireAuthenticatedRole"; }
        }
        
        protected override async System.Threading.Tasks.Task<object> InvokeWithEvents(
            MethodInterceptData data,
            Parameters aspectParameters, 
            HttpControllerContext controllerContext,
            HttpActionDescriptor actionDescriptor,
            Func<System.Threading.Tasks.Task<object>> next)
        {
            var model = TypedAspect.Parse<ClaimsPrincipal>(controllerContext, actionDescriptor, () => new NotAuthenticatedException());
            
            if (model == null || !model.Identity.IsAuthenticated)
            {
                throw new NotAuthenticatedException();
            }
            
            var roles = (aspectParameters ?? new Parameters()).Roles ?? new string[0];
            
            if (!roles.Any(model.IsInRole))
            {
                throw new NotAuthorizedException("Must be in role: (" + string.Join(",", roles) + ")");
            }
            
            return await base.InvokeWithEvents(data, aspectParameters, controllerContext, actionDescriptor, next);
        }
        
        public class Parameters
        {
            public string[] Roles {get;set;}
        }
    }
}


