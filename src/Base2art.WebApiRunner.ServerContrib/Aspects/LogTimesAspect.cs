﻿namespace Base2art.WebApiRunner.Aspects
{
    using System;
    using System.Linq;
    using System.Web.Http.Controllers;
    
    public class LogTimesAspect : ParameterizedMethodInterceptAspect<LogTimesAspect.Parameters>
    {
        private readonly IMetricLogger logger;

        public LogTimesAspect(IMetricLogger logger)
        {
            this.logger = logger;
        }
        
        public override string Name
        {
            get { return "base2art.logMetrics"; }
        }
        
        protected override async System.Threading.Tasks.Task<object> InvokeWithEvents(
            MethodInterceptData data,
            Parameters aspectParameters, 
            HttpControllerContext controllerContext,
            HttpActionDescriptor actionDescriptor,
            Func<System.Threading.Tasks.Task<object>> next)
        {
            var before = DateTime.UtcNow;
            var result = await base.InvokeWithEvents(data, aspectParameters, controllerContext, actionDescriptor, next);
            var after = DateTime.UtcNow;
            
            if (this.logger != null)
            {
                await this.logger.LogAsync(
                    controllerContext.Request.Method.ToString(),
                    controllerContext.Request.RequestUri,
                    data.MethodInfo, 
                    data.InputParameters.Select(x => x).ToArray(), 
                    after - before);
            }
            
            return result;
        }
        
        public class Parameters
        {
        }
    }
}
