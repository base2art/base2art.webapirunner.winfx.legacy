﻿namespace Base2art.WebApiRunner.Aspects
{
    using System;
    using System.Linq;
    using System.Web.Http.Controllers;
    using Base2art.WebApiRunner;
    using Base2art.WebApiRunner.Aspects;
    using Base2art.WebApiRunner.SystemServices;

    public class CachingAspect : ParameterizedMethodInterceptAspect<CachingAspect.Parameters>
    {
        private readonly ICache<string, object> cache;

        public CachingAspect(ICacheProvider provider)
        {
            this.cache = provider.GetSystemCache();
        }
        
        public override string Name
        {
            get { return "base2art.caching"; }
        }
        
        protected override System.Threading.Tasks.Task<object> InvokeWithEvents(MethodInterceptData data, Parameters aspectParameters, HttpControllerContext controllerContext, HttpActionDescriptor actionDescriptor, Func<System.Threading.Tasks.Task<object>> next)
        {
            var timeout = (aspectParameters ?? new Parameters()).Timeout;
            
            AspectCacher.Invoke(
                data.MethodInfo, 
                this.cache,
                data.InputParameters, 
                async () => await base.InvokeWithEvents(data, aspectParameters, controllerContext, actionDescriptor, next),
                data,
                aspectParameters.Timeout
               );
            
            return data.ReturnValue;
        }
        
        public class Parameters
        {
            public TimeSpan Timeout { get; set; }
        }
    }
}




