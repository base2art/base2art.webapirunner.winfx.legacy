﻿namespace Base2art.WebApiRunner.Metrics
{
    using System;
    using System.IO;
    using System.Reflection;
    using System.Threading.Tasks;
    public class FileSystemMetricLogger : IMetricLogger
    {
        private readonly string basePath;

        public FileSystemMetricLogger(string basePath)
        {
            this.basePath = basePath;
        }

        Task IMetricLogger.LogAsync(string method, Uri url, MethodInfo methodInfo, object[] parms, TimeSpan span)
        {
            return Task.Factory.StartNew(() =>
                                         {
                                             var now = DateTime.UtcNow;
                                             var current = new DateTime(now.Year, now.Month, now.Day, now.Hour, 0, 0);
                                             Directory.CreateDirectory(this.basePath);
                                             var path = Path.Combine(this.basePath, current.Ticks + ".mtrx");
                                             if (!File.Exists(path))
                                             {
                                                 File.WriteAllText(path, this.CreateLogLine(method, url, methodInfo, parms, span));
                                             }
                                             else
                                             {
                                                 File.AppendAllText(path, this.CreateLogLine(method, url, methodInfo, parms, span));
                                             }
                                         });
        }

        string CreateLogLine(string method, Uri url, MethodInfo methodInfo, object[] parms, TimeSpan span)
        {
            return string.Concat(method, " ", url, " ", methodInfo.DeclaringType, ".", methodInfo.Name, " ", span.ToString("c"), Environment.NewLine);
        }
    }
}
