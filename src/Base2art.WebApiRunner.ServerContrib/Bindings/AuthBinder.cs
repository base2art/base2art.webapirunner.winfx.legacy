﻿
namespace Base2art.WebApiRunner.Bindings
{
    using System;
    using System.Collections.ObjectModel;
    using System.Diagnostics;
    using System.Linq;
    using System.Net.Http;
    using System.Security.Claims;
    using System.Web.Http.ModelBinding;
    using Base2art.WebApiRunner.Security;
    
    public class AuthBinder : IModelBinder
    {
        private readonly IClaimsPrincipalParser parser;
        
        public AuthBinder(IClaimsPrincipalParser parser)
        {
            this.parser = parser;
        }
        
        public bool BindModel(System.Web.Http.Controllers.HttpActionContext actionContext, ModelBindingContext bindingContext)
        {
            var cookies = actionContext.Request.Headers.GetCookies()
                ?? new Collection<System.Net.Http.Headers.CookieHeaderValue>();
            
            var tokenString = cookies.SelectMany(chv => chv.Cookies).Where(x => x.Name == "auth_jwt")
                .Select(x => x.Value)
                .FirstOrDefault();
            
            if (string.IsNullOrWhiteSpace(tokenString))
            {
                var auth = actionContext.Request.Headers.Authorization;
                
                if (auth != null && string.Equals(auth.Scheme, "Bearer", StringComparison.OrdinalIgnoreCase))
                {
                    tokenString = auth.Parameter;
                }
            }
            
            if (string.IsNullOrWhiteSpace(tokenString))
            {
                return False(bindingContext);
            }
            
            try
            {
                bindingContext.Model = parser.CreateValidated(tokenString);
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex);
                return False(bindingContext);
            }
        }

        private bool False(ModelBindingContext bindingContext)
        {
            bindingContext.Model = new ClaimsPrincipal(new ClaimsIdentity());
            return true;
        }
        
        private class NullLookup : IRoleLookup
        {
            public System.Collections.Generic.HashSet<string> GetRoles(ClaimsPrincipal principal)
            {
                return new System.Collections.Generic.HashSet<string>();
            }
        }
    }
}
