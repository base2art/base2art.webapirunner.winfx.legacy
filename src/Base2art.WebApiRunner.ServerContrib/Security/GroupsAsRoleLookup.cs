﻿namespace Base2art.WebApiRunner.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;

    public class GroupsAsRolesLookup : IRoleLookup
    {
        public HashSet<string> GetRoles(ClaimsPrincipal principal)
        {
            var @set = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            @set.Add("Everyone");
            @set.Add("User:" + principal.Identity.Name);
            foreach (var @group in principal.Claims.Where(x=>x.Type == "group"))
            {
                @set.Add(@group.Value);
            }
            
            return @set;
        }
    }
}
