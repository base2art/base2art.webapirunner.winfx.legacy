﻿namespace Base2art.WebApiRunner.Security
{
    using System;
    using System.IdentityModel.Tokens.Jwt;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Security.Claims;
    using System.Security.Cryptography;
    using System.Security.Principal;
    using Base2art.WebApiRunner.SystemServices;
    using Microsoft.IdentityModel.Tokens;
    
    public class JwtClaimsPrincipalParser : IClaimsPrincipalParser
    {
        private readonly string ssoUrlKeyIdReplacementString;

        private readonly string ssoUrl;

        private readonly ICacheProvider cacheProvider;
        
        private readonly string validIssuer;

        private readonly IRoleLookup lookup;
        
        public JwtClaimsPrincipalParser(
            string ssoUrl,
            string ssoUrlKeyIdReplacementString,
            bool allowInvalidCert, string validIssuer,
            ICacheProvider cacheProvider = null,
            IRoleLookup lookup = null)
        {
            this.lookup = lookup;
            this.validIssuer = validIssuer;
            this.cacheProvider = cacheProvider;
            this.ssoUrl = ssoUrl;
            this.ssoUrlKeyIdReplacementString = ssoUrlKeyIdReplacementString;
            if (allowInvalidCert)
            {
                ServicePointManager.ServerCertificateValidationCallback += (a, b, c, d) => true;
            }
        }

        public ClaimsPrincipal CreateNonValidated(string tokenString)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            Console.WriteLine(tokenHandler.InboundClaimTypeMap);
            var tokenValidationParameters = new TokenValidationParameters() {
                
                IssuerSigningKeyResolver = (token, securityToken2, keyIdentifier, validationParameters) =>
                {
                    
                    var actualKey = new SymmetricSecurityKey(Guid.NewGuid().ToByteArray());
                    actualKey.CryptoProviderFactory = new NullSignatureProviderFactory();
                    actualKey.KeyId = keyIdentifier;
                    return new []{ actualKey };
                },
                NameClaimType = ClaimTypes.NameIdentifier,
                AuthenticationType = "JWT",
                AudienceValidator = (d, s, t) => true,
                IssuerValidator = (d, s, t) => d,
                IssuerSigningKeyValidator = (d, s, t) => true,
                LifetimeValidator = (d, s, t, e) => true,
                CryptoProviderFactory = new NullSignatureProviderFactory()
            };
            
            return this.Create(tokenHandler, tokenValidationParameters, tokenString);
        }

        public ClaimsPrincipal CreateValidated(string tokenString)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var tokenValidationParameters = new TokenValidationParameters() {
                NameClaimType = ClaimTypes.NameIdentifier,
                RoleClaimType = ClaimTypes.Role,
                AuthenticationType = "JWT",
                ValidAudiences = new string[] {
                    "http://localhost/"
                },
                ValidIssuers = new string[] {
                    "http://sso.base2art.com/",
                    validIssuer
                },
                IssuerSigningKeyResolver = (token, securityToken2, keyIdentifier, validationParameters) => new []{ this.GetKeyWrapper(keyIdentifier) }
            };
            
            return this.Create(tokenHandler, tokenValidationParameters, tokenString);
        }

        private ClaimsPrincipal Create(
            JwtSecurityTokenHandler tokenHandler,
            TokenValidationParameters tokenValidationParameters,
            string tokenString)
        {
            SecurityToken key;
            var principal = tokenHandler.ValidateToken(tokenString, tokenValidationParameters, out key);
            principal = new WrappedClaimsPrincipal(principal, this.lookup);
            principal.AddIdentity(new ClaimsIdentity(new Claim[]{new Claim("token", tokenString)}));
            return principal;
        }

        private PublicRsaParameters GetKeyActual(string id)
        {
            var url = new Uri(this.ssoUrl.Replace(this.ssoUrlKeyIdReplacementString, id));
            // Create a request for the URL.
            WebRequest request = WebRequest.Create(url);
            using (WebResponse response = request.GetResponse())
            {
                using (Stream dataStream = response.GetResponseStream())
                {
                    using (StreamReader reader = new StreamReader(dataStream))
                    {
                        string responseFromServer = reader.ReadToEnd();
                        return Newtonsoft.Json.JsonConvert.DeserializeObject<PublicRsaParameters>(responseFromServer);
                    }
                }
            }
        }

        private RsaSecurityKey GetKeyWrapper(string keyIdentifier)
        {
            var rsaParameters = this.GetKeyActual(keyIdentifier) ?? new PublicRsaParameters{Modulus = new byte[0], Exponent = new byte[0]};
            RSACryptoServiceProvider myRSA = new RSACryptoServiceProvider();
            myRSA.ImportParameters(new RSAParameters {
                                       Exponent = rsaParameters.Exponent,
                                       Modulus = rsaParameters.Modulus,
                                   });
            return new RsaSecurityKey(myRSA);
        }

        public class NullSignatureProviderFactory : CryptoProviderFactory
        {
            public NullSignatureProviderFactory()
            {
            }
            
            public override bool IsSupportedAlgorithm(string algorithm, SecurityKey key)
            {
                var baseValue = base.IsSupportedAlgorithm(algorithm, key);
                return baseValue || true;
            }

            public override SignatureProvider CreateForVerifying(SecurityKey key, string algorithm)
            {
                return new NullSignatureProvider(key, algorithm);
            }

            public class NullSignatureProvider : SignatureProvider
            {
                public NullSignatureProvider(SecurityKey key, string algorithm) : base(key, algorithm)
                {
                    
                }
                public override byte[] Sign(byte[] input)
                {
                    return input;
                }
                
                public override bool Verify(byte[] input, byte[] signature)
                {
                    return true;
                }
                
                protected override void Dispose(bool disposing)
                {
                }
            }
        }

        private class PublicRsaParameters
        {
            public byte[] Modulus
            {
                get;
                set;
            }

            public byte[] Exponent
            {
                get;
                set;
            }
        }
    }
}
