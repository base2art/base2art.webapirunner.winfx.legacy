﻿namespace Base2art.WebApiRunner.Security
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;

    public class ConfigurableRoleLookup : IRoleLookup
    {
        private readonly Dictionary<string, List<string>> userLookup;
        
        private readonly Dictionary<string, List<string>> groupLookup;

        public ConfigurableRoleLookup(Dictionary<string, List<string>> userLookup, Dictionary<string, List<string>> groupLookup)
        {
            this.userLookup = userLookup;
            this.groupLookup = groupLookup;
        }
        
        public HashSet<string> GetRoles(ClaimsPrincipal principal)
        {
            string username = principal.Identity.Name;
            string[] groups = principal.Claims.Where(x=>x.Type == "group").Select(x => x.Value).ToArray();
            var @set = new HashSet<string>(StringComparer.InvariantCultureIgnoreCase);
            @set.Add("Everyone");
            AddItems(@set, this.userLookup, @username);
            
            foreach (var @group in groups)
            {
                AddItems(@set, this.groupLookup, @group);
            }
            
            return @set;
        }

        private static void AddItems(HashSet<string> @set, Dictionary<string, List<string>> coll, string key)
        {
            if (coll.ContainsKey(key))
            {
                var roles = coll[key];
                Add(@set, roles);
            }
        }
        
        private static void Add(HashSet<string> @set, List<string> roles)
        {
            foreach (var role in roles)
            {
                @set.Add(role);
            }
        }
    }
}
