﻿namespace Base2art.WebApiRunner.Security
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    
    public class WrappedClaimsPrincipal : ClaimsPrincipal
    {
        private readonly IRoleLookup lookup;

        private readonly ClaimsPrincipal principal;
        
        public WrappedClaimsPrincipal(ClaimsPrincipal principal, IRoleLookup lookup)
            : base(principal)
        {
            this.principal = principal;
            this.lookup = lookup;
        }
        
        public override bool IsInRole(string role)
        {
            if (lookup == null)
            {
                return base.IsInRole(role);
            }
            
            return this.lookup.GetRoles(this.principal).Contains(role);
        }
        //        private class WrappedClaimsIdentity : ClaimsIdentity
        //        {
        //            public WrappedClaimsIdentity(ClaimsIdentity claimsIdentity)
        //                : base(claimsIdentity)
        //            {
        //                throw new System.NotImplementedException();
        //            }
//
        //            public override string Name
        //            {
        //                get
        //                {
        //                    var nameClaim = this.Claims.FirstOrDefault(x => x.Type == "name");
//
        //                    return nameClaim == null ? base.Name : nameClaim.Value;
        //                }
        //            }
        //            /*
//
        //            string name = jst.Subject;
        //            if (nameClaim != null && !string.IsNullOrWhiteSpace(nameClaim.Value))
        //            {
        //                name = nameClaim.Value;
        //            }
//
        //            var groupsClaim = jst.Claims.Where(x => x.Type == "group");
        //            var jwtIdentity = new JwtIdentity(jst.Subject, name, jst.Issuer, groupsClaim.ToArray(), jst.Claims.ToArray(), token);
        //            var jwtPrincipal = new JwtPrincipal(roleLookup, jwtIdentity);
        //            return jwtPrincipal;
        //             */
        //        }

//        private static IEnumerable<ClaimsIdentity> Build(ClaimsPrincipal principal)
//        {
//            var identities = principal.Identities;
//            foreach (var ident in identities) {
//                
//                yield return new ClaimsIdentity(ident, ident.Claims, "JWT", ClaimTypes.NameIdentifier, "role" );
//            }
//        }
    }
}
