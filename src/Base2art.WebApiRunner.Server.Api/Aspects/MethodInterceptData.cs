﻿
using System;
using System.Collections;
using System.Reflection;
using System.Threading.Tasks;

namespace Base2art.WebApiRunner.Aspects
{
    public class MethodInterceptData
    {
        private readonly MethodInfo methodInfo;

        private readonly object[] inputParameters;

        private Task<object> returnValue;
        
        public MethodInterceptData(MethodInfo methodInfo, object[] inputParameters)
        {
            this.inputParameters = inputParameters;
            this.methodInfo = methodInfo;
        }

        public MethodInfo MethodInfo
        {
            get { return this.methodInfo; }
        }

        public Task<object> ReturnValue
        {
            get { return this.returnValue; }
            set { this.returnValue = value; }
        }

        public object[] InputParameters
        {
            get { return this.inputParameters; }
        }
    }
}
