﻿namespace Base2art.WebApiRunner.Aspects
{
    public interface IMethodInterceptAspectProvider
    {
        IMethodInterceptAspect[] Get();
    }
}
