﻿namespace Base2art.WebApiRunner.Aspects
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http.Controllers;

//    public abstract class MethodInterceptAspectBase : IMethodInterceptAspect
//    {
//        public virtual string Name
//        {
//            get
//            {
//                return this.GetType().FullName;
//            }
//        }
//
//        public void Intercept(
//            Func<object[], Task<object>> next, 
//            MethodInterceptData data, 
//            Dictionary<string, object> aspectParameters, 
//            HttpControllerContext controllerContext, 
//            HttpActionDescriptor actionDescriptor)
//        {
//            data.ReturnValue = this.InvokeWithEvents(
//                data, 
//                aspectParameters,
//                controllerContext, 
//                actionDescriptor, 
//                () => next(data.InputParameters));
//        }
//
//        protected virtual async Task<object> InvokeWithEvents(
//            MethodInterceptData data, 
//            Dictionary<string, object> aspectParameters, 
//            HttpControllerContext controllerContext, 
//            HttpActionDescriptor actionDescriptor, 
//            Func<Task<object>> next)
//        {
//            await this.OnExecuting(data, aspectParameters, controllerContext, actionDescriptor);
//            var item = await(Invoke(next));
//            data.ReturnValue = Task.FromResult(item);
//            await this.OnExecuted(data, aspectParameters, controllerContext, actionDescriptor);
//            return await data.ReturnValue;
//        }
//
//        protected async Task<object> Invoke(Func<Task<object>> runner)
//        {
//            var item = await(runner());
//            return item;
//        }
//
//        protected virtual Task OnExecuting(
//            MethodInterceptData data, 
//            Dictionary<string, object> aspectParameters, 
//            HttpControllerContext controllerContext,
//            HttpActionDescriptor actionDescriptor)
//        {
//            return Task.FromResult(true);
//        }
//
//        protected virtual Task OnExecuted(
//            MethodInterceptData data,
//            Dictionary<string, object> aspectParameters, 
//            HttpControllerContext controllerContext,
//            HttpActionDescriptor actionDescriptor)
//        {
//            return Task.FromResult(true);
//        }
//    }
}




