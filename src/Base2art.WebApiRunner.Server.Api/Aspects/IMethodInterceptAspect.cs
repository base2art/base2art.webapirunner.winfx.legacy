﻿namespace Base2art.WebApiRunner.Aspects
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http.Controllers;

    public interface IMethodInterceptAspect
    {
        string Name { get; }
        
        void Intercept(
            Func<object[], Task<object>> next, 
            MethodInterceptData data,  
            Dictionary<string, object> aspectParameters, 
            HttpControllerContext controllerContext, 
            HttpActionDescriptor actionDescriptor);
    }
}
