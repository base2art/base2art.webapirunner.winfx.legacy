﻿namespace Base2art.WebApiRunner.Aspects
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Metadata;
    using System.Web.Http.ModelBinding;
    using System.Web.Http.ValueProviders.Providers;
    
    public abstract class TypedAspect
    {
        public static T Parse<T>(
            HttpControllerContext controllerContext,
            HttpActionDescriptor actionDescriptor,
            Action actionOnNotBound)
            where T: class
        {
            Type modelType = typeof(T);
            
            var config = controllerContext.Configuration;
            var actionContext = new HttpActionContext(controllerContext, actionDescriptor);
            
            
            IEnumerable<IModelBinder> binders = from provider in config.Services.GetModelBinderProviders()
                                                         select provider.GetBinder(config, modelType);
            
            ModelMetadataProvider modelMetadataProviders = config.Services.GetModelMetadataProvider();
            var metadata = modelMetadataProviders.GetMetadataForType(null, modelType);
            
            var valueProviderFactories = config.Services.GetValueProviderFactories().Where(x => x.GetType() == typeof(RouteDataValueProviderFactory)).ToArray();
            var valueProviders = valueProviderFactories.Select(x => x.GetValueProvider(actionContext)).ToArray();
            
            var binder = binders.FirstOrDefault(x => x != null);
            var modelBindingContext = new ModelBindingContext();
            modelBindingContext.ModelMetadata = metadata;
            modelBindingContext.ValueProvider = valueProviders.FirstOrDefault();
            
            if (!binder.BindModel(actionContext, modelBindingContext))
            {
                actionOnNotBound();
            }

            return modelBindingContext.Model as T;
        }
    }
}
