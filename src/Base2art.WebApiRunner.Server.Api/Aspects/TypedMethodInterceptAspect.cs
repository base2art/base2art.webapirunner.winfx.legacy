﻿namespace Base2art.WebApiRunner.Aspects
{
    using System;
    using System.Collections.Generic;
    using System.Threading.Tasks;
    using System.Web.Http.Controllers;

    public abstract class TypedMethodInterceptAspect<TModel> : IMethodInterceptAspect 
        where TModel : class
    {
        public virtual string Name
        {
            get { return this.GetType().FullName; }
        }

        void IMethodInterceptAspect.Intercept(
            Func<object[], Task<object>> next,
            MethodInterceptData data,
            Dictionary<string, object> aspectParameters,
            HttpControllerContext controllerContext,
            HttpActionDescriptor actionDescriptor)
        {
            data.ReturnValue = this.InvokeWithEvents(
                data,
                controllerContext, 
                actionDescriptor, 
                () => next(data.InputParameters));
        }

        protected virtual async Task<object> InvokeWithEvents(
            MethodInterceptData data, 
            HttpControllerContext controllerContext, 
            HttpActionDescriptor actionDescriptor, 
            Func<Task<object>> next)
        {
            await this.OnExecuting(data, controllerContext, actionDescriptor);
            var item = await(Invoke(next));
            data.ReturnValue = Task.FromResult(item);
            await this.OnExecuted(data, controllerContext, actionDescriptor);
            return await data.ReturnValue;
        }

        protected async Task<object> Invoke(Func<Task<object>> runner)
        {
            var item = await(runner());
            return item;
        }

        protected virtual Task OnExecuting(MethodInterceptData data, HttpControllerContext controllerContext, HttpActionDescriptor actionDescriptor)
        {
            return Task.FromResult(true);
        }

        protected virtual Task OnExecuted(MethodInterceptData data, HttpControllerContext controllerContext, HttpActionDescriptor actionDescriptor)
        {
            return Task.FromResult(true);
        }
    }
}



