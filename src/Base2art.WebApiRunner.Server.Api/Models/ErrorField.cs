namespace Base2art.WebApiRunner.Models
{
   public class ErrorField
   {
      public string Message { get; set; }

      public string Code { get; set; }

      public string Path { get; set; }
   }
}
