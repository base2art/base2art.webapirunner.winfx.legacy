namespace Base2art.WebApiRunner.Models
{
   public class Error
    {
        public string Message { get; set; }
        
        public string Code { get; set; }
        
        public ErrorField[] Fields { get; set; }
    }
}
