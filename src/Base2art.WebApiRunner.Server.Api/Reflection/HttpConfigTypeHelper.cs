﻿namespace Base2art.WebApiRunner.Reflection
{
    using System.Web.Http.Dependencies;
    public static class HttpConfigTypeHelper
    {
        public static T GetService<T>(this IDependencyScope resolver)
        {
            return (T)resolver.GetService(typeof(T));
        }
    }
}
