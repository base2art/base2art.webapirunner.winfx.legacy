﻿namespace Base2art.WebApiRunner
{
    using System;
    using System.Threading.Tasks;

    public interface IExceptionHandler
    {
        Task HandleAsync(Exception exception);
    }
}
