﻿
namespace Base2art.WebApiRunner.Middleware
{
    using System.Web.Http;
    public interface IMiddleware<TOptions>
    {
        void Run(HttpConfiguration config, TOptions[] options);
    }
}
