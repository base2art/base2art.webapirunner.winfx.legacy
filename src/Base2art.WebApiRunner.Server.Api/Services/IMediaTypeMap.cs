﻿
namespace Base2art.WebApiRunner.Services
{
    public interface IMediaTypeMap
    {
        string GetMimeType(string extension);
    }
}
