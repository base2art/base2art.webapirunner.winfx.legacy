
$msbuild = "C:\Windows\Microsoft.NET\Framework64\v4.0.30319\MSBuild.exe"

if (-Not (Test-Path $msbuild)) {
  $msbuild = "C:\Windows\Microsoft.NET\Framework\v4.0.30319\MSBuild.exe"
}

echo "Cleaning..."
& $msbuild /t:Clean /verbosity:quiet /p:Configuration=Debug /m
& $msbuild /t:Clean /verbosity:quiet /p:Configuration=Release /m

echo "Restoring Packages..."
$nuget = ".\packages\NuGet.CommandLine.3.4.3\tools\NuGet.exe"

if (-Not (Test-Path $nuget)) {
  $nuget = "..\NuGet.CommandLine\tools\NuGet.exe"
}

& $nuget Restore




echo "Building..."
& $msbuild /t:Build /verbosity:quiet /p:Configuration=Debug /m /p:DebugType=PdbOnly

echo "RE-Building..."
& $msbuild /t:Build /verbosity:quiet /p:Configuration=Debug /m /p:DebugType=PdbOnly

echo "Building..."
& $msbuild /t:Build /verbosity:quiet /p:Configuration=Release /m /p:DebugType=PdbOnly

echo "RE-Building..."
& $msbuild /t:Build /verbosity:quiet /p:Configuration=Release /m /p:DebugType=PdbOnly



echo "updating package versions..."

$oldVersion = "{project.version}"
$newVersion = (GET-DATE).ToUniversalTime().ToString("0.0.0.0-yyMMddHHmm")

$git_version = git rev-parse --abbrev-ref HEAD

if ($git_version -match "next\-v?[0-9]{1,5}\.[0-9]{1,5}\.[0-9]{1,5}\.[0-9]{1,5}") {
  [regex]$rx='next\-v?([0-9]{1,5}\.[0-9]{1,5}\.[0-9]{1,5}\.[0-9]{1,5})'
  $newVersion = $rx.Match($git_version).Groups[1].Value
}

$oldVersion = $oldVersion.Trim()

cd conf

$specs = Get-ChildItem -Filter "*.nuspec"

foreach ($item in $specs) {
  
  $fileVersion = $oldVersion
  $itemContent = [System.IO.File]::ReadAllText($item.FullName)
  $itemContent = $itemContent.Replace($oldVersion, $newVersion)
  [System.IO.File]::WriteAllText($item.FullName, $itemContent)
}

cd ..

echo "packaging..."
cd conf

$specs = Get-ChildItem -Filter "*.nuspec"

foreach ($item in $specs) {

  ..\packages\NuGet.CommandLine.3.4.3\tools\NuGet.exe pack  $item.Name
}

$packs = Get-ChildItem -Filter "*.nupkg"
if (-Not (Test-Path "..\output\")) {
  mkdir -Path "..\output\"
}


Set-Content -Path "..\nugetpush.ps1" -Value ""
$apiKey = Get-Content -Path "~\code\nuget-key.api"
foreach ($item in $packs) {
  mv $item.FullName "..\output\" -Force
  Add-Content -Path "..\nugetpush.ps1" -Value ".\packages\NuGet.CommandLine.3.4.3\tools\NuGet.exe push .\output\$($item.Name)  $($apikey) -Source https://www.nuget.org/api/v2/package"
}


cd ..



