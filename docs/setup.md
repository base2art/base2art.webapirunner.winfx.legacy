

1) Create class library project.

Assume the project layout for the following steps:

/ - root
  /.git/   - the git metadata
  /conf/   - the folder containing yaml configuration files
  /src/    - the folder containing production class libraries
  /tests/  - the folder containing unit-test class libraries


2) Add a configuration file

Example:


serverSettings:
  http:
  - host: audit.local.dwtapps.com
    rootPath: /
    port: 80

adminServerSettings:
  http:
  - host: audit.local.dwtapps.com
    rootPath: /
    port: 81

assemblySearchPaths:
  
  - "../src/Dwt.DmsAudit/bin/debug"

